﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 设备服务
    /// </summary>
    [Route("api/WareEquipment")]
    [ApiDescriptionSettings("仓库基础数据", Name = "Equipment", Order = 100)]

    public class WareEquipmentService : IDynamicApiController, ITransient
    {
        private readonly IRepository<WareEquipment, MasterDbContextLocator> _wareEquipmentRep; //设备仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="wareEquipmentRep"></param>
        public WareEquipmentService(
            IRepository<WareEquipment, MasterDbContextLocator> wareEquipmentRep
        )
        {
            _wareEquipmentRep = wareEquipmentRep;
        }
        #endregion

        /// <summary>
        /// 分页查询设备基础
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        public async Task<PageResult<WareEquipmentOutput>> Page([FromQuery] WareEquipmentSearch input)
        {
            return await _wareEquipmentRep.DetachedEntities
                .Where(input.EquipmentTypeName != null, u => u.EquipmentTypeName == input.EquipmentTypeName)
                .Where(!string.IsNullOrEmpty(input.EquipmentName), u => EF.Functions.Like(u.EquipmentName, $"%{input.EquipmentName.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.ExhibitionName), u => u.ExhibitionName == input.ExhibitionName)
                .OrderBy(PageInputOrder.OrderBuilder<WareEquipmentSearch>(input))
                .ProjectToType<WareEquipmentOutput>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);
        }
    }
}
