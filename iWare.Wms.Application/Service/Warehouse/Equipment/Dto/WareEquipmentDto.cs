﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 设备表输出参数
    /// </summary>
    public class WareEquipmentDto
    {
        /// <summary>
        /// 设备类型
        /// </summary>
        public string EquipmentTypeName { get; set; }
        
        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentName { get; set; }
        
        /// <summary>
        /// 设备展示名
        /// </summary>
        public string ExhibitionName { get; set; }
        
        /// <summary>
        /// 设备品牌
        /// </summary>
        public string EquipmentBrand { get; set; }
        
        /// <summary>
        /// 设备IP
        /// </summary>
        public string IP { get; set; }
        
        /// <summary>
        /// 设备端口
        /// </summary>
        public int Port { get; set; }
        
        /// <summary>
        /// 所属区域code
        /// </summary>
        public string RegionName { get; set; }
        
        /// <summary>
        /// 设备标识
        /// </summary>
        public string EquipmentIdentification { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }
        
        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
