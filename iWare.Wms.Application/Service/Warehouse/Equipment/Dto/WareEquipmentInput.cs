﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 设备基础表查询参数
    /// </summary>
    public class WareEquipmentSearch : PageInputBase
    {
        /// <summary>
        /// 设备类型
        /// </summary>
        public string EquipmentTypeName { get; set; }

        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentName { get; set; }

        /// <summary>
        /// 设备展示名
        /// </summary>
        public string ExhibitionName { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 设备基础表输入参数
    /// </summary>
    public class WareEquipmentInput
    {
        /// <summary>
        /// 设备类型
        /// </summary>
        public string EquipmentTypeName { get; set; }

        /// <summary>
        /// 设备名称
        /// </summary>
        public string EquipmentName { get; set; }

        /// <summary>
        /// 设备展示名
        /// </summary>
        public string ExhibitionName { get; set; }

        /// <summary>
        /// 设备品牌
        /// </summary>
        public string EquipmentBrand { get; set; }

        /// <summary>
        /// 设备IP
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// 设备端口
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 所属区域code
        /// </summary>
        public string RegionName { get; set; }

        /// <summary>
        /// 设备标识
        /// </summary>
        public string EquipmentIdentification { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareEquipmentInput : WareEquipmentInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareEquipmentInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareEquipmentInput : WareEquipmentInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeWareEquipmentInput : BaseId
    {
    }
}
