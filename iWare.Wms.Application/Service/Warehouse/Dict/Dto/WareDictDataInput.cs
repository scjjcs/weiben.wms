﻿using Furion.DataValidation;
using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 仓库字典值表查询参数
    /// </summary>
    public class WareDictDataSearch : PageInputBase
    {
        /// <summary>
        /// 字典类型Id
        /// </summary>
        public long TypeId { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
    }

    /// <summary>
    /// 查询
    /// </summary>
    public class QueryWareDictDataListInput
    {
        /// <summary>
        /// 字典类型Id
        /// </summary>
        [Required(ErrorMessage = "字典类型Id不能为空"), DataValidation(ValidationTypes.Numeric)]
        public long TypeId { get; set; }
    }

    /// <summary>
    /// 查询
    /// </summary>
    public class QueryWareDictDataListByCodeInput
    {
        /// <summary>
        /// 字典类型Code
        /// </summary>
        //[Required(ErrorMessage = "字典类型Code不能为空")]
        public string Code { get; set; }

        /// <summary>
        /// 传递多个Code进行查询
        /// </summary>
        public List<string> CodeList { get; set; }
    }

    /// <summary>
    /// 仓库字典值表输入参数
    /// </summary>
    public class WareDictDataInput
    {
        /// <summary>
        /// 字典类型Id
        /// </summary>
        public virtual long TypeId { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public virtual string Value { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Remark { get; set; }

        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public virtual CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 新增
    /// </summary>
    public class AddWareDictDataInput
    {
        /// <summary>
        /// 字典类型Id
        /// </summary>
        [Required(ErrorMessage = "字典类型Id不能为空"), DataValidation(ValidationTypes.Numeric)]
        public long TypeId { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        [Required(ErrorMessage = "字典值不能为空")]
        public string Value { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [Required(ErrorMessage = "字典值编码不能为空")]
        public string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 删除
    /// </summary>
    public class DeleteWareDictDataInput : BaseId
    {
    }

    /// <summary>
    /// 更新
    /// </summary>
    public class UpdateWareDictDataInput
    {
        /// <summary>
        /// 字典值Id
        /// </summary>
        [Required(ErrorMessage = "字典值Id不能为空"), DataValidation(ValidationTypes.Numeric)]
        public long Id { get; set; }

        /// <summary>
        /// 字典类型Id
        /// </summary>
        [Required(ErrorMessage = "字典类型Id不能为空"), DataValidation(ValidationTypes.Numeric)]
        public long TypeId { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        [Required(ErrorMessage = "字典值不能为空")]
        public string Value { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [Required(ErrorMessage = "字典值编码不能为空")]
        public string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ChageStateWareDictDataInput : BaseId
    {
        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 查询
    /// </summary>
    public class QueryWareDictDataInput : BaseId
    {
    }
}
