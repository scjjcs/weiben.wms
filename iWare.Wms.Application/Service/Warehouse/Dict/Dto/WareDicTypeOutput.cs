﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 仓库字典类型表输出参数
    /// </summary>
    public class WareDicTypeOutput
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public CommonStatus Status { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
