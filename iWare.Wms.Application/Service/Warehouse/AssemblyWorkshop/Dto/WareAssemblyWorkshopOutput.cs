﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 组装车间输出参数
    /// </summary>
    public class WareAssemblyWorkshopOutput
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 计划数
        /// </summary>
        public int PlanNum { get; set; }

        /// <summary>
        /// 实际数
        /// </summary>
        public int ActuaityNum { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 百分比
        /// </summary>
        public string Percentage { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }
    }
}
