﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 组装车间输出参数
    /// </summary>
    public class WareAssemblyWorkshopDto
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 计划数
        /// </summary>
        public int PlanNum { get; set; }

        /// <summary>
        /// 实际数
        /// </summary>
        public int ActuaityNum { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 百分比
        /// </summary>
        public string Percentage { get; set; }
    }
}
