﻿using iWare.Wms.Core;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 组装车间输入参数
    /// </summary>
    public class WareAssemblyWorkshopSearch : PageInputBase
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class WareAssemblyWorkshopInput 
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 计划数
        /// </summary>
        public int PlanNum { get; set; }

        /// <summary>
        /// 实际数
        /// </summary>
        public int ActuaityNum { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 百分比
        /// </summary>
        public string Percentage { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareAssemblyWorkshopInput : WareAssemblyWorkshopInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareAssemblyWorkshopInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareAssemblyWorkshopInput : WareAssemblyWorkshopInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }
}
