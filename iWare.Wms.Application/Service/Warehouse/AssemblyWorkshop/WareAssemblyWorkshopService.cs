﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 看板组装车间服务
    /// </summary>
    [Route("api/WareAssemblyWorkshop")]
    [ApiDescriptionSettings("仓库基础数据", Name = "AssemblyWorkshop", Order = 100)]
    public class WareAssemblyWorkshopService : ControllerBase, IDynamicApiController, ITransient
    {
        private readonly IRepository<WareAssemblyWorkshop, MasterDbContextLocator> _wareAssemblyWorkshopRep;

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="wareAssemblyWorkshopRep"></param>
        public WareAssemblyWorkshopService(
            IRepository<WareAssemblyWorkshop, MasterDbContextLocator> wareAssemblyWorkshopRep
        )
        {
            _wareAssemblyWorkshopRep = wareAssemblyWorkshopRep;
        }

        /// <summary>
        /// 分页查询组装车间
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        public async Task<PageResult<WareAssemblyWorkshopOutput>> Page([FromQuery] WareAssemblyWorkshopSearch input)
        {
            return await _wareAssemblyWorkshopRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(input.ProjectName), u => EF.Functions.Like(u.ProjectName, $"%{input.ProjectName.Trim()}%"))
                .OrderBy(PageInputOrder.OrderBuilder<WareAssemblyWorkshopSearch>(input))
                .ProjectToType<WareAssemblyWorkshopOutput>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);
        }

        /// <summary>
        /// 新增组装车间表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task Add([FromBody] AddWareAssemblyWorkshopInput input)
        {
            var isExist = await _wareAssemblyWorkshopRep.DetachedEntities.AnyAsync(u => u.ProjectName == input.ProjectName);
            if (isExist) throw Oops.Oh("数据已存在");

            var wareAssemblyWorkshop = input.Adapt<WareAssemblyWorkshop>();
            await _wareAssemblyWorkshopRep.InsertAsync(wareAssemblyWorkshop);
        }

        /// <summary>
        /// 删除组装车间表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public async Task Delete([FromBody] DeleteWareAssemblyWorkshopInput input)
        {
            int length = input.Id.Count;
            for (int i = 0; i < length; i++)
            {
                long Id = input.Id[i];
                var wareAssemblyWorkshop = await _wareAssemblyWorkshopRep.FirstOrDefaultAsync(u => u.Id == Id);
                await _wareAssemblyWorkshopRep.DeleteAsync(wareAssemblyWorkshop);
            }
        }

        /// <summary>
        /// 更新组装车间表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        public async Task Update([FromBody] UpdateWareAssemblyWorkshopInput input)
        {
            var isExist = await _wareAssemblyWorkshopRep.AnyAsync(u => u.Id == input.Id, false);
            if (!isExist) throw Oops.Oh("数据不存在");

            var wareAssemblyWorkshop = input.Adapt<WareAssemblyWorkshop>();
            await _wareAssemblyWorkshopRep.UpdateAsync(wareAssemblyWorkshop, ignoreNullValues: true);
        }
        #endregion
    }
}
