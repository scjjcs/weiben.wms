﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 分页查询输入参数
    /// </summary>
    public class WareAreaInput : PageInputBase
    {
        /// <summary>
        /// 编码
        /// </summary>
        public string AreaCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public AreaTypeEnum AreaType { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareAreaInput
    {
        /// <summary>
        /// 编码
        /// </summary>
        [Required(ErrorMessage = "编码不能为空")]
        public string AreaCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Required(ErrorMessage = "名称不能为空")]
        public string AreaName { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Required(ErrorMessage = "类型不能为空")]
        public AreaTypeEnum AreaType { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareAreaInput
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string AreaCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public AreaTypeEnum AreaType { get; set; }
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareAreaInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryWareAreaInput : BaseId
    {
    }
}