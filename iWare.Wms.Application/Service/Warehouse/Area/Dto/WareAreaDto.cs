﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 
    /// </summary>
    public class WareAreaDto
    {
        /// <summary>
        /// 编码
        /// </summary>
        public string AreaCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public AreaTypeEnum AreaType { get; set; }
    }
}
