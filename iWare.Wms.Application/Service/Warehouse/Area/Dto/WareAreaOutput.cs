﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 所属库区输出参数
    /// </summary>
    public class WareAreaOutput
    {
        /// <summary>
        /// 应用Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string AreaCode { get; set; }

        /// <summary>
        /// 库区类型
        /// </summary>
        public AreaTypeEnum? AreaType { get; set; }

        /// <summary>
        /// 库区类型名称
        /// </summary>
        public string AreaTypeName { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public string CreatedUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }
    }
}