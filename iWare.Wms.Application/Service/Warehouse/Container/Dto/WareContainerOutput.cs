﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 容器表输出参数
    /// </summary>
    public class WareContainerOutput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        public string Parameter2 { get; set; }

        /// <summary>
        /// 库区-区分立体库与平库容器
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 库区名称
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 是否虚拟（字典 1是 0否）
        /// </summary>
        public YesOrNot IsVirtual { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-空闲_0、使用_1、禁用_-1
        /// </summary>
        public ContainerStatusEnum ContainerStatus { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }

    /// <summary>
    /// 根据条件查询未绑定的容器输入参数
    /// </summary>
    public class WareContainerByStatusOutput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string Code { get; set; }
    }
}
