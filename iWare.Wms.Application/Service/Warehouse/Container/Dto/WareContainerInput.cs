﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 容器表查询参数
    /// </summary>
    public class WareContainerSearch : PageInputBase
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 容器名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        public string Parameter2 { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 容器状态-空闲_0、使用_1、禁用_-1
        /// </summary>
        public ContainerStatusEnum? ContainerStatus { get; set; }

        /// <summary>
        /// 是否虚拟（字典 -10全部 1是 0否）
        /// </summary>
        public YesOrNot IsVirtual { get; set; }
    }

    /// <summary>
    /// 容器表输入参数
    /// </summary>
    public class WareContainerInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        public string Parameter2 { get; set; }

        /// <summary>
        /// 是否虚拟（字典 1是 0否）
        /// </summary>
        public YesOrNot? IsVirtual { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-空闲_0、使用_1、禁用_-1
        /// </summary>
        public ContainerStatusEnum ContainerStatus { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareContainerInput : WareContainerInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareContainerInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareContainerInput : WareContainerInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeWareContainerInput : BaseId
    {
    }
}
