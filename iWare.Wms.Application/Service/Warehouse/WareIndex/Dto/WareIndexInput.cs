﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 首页库位信息输入参
    /// </summary>
    public class LocationIndexInput
    {
        /// <summary>
        /// 库区ID
        /// </summary>
        public long AreaID { get; set; }
    }
}
