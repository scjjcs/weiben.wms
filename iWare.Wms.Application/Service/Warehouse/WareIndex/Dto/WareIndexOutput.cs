﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 首页输出参
    /// </summary>
    public class WareIndexOutput
    {
        /// <summary>
        /// 今日出库任务数
        /// </summary>
        public object OutTaskNum { get; set; } = 0;

        /// <summary>
        /// 今日入库任务数
        /// </summary>
        public object InTaskNum { get; set; } = 0;

        /// <summary>
        /// 本周任务详情
        /// </summary>
        public List<TaskTable> taskTable { get; set; }
    }


    /// <summary>
    /// 本周任务详情
    /// </summary>
    public class TaskTable
    {
        /// <summary>
        /// 出库任务数
        /// </summary>
        public object chukuNum { get; set; } = 0;

        /// <summary>
        /// 入库任务数
        /// </summary>
        public object rukuNum { get; set; } = 0;

        /// <summary>
        /// 执行日期
        /// </summary>
        public object dateStr { get; set; }
    }

    /// <summary>
    /// 首页库位输出参
    /// </summary>
    public class LocationIndexOutput
    {
        /// <summary>
        /// 名称
        /// </summary>
        public object Name { get; set; } = 0;

        /// <summary>
        /// 数量
        /// </summary>
        public object Value { get; set; } = 0;
    }
}
