﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 首页
    /// </summary>
    [Route("api/WareIndex")]
    [ApiDescriptionSettings("仓库基础数据", Name = "WareIndex", Order = 100)]

    public class WareIndexServer : IDynamicApiController, ITransient
    {
        private readonly IRepository<WareTask, MasterDbContextLocator> _wareTaskRep; //出入库任务
        private readonly IRepository<WareLocation, MasterDbContextLocator> _wareLocationRep; //库位表

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="wareTaskRep"></param>
        /// <param name="wareLocationRep"></param>
        public WareIndexServer(
            IRepository<WareTask, MasterDbContextLocator> wareTaskRep,
            IRepository<WareLocation, MasterDbContextLocator> wareLocationRep
        )
        {
            _wareTaskRep = wareTaskRep;
            _wareLocationRep = wareLocationRep;
        }
        #endregion

        /// <summary>
        /// 首页任务信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("TaskIndex")]
        [AllowAnonymous]
        public async Task<WareIndexOutput> TaskIndex()
        {
            var start = DateTime.Now.Date.AddDays(0);
            var end = DateTime.Now.Date.AddDays(1);
            //创建首页任务
            var wareIndexOutput = new WareIndexOutput();

            //查询今日出库任务数
            wareIndexOutput.OutTaskNum = _wareTaskRep.Where(p => p.CreatedTime >= start && p.CreatedTime <= end 
            && p.TaskType == TaskType.Out).Count();

            //查询今日入库任务数
            wareIndexOutput.InTaskNum = _wareTaskRep.Where(p => p.CreatedTime >= start && p.CreatedTime <= end
            && p.TaskType == TaskType.In).Count();

            //近七天
            int days = -7;

            //查询近一周的任务详情
            var taskList = await _wareTaskRep.Where(p => p.CreatedTime >= DateTime.Now.Date.AddDays(days)).ToListAsync();

            //给任务详情输出参数赋值
            var taskTableList = new List<TaskTable>();
            for (int i = days; i <= 0; i++)
            {
                var starttime = DateTime.Now.Date.AddDays(i);
                var endtime = DateTime.Now.Date.AddDays(i + 1);
                var areaTaskList = taskList.Where(p => p.CreatedTime >= starttime && p.CreatedTime <= endtime).ToList();
                var taskTable = new TaskTable()
                {
                    chukuNum = areaTaskList.Where(n => n.TaskType == TaskType.Out).Count(),
                    rukuNum = areaTaskList.Where(n => n.TaskType == TaskType.In).Count(),
                    dateStr = starttime.ToString("MM/dd")
                };
                taskTableList.Add(taskTable);
            }

            //本周任务详情
            wareIndexOutput.taskTable = taskTableList;

            return wareIndexOutput;
        }

        /// <summary>
        /// 首页库位信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LocationIndex")]
        [AllowAnonymous]
        public async Task<List<LocationIndexOutput>> LocationIndex([FromBody] LocationIndexInput input)
        {
            //创建首页库位信息
            var locationIndexOutput = new LocationIndexOutput();

            //查询库位信息
            var LocationList = await _wareLocationRep.DetachedEntities.Where(z => z.AreaID == input.AreaID).ToListAsync();

            var LocationIndexList = new List<LocationIndexOutput>();
            var LocationIndex1 = new LocationIndexOutput();
            var LocationIndex2 = new LocationIndexOutput();
            var LocationIndex3 = new LocationIndexOutput();
            var LocationIndex4 = new LocationIndexOutput();
            var LocationIndex5 = new LocationIndexOutput();

            //查询库位空闲信息
            LocationIndex1.Name = "空闲";
            LocationIndex1.Value = LocationList.Where(z => z.LocationStatus == LocationStatusEnum.kongxian).Count();
            LocationIndexList.Add(LocationIndex1);

            //查询库位待入信息
            LocationIndex2.Name = "待入";
            LocationIndex2.Value = LocationList.Where(z => z.LocationStatus == LocationStatusEnum.dairu).Count();
            LocationIndexList.Add(LocationIndex2);

            //查询库位存货信息
            LocationIndex3.Name = "存货";
            LocationIndex3.Value = LocationList.Where(z => z.LocationStatus == LocationStatusEnum.cunhuo
            && z.IsEmptyContainer == YesOrNot.N).Count();
            LocationIndexList.Add(LocationIndex3);

            //查询库位待出信息
            LocationIndex4.Name = "待出";
            LocationIndex4.Value = LocationList.Where(z => z.LocationStatus == LocationStatusEnum.daichu).Count();
            LocationIndexList.Add(LocationIndex4);

            //查询库位空托信息
            LocationIndex5.Name = "空托";
            LocationIndex5.Value = LocationList.Where(z => z.LocationStatus == LocationStatusEnum.cunhuo
            && z.IsEmptyContainer == YesOrNot.Y).Count();
            LocationIndexList.Add(LocationIndex5);

            return LocationIndexList;
        }
    }
}
