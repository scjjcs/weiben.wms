﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// Agv任务输出参数
    /// </summary>
    public class WareAgvTaskOutput
    {
        /// <summary>
        /// 任务类型-入库;出库
        /// </summary>
        public string TaskType { get; set; }

        /// <summary>
        /// 起始位置
        /// </summary>
        public string StartPlace { get; set; }

        /// <summary>
        /// 起始位置名称
        /// </summary>
        public string StartPlaceCode { get; set; }

        /// <summary>
        /// 结束位置
        /// </summary>
        public string EndPlace { get; set; }

        /// <summary>
        /// 结束位置名称
        /// </summary>
        public string EndPlaceCode { get; set; }

        /// <summary>
        /// 状态
        /// PRISTINE：待执行
        /// TRAVELLING：执行中
        /// FINISHED：完成
        /// </summary>
        public string AgvState { get; set; }

        /// <summary>
        /// 当前执行的任务
        /// </summary>
        public string TransportOrder { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }
    }
}
