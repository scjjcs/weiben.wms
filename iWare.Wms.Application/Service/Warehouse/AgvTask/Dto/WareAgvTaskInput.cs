﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// Agv任务输入参数
    /// </summary>
    public class WareAgvTaskSearch : PageInputBase
    {
        /// <summary>
        /// 任务类型-全部;入库;出库
        /// </summary>
        public string TaskType { get; set; }

        /// <summary>
        /// 起始位置
        /// </summary>
        public string StartPlace { get; set; }

        /// <summary>
        /// 结束位置
        /// </summary>
        public string EndPlace { get; set; }

        /// <summary>
        /// 状态
        /// 全部：全部
        /// PRISTINE：待执行
        /// TRAVELLING：执行中
        /// FINISHED：完成
        /// </summary>
        public string AgvState { get; set; }

        /// <summary>
        /// 当前执行的任务
        /// </summary>
        public string TransportOrder { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string CreatedUserName { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class WareAgvTaskInput
    {
        /// <summary>
        /// 任务类型-入库;出库
        /// </summary>
        public string TaskType { get; set; }

        /// <summary>
        /// 起始位置
        /// </summary>
        public string StartPlace { get; set; }

        /// <summary>
        /// 结束位置
        /// </summary>
        public string EndPlace { get; set; }

        /// <summary>
        /// 状态
        /// PRISTINE：待执行
        /// TRAVELLING：执行中
        /// FINISHED：完成
        /// </summary>
        public string AgvState { get; set; }

        /// <summary>
        /// 当前执行的任务
        /// </summary>
        public string TransportOrder { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareAgvTaskInput : WareAgvTaskInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareAgvTaskInput
    {
        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareAgvTaskInput : WareAgvTaskInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 强制完成输入参数
    /// </summary>
    public class UpdateAgvStateInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeWareAgvTaskInput : BaseId
    {
    }
}
