﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 项目表输出参数
    /// </summary>
    public class ProjectManageDto
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 项目说明
        /// </summary>
        public string Describe { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
