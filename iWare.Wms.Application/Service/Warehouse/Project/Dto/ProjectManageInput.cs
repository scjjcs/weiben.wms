﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 项目管理查询参数
    /// </summary>
    public class ProjectManageSearch : PageInputBase
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 项目管理输入参数
    /// </summary>
    public class ProjectManageInput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目说明
        /// </summary>
        public string Describe { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddProjectManageInput : ProjectManageInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteProjectManageInput : BaseId
    {
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateProjectManageInput : ProjectManageInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeProjectManageInput : BaseId
    {
    }
}
