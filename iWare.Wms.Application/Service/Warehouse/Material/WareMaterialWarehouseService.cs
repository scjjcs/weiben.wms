﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 品号仓库服务
    /// </summary>
    [Route("api/WareMaterialWarehouse")]
    [ApiDescriptionSettings("仓库基础数据", Name = "MaterialWarehouse", Order = 100)]

    public class WareMaterialWarehouseService : IDynamicApiController, ITransient
    {
        private readonly IRepository<WareMaterialWarehouse, MasterDbContextLocator> _wareMaterialWarehouseRep; //品号仓库仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="wareMaterialWarehouseRep"></param>

        public WareMaterialWarehouseService(
            IRepository<WareMaterialWarehouse, MasterDbContextLocator> wareMaterialWarehouseRep
        )
        {
            _wareMaterialWarehouseRep = wareMaterialWarehouseRep;
        }
        #endregion

        /// <summary>
        /// 分页查询品号仓库
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        public async Task<PageResult<WareMaterialWarehouseOutput>> Page([FromQuery] WareMaterialWarehouseSearch input)
        {
            return await _wareMaterialWarehouseRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(input.Code), u => EF.Functions.Like(u.Code, $"%{input.Code.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.Name), u => EF.Functions.Like(u.Name, $"%{input.Name.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.SearchBeginTime), u => u.CreatedTime >= DateTime.Parse(input.SearchBeginTime.Trim()) &&
                 u.CreatedTime <= DateTime.Parse(input.SearchEndTime.Trim()))
                .OrderBy(PageInputOrder.OrderBuilder<WareMaterialWarehouseSearch>(input))
                .ProjectToType<WareMaterialWarehouseOutput>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);
        }
    }
}
