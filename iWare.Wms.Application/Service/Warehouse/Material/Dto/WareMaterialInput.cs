﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 物料表查询参数
    /// </summary>
    public class WareMaterialSearch : PageInputBase
    {
        /// <summary>
        /// 安全状态(不安全_0; 全部_-10)
        /// </summary>
        public YesOrNot SecurityStatus { get; set; } = YesOrNot.N;

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus? Status { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }
    }

    /// <summary>
    /// 物料表输入参数
    /// </summary>
    public class WareMaterialInput
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string Describe { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 参数一
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 参数二
        /// </summary>
        public string Parameter2 { get; set; }

        /// <summary>
        /// 参数三
        /// </summary>
        public string Parameter3 { get; set; }

        /// <summary>
        /// 参数四
        /// </summary>
        public string Parameter4 { get; set; }

        /// <summary>
        /// 参数五
        /// </summary>
        public string Parameter5 { get; set; }

        /// <summary>
        /// 备库状态-不备库_0；备库_1
        /// </summary>
        public YesOrNot StandbyStatus { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareMaterialInput : WareMaterialInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareMaterialInput 
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareMaterialInput : WareMaterialInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryWareMaterialInput : BaseId
    {
    }
}
