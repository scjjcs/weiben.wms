﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 物料表输出参数
    /// </summary>
    public class WareMaterialDto
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }
        
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }
        
        /// <summary>
        /// 商品描述
        /// </summary>
        public string Describe { get; set; }
        
        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }
        
        /// <summary>
        /// 参数一
        /// </summary>
        public string Parameter1 { get; set; }
        
        /// <summary>
        /// 参数二
        /// </summary>
        public string Parameter2 { get; set; }
        
        /// <summary>
        /// 参数三
        /// </summary>
        public string Parameter3 { get; set; }
        
        /// <summary>
        /// 参数四
        /// </summary>
        public string Parameter4 { get; set; }
        
        /// <summary>
        /// 参数五
        /// </summary>
        public string Parameter5 { get; set; }
        
        /// <summary>
        /// 备库状态-不备库_0；备库_1
        /// </summary>
        public YesOrNot StandbyStatus { get; set; }
        
        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }
    }
}
