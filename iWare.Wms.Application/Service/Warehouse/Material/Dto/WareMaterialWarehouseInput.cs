﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 品号仓库/物料仓库表查询参数
    /// </summary>
    public class WareMaterialWarehouseSearch : PageInputBase
    {
        /// <summary>
        /// 品号
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 仓库
        /// </summary>
        public virtual string Name { get; set; }
    }

    /// <summary>
    /// 品号仓库/物料仓库表输入参数
    /// </summary>
    public class WareMaterialWarehouseInput
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 仓库
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 安全存量
        /// </summary>
        public decimal SafetyQuantity { get; set; }

        /// <summary>
        /// 经济批量
        /// </summary>
        public decimal EconomicQuantity { get; set; }

        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal InventoryQuantity { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareMaterialWarehouseInput : WareMaterialWarehouseInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareMaterialWarehouseInput : BaseId
    {
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareMaterialWarehouseInput : WareMaterialWarehouseInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeWareMaterialWarehouseInput : BaseId
    {
    }
}
