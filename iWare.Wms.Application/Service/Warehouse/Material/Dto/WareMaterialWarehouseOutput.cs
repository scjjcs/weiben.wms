﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 品号仓库/物料仓库表输出参数
    /// </summary>
    public class WareMaterialWarehouseOutput
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 仓库
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 安全存量
        /// </summary>
        public decimal SafetyQuantity { get; set; }

        /// <summary>
        /// 经济批量
        /// </summary>
        public decimal EconomicQuantity { get; set; }

        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal InventoryQuantity { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
