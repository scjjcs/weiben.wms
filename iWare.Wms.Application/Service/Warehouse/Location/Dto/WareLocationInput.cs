﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 库位表查询参数
    /// </summary>
    public class WareLocationSearch : PageInputBase
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 所属企业
        /// </summary>
        public string WareOne { get; set; }

        /// <summary>
        /// 所属厂区
        /// </summary>
        public string WareTwo { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 库区类型
        /// </summary>
        public string WareThree { get; set; }

        /// <summary>
        /// 库位属性
        /// </summary>
        public string Attribute { get; set; }

        /// <summary>
        /// 是否锁定
        /// </summary>
        public YesOrNot IsLock { get; set; } = YesOrNot.N;

        /// <summary>
        /// 是否空托盘
        /// </summary>
        public YesOrNot IsEmptyContainer { get; set; } = YesOrNot.N;

        /// <summary>
        /// 库位状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1
        /// </summary>
        public LocationStatusEnum LocationStatus { get; set; }

        /// <summary>
        /// 是否虚拟（字典 1是 0否）   
        /// </summary>
        public YesOrNot IsVirtual { get; set; }
    }

    /// <summary>
    /// 库位表输入参数
    /// </summary>
    public class WareLocationInput
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 所属企业
        /// </summary>
        public string WareOne { get; set; }

        /// <summary>
        /// 所属厂区
        /// </summary>
        public string WareTwo { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 库区类型
        /// </summary>
        public string WareThree { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        public int RowNo { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        public int ColumnNo { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        public int LayerNo { get; set; }

        /// <summary>
        /// 深号
        /// </summary>
        public int DeepcellNo { get; set; }

        /// <summary>
        /// 巷道/货架
        /// </summary>
        public int Aisle { get; set; }

        /// <summary>
        /// 长
        /// </summary>
        public decimal Long { get; set; }

        /// <summary>
        /// 宽
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// 高
        /// </summary>
        public decimal High { get; set; }

        /// <summary>
        /// 承重
        /// </summary>
        public string Heavy { get; set; }

        /// <summary>
        /// 属性
        /// </summary>
        public string Attribute { get; set; }

        /// <summary>
        /// 是否锁定
        /// </summary>
        public YesOrNot IsLock { get; set; } = YesOrNot.N;

        /// <summary>
        /// 是否空托盘
        /// </summary>
        public YesOrNot IsEmptyContainer { get; set; } = YesOrNot.N;

        /// <summary>
        /// 库位状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1
        /// </summary>
        public LocationStatusEnum? LocationStatus { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareLocationInput : WareLocationInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareLocationInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 锁定和解锁参数
    /// </summary>
    public class IsLockWareLocationInput
    {
        /// <summary>
        /// Ids主键
        /// </summary>
        [Required(ErrorMessage = "Ids主键不能为空")]
        public List<long> Ids { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareLocationInput : WareLocationInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeWareLocationInput : BaseId
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        public string Code { get; set; }
    }

    /// <summary>
    /// 更新库位属性输入参数
    /// </summary>
    public class UpdateAttributeInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 库位属性
        /// </summary>
        public string Attribute { get; set; }
    }

    /// <summary>
    /// 批量更新库存锁定输入参数
    /// </summary>
    public class UpdateLockInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 是否锁定
        /// </summary>
        public YesOrNot IsLock { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateLocationInput
    {
        /// <summary>
        /// 库区Id
        /// </summary>
        [Required(ErrorMessage = "库区Id不能为空")]
        public long AreaId { get; set; }
    }
}
