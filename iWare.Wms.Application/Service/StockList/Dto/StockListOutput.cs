﻿using iWare.Wms.Core;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 库存表输出参数
    /// </summary>
    public class StockListOutput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 实际库存数量
        /// </summary>
        public decimal StockQuantity { get; set; }

        /// <summary>
        /// 当前库存数量
        /// </summary>
        public decimal CurrentQuantity { get; set; }

        /// <summary>
        /// 分拣数量
        /// </summary>
        public decimal SortQuantity { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTimeOffset? ExpirationTime { get; set; }

        /// <summary>
        /// 物料库存周期
        /// </summary>
        public StockCircleEnum StockCircle { get; set; }

        /// <summary>
        /// 物料库存周期名称
        /// </summary>
        public string StockCircleName { get; set; }

        /// <summary>
        /// 原库位编号
        /// </summary>
        public string OriginalLocationCode { get; set; }

        /// <summary>
        /// 所属库区名称
        /// </summary>
        public string ReservoirArea { get; set; }
    }
}
