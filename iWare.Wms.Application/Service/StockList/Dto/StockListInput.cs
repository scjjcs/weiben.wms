﻿using Furion.DataValidation;
using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 库存表查询参数
    /// </summary>
    public class StockListSearch : PageInputBase
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 物料库存周期
        /// </summary>
        public StockCircleEnum StockCircle { get; set; } = StockCircleEnum.ALL;

        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }
    }

    /// <summary>
    /// 更新物料库存周期输入参数
    /// </summary>
    public class EditStockCircleInput : BaseId
    {
        /// <summary>
        /// 物料库存周期
        /// </summary>
        public StockCircleEnum StockCircle { get; set; }
    }

    /// <summary>
    /// 更新物料库存到期时间输入参数
    /// </summary>
    public class UpdateDateInput
    {
        /// <summary>
        /// 库存Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 物料库存周期
        /// </summary>
        public string StockDate { get; set; }
    }
}
