﻿using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application.DING
{
    /// <summary>
    /// 根据送货单号查询收货输入参数
    /// </summary>
    public class DeliverySearchByDeliveryNoInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        [Required(ErrorMessage = "送货单号不能为空")]
        public string DeliveryNo { get; set; }
    }

    /// <summary>
    /// 根据规格查询收货输入参数
    /// </summary>
    public class DeliverySearchBySpecificationInput
    {
        /// <summary>
        /// 规格
        /// </summary>
        [Required(ErrorMessage = "规格不能为空")]
        public string Specification { get; set; }
    }

    /// <summary>
    /// 订单回传单号
    /// </summary>
    public class DeliveryNoInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        [Required(ErrorMessage = "送货单号不能为空")]
        public string DeliveryNo { get; set; }
    }

    /// <summary>
    /// 订单回传Id
    /// </summary>
    public class DeliveryIdInput
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public string Id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DingDingSystemToken
    {
        /// <summary>
        /// 
        /// </summary>
        public int errcode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string errmsg { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int expires_in { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string access_token { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DingDingAccessToken
    {
        /// <summary>
        /// 
        /// </summary>
        public int errcode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string errmsg { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int expires_in { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string access_token { get; set; }
    }
}
