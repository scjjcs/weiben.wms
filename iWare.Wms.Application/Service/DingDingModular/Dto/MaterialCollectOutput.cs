﻿using iWare.Wms.Core;

namespace iWare.Wms.Application.DING
{
    /// <summary>
    /// 查询送货单输出数据
    /// </summary>
    public class DeliveryOutput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 物料收货-收货明细
        /// </summary>
        public List<MaterialOutput> items { get; set; }
    }

    /// <summary>
    /// 物料信息
    /// </summary>
    public class MaterialOutput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 序号   --对应ERP序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号   --对应ERP品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>   
        /// 品名    --对应ERP品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 收货数量
        /// </summary>
        public decimal DeliveryQuantity { get; set; }
    }

    /// <summary>
    /// 物料质检结果表
    /// </summary>
    public class TableField_l9qmo4snItem
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string textField_laki7n3o { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string selectField_l9qmo4su { get; set; }

        /// <summary>
        /// 不合格数
        /// </summary>
        public int numberField_l9qmo4sy { get; set; }

        /// <summary>
        /// 总数
        /// </summary>
        public int numberField_lb4ifn41 { get; set; }

        /// <summary>
        /// 异常原因
        /// </summary>
        public List<string> multiSelectField_lcptssei { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class FormData
    {
        /// <summary>
        /// 质检明细
        /// </summary>
        public List<TableField_l9qmo4snItem> tableField_l9qmo4sn { get; set; }

        /// <summary>
        /// 单号
        /// </summary>
        public long dateField_l9qri8lh { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public long textField_l9qmo4sl { get; set; }

        /// <summary>
        /// 是否让步接收
        /// </summary>
        public string radioField_l9qmo4sw { get; set; } = "";
    }

    /// <summary>
    /// 
    /// </summary>
    public class DataItem
    {
        /// <summary>
        /// 
        /// </summary>
        public FormData formData { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RootResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<DataItem> data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int currentPage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int totalCount { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RootResultId
    {
        /// <summary>
        /// 修改时间
        /// </summary>
        public string modifiedTimeGMT { get; set; }

        /// <summary>
        /// 表单实例ID
        /// </summary>
        public string formInstId { get; set; }

        /// <summary>
        /// 表单数据详情
        /// </summary>
        public FormData formData { get; set; }
    }
}
