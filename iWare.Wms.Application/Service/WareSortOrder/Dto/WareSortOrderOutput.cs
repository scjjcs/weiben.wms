﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 分拣管理输出参数
    /// </summary>
    public class WareSortOrderOutput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 来源单号    
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 明细单据ID
        /// </summary>
        public long OrderDetailID { get; set; }

        /// <summary>
        /// 分拣组盘单号    
        /// </summary>
        public string ContainerOrderNo { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号  
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名  
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 分拣数    
        /// </summary>
        public decimal SortQuantity { get; set; }

        /// <summary>
        /// 实际分拣数    
        /// </summary>
        public decimal ActualQuantity { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 备注    
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-未分拣_1、分拣完成_2
        /// </summary>
        public SortStatusEnum SortStatus { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public string CreatedUserName { get; set; }

        /// <summary>
        /// 修改者名称
        /// </summary>
        public string UpdatedUserName { get; set; }
    }
}
