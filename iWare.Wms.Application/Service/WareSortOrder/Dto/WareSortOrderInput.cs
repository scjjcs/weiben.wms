﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 分拣管理查询参数
    /// </summary>
    public class WareSortOrderSearch : PageInputBase
    {
        /// <summary>
        /// 来源单号    
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 分拣组盘单号    
        /// </summary>
        public string ContainerOrderNo { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 状态-未进行_0、未分拣_1、分拣中_2、分拣完成_3
        /// </summary>
        public SortStatusEnum SortStatus { get; set; } = SortStatusEnum.All;

        /// <summary>
        /// 操作人
        /// </summary>
        public string CreatedUserName { get; set; }
    }

    /// <summary>
    /// 分拣表输入参数
    /// </summary>
    public class WareSortOrderInput
    {
        /// <summary>
        /// 分拣来源单号    
        /// </summary>
        public string SortOrderNo { get; set; }

        /// <summary>
        /// 分拣组盘单号    
        /// </summary>
        public string ContainerOrderNo { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号  
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名  
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 分拣数    
        /// </summary>
        public decimal SortQuantity { get; set; }

        /// <summary>
        /// 实际分拣数    
        /// </summary>
        public decimal ActualQuantity { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 备注    
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-未进行_0、未分拣_1、分拣中_2、分拣完成_3
        /// </summary>
        public SortStatusEnum SortStatus { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareSortOrderInput : WareSortOrderInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareSortOrderInput : BaseId
    {
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareSortOrderInput : WareSortOrderInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeWareSortOrderInput : BaseId
    {
    }

    /// <summary>
    /// 强制分拣完成输入参数
    /// </summary>
    public class SortOrderFinishInput
    {
        /// <summary>
        /// 分拣表Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 分拣组盘单号
        /// </summary>
        public string ContainerOrderNo { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号  
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 本次分拣数量
        /// </summary>
        public decimal LocalQuantity { get; set; }
    }
}
