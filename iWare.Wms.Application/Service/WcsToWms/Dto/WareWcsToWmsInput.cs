﻿using iWare.Wms.Core;
namespace iWare.Wms.Application
{
    /// <summary>
    /// 创建主任务、AGV任务输入参数
    /// </summary>
    public class WareWcsToWmsInput
    {
        /// <summary>
        /// 下线口
        /// </summary>
        //public BlankingProductionLineEnum BlankingProductionLine { get; set; }
        /// <summary>
        /// 
        /// </summary>
        //public List<FoamingDestinationsInput> destinations { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class FoamingDestinationsInput
    {
        /// <summary>
        /// 位置（起点）/ 位置（终点）
        /// </summary>
        /// <example>001H01B0101</example>
        public string locationName { get; set; }

        /// <summary>
        /// 取货动作：层数 / 放货动作：层数  2
        /// </summary>
        /// <example>Load cargo:00</example>
        public string operation { get; set; }
    }

    /// <summary>
    /// 查询所有空闲库位输入参数
    /// </summary>
    public class GetPlaceInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 大小  小 中 大
        /// </summary>
        public string High { get; set; }
    }

    /// <summary>
    /// 更新任务输入参数
    /// </summary>
    public class UpdateTaskInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public TaskStatusEnum TaskStatus { get; set; }


        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }
    }
    /// <summary>
    /// 强制完成
    /// </summary>
    public class FinishTaskInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }
    }


    /// <summary>
    /// 更新任务输入参数
    /// </summary>
    public class CreateRelocationInput
    {
        /// <summary>
        /// 任务起点
        /// </summary>
        public string SourcePlace { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }

    }

    /// <summary>
    /// 添加设备报警信息输入参数
    /// </summary>
    public class AddDeviceWaringInput
    {
        /// <summary>
        /// WcsId
        /// </summary>
        public int WcsId { get; set; }

        /// <summary>
        /// 设备名称
        /// </summary>
        public string DeviceName { get; set; }

        /// <summary>
        /// 故障名称
        /// </summary>
        public string FaultName { get; set; }

        /// <summary>
        /// 发生时间
        /// </summary>
        public DateTimeOffset? StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTimeOffset? EndTime { get; set; }

        /// <summary>
        /// 持续时间
        /// </summary>
        public int RunningTime { get; set; }

    }
}
