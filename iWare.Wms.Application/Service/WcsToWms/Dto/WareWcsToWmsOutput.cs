﻿using iWare.Wms.Core;
namespace iWare.Wms.Application
{
    /// <summary>
    /// 查询所有WCS可以读取的任务输出参数
    /// </summary>
    public class GetSendTaskOutput
    {
        /// <summary>
        /// 任务详情
        /// </summary>
        public SendTaskDetailOutput taskDetail { get; set; }

        /// <summary>
        /// 物料详情
        /// </summary>
        public SendMaterialDetailOutput materialDetail { get; set; }
    }

    /// <summary>
    /// 任务详情
    /// </summary>
    public class SendTaskDetailOutput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }
        /// <summary>
        /// 任务类型
        /// </summary>
        public TaskType TaskType { get; set; }
        /// <summary>
        /// 任务优先级别
        /// </summary>
        public int Priority { get; set; }
        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }
        /// <summary>
        /// 任务的起点
        /// </summary>
        public string SourcePlace { get; set; }

        /// <summary>
        ///是否平库
        /// </summary>
        public YesOrNot IsLOrP { get; set; }
    }

    /// <summary>
    /// 物料详情
    /// </summary>
    public class SendMaterialDetailOutput
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public string MaterialDensity { get; set; }
    }

    /// <summary>
    ///获取空闲的库位
    /// </summary>
    public class GetPlaceOutput
    {
        /// <summary>
        /// 入库库位目标点
        /// </summary>
        public string ToPlace { get; set; }

        /// <summary>
        /// 移库库位起点
        /// </summary>
        public string SourcePlace { get; set; }

        /// <summary>
        /// 任务是否存在
        /// </summary>
        public bool TaskIsExist { get; set; } = true;
    }

    /// <summary>
    /// 获取移库库位
    /// </summary>
    public class GetYiKuPlaceOutput
    {
        /// <summary>
        /// 移库库位目标点
        /// </summary>
        public string ToPlace { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }
    }
}
