﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 单据出库分页输入参数
    /// </summary>
    public class OrderIssueSearch : PageInputBase
    {
        /// <summary>
        /// 出库单据  
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public IssueStausEnum OrderStatus { get; set; } = IssueStausEnum.ALL;

        /// <summary>
        /// 单别（异库调拨单、装配调拨单、库位调拨单、零组件发货、补发货单、标准销货单）
        /// </summary>
        [Required(ErrorMessage = "单别不能为空")]
        public string OrderCategory { get; set; }
    }

    /// <summary>
    /// 查询单据出库输入参数
    /// </summary>
    public class OrderIssueDetailSearch : PageInputBase
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }
    }

    /// <summary>
    /// 单据出库创建库存明细输入参数
    /// </summary>
    public class OrderIssueStockInput : PageInputBase
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }
    }

    /// <summary>
    /// 单据明细输入参数
    /// </summary>
    public class WareOrderDetailsInput
    {
        /// <summary>
        /// 库位编号    
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号  
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名  
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4    
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单据数    
        /// </summary>
        public decimal OrderQuantity { get; set; }
    }

    /// <summary>
    /// 单据出库输入参数
    /// </summary>
    public class OrderIssueInput
    {
        /// <summary>
        /// 物料明细
        /// </summary>
        public List<WareOrderDetailsInput> WareOrderDetailss { get; set; }
    }

    /// <summary>
    /// 新增单据出库输入参数
    /// </summary>
    public class AddOrderIssueInput : OrderIssueInput
    {
        /// <summary>
        /// 单别（1204_异库调拨单、1203_装配调拨单、1201_库位调拨单、2D01_零组件发货）
        /// </summary>
        public string OrderCategory { get; set; }
    }

    /// <summary>
    /// 编辑单据出库输入参数
    /// </summary>
    public class EditOrderIssueInput : OrderIssueInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 删除单据出库输入参数
    /// </summary>
    public class DeleteOrderIssueInput : BaseId
    {
    }

    /// <summary>
    /// 删除单据明细出库输入参数
    /// </summary>
    public class DeleteOrderDetailsIssueInput : BaseId
    {
    }

    /// <summary>
    /// 查询单据出库输入参数
    /// </summary>
    public class SearchOrderIssueInput : OrderIssueInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }
    }

    /// <summary>
    /// 分配库位输入参数
    /// </summary>
    public class DistributionLocationInput
    {
        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号  
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 规格  
        /// </summary>
        public string SpecificationModel { get; set; }
    }

    /// <summary>
    /// 分配库位确认输入参数
    /// </summary>
    public class DistributionConfirmInput : BaseId
    {
        /// <summary>
        /// 库位编号    
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        public string ContainerCode { get; set; }
    }

    /// <summary>
    /// 一键推荐库位输入参数
    /// </summary>
    public class OnekeyDistributionLocationInput
    {
        /// <summary>
        /// 单据详情Id主键
        /// </summary>
        [Required(ErrorMessage = "单据详情Id主键不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 批量物料单据下发输入参数
    /// </summary>
    public class BatchTransferIssueInput : BaseId
    {
        /// <summary>
        /// 出库口
        /// </summary>
        public int ToPlaceId { get; set; }
    }

    /// <summary>
    /// 单个物料单据下发输入参数
    /// </summary>
    public class SingleTransferIssueInput : BaseId
    {
        /// <summary>
        /// 出库口
        /// </summary>
        public int ToPlaceId { get; set; }
    }

    /// <summary>
    /// 打印单据出库输入参数
    /// </summary>
    public class PrintWareOrderInput
    {
        /// <summary>
        /// Id不能为空
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 打印分拣单据输入参数
    /// </summary>
    public class PrintWareSortingOrderInput
    {
        /// <summary>
        /// Id不能为空
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 更新库位为占位
    /// </summary>
    public class EditPlPlaceInput
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        [Required(ErrorMessage = "库位编号不能为空")]
        public string LocationCode { get; set; }
    }

    /// <summary>
    /// 打印单据出库输入参数
    /// </summary>
    public class PrintOrderIssueDetailSearch
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public long Id { get; set; }
    }


    /// <summary>
    /// 新增单据出库输入参数
    /// </summary>
    public class AddShipmentInput : OrderIssueInput
    {
        /// <summary>
        /// 单别（1204_异库调拨单、1203_装配调拨单、1201_库位调拨单、2D01_零组件发货）
        /// </summary>
        public string OrderCategory { get; set; }
    }

    /// <summary>
    /// 新增发货预约输入参数
    /// </summary>
    public class ShipmentReservationInput
    {
        /// <summary>
        /// 发货单据Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 发货预约参数
        /// </summary>
        public ShipmentReservation ShipmentReservation { get; set; }
    }

    /// <summary>
    /// 发货预约参数
    /// </summary>
    public class ShipmentReservation
    {
        /// <summary>
        /// 预约单号
        /// </summary>
        public string AppointmentNo { get; set; }

        /// <summary>
        /// 发货单ID
        /// </summary>
        public long ShipmentID { get; set; }

        /// <summary>
        /// 发货单号
        /// </summary>
        public string ShipmentNo { get; set; }

        /// <summary>
        /// 运输公司
        /// </summary>
        public string TransportCompany { get; set; }

        /// <summary>
        /// 物流单号
        /// </summary>
        public string ExpressInfoNo { get; set; }

        /// <summary>
        /// 运输车辆
        /// </summary>
        public string TransportVehicle { get; set; }

        /// <summary>
        /// 司机姓名
        /// </summary>
        public string DriverName { get; set; }

        /// <summary>
        /// 驾驶员资格
        /// </summary>
        public string DriverQualification { get; set; }

        /// <summary>
        /// 司机电话
        /// </summary>
        public string DriverPhone { get; set; }

        /// <summary>
        /// 发货地址
        /// </summary>
        public string DeliverGoodsPlaceShip { get; set; }

        /// <summary>
        /// 发货仓库
        /// </summary>
        public string DeliverGoodsWarehouseCode { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string CollectDeliveryPlaceShip { get; set; }

        /// <summary>
        /// 收货仓库
        /// </summary>
        public string CollectDeliveryWarehouseCode { get; set; }

        /// <summary>
        /// 预计到达日期
        /// </summary>
        public DateTimeOffset? EstimatedDate { get; set; }

        /// <summary>
        /// 发货日期
        /// </summary>
        public DateTimeOffset? DeliverDate { get; set; }

        /// <summary>
        /// 收货人
        /// </summary>
        public string CollectDeliveryUserName { get; set; }

        /// <summary>
        /// 收货人电话
        /// </summary>
        public string CollectDeliveryUserPhone { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; } = AuditStatusEnum.yuyuezhong;

        /// <summary>
        /// 图片
        /// </summary>
        public List<string> Images { get; set; }
    }
}
