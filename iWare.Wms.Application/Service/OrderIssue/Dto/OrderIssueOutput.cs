﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 手动单出库输出参数
    /// </summary>
    public class OrderIssueOutput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 出库单据  
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 单别
        /// </summary>
        public string OrderCategory { get; set; } = "N/A";

        /// <summary>
        /// 出库单据类型
        /// </summary>
        public IssueTypeEnum OrderType { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 单据总数    
        /// </summary>
        public decimal OrderQuantityTotal { get; set; }

        /// <summary>
        /// 实际下发总数    
        /// </summary>
        public decimal ActualQuantityTotal { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public IssueStausEnum OrderStatus { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public string CreatedUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset CreatedTime { get; set; }

        /// <summary>
        /// 更新者名称
        /// </summary>
        public string UpdatedUserName { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset UpdatedTime { get; set; }
    }

    /// <summary>
    /// 手动单出库创建库存明细输出参数
    /// </summary>
    public class OrderIssueStockOutput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 原库位编号
        /// </summary>
        public string OriginalLocationCode { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 当前库存数量
        /// </summary>
        public decimal CurrentQuantity { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 库位状态
        /// </summary>
        public LocationStatusEnum? LocationStatus { get; set; }
    }

    /// <summary>
    /// 打印单据出库输出参数
    /// </summary>
    public class PrintWareOrderDetailsOutput
    {
        /// <summary>
        /// 出库单据  
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 单别
        /// </summary>
        public string OrderCategory { get; set; }

        /// <summary>
        /// 单别名称
        /// </summary>
        public string OrderCategoryName { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 单据总数    
        /// </summary>
        public decimal OrderQuantityTotal { get; set; }

        /// <summary>
        /// 部门编号
        /// </summary>
        public string DepartmentNumber { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentNumberName { get; set; }

        /// <summary>
        /// 单据备注
        /// </summary>
        public string OrderRemark { get; set; }

        /// <summary>
        /// 审核码
        /// </summary>
        public string AuditCode { get; set; }

        /// <summary>
        /// 审核码
        /// </summary>
        public string AuditCodeName { get; set; }

        /// <summary>
        /// 工厂
        /// </summary>
        public string FactoryCode { get; set; }

        /// <summary>
        /// 单据日期
        /// </summary>
        public string OrderDate { get; set; }

        /// <summary>
        /// 工位编号
        /// </summary>
        public string StationNumber { get; set; }

        /// <summary>
        /// 主件品号
        /// </summary>
        public string MainPartNumber { get; set; }

        /// <summary>
        /// 设备号
        /// </summary>
        public string EquipmentNumber { get; set; }

        /// <summary>
        /// 单据明细
        /// </summary>
        public List<WareOrderDetails> WareOrderDetails { get; set; }
    }

    /// <summary>
    /// 打印分拣单据输出参数
    /// </summary>
    public class PrintWareSortOrderOutput
    {
        /// <summary>
        /// 来源单号    
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 分拣明细输出参数
        /// </summary>
        public List<PrintWareSortOrderDetailsOutput> sortOrderDetailss { get; set; }
    }

    /// <summary>
    /// 分拣明细输出参数
    /// </summary>
    public class PrintWareSortOrderDetailsOutput
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 品号  
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名  
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 分拣数    
        /// </summary>
        public decimal SortQuantity { get; set; }

        /// <summary>
        /// 实际分拣数    
        /// </summary>
        public decimal ActualQuantity { get; set; }
    }

    /// <summary>
    /// 单据明细输出参数
    /// </summary>
    public class WareOrderDetailsOutput
    {
        /// <summary>
        /// 单据明细Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 单据主表Id  
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位编号    
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号  
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名  
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4    
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单据数    
        /// </summary>
        public decimal OrderQuantity { get; set; }

        /// <summary>
        /// 下发数    
        /// </summary>
        public decimal ActualQuantity { get; set; }

        /// <summary>
        /// 备注    
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 当前库存数量
        /// </summary>
        public decimal CurrentQuantity { get; set; }

        /// <summary>
        /// 库位状态
        /// </summary>
        public LocationStatusEnum? LocationStatus { get; set; }

        /// <summary>
        /// 转入库  ----ERP对应的转入库    
        /// </summary>
        public string TransferToReceipt { get; set; }

        /// <summary>
        /// 转入库位  ----ERP对应的转入库位
        /// </summary>
        public string TransferredILocation { get; set; }

        /// <summary>
        /// 转出库  ----ERP对应的转出库    
        /// </summary>
        public string TransferToIssue { get; set; }

        /// <summary>
        /// 转出库位  ----ERP对应的转出库位
        /// </summary>
        public string TransferOutLocation { get; set; }

        /// <summary>
        /// 是否已下发完成
        /// </summary>
        public YesOrNot IsXiafaComplete { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public string ReservoirArea { get; set; }

        /// <summary>
        /// 单据主表
        /// </summary>
        public WareOrder WareOrder { get; set; }
    }
}
