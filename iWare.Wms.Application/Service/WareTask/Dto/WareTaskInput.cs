﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 任务查询参数
    /// </summary>
    public class WareTaskSearch : PageInputBase
    {
        /// <summary>
        /// 任务类型-全部_-10、入库任务_1、出库任务_2、移库任务_3
        /// </summary>
        public TaskType TaskType { get; set; } = TaskType.All;

        /// <summary>
        /// 组盘单据   
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 任务号  
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务状态-全部_-10、未执行_0、执行中_1、已完成_2
        /// </summary>
        public TaskStatusEnum TaskStatus { get; set; } = TaskStatusEnum.All;

        /// <summary>
        /// 容器编号  
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public virtual string CreatedUserName { get; set; }
    }

    /// <summary>
    /// 移库输入参
    /// </summary>
    public class YiKuCallbackInput
    {
        /// <summary>
        /// 目标库位
        /// </summary>
        public string? endCode { get; set; }

        /// <summary>
        /// 起始库位
        /// </summary>
        public string? startCode { get; set; }
    }

    /// <summary>
    /// 任务输入参数
    /// </summary>
    public class WareTaskInput
    {
        /// <summary>
        /// 任务类型-全部_-10、入库任务_1、出库任务_2、移库任务_3 
        /// </summary>
        public TaskType TaskType { get; set; } = TaskType.In;

        /// <summary>
        /// 来源单据号    
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 任务号  
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务名称    
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务描述    
        /// </summary>
        public string Describe { get; set; }

        /// <summary>
        /// 优先级    
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// 任务状态-全部_-10、未执行_0、执行中_1、已完成_2 
        /// </summary>
        public TaskStatusEnum Status { get; set; } = TaskStatusEnum.NotProgress;
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddWareTaskInput : WareTaskInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteWareTaskInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 重新输入参数
    /// </summary>
    public class QueryeWareTaskInput : BaseId
    {
    }

    /// <summary>
    /// 强制完成任务输入参数
    /// </summary>
    public class TaskCallbackInput : BaseId
    {
    }

    /// <summary>
    /// 更新任务级别输入参数
    /// </summary>
    public class UpdateTaskPriorityInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 优先级
        /// </summary>
        public int Priority { get; set; }
    }
}
