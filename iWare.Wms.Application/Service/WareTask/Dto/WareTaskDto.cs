﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 任务输出参数
    /// </summary>
    public class WareTaskDto
    {
        /// <summary>
        /// 类型-入库_1、出库_2、移库_3   
        /// </summary>
        public TaskType TaskType { get; set; } = TaskType.In;

        /// <summary>
        /// 来源单据号    
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 任务号  
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务名称    
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务描述    
        /// </summary>
        public string Describe { get; set; }

        /// <summary>
        /// 优先级    
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// 状态-未执行_0、执行中_1、已完成_2    
        /// </summary>
        public TaskStatusEnum Status { get; set; } = TaskStatusEnum.NotProgress;

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
