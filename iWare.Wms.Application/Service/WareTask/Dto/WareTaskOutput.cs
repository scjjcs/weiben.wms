﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 任务输出参数
    /// </summary>
    public class WareTaskOutput
    {
        /// <summary>
        /// 组盘单据号    
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 任务号  
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务方式;任务方式-手动_1、自动_2
        /// </summary>
        public TaskModel TaskModel { get; set; }

        /// <summary>
        /// 任务类型-入库_1、出库_2、移库_3
        /// </summary>
        public TaskType TaskType { get; set; }

        /// <summary>
        /// 任务名称    
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务排序    
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// 出发地库位编号   
        /// </summary>
        public string FromLocationCode { get; set; }

        /// <summary>
        /// 目的地库位编号   
        /// </summary>
        public string ToLocationCode { get; set; }

        /// <summary>
        /// 状态-未执行_0、执行中_1、已完成_2
        /// </summary>
        public TaskStatusEnum TaskStatus { get; set; } = TaskStatusEnum.NotProgress;

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 是否在输送线上
        /// </summary>
        public YesOrNot IsShuSongXian { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public string ReservoirArea { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        public string Weight { get; set; }
    }
}
