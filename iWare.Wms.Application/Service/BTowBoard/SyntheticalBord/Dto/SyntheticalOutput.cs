﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 出入库任务信息输出参
    /// </summary>
    public class TaskOutput
    {
        /// <summary>
        /// 任务总数
        /// </summary>
        public object TaskSum { get; set; } = 0;

        /// <summary>
        /// 任务已完成数
        /// </summary>
        public object TaskBeNumber { get; set; } = 0;

        /// <summary>
        /// 任务未完成数
        /// </summary>
        public object TaskNotNumber { get; set; } = 0;

        /// <summary>
        /// 出入库任务详情
        /// </summary>
        public List<TaskSpecifics> tasks { get; set; }
    }

    /// <summary>
    /// 出入库任务详情
    /// </summary>
    public class TaskSpecifics
    {
        /// <summary>
        /// 任务类型
        /// </summary>
        public object TaskType { get; set; }

        /// <summary>
        /// 仓库类型
        /// </summary>
        public object WarehouseType { get; set; }

        /// <summary>
        /// 项目号
        /// </summary>
        public object ProjectCode { get; set; }

        /// <summary>
        /// 总数
        /// </summary>
        public object Sum { get; set; } = 0;

        /// <summary>
        /// 已完成数
        /// </summary>
        public object BeNumber{ get; set; } = 0;

        /// <summary>
        /// 未完成数
        /// </summary>
        public object NotNumber { get; set; } = 0;
    }

    /// <summary>
    /// 库位数量
    /// </summary>
    public class LocatorSumOutput
    {
        /// <summary>
        /// 立体库存货数
        /// </summary>
        public object LStockNumber { get; set; } = 0;

        /// <summary>
        /// 立体空闲数
        /// </summary>
        public object LIdleNumber { get; set; } = 0;

        /// <summary>
        /// 平库存货数
        /// </summary>
        public object PStockNumber { get; set; } = 0;

        /// <summary>
        /// 平库空闲数
        /// </summary>
        public object PIdleNumber { get; set; } = 0;
    }

    /// <summary>
    /// 保质期预警输出参
    /// </summary>
    public class EarlyWarningOutput
    {
        /// <summary>
        /// 货物总数
        /// </summary>
        public object GoodsSum { get; set; } = 0;

        /// <summary>
        /// 预警详情
        /// </summary>
        public List<EarlyWarning> earlyWarnings { get; set; }
    }

    /// <summary>
    /// 预警详情
    /// </summary>
    public class EarlyWarning
    {
        /// <summary>
        /// 货物名称
        /// </summary>
        public object GoodsName { get; set; }

        /// <summary>
        /// 所在位置
        /// </summary>
        public object PresentLocation { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public object GoodsNumber { get; set; } = 0;

        /// <summary>
        /// 到期日期
        /// </summary>
        public object ExpirationTime { get; set; }
    }

    /// <summary>
    /// 详情信息
    /// </summary>
    public class InformationOutput
    {
        /// <summary>
        /// 预约详情
        /// </summary>
        public DocketSpecifics SubscribeSpecifics { get; set; }

        /// <summary>
        /// 收货详情
        /// </summary>
        public DocketSpecifics CollectSpecifics { get; set; }

        /// <summary>
        /// 入库详情
        /// </summary>
        public DocketSpecifics EntrySpecifics { get; set; }

        /// <summary>
        /// 调拨详情
        /// </summary>
        public DocketSpecifics AllocateSpecifics { get; set; }

        /// <summary>
        /// 发货详情
        /// </summary>
        public DocketSpecifics DeliverySpecifics { get; set; }
    }

    /// <summary>
    /// 单据详情
    /// </summary>
    public class DocketSpecifics
    {
        /// <summary>
        /// 计划数量
        /// </summary>
        public object PlanNumber { get; set; } = 0;

        /// <summary>
        /// 完成数量
        /// </summary>
        public object CompleteNumber { get; set; } = 0;

        /// <summary>
        /// 剩余数量
        /// </summary>
        public object SurplusNumber { get; set; } = 0;

        /// <summary>
        /// 以前剩余数量
        /// </summary>
        public object AgoSurplusNumber { get; set; } = 0;

        /// <summary>
        /// 单据信息
        /// </summary>
        public List<Docket> dockets { get; set; }
    }

    /// <summary>
    /// 单据信息
    /// </summary>
    public class Docket
    {
        /// <summary>
        /// 项目号
        /// </summary>
        public object ProjectCode { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public object Number { get; set; } = 0;

        /// <summary>
        /// 合格数量
        /// </summary>
        public object PassNumber { get; set; } = 0;
    }



}
