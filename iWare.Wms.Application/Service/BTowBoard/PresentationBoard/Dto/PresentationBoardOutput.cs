﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 仓库介绍-基础信息输出参数
    /// </summary>
    public class WarehouseIntroductionOutput
    {
        /// <summary>
        /// 仓库面积
        /// </summary>
        public string WarehouseArea { get; set; }

        /// <summary>
        /// 总库位
        /// </summary>
        public string TotalStorageLocation { get; set; }

        /// <summary>
        /// 库区数
        /// </summary>
        public string AreaNum { get; set; }

        /// <summary>
        /// 货物种类
        /// </summary>
        public string TypeOfGoods { get; set; }

        /// <summary>
        /// 货物使用种类
        /// </summary>
        public string TypeOfUse { get; set; }
    }

    /// <summary>
    /// 仓库介绍-出入库任务输出参数
    /// </summary>
    public class GetInAndOutTaskOutput
    {
        /// <summary>
        /// 入库计划数
        /// </summary>
        public int InPlanNum { get; set; } = 0;

        /// <summary>
        /// 入库实际数
        /// </summary>
        public int InActualNum { get; set; } = 0;

        /// <summary>
        /// 出库计划数
        /// </summary>
        public int OutPlanNum { get; set; } = 0;

        /// <summary>
        /// 出库实际数
        /// </summary>
        public int OutActualNum { get; set; } = 0;
    }

    /// <summary>
    /// AGV库/立体库库位信息输出参数
    /// </summary>
    public class WarehouseLocationOutput
    {
        /// <summary>
        /// 总库位
        /// </summary>
        public int TotalStorageLocation { get; set; }

        /// <summary>
        /// 空托盘
        /// </summary>
        public int EmptyTray { get; set; }

        /// <summary>
        /// 空库位
        /// </summary>
        public int EmptyLocation { get; set; }

        /// <summary>
        /// 货物/有货
        /// </summary>
        public int Goods { get; set; }
    }

    /// <summary>
    /// AGV库报警信息输出参数
    /// </summary>
    public class AGVAlarmOutput
    {
        /// <summary>
        /// AGV-01
        /// </summary>
        public AlarmOutput AGV01 { get; set; }

        /// <summary>
        /// AGV-02
        /// </summary>
        public AlarmOutput AGV02 { get; set; }
    }

    /// <summary>
    /// 设备报警信息输出参数
    /// </summary>
    public class AlarmOutput
    {
        /// <summary>
        /// 设备名称
        /// </summary>
        public string AGVName { get; set; }

        /// <summary>
        /// 设备状态
        /// </summary>
        public string AGVStatus { get; set; }

        /// <summary>
        /// 报警时间
        /// </summary>
        public string AlarmTime { get; set; }

        /// <summary>
        /// 报警代码
        /// </summary>
        public string AlarmCode { get; set; }
    }

    /// <summary>
    /// 立体库库口信息输出参数
    /// </summary>
    public class WarehouseEntranceOutput
    {
        /// <summary>
        /// 入库口
        /// </summary>
        public string Entrance { get; set; }

        /// <summary>
        /// 出库口
        /// </summary>
        public string Exit { get; set; }
    }

    /// <summary>
    /// 组装车间输出参数
    /// </summary>
    public class AssemblyWorkshopOutput
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 计划数
        /// </summary>
        public int PlanNum { get; set; }

        /// <summary>
        /// 实际数
        /// </summary>
        public int ActuaityNum { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 百分比
        /// </summary>
        public string Percentage { get; set; }
    }
}
