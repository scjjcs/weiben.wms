﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 仓库管理看板
    /// </summary>
    public class StorehouseOutput
    {
        /// <summary>
        /// 存货货位数量
        /// </summary>
        public object StockNumber1 { get; set; }

        /// <summary>
        /// 托盘贷位数量
        /// </summary>
        public object StockNumber2 { get; set; }

        /// <summary>
        /// 空余货位数量
        /// </summary>
        public object StockNumber3 { get; set; }

        /// <summary>
        /// 待出库任务
        /// </summary>
        public object OutTaskNumber { get; set; }

        /// <summary>
        /// 待入库任务
        /// </summary>
        public object InTaskNumber { get; set; }

        /// <summary>
        /// 出库执行信息
        /// </summary>
        public DiscrepancyMessage OutDiscrepancy { get; set; }

        /// <summary>
        /// 入库执行信息
        /// </summary>
        public DiscrepancyMessage InDiscrepancy { get; set; }

        /// <summary>
        /// 待出库任务
        /// </summary>
        public List<TaskMessage> OutTask { get; set; }
        
        /// <summary>
        /// 待入库任务
        /// </summary>
        public List<TaskMessage> InTask { get; set; }
    }

    /// <summary>
    /// 出入库口执行信息
    /// </summary>
    public class DiscrepancyMessage
    {
        /// <summary>
        /// 项目号
        /// </summary>
        public object ProjectId { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        public object TaskId { get; set; }

        /// <summary>
        /// 货物名称
        /// </summary>
        public object MaterialName { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTimeOffset? StartTime { get; set; }
    }

    /// <summary>
    /// 出入库任务信息
    /// </summary>
    public class TaskMessage
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public object MaterialName { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        public object TaskId { get; set; }

        /// <summary>
        /// 项目号
        /// </summary>
        public object ProjectId { get; set; }
    }

    /// <summary>
    /// 库位X,Y轴输入参
    /// </summary>
    public class LocationInput
    {
        /// <summary>
        /// X轴坐标
        /// </summary>
        public int SRMX { get; set; }

        /// <summary>
        /// Y轴坐标
        /// </summary>
        public int SRMY { get; set; }
    }
}
