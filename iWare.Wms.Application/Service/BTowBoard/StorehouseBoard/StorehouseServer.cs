﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using iWare.Wms.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 仓库管理看板服务
    /// </summary>
    [Route("api/StorehouseBoard")]
    [ApiDescriptionSettings("仓库看板", Name = "StorehouseBoard", Order = 112)]

    public class StorehouseServer : IDynamicApiController, ITransient
    {
        private readonly ISqlRepository<StereoscopicDbContextLocation> _stereoscopicRep;
        private readonly IRepository<WareLocation, MasterDbContextLocator> _wareLocationRep; //库位表
        private readonly IRepository<v_accessrecord, MasterDbContextLocator> _v_accessrecord; //出入库明细试图仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="stereoscopicRep"></param>
        /// <param name="v_accessrecord"></param>
        /// <param name="wareLocationRep"></param>
        public StorehouseServer(
             ISqlRepository<StereoscopicDbContextLocation> stereoscopicRep,
             IRepository<v_accessrecord, MasterDbContextLocator> v_accessrecord,
             IRepository<WareLocation, MasterDbContextLocator> wareLocationRep

        )
        {
            _stereoscopicRep = stereoscopicRep;
            _v_accessrecord = v_accessrecord;
            _wareLocationRep = wareLocationRep;
        }
        #endregion
        /// <summary>
        /// X坐标
        /// </summary>
        public static int SRMX1 { get; set; }

        /// <summary>
        /// Y坐标
        /// </summary>
        public static int SRMY1 { get; set; }

        /// <summary>
        /// 立体库仓库管理
        /// </summary>
        /// <returns></returns>
        [HttpGet("SolidWarehouseBoard")]
        [AllowAnonymous]
        public async Task<StorehouseOutput> SolidWarehouse()
        {
            var SolidStorehouse = new StorehouseOutput();

            //查询立体库的出入明细信息
            var accessrecord = await _v_accessrecord.DetachedEntities.Where(z => z.FromLocationCode.Contains("W1L") || z.ToLocationCode.Contains("W1L")).ToListAsync();

            #region 出入库口执行情况

            //查询立体库出库执行信息并赋值
            SolidStorehouse.OutDiscrepancy = DTaskList(accessrecord, TaskType.Out, TaskStatusEnum.Progress);

            //查询立体库入库执行信息并赋值
            SolidStorehouse.InDiscrepancy = DTaskList(accessrecord, TaskType.In, TaskStatusEnum.Progress);

            #endregion

            #region 出入库任务信息

            //查询待出库信息并循环赋值
            var OTaskList = TaskList(accessrecord, TaskType.Out, TaskStatusEnum.NotProgress);
            SolidStorehouse.OutTask = OTaskList;
            SolidStorehouse.OutTaskNumber = OTaskList.Count();

            //查询待入库信息并循环赋值
            var ITaskList = TaskList(accessrecord, TaskType.In, TaskStatusEnum.NotProgress);
            SolidStorehouse.InTask = ITaskList;
            SolidStorehouse.InTaskNumber = ITaskList.Count();

            #endregion

            #region 库存情况

            //查询立体库库位表
            var Place = await _wareLocationRep.DetachedEntities.Where(z => z.AreaID == 374444843716677).ToListAsync();

            var StockNumber1 = Place.Where(z => z.LocationStatus == LocationStatusEnum.daichu || z.LocationStatus == LocationStatusEnum.cunhuo); //存货货位
            var StockNumber2 = Place.Where(z => z.IsEmptyContainer == YesOrNot.Y); //托盘贷位
            var StockNumber3 = Place.Where(z => z.LocationStatus == LocationStatusEnum.dairu || z.LocationStatus == LocationStatusEnum.kongxian); //空余货位

            SolidStorehouse.StockNumber1 = StockNumber1.Count();
            SolidStorehouse.StockNumber2 = StockNumber2.Count();
            SolidStorehouse.StockNumber3 = StockNumber3.Count();

            #endregion


            return SolidStorehouse;
        }

        /// <summary>
        /// 获取立体库堆垛机参数
        /// </summary>
        /// <param name="locationInput"></param>
        /// <returns></returns>
        [HttpPost("LocationIn")]
        [AllowAnonymous]
        public async Task<LocationInput> LocationIn ([FromBody] LocationInput locationInput)
        {
            try
            {
                // 写日志文件
                Log.Error("LocationIn" + locationInput.SRMX +"XY" + locationInput.SRMY);

                SRMX1 = locationInput.SRMX;
                SRMY1 = locationInput.SRMY;

                return locationInput;
            }
            catch (Exception ex)
            {
                throw Oops.Oh(ex.Message);
            }
        }

        /// <summary>
        /// 输出立体库堆垛机参数
        /// </summary>
        /// <returns></returns>
        [HttpGet("LocationOut")]
        [AllowAnonymous]
        public async Task<LocationInput> LocationOut()
        {
            try
            {
                var aa = new LocationInput()
                {
                    SRMX = SRMX1,
                    SRMY = SRMY1
                };

                return aa;
            }
            catch (Exception ex)
            {
                throw Oops.Oh(ex.Message);
            }
        }


        #region 虚拟数据
        /// <summary>
        /// 平库仓库管理
        /// </summary>
        /// <returns></returns>
        [HttpGet("FlatStorehouse")]
        [AllowAnonymous]
        [NonAction]
        public async Task<StorehouseOutput> FlatStorehouse()
        {
            var SolidStorehouse = new StorehouseOutput();
            SolidStorehouse.StockNumber1 = 184;
            SolidStorehouse.StockNumber2 = 21;
            SolidStorehouse.StockNumber3 = 87;
            SolidStorehouse.OutTaskNumber = 10;
            SolidStorehouse.InTaskNumber = 10;
            SolidStorehouse.OutDiscrepancy = new DiscrepancyMessage
            {
                StartTime = DateTime.Now,
                ProjectId = "LA21010",
                TaskId = "RW1231231",
                MaterialName = "机器人"
            };

            SolidStorehouse.InDiscrepancy = new DiscrepancyMessage
            {
                StartTime = DateTime.Now.AddHours(-1),
                ProjectId = "LA21010",
                TaskId = "RW1231231",
                MaterialName = "机器人"
            };

            SolidStorehouse.OutTask = TaskMessage1();
            SolidStorehouse.InTask = TaskMessage2();

            return SolidStorehouse;
        }

        /// <summary>
        /// 待出库任务
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public List<TaskMessage> TaskMessage1()
        {
            var TaskMessage = new List<TaskMessage>();
            var Task1 = new TaskMessage {MaterialName = "压头", TaskId = "390787456675910", ProjectId = "LA21010" };
            var Task2 = new TaskMessage { MaterialName = "滑块顶块", TaskId = "391041847521350", ProjectId = "LA21038" };
            var Task3 = new TaskMessage { MaterialName = "气缸接头", TaskId = "391041847521350", ProjectId = "LA20006" };
            var Task4 = new TaskMessage { MaterialName = "导轨安装板-B", TaskId = "391041847521350", ProjectId = "LA20002" };
            var Task5 = new TaskMessage { MaterialName = "40A冷压针 母", TaskId = "390819419713606", ProjectId = "LA18002" };
            var Task6 = new TaskMessage { MaterialName = "垫板", TaskId = "390819419713606", ProjectId = "LA23256" };
            var Task7 = new TaskMessage { MaterialName = "垫块", TaskId = "390796461953094", ProjectId = "LA34563" };
            var Task8 = new TaskMessage { MaterialName = "压钢球机构", TaskId = "390796461953094", ProjectId = "LA21093" };
            var Task9 = new TaskMessage { MaterialName = "滑块顶块", TaskId = "390779053596742", ProjectId = "LA21345" };
            var Task10 = new TaskMessage { MaterialName = "气缸接头", TaskId = "390779053596742", ProjectId = "LA21234" };
            TaskMessage.Add(Task1);
            TaskMessage.Add(Task2);
            TaskMessage.Add(Task3);
            TaskMessage.Add(Task4);
            TaskMessage.Add(Task5);
            TaskMessage.Add(Task6);
            TaskMessage.Add(Task7);
            TaskMessage.Add(Task8);
            TaskMessage.Add(Task9);
            TaskMessage.Add(Task10);
            return TaskMessage;
        }

        /// <summary>
        /// 待入库任务
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public List<TaskMessage> TaskMessage2()
        {
            var TaskMessage = new List<TaskMessage>();
            var Task1 = new TaskMessage { MaterialName = "导轨安装板-B", TaskId = "390779053596742", ProjectId = "LA26742" };
            var Task2 = new TaskMessage { MaterialName = "存放架申购", TaskId = "390766068731976", ProjectId = "LA21976" };
            var Task3 = new TaskMessage { MaterialName = "转接板", TaskId = "390766068731976", ProjectId = "LA21976" };
            var Task4 = new TaskMessage { MaterialName = "电箱固定架", TaskId = "390766068731976", ProjectId = "LA21979" };
            var Task5 = new TaskMessage { MaterialName = "存放架申购", TaskId = "390712251637830", ProjectId = "LA11276" };
            var Task6 = new TaskMessage { MaterialName = "转接板", TaskId = "390712251637830", ProjectId = "LA27830" };
            var Task7 = new TaskMessage { MaterialName = "电箱固定架", TaskId = "390712251637830", ProjectId = "LA37823" };
            var Task8 = new TaskMessage { MaterialName = "垫块", TaskId = "390423411126342", ProjectId = "LA26342" };
            var Task9 = new TaskMessage { MaterialName = "压钢球机构", TaskId = "390423411126342", ProjectId = "LA26212" };
            var Task10 = new TaskMessage { MaterialName = "24VDC开关电源", TaskId = "390416159416390", ProjectId = "LA26390" };
            TaskMessage.Add(Task1);
            TaskMessage.Add(Task2);
            TaskMessage.Add(Task3);
            TaskMessage.Add(Task4);
            TaskMessage.Add(Task5);
            TaskMessage.Add(Task6);
            TaskMessage.Add(Task7);
            TaskMessage.Add(Task8);
            TaskMessage.Add(Task9);
            TaskMessage.Add(Task10);
            return TaskMessage;
        }

        #endregion


        #region 查询仓库数据

        /// <summary>
        /// 查询出入库执行信息
        /// </summary>
        /// <param name="accessrecord"></param>
        /// <param name="taskType"></param>
        /// <param name="taskStatusEnum"></param>
        /// <returns></returns>
        [NonAction]
        public DiscrepancyMessage DTaskList(List<v_accessrecord> accessrecord, TaskType taskType, TaskStatusEnum taskStatusEnum)
        {
            //查询立体库出库执行信息并赋值
            var DTaskList = accessrecord.Where(z => z.TaskType == taskType && z.TaskStatus == taskStatusEnum);
            var DTask = DTaskList.FirstOrDefault();

            var Discrepancy = new DiscrepancyMessage();

            if (DTaskList.Count() > 0)
            {

                Discrepancy.ProjectId = DTask.ProjectCode;
                Discrepancy.TaskId = DTask.TaskNo;
                Discrepancy.MaterialName = DTask.Name;
                Discrepancy.StartTime = DTask.TaskCreatedTime;
            }

            return Discrepancy;
        }

        /// <summary>
        /// 出入库信息赋值
        /// </summary>
        /// <param name="accessrecord"></param>
        /// <param name="taskType"></param>
        /// <param name="taskStatusEnum"></param>
        /// <returns></returns>
        [NonAction]
        public List<TaskMessage> TaskList(List<v_accessrecord> accessrecord, TaskType taskType, TaskStatusEnum taskStatusEnum)
        {
            //查询立体库出入库执行信息并赋值
            var TaskList = accessrecord.Where(z => z.TaskType == taskType && z.TaskStatus == taskStatusEnum);
            var ListTask = new List<TaskMessage>();

            foreach (var item in TaskList)
            {
                var taskMessage = new TaskMessage()
                {
                    ProjectId = item.ProjectCode,
                    TaskId = item.TaskNo,
                    MaterialName = item.Name,
                };
                ListTask.Add(taskMessage);
            }

            return ListTask;
        }

        #endregion
    }
}
