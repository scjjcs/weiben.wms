﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 项目装配信息看板服务
    /// </summary>
    [Route("api/FabricateBoard")]
    [ApiDescriptionSettings("仓库看板", Name = "FabricateBoard", Order = 112)]

    public class FabricateServer : IDynamicApiController, ITransient
    {
        private readonly IRepository<GoodsDelivery, MasterDbContextLocator> _goodsDeliveryRep; //送货单仓储
        private readonly IRepository<CollectDelivery, MasterDbContextLocator> _collectDeliveryRep; //收货单仓储
        private readonly IRepository<CollectDeliveryDetails, MasterDbContextLocator> _collectDeliveryDetailsRep; //收货单明细仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="goodsDeliveryRep"></param>
        /// <param name="collectDeliveryRep"></param>
        /// <param name="collectDeliveryDetailsRep"></param>
        public FabricateServer(
             IRepository<GoodsDelivery, MasterDbContextLocator> goodsDeliveryRep,
             IRepository<CollectDelivery, MasterDbContextLocator> collectDeliveryRep,
             IRepository<CollectDeliveryDetails, MasterDbContextLocator> collectDeliveryDetailsRep
         )
        {
            _goodsDeliveryRep = goodsDeliveryRep;
            _collectDeliveryRep = collectDeliveryRep;
            _collectDeliveryDetailsRep = collectDeliveryDetailsRep;
        }
        #endregion

        /// <summary>
        /// 项目数据信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("FabricateNum")]
        [AllowAnonymous]
        public async Task<FabricateNumOutput> FabricateNum()
        {
            var FabricateNumber = new FabricateNumOutput
            {
                SumNumber = 2227,
                DeliveryNumber = 1645,
                ProcessNumber = 31,
                NotStartedNumber = 7,
                PassNumber = 98.7,
                TimelyNumber = 99.5,
            };

            return FabricateNumber;
        }

        /// <summary>
        /// 项目信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("WarehouseIntel")]
        [AllowAnonymous]
        public async Task<List<WarehouseIntelOutput>> WarehouseIntel()
        {
            var WarehouseIntelList = new List<WarehouseIntelOutput>();

            var WarehouseIntel1 = new WarehouseIntelOutput { WarehouseId = 10101, WarehouseCode = "MA22726", WarehouseName = "君屹Minth PSA Smart Car Serbia项目", DeliverTime = Convert.ToDateTime("2023/6/10"), WarehouseState = "装配进度", StateValue = 85 };
            var WarehouseIntel2 = new WarehouseIntelOutput { WarehouseId = 10102, WarehouseCode = "AT21045", WarehouseName = "第三代半导体控制器总装线", DeliverTime = Convert.ToDateTime("2023/7/1"), WarehouseState = "装配进度", StateValue = 80 };
            WarehouseIntelList.Add(WarehouseIntel1);
            WarehouseIntelList.Add(WarehouseIntel2);
            return WarehouseIntelList;
        }

        /// <summary>
        /// 叫料任务(查询)
        /// </summary>
        /// <returns></returns>
        [HttpGet("WarehouseTask")]
        [AllowAnonymous]
        public async Task<WarehouseTaskOutput> WarehouseTask(string WarehouseId)
        {
            var warehouseTaskOut = new WarehouseTaskOutput();

            var WarehouseTaskList = new List<WarehouseTask>();

            if (WarehouseId == "10101")
            {
                var ListMatter = await _collectDeliveryDetailsRep.Where(z => z.ProjectCode == "MA22726").ToListAsync();

                foreach (var item in ListMatter)
                {
                    var WarehouseTask = new WarehouseTask
                    {
                        MatterName = item.Name,
                        MatterId = item.Code,
                        MatterNumber = item.CollectQuantity,
                        MatterStatus = "新增叫料"
                    };
                    WarehouseTaskList.Add(WarehouseTask);
                }

                //var WarehouseTask1 = new WarehouseTask { MatterName = "托盘", MatterId = "调RW0001", MatterNumber = 23, MatterStatus = "新增叫料" };
                //var WarehouseTask2 = new WarehouseTask { MatterName = "螺丝", MatterId = "R12312122", MatterNumber = 38, MatterStatus = "送料中" };
                //var WarehouseTask3 = new WarehouseTask { MatterName = "线圈", MatterId = "R12312126", MatterNumber = 12, MatterStatus = "驳回，缺料" };
                //var WarehouseTask4 = new WarehouseTask { MatterName = "电阻", MatterId = "R12312122", MatterNumber = 25, MatterStatus = "送达" };
                //var WarehouseTask5 = new WarehouseTask { MatterName = "扳手", MatterId = "R12312165", MatterNumber = 30, MatterStatus = "完成" };
                //WarehouseTaskList.Add(WarehouseTask1);
                //WarehouseTaskList.Add(WarehouseTask2);
                //WarehouseTaskList.Add(WarehouseTask3);
                //WarehouseTaskList.Add(WarehouseTask4);
                //WarehouseTaskList.Add(WarehouseTask5);
            }
            else if (WarehouseId == "10102")
            {
                var ListMatter = await _collectDeliveryDetailsRep.Where(z => z.ProjectCode == "AT21045").ToListAsync();

                foreach (var item in ListMatter)
                {
                    var WarehouseTask = new WarehouseTask
                    {
                        MatterName = item.Name,
                        MatterId = item.Code,
                        MatterNumber = item.CollectQuantity,
                        MatterStatus = "新增叫料"
                    };
                    WarehouseTaskList.Add(WarehouseTask);
                }
            }

            warehouseTaskOut.TaskNumber = 2;
            warehouseTaskOut.warehouseTask = WarehouseTaskList;

            return warehouseTaskOut;
        }
    }
}
