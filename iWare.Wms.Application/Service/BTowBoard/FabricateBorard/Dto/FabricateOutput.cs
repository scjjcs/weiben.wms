﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 项目装配数输出参
    /// </summary>
    public class FabricateNumOutput
    {
        /// <summary>
        /// 年总数量
        /// </summary>
        public object SumNumber { get; set; }

        /// <summary>
        /// 已发货数
        /// </summary>
        public object DeliveryNumber { get; set; }

        /// <summary>
        /// 加工中
        /// </summary>
        public object ProcessNumber { get; set; }

        /// <summary>
        /// 未开始
        /// </summary>
        public object NotStartedNumber { get; set; }

        /// <summary>
        /// 合格率
        /// </summary>
        public object PassNumber { get; set; }

        /// <summary>
        /// 及时交付
        /// </summary>
        public object TimelyNumber { get; set; }

    }

    /// <summary>
    /// 项目信息输出参
    /// </summary>
    public class WarehouseIntelOutput
    {
        /// <summary>
        /// 项目Id
        /// </summary>
        public object WarehouseId { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public object WarehouseCode { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public object WarehouseName { get; set; }

        /// <summary>
        /// 交期时间
        /// </summary>
        public object DeliverTime { get; set; }

        /// <summary>
        /// 项目状态
        /// </summary>
        public object WarehouseState { get; set; }

        /// <summary>
        /// 状态值
        /// </summary>
        public object StateValue { get; set; }
    }

    /// <summary>
    /// 叫料任务输出参
    /// </summary>
    public class WarehouseTaskOutput
    {
        /// <summary>
        /// 任务数
        /// </summary>
        public object TaskNumber { get; set; }

        /// <summary>
        /// 叫料任务详情
        /// </summary>
        public List<WarehouseTask> warehouseTask { get; set; }
    }

    /// <summary>
    /// 叫料任务详情
    /// </summary>
    public class WarehouseTask
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public object MatterName { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        public object MatterId { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public object MatterNumber { get; set; }

        /// <summary>
        /// 物料状态
        /// </summary>
        public object MatterStatus { get; set; }
    }

}
