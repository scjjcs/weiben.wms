﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static iWare.Wms.Application.ScreenFreightOutput;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 收货,送货看板服务
    /// </summary>
    [Route("api/ScreenFreightBoard")]
    [ApiDescriptionSettings("仓库看板", Name = "ScreenFreightBoard", Order = 112)]

    public class ScreenFreightServer : IDynamicApiController, ITransient
    {
        private readonly IRepository<GoodsDelivery, MasterDbContextLocator> _goodsDeliveryRep; //送货单仓储
        private readonly IRepository<CollectDelivery, MasterDbContextLocator> _collectDeliveryRep; //收货单仓储
        private readonly IRepository<CollectDeliveryDetails, MasterDbContextLocator> _collectDeliveryDetailsRep; //收货单明细仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="goodsDeliveryRep"></param>
        /// <param name="collectDeliveryRep"></param>
        /// <param name="collectDeliveryDetailsRep"></param>
        public ScreenFreightServer(
             IRepository<GoodsDelivery, MasterDbContextLocator> goodsDeliveryRep,
             IRepository<CollectDelivery, MasterDbContextLocator> collectDeliveryRep,
             IRepository<CollectDeliveryDetails, MasterDbContextLocator> collectDeliveryDetailsRep
         )
        {
            _goodsDeliveryRep = goodsDeliveryRep;
            _collectDeliveryRep = collectDeliveryRep;
            _collectDeliveryDetailsRep = collectDeliveryDetailsRep;
        }
        #endregion

        ///当天0时0分0秒：
        DateTime start = Convert.ToDateTime(DateTime.Now.AddDays(-2).ToString("D").ToString());

        //当天23时59分59秒：
        DateTime end = Convert.ToDateTime(DateTime.Now.AddDays(1).ToString("D").ToString()).AddSeconds(-1);

        /// <summary>
        /// 收货检验区
        /// </summary>
        /// <returns></returns>
        [HttpGet("ScreenRece")]
        [AllowAnonymous]
        public async Task<ScreenReceOutput> ScreenRece()
        {
            //收货检验信息
            var ScreenRece = new ScreenReceOutput();

            var receNum = new ReceNum();
            #region 查询已收/待收送货单数
            //查询今日送货详情
            var Inspect = await _collectDeliveryRep.Where(z => z.IsDeleted == false && z.CreatedTime > start && z.CreatedTime < end).ToListAsync();

            //查询今日待收送货单数
            var NotDeliveryNum = await _goodsDeliveryRep.Where(z => z.Status != AuditStatusEnum.songhuowancheng 
            && z.GoodsDeliveryAppointments.EstimatedDate > start && z.GoodsDeliveryAppointments.EstimatedDate < end).CountAsync();

            receNum.DeliveryNum = Inspect.Count;
            receNum.NotDeliveryNum = NotDeliveryNum;
            #endregion

            #region 查询质检/未质检单数
            //质检/未质检数
            var InspectNum = 0;
            var NotInspectNum = 0;

            //判断该单据是否都检验完成
            foreach (var item in Inspect)
            {
                //查询未质检的送货单详情进行判断
                var SunNum = await _collectDeliveryDetailsRep
                .Where(z => z.CollectDeliveryId == item.Id && z.InspectionMethod != InspectionMethodEnum.mianjian && z.IsQuality == YesOrNot.N && z.CreatedTime > start && z.CreatedTime < end).CountAsync();

                if (SunNum > 0)
                {
                    NotInspectNum ++;
                }
                SunNum = await _collectDeliveryDetailsRep
                .Where(z => z.CollectDeliveryId == item.Id && z.InspectionMethod != InspectionMethodEnum.mianjian && z.IsQuality == YesOrNot.Y && z.CreatedTime > start && z.CreatedTime < end).CountAsync();
                if (SunNum > 0)
                {
                    InspectNum++;
                }
            }

            receNum.InspectNum = InspectNum;
            receNum.NotInspectNum = NotInspectNum;
            #endregion

            #region 查询合格/不合格单数
            //查询合格的数量
            var ConsentNum = 0;
            var Consent = await _collectDeliveryDetailsRep
                .Where(z => z.InspectionMethod == InspectionMethodEnum.mianjian ||( z.IsQuality == YesOrNot.Y 
                && (z.QualityResult == QualityEnum.HeGe || z.QualityResult == QualityEnum.RangBu )) 
                && z.CreatedTime > start && z.CreatedTime < end).ToListAsync();

            foreach (var item in Consent)
            {
                ConsentNum += item.QualityNumber;
            }

            //查询不合格的数量
            var NotConsentNum = 0;
            var NotConsent = await _collectDeliveryDetailsRep
                .Where(z => z.InspectionMethod != InspectionMethodEnum.mianjian && z.IsQuality == YesOrNot.Y 
                && z.QualityResult != QualityEnum.HeGe && z.QualityResult != QualityEnum.RangBu 
                && z.CreatedTime > start && z.CreatedTime < end).ToListAsync();

            foreach (var item in NotConsent)
            {
                NotConsentNum += item.QualityNumber;
            }

            receNum.ConsentNum = ConsentNum;
            receNum.NotConsentNum = NotConsentNum;
            #endregion

            ScreenRece.receNum = receNum;

            #region 查询今日收货信息
            //查询待质检物料信息
            var Inspection = await _collectDeliveryDetailsRep.DetachedEntities
                .Where(z => z.InspectionMethod != InspectionMethodEnum.mianjian && z.IsQuality == YesOrNot.N
                 && z.CreatedTime > start && z.CreatedTime < end).ProjectToType<CollectDeliveryDetails>().ToListAsync();

            var ListAwaitInformation = new List<AwaitInformation>();

            //循环赋值待质检物料信息
            foreach (var item in Inspection)
            {
                var awaitInformation = new AwaitInformation()
                {
                    Name = item.Name,
                    CollectId = item.CollectDelivery.DeliveryNo,
                    ProjectCode = item.ProjectCode,
                    Quantity = item.CollectQuantity,
                    Result = "待检",
                    CreatedTime = item.CreatedTime
                };

                ListAwaitInformation.Add(awaitInformation);
            }

            //查询质检不合格物料信息
            var Unqualified = await _collectDeliveryDetailsRep.DetachedEntities
                .Where(z => z.InspectionMethod != InspectionMethodEnum.mianjian && z.IsQuality == YesOrNot.Y &&
                z.QualityResult != QualityEnum.HeGe && z.QualityResult != QualityEnum.RangBu
                && z.CreatedTime > start && z.CreatedTime < end).ToListAsync();

            var ListNotAwait = new List<AwaitInformation>();

            //循环赋值待质检物料信息
            foreach (var item in Unqualified)
            {
                var awaitInformation = new AwaitInformation()
                {
                    Name = item.Name,
                    CollectId = item.CollectDeliveryId,
                    ProjectCode = item.ProjectCode,
                    Quantity = item.QualityNumber,
                    Result = "不合格",
                    CreatedTime = item.CreatedTime
                };

                ListNotAwait.Add(awaitInformation);
            }
            #endregion

            ScreenRece.awaitInformation = ListAwaitInformation;
            ScreenRece.notInspection = ListNotAwait;
            ScreenRece.SumNumber = Inspection.Count + Unqualified.Count;
            return ScreenRece;
        }

        /// <summary>
        ///  送检验区
        /// </summary>
        /// <returns></returns>
        [HttpGet("DeliverGoods")]
        [AllowAnonymous]
        public async Task<ScreenReceOutput> DeliverGoods()
        {
            var ScreenRece = new ScreenReceOutput();

            ReceNum receNum = new ReceNum()
            {
                DeliveryNum = 1,
                NotDeliveryNum = 7,
                InspectNum = 8,
                NotInspectNum = 3,
                ConsentNum = 126,
                NotConsentNum = 37,
            };

            ScreenRece.SumNumber = 8;
            ScreenRece.awaitInformation = awaitInformation1();
            ScreenRece.notInspection = awaitInformation2();

            ScreenRece.receNum = receNum;

            return ScreenRece;
        }

        /// <summary>
        /// 今日发货信息
        /// </summary>
        /// <returns></returns>
        public List<AwaitInformation> awaitInformation1()
        {
            var ScreenRece = new List<AwaitInformation>();
            var Information1 = new AwaitInformation {Name = "托盘", CollectId = "20230308002", ProjectCode = "LA21010", Quantity = 99, Result = "已发", CreatedTime = DateTime.Now.AddHours(-1) };
            var Information2 = new AwaitInformation {Name = "螺丝", CollectId = "20230309005", ProjectCode = "LA21038", Quantity = 38, Result = "已发", CreatedTime = DateTime.Now.AddHours(-2) };
            var Information3 = new AwaitInformation {Name = "线圈", CollectId = "20230309004", ProjectCode = "LA20006", Quantity = 67, Result = "已发", CreatedTime = DateTime.Now.AddHours(-1) };
            var Information4 = new AwaitInformation {Name = "电阻", CollectId = "20230309003", ProjectCode = "LA20002", Quantity = 54, Result = "待检", CreatedTime = DateTime.Now.AddHours(-2) };
            var Information5 = new AwaitInformation {Name = "扳手", CollectId = "20230307010", ProjectCode = "LA18002", Quantity = 65, Result = "待检", CreatedTime = DateTime.Now.AddHours(-3) };
            var Information6 = new AwaitInformation { Name = "电缆", CollectId = "20230307011", ProjectCode = "GI21002", Quantity = 38, Result = "待检", CreatedTime = DateTime.Now.AddHours(-4) };
            var Information7 = new AwaitInformation { Name = "铝管", CollectId = "20230307007", ProjectCode = "AT21018", Quantity = 67, Result = "待检", CreatedTime = DateTime.Now.AddHours(-2) };
            var Information8 = new AwaitInformation { Name = "控制线", CollectId = "20230307008", ProjectCode = "AT21018", Quantity = 54, Result = "待检", CreatedTime = DateTime.Now.AddHours(-1) };
            var Information9 = new AwaitInformation { Name = "气接头", CollectId = "20230307009", ProjectCode = "MA22808", Quantity = 65, Result = "待检", CreatedTime = DateTime.Now.AddHours(-2) };
            ScreenRece.Add(Information1);
            ScreenRece.Add(Information2);
            ScreenRece.Add(Information3);
            ScreenRece.Add(Information4);
            ScreenRece.Add(Information5);
            ScreenRece.Add(Information6);
            ScreenRece.Add(Information7);
            ScreenRece.Add(Information8);
            ScreenRece.Add(Information9);
            return ScreenRece;

        }

        /// <summary>
        /// 今日不合格信息
        /// </summary>
        /// <returns></returns>
        public List<AwaitInformation> awaitInformation2()
        {
            var ScreenRece = new List<AwaitInformation>();
            var Information1 = new AwaitInformation { Name = "托盘", CollectId = "20230307009", ProjectCode = "LA21086", Quantity = 10, Result = "不合格", CreatedTime = DateTime.Now.AddHours(-2) };
            var Information2 = new AwaitInformation { Name = "螺丝", CollectId = "20230307008", ProjectCode = "LA21234", Quantity = 21, Result = "不合格", CreatedTime = DateTime.Now.AddHours(-4) };
            ScreenRece.Add(Information1);
            ScreenRece.Add(Information2);

            return ScreenRece;

        }
    }
}
