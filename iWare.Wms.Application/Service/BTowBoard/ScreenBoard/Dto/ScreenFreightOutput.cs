﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 收货,送货数据输出参
    /// </summary>
    public class ScreenFreightOutput
    {
        /// <summary>
        /// 收货/发货检验输出参
        /// </summary>
        public class ScreenReceOutput
        {
            /// <summary>
            /// 单据数量
            /// </summary>
            public ReceNum receNum { get; set; }

            /// <summary>
            /// 待检信息
            /// </summary>
            public List<AwaitInformation> awaitInformation { get; set; }

            /// <summary>
            /// 质检不合格信息
            /// </summary>
            public List<AwaitInformation> notInspection { get; set; }

            /// <summary>
            /// 信息总数
            /// </summary>
            public object SumNumber { get; set; }

        }

        /// <summary>
        /// 收货/发货单据数量
        /// </summary>
        public class ReceNum
        {
            /// <summary>
            /// 今日已收货/发货数
            /// </summary>
            public object DeliveryNum { get; set; }

            /// <summary>
            /// 剩余待收/待发数
            /// </summary>
            public object NotDeliveryNum { get; set; }

            /// <summary>
            /// 今日已检数
            /// </summary>
            public object InspectNum { get; set; }

            /// <summary>
            /// 剩余待检数
            /// </summary>
            public object NotInspectNum { get; set; }

            /// <summary>
            /// 今日合格数
            /// </summary>
            public object ConsentNum { get; set; }

            /// <summary>
            /// 今日不合格数
            /// </summary>
            public object NotConsentNum { get; set; }
        }

        /// <summary>
        /// 收货/发货待检信息
        /// </summary>
        public class AwaitInformation
        {
            /// <summary>
            /// 物料名称
            /// </summary>
            public object Name { get; set; }

            /// <summary>
            /// 单据号
            /// </summary>
            public object CollectId { get; set; }

            /// <summary>
            /// 项目号
            /// </summary>
            public object ProjectCode { get; set; }

            /// <summary>
            /// 数量
            /// </summary>
            public object Quantity { get; set; }

            /// <summary>
            /// 状态
            /// </summary>
            public object Result { get; set; }

            /// <summary>
            /// 收货/发货时间
            /// </summary>
            public DateTimeOffset? CreatedTime { get; set; }
        }
    }
}
