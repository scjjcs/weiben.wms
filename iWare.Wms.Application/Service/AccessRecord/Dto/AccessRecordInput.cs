﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 出入明细输入参数
    /// </summary>
    public class AccessRecordInput : PageInputBase
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 任务方式-手动_1、自动_2
        /// </summary>
        public TaskModel TaskModel { get; set; } = TaskModel.ALL;

        /// <summary>
        /// 任务类型-入库_1、出库_2、移库_3
        /// </summary>
        public TaskType TaskType { get; set; } = TaskType.All;

        /// <summary>
        /// 状态-未执行_0、1-已暂停、执行中_2、已完成_3
        /// </summary>
        public TaskStatusEnum TaskStatus { get; set; } = TaskStatusEnum.All;

        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务表对应的容器编号
        /// </summary>
        public string TaskContainerCode { get; set; }

        ///<summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号    
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public virtual string TaskCreatedUserName { get; set; }
    }

    /// <summary>
    /// 打印和导出出入明细输入参数
    /// </summary>
    public class AccessRecordListInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 任务方式-手动_1、自动_2
        /// </summary>
        public TaskModel? TaskModel { get; set; }

        /// <summary>
        /// 任务类型-入库_1、出库_2、移库_3
        /// </summary>
        public TaskType? TaskType { get; set; }

        /// <summary>
        /// 状态-未执行_0、1-已暂停、执行中_2、已完成_3
        /// </summary>
        public TaskStatusEnum? TaskStatus { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务表对应的容器编号
        /// </summary>
        public string TaskContainerCode { get; set; }

        ///<summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号    
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 搜索开始时间
        /// </summary>
        public string SearchBeginTime { get; set; }

        /// <summary>
        /// 搜索结束时间
        /// </summary>
        public string SearchEndTime { get; set; }
    }
}
