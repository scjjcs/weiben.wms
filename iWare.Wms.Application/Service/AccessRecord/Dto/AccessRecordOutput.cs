﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 出入库明细输出参数
    /// </summary>
    public class AccessRecordOutput
    {
        /// <summary>
        /// 组盘单据
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 容器编码
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 任务号  
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务方式-手动_1、自动_2
        /// </summary>
        public TaskModel? TaskModel { get; set; }

        /// <summary>
        /// 任务类型-入库_1、出库_2、移库_3
        /// </summary>
        public TaskType? TaskType { get; set; }

        /// <summary>
        /// 任务状态-未执行_0、1-已暂停、执行中_2、已完成_3
        /// </summary>
        public TaskStatusEnum? TaskStatus { get; set; } = TaskStatusEnum.NotProgress;

        /// <summary>
        /// 任务名称   
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 来源   
        /// </summary>
        public string FromLocationCode { get; set; }

        /// <summary>
        /// 目标   
        /// </summary>
        public string ToLocationCode { get; set; }

        /// <summary>
        /// 任务等级    
        /// </summary>
        public int? Priority { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号    
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名    
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 操作数量
        /// </summary>
        public decimal? BindQuantity { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 任务表对应的容器编号
        /// </summary>
        public string TaskContainerCode { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string TaskCreatedUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? TaskCreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? TaskUpdatedTime { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 采购类型名称
        /// </summary>
        public string PurchaseTypeName { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public string ReservoirArea { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        public string Weight { get; set; }
    }
}
