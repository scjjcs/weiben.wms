﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 登录用户角色参数
    /// </summary>
    public class RoleOutput
    {
        /// <summary>
        /// 角色类型-集团角色_0、加盟商角色_1、门店角色_2
        /// </summary>
        public RoleTypeEnum RoleType { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 角色表
    /// </summary>
    public class SysRoleOutputs : DEntityBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 数据范围类型（字典 1全部数据 2本部门及以下数据 3本部门数据 4仅本人数据 5自定义数据）
        /// </summary>
        public DataScopeType DataScopeType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 角色类型-集团角色_0、加盟商角色_1、门店角色_2
        /// </summary>
        public RoleTypeEnum RoleType { get; set; }

        ///// <summary>
        ///// 多对多（用户）
        ///// </summary>
        //public ICollection<SysUser> SysUsers { get; set; }

        ///// <summary>
        ///// 多对多中间表（用户角色）
        ///// </summary>
        //public List<SysUserRole> SysUserRoles { get; set; }

        ///// <summary>
        ///// 多对多（机构）
        ///// </summary>
        //public ICollection<SysOrg> SysOrgs { get; set; }

        ///// <summary>
        ///// 多对多中间表（角色-机构 数据范围）
        ///// </summary>
        //public List<SysRoleDataScope> SysRoleDataScopes { get; set; }

        ///// <summary>
        ///// 多对多（菜单）
        ///// </summary>
        //public ICollection<SysMenu> SysMenus { get; set; }

        ///// <summary>
        ///// 多对多中间表（角色-菜单）
        ///// </summary>
        //public List<SysRoleMenu> SysRoleMenus { get; set; }
        /// <summary>
        /// 角色数量
        /// </summary>
        public int? RoleSums { get; set; } = 0;

    }
}