﻿namespace iWare.Wms.Application.Service.System.LowCode.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class ContrasOutput
    {
        /// <summary>
        /// 
        /// </summary>
        public List<ContrastLowCode_Database> Add { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<ContrastLowCode_Database> Del { get; set; }
    }
}