﻿using iWare.Wms.Core.Util.LowCode.Front.Model;
using Furion.Extras.iWare.Wms.Service.LowCode.Dto;

namespace iWare.Wms.Application.Service.System.LowCode.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class Front_CodeGenerate
    {
        /// <summary>
        /// 
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TableDesc { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BusName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NameSpace { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ProName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CamelizeClassName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<CodeGenConfig> QueryWhetherList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<CodeGenConfig> TableField { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<CodeGenConfig> FileTableField { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<GenEntity_Field> Fields { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? LowCodeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FormDesign { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DynamicData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Front_Dynamic> DynamicLoad_Dict { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsFile { get; set; }
    }
}