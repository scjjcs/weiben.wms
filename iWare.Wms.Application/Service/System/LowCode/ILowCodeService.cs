﻿using iWare.Wms.Application.Service.System.LowCode.Dto;
using iWare.Wms.Core;
using Furion.Extras.iWare.Wms.Entity;
using Furion.Extras.iWare.Wms.Service.LowCode.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Furion.Extras.iWare.Wms.Service.LowCode
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILowCodeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Add(AddLowCodeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        Task Delete(List<DeleteLowCodeInput> inputs);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<SysLowCode>> QueryPageList([FromQuery] LowCodePageInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Update(UpdateLowCodeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contrast"></param>
        /// <returns></returns>
        ContrasOutput Contrast(ContrastLowCode contrast);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> RunLocal(long id);
    }
}