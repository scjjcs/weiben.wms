﻿using Furion.Extras.iWare.Wms.Service.LowCode.Dto;
using System.Diagnostics.CodeAnalysis;

namespace Furion.Extras.iWare.Wms.Service.LowCode
{
    /// <summary>
    /// 
    /// </summary>
    public class GenEntityComparer : IEqualityComparer<GenEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Equals(GenEntity x, GenEntity y)
        {
            bool result = true;
            if (x == null && y == null)
            {
                result = true;
            }
            else if (x == null ^ y == null)
            {
                result = false;
            }
            else
            {
                result = (x.TableDesc == y.TableDesc) &&
                    (x.ClassName == y.ClassName) &&
                    (x.TableName == y.TableName) &&
                    (x.NameSpace == y.NameSpace);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int GetHashCode([DisallowNull] GenEntity obj)
        {
            return obj == null ? 0 : obj.ToString().GetHashCode();
        }
    }
}