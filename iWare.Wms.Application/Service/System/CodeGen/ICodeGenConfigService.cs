﻿using iWare.Wms.Core;
using Microsoft.AspNetCore.Mvc;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICodeGenConfigService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Add(CodeGenConfig input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableColumnOuputList"></param>
        /// <param name="codeGenerate"></param>
        /// <returns></returns>
        Task DelAndAddList(List<TableColumnOuput> tableColumnOuputList, SysCodeGen codeGenerate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codeGenId"></param>
        /// <returns></returns>
        Task Delete(long codeGenId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<SysCodeGenConfig> Detail([FromQuery] CodeGenConfig input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<List<CodeGenConfig>> List([FromQuery] CodeGenConfig input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputList"></param>
        /// <returns></returns>
        Task Update(List<CodeGenConfig> inputList);
    }
}