using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 访问日志服务
    /// </summary>
    [ApiDescriptionSettings(Name = "VisLog", Order = 100)]
    [Route("api")]
    public class SysVisLogService : ISysVisLogService, IDynamicApiController, ITransient
    {
        private readonly IRepository<SysLogVis> _sysVisLogRep;  // 访问日志表仓储
        private readonly IRepository<SysLogOp> _sysOpLogRep; // 操作日志表仓储
        private readonly IRepository<SysLogEx> _sysExLogRep;  // 操作日志表仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="sysVisLogRep"></param>
        /// <param name="sysOpLogRep"></param>
        /// <param name="sysExLogRep"></param>
        public SysVisLogService(
            IRepository<SysLogVis> sysVisLogRep,
            IRepository<SysLogOp> sysOpLogRep,
            IRepository<SysLogEx> sysExLogRep)
        {
            _sysVisLogRep = sysVisLogRep;
            _sysOpLogRep = sysOpLogRep;
            _sysExLogRep = sysExLogRep;
        }
        #endregion

        /// <summary>
        /// 分页查询访问日志
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("sysVisLog/page")]
        public async Task<PageResult<VisLogOutput>> QueryVisLogPageList([FromQuery] VisLogPageInput input)
        {
            var name = !string.IsNullOrEmpty(input.Name?.Trim());
            var success = !string.IsNullOrEmpty(input.Success.ToString());
            var searchBeginTime = !string.IsNullOrEmpty(input.SearchBeginTime?.Trim());

            return await _sysVisLogRep.DetachedEntities.Where((name, u => EF.Functions.Like(u.Name, $"%{input.Name.Trim()}%")))
                                                       .Where(input.VisType >= 0, u => u.VisType == input.VisType)
                                                       .Where(success, u => u.Success == input.Success)
                                                       .Where(searchBeginTime, u => u.VisTime >= DateTime.Parse(input.SearchBeginTime.Trim())
                                                                                                            && u.VisTime <= DateTime.Parse(input.SearchEndTime.Trim()))
                                                       .OrderByDescending(u => u.Id)
                                                       .ProjectToType<VisLogOutput>()
                                                       .ToADPagedListAsync(input.PageNo, input.PageSize);
        }

        /// <summary>
        /// 清空访问日志
        /// </summary>
        /// <returns></returns>
        [HttpPost("sysVisLog/delete")]
        public async Task ClearVisLog()
        {
            await _sysVisLogRep.Context.DeleteRangeAsync<SysLogVis>();
        }

        /// <summary>
        /// 清空访问日志、操作日志、异常日志
        /// </summary>
        /// <returns></returns>
        [HttpPost("sysVisLog/clearAllLog")]
        [AllowAnonymous]
        public async Task ClearAllLog()
        {
            //访问日志
            await _sysVisLogRep.Context.DeleteRangeAsync<SysLogVis>();
            //操作日志
            await _sysOpLogRep.Context.DeleteRangeAsync<SysLogOp>();
            //异常日志
            await _sysExLogRep.Context.DeleteRangeAsync<SysLogEx>();
        }
    }
}