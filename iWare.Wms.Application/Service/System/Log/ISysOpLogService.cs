﻿using iWare.Wms.Core;
using Microsoft.AspNetCore.Mvc;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysOpLogService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task ClearOpLog();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<OpLogOutput>> QueryOpLogPageList([FromQuery] OpLogPageInput input);
    }
}