﻿using iWare.Wms.Core;
using Microsoft.AspNetCore.Mvc;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysVisLogService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task ClearVisLog();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<VisLogOutput>> QueryVisLogPageList([FromQuery] VisLogPageInput input);
    }
}