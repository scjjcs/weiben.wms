﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface IClickWordCaptcha
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ClickWordCaptchaResult> CheckCode(ClickWordCaptchaInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        Task<ClickWordCaptchaResult> CreateCaptchaImage(string code, int width, int height, int point = 3);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        string RandomCode(int number);

        /// <summary>
        /// 产生验证码
        /// </summary>
        /// <param name="number">验证码长度</param>
        /// <returns></returns>
        string CreateCode(int number = 4);
    }
}