﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 用户参数
    /// </summary>
    public class UserOutput
    {
        #region 新增20220714
        /// <summary>
        /// 角色信息
        /// </summary>
        public string SysRoleInfo { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public AdminType AdminType { get; set; }
        #endregion

        /// <summary>
        /// Id
        /// </summary>
        public virtual string Id { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public virtual string Account { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public virtual string NickName { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public virtual string Avatar { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public virtual DateTimeOffset? Birthday { get; set; }

        /// <summary>
        /// 性别-男_1、女_2
        /// </summary>
        public virtual int Sex { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public virtual string Tel { get; set; }

        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public virtual int Status { get; set; }

        /// <summary>
        /// 员工信息
        /// </summary>
        public EmpOutput SysEmpInfo { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset CreatedTime { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UpdateUserOutput 
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 性别-男_1、女_2
        /// </summary>
        public int Sex { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// 员工信息
        /// </summary>
        public EmpOutput2 SysEmpParam { get; set; } = new EmpOutput2();

        /// <summary>
        /// 管理员类型-超级管理员_1、管理员_2、普通账号_3、供应商_4
        /// </summary>
        public AdminType AdminType { get; set; }

        /// <summary>
        /// 状态 正常_0、停用_1、删除_2
        /// </summary>
        public int Status { get; set; }
    }
}