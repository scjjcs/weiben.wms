using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DataEncryption;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using iWare.Wms.Core;
using Magicodes.ExporterAndImporter.Excel;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Yitter.IdGenerator;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 用户服务
    /// </summary>
    [ApiDescriptionSettings(Name = "User", Order = 150)]
    [Route("api")]
    public class SysUserService : ISysUserService, IDynamicApiController, ITransient
    {
        private readonly IRepository<SysUser> _sysUserRep;  // 用户表仓储
        private readonly ISysCacheService _sysCacheService;
        private readonly ISysEmpService _sysEmpService;
        private readonly ISysUserDataScopeService _sysUserDataScopeService;
        private readonly ISysUserRoleService _sysUserRoleService;
        private readonly ISysOrgService _sysOrgService;

        #region 新增 20220714
        private readonly ISysRoleService _sysRoleService;
        private readonly IRepository<SysUserRole> _sysUserRoleRep;  // 用户权限表仓储
        #endregion

        #region 新增 20221108
        private readonly IRepository<SupplierInfo> _supplierInfoRep;
        #endregion

        #region 新增 20221125
        private readonly IGeneralCaptcha _generalCaptchaService;
        #endregion

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="sysUserRep"></param>
        /// <param name="sysCacheService"></param>
        /// <param name="sysEmpService"></param>
        /// <param name="sysUserDataScopeService"></param>
        /// <param name="sysUserRoleService"></param>
        /// <param name="sysOrgService"></param>
        /// <param name="sysRoleService"></param>
        /// <param name="sysUserRoleRep"></param>
        /// <param name="supplierInfoRep"></param>
        /// <param name="generalCaptchaService"></param>
        public SysUserService(IRepository<SysUser> sysUserRep,
                              ISysCacheService sysCacheService,
                              ISysEmpService sysEmpService,
                              ISysUserDataScopeService sysUserDataScopeService,
                              ISysUserRoleService sysUserRoleService,
                              ISysOrgService sysOrgService,
                              ISysRoleService sysRoleService,
                              IRepository<SysUserRole> sysUserRoleRep,
                              IRepository<SupplierInfo> supplierInfoRep,
                              IGeneralCaptcha generalCaptchaService)
        {
            _sysUserRep = sysUserRep;
            _sysCacheService = sysCacheService;
            _sysEmpService = sysEmpService;
            _sysUserDataScopeService = sysUserDataScopeService;
            _sysUserRoleService = sysUserRoleService;
            _sysOrgService = sysOrgService;
            _sysRoleService = sysRoleService;
            _sysUserRoleRep = sysUserRoleRep;
            _supplierInfoRep = supplierInfoRep;
            _generalCaptchaService = generalCaptchaService;
        }
        #endregion

        /// <summary>
        /// 分页查询用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("sysUser/page")]
        public async Task<PageResult<UserOutput>> QueryUserPageList([FromQuery] UserPageInput input)
        {
            var searchValue = input.SearchValue;

            #region 一直注释的
            //var pid = input.SysEmpParam.OrgId;
            //var sysEmpRep = Db.GetRepository<SysEmp>();
            //var sysOrgRep = Db.GetRepository<SysOrg>();
            //var dataScopes = await GetUserDataScopeIdList(CurrentUserInfo.UserId);
            #endregion

            var users = await _sysUserRep.DetachedEntities
                                        //.Join(sysEmpRep.DetachedEntities, u => u.Id, e => e.Id, (u, e) => new { u, e })
                                        //.Join(sysOrgRep.DetachedEntities, n => n.e.OrgId, o => o.Id, (n, o) => new { n, o })
                                        //.Where(!string.IsNullOrEmpty(searchValue), x => (x.SysUser.Account.Contains(input.SearchValue) ||
                                        //                                                                                x.SysUser.Name.Contains(input.SearchValue) ||
                                        //                                                                                x.SysUser.Phone.Contains(input.SearchValue)))
                                        //.Where(!string.IsNullOrEmpty(pid), x => (x.n.e.OrgId == long.Parse(pid) ||
                                        //                                   x.o.Pids.Contains($"[{pid.Trim()}]")))
                                        //.Where(input.SearchStatus >= 0, x => x.Status == input.SearchStatus)

                                        .Where(!string.IsNullOrEmpty(input.Account), x => x.Account.Contains(input.Account))
                                        .Where(!string.IsNullOrEmpty(input.Name), x => x.Name.Contains(input.Name))
                                        .Where(!string.IsNullOrEmpty(input.NickName), x => x.NickName.Contains(input.NickName))
                                        .Where(input.AdminType != null && input.AdminType != AdminType.All, x => x.AdminType != AdminType.SuperAdmin
                                                                                                                                       && x.AdminType == input.AdminType)//排除超级管理员
                                        .Where(input.AdminType == AdminType.All || input.AdminType == null, x => x.AdminType != AdminType.SuperAdmin)
                                        .Where(input.Status != null, x => x.Status == input.Status)
                                        .OrderByDescending(t => t.CreatedTime)
                                        .Select(u => u.Adapt<UserOutput>())
                                        .ToADPagedListAsync(input.PageNo, input.PageSize);

            // 定义用户集合
            var userOutputList = new List<UserOutput>();

            //获取角色信息
            if (input.SysRoleName != null)
            {
                var sysUserRoleList = await _sysUserRoleRep.Where(w => w.SysRole.Name == input.SysRoleName)
                                                                         .ProjectToType<SysUserRole>()
                                                                         .ToListAsync();

                foreach (var userRole in sysUserRoleList)
                {
                    foreach (var user in users.Rows)
                    {
                        if (userRole.SysUserId == long.Parse(user.Id))
                        {
                            var UserRole = (await _sysRoleService.GetUserRoleList(long.Parse(user.Id))).Select(w => w.Name).ToList();

                            var userOutputModel = user.Adapt<UserOutput>();
                            if (UserRole.Count > 0) userOutputModel.SysRoleInfo = string.Join(",", UserRole);
                            userOutputList.Add(userOutputModel);
                        }
                    }
                }
            }
            else
            {
                foreach (var user in users.Rows)
                {
                    var UserRole = (await _sysRoleService.GetUserRoleList(long.Parse(user.Id))).Select(w => w.Name).ToList();
                    if (UserRole.Count > 0) user.SysRoleInfo = string.Join(",", UserRole);
                }
            }

            if (input.SysRoleName != null) return userOutputList.OrderByDescending(t => t.CreatedTime)
                                                                .ToADPagedList(input.PageNo, input.PageSize);
            else return users;

            #region 一直注释的
            //foreach (var user in users.Rows)
            //{
            //    user.SysEmpInfo = await _sysEmpService.GetEmpInfo(long.Parse(user.Id));
            //}
            #endregion

            //return users;
        }

        /// <summary>
        /// 增加用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/add")]
        public async Task AddUser(AddUserInput input)
        {
            // 数据范围检查
            //CheckDataScope(input.SysEmpParam.OrgId);

            var isExist = await _sysUserRep.AnyAsync(u => u.Account == input.Account && !u.IsDeleted, false, true);
            if (isExist) throw Oops.Oh(ErrorCode.D1003);

            var user = input.Adapt<SysUser>();
            user.Password = MD5Encryption.Encrypt(input.Password);
            if (string.IsNullOrEmpty(user.Name))
                user.Name = user.Name;
            if (string.IsNullOrEmpty(user.NickName))
                user.NickName = user.NickName;
            await _sysUserRep.InsertNowAsync(user, true);
            //input.SysEmpParam.Id = newUser.Entity.Id.ToString();
            // 增加员工信息
            //await _sysEmpService.AddOrUpdate(input.SysEmpParam);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/delete")]
        public async Task DeleteUser(DeleteUserInput input)
        {
            if (input == null || input.Id == null || input.Id.Count == 0)
            {
                throw Oops.Oh(ErrorCode.D4008);
            }
            int length = input.Id.Count;
            for (int i = 0; i < length; i++)
            {
                long Ids = input.Id[i];
                // 数据范围检查
                //var orgId = await _sysEmpService.GetEmpOrgId(Ids);
                //CheckDataScope(orgId.ToString());
                var user = (_sysUserRep.SqlQuery<SysUser>(@$"  SELECT * FROM dbo.sys_user WHERE Id = '{Ids}'"))?.FirstOrDefault();
                if (user == null)
                    throw Oops.Oh(ErrorCode.D1002);

                if (user.AdminType == AdminType.SuperAdmin)
                    throw Oops.Oh(ErrorCode.D1014);

                //if (user.AdminType == AdminType.Admin)
                //    throw Oops.Oh(ErrorCode.D1018);

                if (user.Id == CurrentUserInfo.UserId)
                    throw Oops.Oh(ErrorCode.D1001);

                // 直接删除用户
                await user.DeleteAsync();

                // 删除员工及附属机构职位信息
                //await _sysEmpService.DeleteEmpInfoByUserId(Ids);//empId与userId相同

                //删除该用户对应的用户-角色表关联信息
                await _sysUserRoleService.DeleteUserRoleListByUserId(Ids);

                //删除该用户对应的用户-数据范围表关联信息
                //await _sysUserDataScopeService.DeleteUserDataScopeListByUserId(Ids);
            }
        }

        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/edit")]
        public async Task UpdateUser(UpdateUserInput input)
        {
            //// 数据范围检查
            //CheckDataScope(input.SysEmpParam.OrgId);

            // 排除自己并且判断与其他是否相同
            var sysUser = await _sysUserRep
                .FirstOrDefaultAsync(u => u.Account == input.Account && u.Id == input.Id);
            if (sysUser == null) throw Oops.Oh(ErrorCode.D1002);

            if (input.AdminType.Equals("管理员") || input.AdminType.Equals("2"))
                sysUser.AdminType = AdminType.Admin;
            else if (input.AdminType.Equals("普通账号") || input.AdminType.Equals("3"))
                sysUser.AdminType = AdminType.None;
            else if (input.AdminType.Equals("供应商") || input.AdminType.Equals("4"))
                sysUser.AdminType = AdminType.Supplier;
            else sysUser.AdminType = AdminType.SuperAdmin;

            //var user = sysUser.Adapt<SysUser>();
            sysUser.Name = input.Name;
            sysUser.NickName = input.NickName;
            sysUser.Email = input.Email;
            sysUser.Phone = input.Phone;
            if (input.Status == 0) sysUser.Status = CommonStatus.ENABLE;
            else if (input.Status == 1) sysUser.Status = CommonStatus.DISABLE;
            else sysUser.Status = CommonStatus.DELETED;
            await sysUser.UpdateExcludeAsync(new[] { nameof(SysUser.Password) }, true);

            //input.SysEmpParam.Id = user.Id.ToString();
            //// 更新员工及附属机构职位信息
            //await _sysEmpService.AddOrUpdate(input.SysEmpParam);
        }

        /// <summary>
        /// 查看用户
        /// </summary>
        /// <returns></returns>
        [HttpGet("sysUser/detail")]
        public async Task<UserOutput> GetUser(long id)
        {
            var user = await _sysUserRep.DetachedEntities.FirstOrDefaultAsync(u => u.Id == id);
            var userDto = user.Adapt<UserOutput>();
            if (userDto != null)
            {
                userDto.SysEmpInfo = await _sysEmpService.GetEmpInfo(user.Id);
            }
            return userDto;
        }

        /// <summary>
        /// 修改用户状态
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/changeStatus")]
        public async Task ChangeUserStatus(UpdateUserStatusInput input)
        {
            var user = await _sysUserRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            if (user.AdminType == AdminType.SuperAdmin)
                throw Oops.Oh(ErrorCode.D1015);

            if (!Enum.IsDefined(typeof(CommonStatus), input.Status))
                throw Oops.Oh(ErrorCode.D3005);
            user.Status = input.Status;
        }

        /// <summary>
        /// 授权用户角色
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/grantRole")]
        public async Task GrantUserRole(UpdateUserRoleDataInput input)
        {
            var user = await _sysUserRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            if (user.AdminType == AdminType.SuperAdmin)
                throw Oops.Oh(ErrorCode.D1022);

            //if (user.AdminType == AdminType.Admin)
            //    throw Oops.Oh(ErrorCode.D1008);

            // 数据范围检查
            CheckDataScope(input.SysEmpParam.OrgId);
            await _sysUserRoleService.GrantRole(input);
        }

        /// <summary>
        /// 授权用户数据范围
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/grantData")]
        public async Task GrantUserData(UpdateUserRoleDataInput input)
        {
            // 清除缓存
            await _sysCacheService.RemoveAsync(CommonConst.CACHE_KEY_DATASCOPE + $"{input.Id}");

            // 数据范围检查
            CheckDataScope(input.SysEmpParam.OrgId);
            await _sysUserDataScopeService.GrantData(input);
        }

        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/updateInfo")]
        public async Task UpdateUserInfo(UpdateUserBaseInfoInput input)
        {
            var user = input.Adapt<SysUser>();
            await user.UpdateExcludeAsync(new[] { nameof(SysUser.AdminType), nameof(SysUser.LastLoginIp), nameof(SysUser.LastLoginTime) });
        }

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/updatePwd")]
        public async Task UpdateUserPwd(ChangePasswordUserInput input)
        {
            var user = await _sysUserRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            if (MD5Encryption.Encrypt(input.Password) != user.Password)
                throw Oops.Oh(ErrorCode.D1004);
            if (MD5Encryption.Encrypt(input.NewPassword) != MD5Encryption.Encrypt(input.ConfirmPassword))
                throw Oops.Oh(ErrorCode.D4009);
            user.Password = MD5Encryption.Encrypt(input.NewPassword);
        }

        /// <summary>
        /// 获取用户拥有角色
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("sysUser/ownRole")]
        public async Task<List<long>> GetUserOwnRole([FromQuery] QueryUserInput input)
        {
            return await _sysUserRoleService.GetUserRoleIdList(input.Id);
        }

        /// <summary>
        /// 获取用户拥有数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("sysUser/ownData")]
        public async Task<List<long>> GetUserOwnData([FromQuery] QueryUserInput input)
        {
            return await _sysUserDataScopeService.GetUserDataScopeIdList(input.Id);
        }

        /// <summary>
        /// 重置用户密码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/resetPwd")]
        public async Task ResetUserPwd(QueryUserInput input)
        {
            var user = await _sysUserRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            user.Password = MD5Encryption.Encrypt(CommonConst.DEFAULT_PASSWORD);
        }

        /// <summary>
        /// 修改用户头像
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("sysUser/updateAvatar")]
        public async Task UpdateAvatar(UploadAvatarInput input)
        {
            var user = await _sysUserRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            user.Avatar = input.Avatar.ToString();
        }

        /// <summary>
        /// 获取用户选择器
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous] //公告中需要使用，开放权限
        [HttpGet("sysUser/selector")]
        public async Task<List<UserOutput>> GetUserSelector([FromQuery] UserSelectorInput input)
        {
            var name = !string.IsNullOrEmpty(input.Name?.Trim());
            var result = await _sysUserRep.DetachedEntities
                                    .Where(name, u => EF.Functions.Like(u.Name, $"%{input.Name.Trim()}%"))
                                    .Where(u => u.Status != CommonStatus.DELETED)
                                    .Where(u => u.AdminType != AdminType.SuperAdmin)
                                    .ToListAsync();
            return result.Adapt<List<UserOutput>>();
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("sysUser/userList")]
        public async Task<List<UserOutput>> GetSysUserList()
        {
            var userOut = await _sysUserRep.DetachedEntities.ToListAsync();
            return userOut.Adapt<List<UserOutput>>();
        }

        /// <summary>
        /// 用户导出
        /// </summary>
        /// <returns></returns>
        [HttpGet("sysUser/export")]
        public async Task<IActionResult> ExportUser()
        {
            var users = await _sysUserRep.DetachedEntities.AsQueryable()
                                            .ProjectToType<UserOutput>()
                                            .ToListAsync();
            var exporter = new ExcelExporter();
            var result = await exporter.ExportAsByteArray(users);
            var memoryStream = new MemoryStream(result);
            return await Task.FromResult(new FileStreamResult(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                FileDownloadName = $"{DateTimeOffset.Now:yyyyMMdd_HHmmss}_user.xlsx"
            });
        }

        /// <summary>
        /// 用户导入
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("sysUser/import")]
        public async Task ImportUser(IFormFile file)
        {
            var path = Path.Combine(Path.GetTempPath(), $"{YitIdHelper.NextId()}.xlsx");
            using (var stream = File.Create(path))
            {
                await file.CopyToAsync(stream);
            }

            //var rows = MiniExcel.Query(path); // 解析
            //foreach (var row in rows)
            //{
            //    var a = row.A;
            //    var b = row.B;
            //    // 入库等操作

            //}
        }

        /// <summary>
        /// 根据用户Id获取用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [NonAction]
        public async Task<SysUser> GetUserById(long userId)
        {
            return await _sysUserRep.DetachedEntities.FirstOrDefaultAsync(u => u.Id == userId);
        }

        /// <summary>
        /// 将OAuth账号转换成账号
        /// </summary>
        /// <param name="authUser"></param>
        /// <param name="sysUser"></param>
        /// <returns></returns>
        [NonAction]
        public async Task SaveAuthUserToUser(AuthUserInput authUser, CreateUserInput sysUser)
        {
            var user = sysUser.Adapt<SysUser>();
            user.AdminType = AdminType.None; // 非管理员

            // oauth账号与系统账号判断
            var isExist = await _sysUserRep.DetachedEntities.AnyAsync(u => u.Account == authUser.Username);
            user.Account = isExist ? authUser.Username + DateTime.Now.Ticks : authUser.Username;
            user.Name = user.NickName = authUser.Nickname;
            user.Email = authUser.Email;
            user.Sex = authUser.Gender;
            await user.InsertAsync();
        }

        /// <summary>
        /// 获取用户数据范围（机构Id集合）并缓存
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [NonAction]
        public async Task<List<long>> GetUserDataScopeIdList(long userId)
        {
            var dataScopes = await _sysCacheService.GetDataScope(userId); // 先从缓存里面读取
            if (dataScopes == null || dataScopes.Count < 1)
            {
                if (!CurrentUserInfo.IsSuperAdmin)
                {
                    var orgId = await _sysEmpService.GetEmpOrgId(userId);
                    // 获取该用户对应的数据范围集合
                    var userDataScopeIdListForUser = await _sysUserDataScopeService.GetUserDataScopeIdList(userId);
                    // 获取该用户的角色对应的数据范围集合
                    var userDataScopeIdListForRole = await _sysUserRoleService.GetUserRoleDataScopeIdList(userId, orgId);
                    dataScopes = userDataScopeIdListForUser.Concat(userDataScopeIdListForRole).Distinct().ToList(); // 并集
                }
                else
                {
                    dataScopes = await _sysOrgService.GetAllDataScopeIdList();
                }
                await _sysCacheService.SetDataScope(userId, dataScopes); // 缓存结果
            }
            return dataScopes;
        }

        /// <summary>
        /// 获取用户数据范围（机构Id集合）
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public async Task<List<long>> GetUserDataScopeIdList()
        {
            var userId = CurrentUserInfo.UserId;
            var dataScopes = await GetUserDataScopeIdList(userId);
            return dataScopes;
        }

        /// <summary>
        /// 检查普通用户数据范围
        /// 当有用户有多个组织时，在登录时选择一个组织，所以组织id（orgId）从前端传过来
        /// </summary>
        /// <param name="orgId"></param>
        /// <returns></returns>
        private async void CheckDataScope(string orgId)
        {
            // 如果当前用户不是超级管理员，则进行数据范围校验
            if (!CurrentUserInfo.IsSuperAdmin)
            {
                var dataScopes = await GetUserDataScopeIdList(CurrentUserInfo.UserId);
                if (dataScopes == null || (orgId != null && !dataScopes.Any(u => u == long.Parse(orgId))))
                    throw Oops.Oh(ErrorCode.D1013);
            }
        }

        #region 新增供应商用户数据
        /// <summary>
        /// 新增供应商用户数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("addUserSupplierInfo")]
        public async Task AddUserSupplierInfo()
        {
            var list = (await _supplierInfoRep.DetachedEntities.ToListAsync());
            foreach (var item in list)
            {
                var isExist = await _sysUserRep.AnyAsync(u => u.Account == item.Code && !u.IsDeleted, false, true);
                if (!isExist)
                {
                    // 新增用户表信息
                    var user = new SysUser
                    {
                        Account = item.Code,
                        NickName = item.Code,
                        Password = "e10adc3949ba59abbe56e057f20f883e",
                        Name = item.Name,
                        AdminType = AdminType.Supplier,
                    };
                    await _sysUserRep.InsertNowAsync(user, true);

                    // 新增用户角色表信息
                    var sysUserRole = new SysUserRole
                    {
                        SysUserId = user.Id,
                        SysRoleId = 313994508521541
                    };
                    await _sysUserRoleRep.InsertNowAsync(sysUserRole);
                }
            }

            //var list = await _sysUserRep.DetachedEntities.Where(z => z.AdminType == AdminType.Supplier).ToListAsync();
            //foreach (var item in list)
            //{
            //    var userRole = await _sysUserRoleRep.FirstOrDefaultAsync(z => z.SysUserId == item.Id && z.SysRoleId == 396085390729285);
            //    if (userRole == null)
            //    {
            //        // 新增用户角色表信息
            //        var sysUserRole = new SysUserRole
            //        {
            //            SysUserId = item.Id,
            //            SysRoleId = 396085390729285
            //        };
            //        await _sysUserRoleRep.InsertNowAsync(sysUserRole);
            //    }
            //}
        }
        #endregion

        /// <summary>
        /// 生成随机验证码
        /// </summary>
        /// <returns></returns>
        [HttpGet("generateRandom")]
        [AllowAnonymous]
        public async Task<string> GenerateRandom()
        {
            return _generalCaptchaService.GenerateRandom(4);
        }
    }
}