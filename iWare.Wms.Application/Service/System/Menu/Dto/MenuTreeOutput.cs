using iWare.Wms.Core;
using System.Collections;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 菜单树---授权、新增编辑时选择
    /// </summary>
    public class MenuTreeOutput : ITreeNode
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 父Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value => IntValue.ToString();

        /// <summary>
        /// 
        /// </summary>
        public long IntValue { get; set; }

        /// <summary>
        /// 排序，越小优先级越高
        /// </summary>
        public MenuWeight Weight { get; set; }

        /// <summary>
        /// 子节点
        /// </summary>
        public List<MenuTreeOutput> Children { get; set; } = new List<MenuTreeOutput>();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public long GetId()
        {
            return Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public long GetPid()
        {
            return ParentId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="children"></param>
        public void SetChildren(IList children)
        {
            Children = (List<MenuTreeOutput>)children;
        }
    }
}