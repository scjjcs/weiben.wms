using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 上传文件参数
    /// </summary>
    public class FilePageInput : PageInputBase
    {
        /// <summary>
        /// 文件存储位置（1:阿里云，2:腾讯云，3:minio，4:本地）
        /// </summary>
        public int? FileLocation { get; set; }

        /// <summary>
        /// 文件仓库
        /// </summary>
        public string FileBucket { get; set; }

        /// <summary>
        /// 文件名称（上传时候的文件名）
        /// </summary>
        public string FileOriginName { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteFileInfoInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class QueryFileInfoInput : BaseId
    {
    }

    /// <summary>
    /// 预览文件输入参数
    /// </summary>
    public class PreviewFileInfoInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }
}