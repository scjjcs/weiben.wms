﻿using iWare.Wms.Core;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// LES送货单输入参数
    /// </summary>
    public class LESGoodsDeliveryInput : PageInputBase
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        public long DeliveryID { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货/采购类型
        /// </summary>
        public string DeliveryType { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        public string SuppName { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; } = AuditStatusEnum.all;

        /// <summary>
        /// 是否加急
        /// </summary>
        public YesOrNot IsJiaJiJian { get; set; } = YesOrNot.All;
    }

    /// <summary>
    /// 更新送货单状态
    /// </summary>
    public class EditStatusInput
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        [Required(ErrorMessage = "送货单ID不能为空")]
        public long GoodsDeliveryId { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        [Required(ErrorMessage = "送货单号不能为空")]
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        [Required(ErrorMessage = "状态不能为空")]
        public AuditStatusEnum Status { get; set; }
    }
}
