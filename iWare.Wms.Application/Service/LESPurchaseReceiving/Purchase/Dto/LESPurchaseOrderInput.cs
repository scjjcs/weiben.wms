﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// LES采购订单输入参数
    /// </summary>
    public class LESPurchaseOrderInput : PageInputBase
    {
        /// <summary>
        /// 采购单别
        /// </summary>
        public string PurchaseType { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        public string SupplierInfoName { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 状态-撤销或者删除_-1、未进行_0、进行中_1、完成_2
        /// </summary>
        public PurchaseStatusEnum Status { get; set; } = PurchaseStatusEnum.All;

        /// <summary>
        /// 是否非标件
        /// </summary>
        public YesOrNot IsJiaJiJian { get; set; } = YesOrNot.All;
    }
}
