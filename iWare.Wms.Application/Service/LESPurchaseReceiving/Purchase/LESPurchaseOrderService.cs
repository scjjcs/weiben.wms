﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// LES采购订单服务
    /// </summary>
    [Route("api/LESPurchaseOrder")]
    [ApiDescriptionSettings("LES供应商模块", Name = "LESPurchaseOrder", Order = 110)]

    public class LESPurchaseOrderService : IDynamicApiController, ITransient
    {
        private readonly IRepository<WareDictData, MasterDbContextLocator> _wareDictDataRep; //仓库字典中仓储
        private readonly IRepository<GoodsDelivery, MasterDbContextLocator> _goodsDeliveryRep; //送货单仓储
        private readonly IRepository<PurchaseOrderVsGoodsDelivery, MasterDbContextLocator> _purchaseOrderVsGoodsDeliverysRep; //采购订单与送货单关系仓储
        private readonly IRepository<V_PurchaseOrder, MasterDbContextLocator> _v_purchaseOrderRep; //采购订单试图仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="wareDictDataRep"></param>
        /// <param name="goodsDeliveryRep"></param>
        /// <param name="purchaseOrderVsGoodsDeliverysRep"></param>
        /// <param name="v_purchaseOrderRep"></param>
        public LESPurchaseOrderService(
            IRepository<WareDictData, MasterDbContextLocator> wareDictDataRep,
            IRepository<GoodsDelivery, MasterDbContextLocator> goodsDeliveryRep,
            IRepository<PurchaseOrderVsGoodsDelivery, MasterDbContextLocator> purchaseOrderVsGoodsDeliverysRep,
            IRepository<V_PurchaseOrder, MasterDbContextLocator> v_purchaseOrderRep
           )
        {
            _wareDictDataRep = wareDictDataRep;
            _goodsDeliveryRep = goodsDeliveryRep;
            _purchaseOrderVsGoodsDeliverysRep = purchaseOrderVsGoodsDeliverysRep;
            _v_purchaseOrderRep = v_purchaseOrderRep;
        }
        #endregion

        /// <summary>
        /// 分页查询LES采购订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("Page")]
        public async Task<PageResult<V_PurchaseOrder>> Page([FromQuery] LESPurchaseOrderInput input)
        {
            var purchaseOrders = await _v_purchaseOrderRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(input.PurchaseNo), u => EF.Functions.Like(u.PurchaseNo, $"%{input.PurchaseNo.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.ProjectCode), u => EF.Functions.Like(u.ProjectCode, $"%{input.ProjectCode.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.SupplierInfoName), u => EF.Functions.Like(u.SupplierInfoName, $"%{input.SupplierInfoName.Trim()}%"))
                .OrderBy(z => z.Status)
                .ProjectToType<V_PurchaseOrder>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);

            //获取送货单数量
            foreach (var item in purchaseOrders.Rows)
            {
                if (!string.IsNullOrEmpty(item.PurchaserUserId))
                {
                    var wareDictData = await _wareDictDataRep.DetachedEntities.Where(z => z.Code == item.PurchaserUserId.Trim()).FirstOrDefaultAsync();
                    item.PurchaserUserName = wareDictData == null ? "" : wareDictData.Value;
                }
                else item.PurchaserUserName = "";

                var vsList = await _purchaseOrderVsGoodsDeliverysRep.DetachedEntities.Where(p => p.PurchaseOrderId == item.Id)
                    .Select(p => p.GoodsDeliveryId).ToListAsync();
                item.DeliveryQuantityTotal = (await _goodsDeliveryRep.DetachedEntities.Where(p => vsList.Contains(p.Id))
                    .ToListAsync()).Sum(p => p.DeliveryQuantityTotal);
                item.OutQuantityTotal = item.PurchaseNumber;
            }

            return purchaseOrders;
        }
    }
}
