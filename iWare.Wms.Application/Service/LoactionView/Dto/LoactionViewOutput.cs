﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 库位试图输出参数
    /// </summary>
    public class LoactionViewOutput
    {
        /// <summary>
        /// 闲置
        /// </summary>
        public int kongxian { get; set; }

        /// <summary>
        /// 待入
        /// </summary>
        public int dairu { get; set; }

        /// <summary>
        /// 待出
        /// </summary>
        public int daichu { get; set; }

        /// <summary>
        /// 锁定
        /// </summary>
        public int locking { get; set; }

        /// <summary>
        /// 存货
        /// </summary>
        public int cunhuo { get; set; }

        /// <summary>
        /// 空托
        /// </summary>
        public int emptyContainerNum { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<WareLocationData> wareLocationData { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class WareLocationData
    {
        /// <summary>
        /// 排
        /// </summary>
        public int RowNo { get; set; }

        /// <summary>
        /// 层属性数据
        /// </summary>
        public List<WareLocationColumnNos> wareLocationColumnNos { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class WareLocationColumnNos
    {
        /// <summary>
        /// 层
        /// </summary>
        public int LayerNo { get; set; }

        /// <summary>
        /// 列属性数据
        /// </summary>
        public List<LocationDetailOutput> locationDetailOutput { get; set; }
    }
    /// <summary>
    /// 库位明细输出参数
    /// </summary>
    public class LocationDetailOutput
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 属性
        /// </summary>
        public string Attribute { get; set; }

        /// <summary>
        /// 承重 /1T  立体库都是1T
        /// </summary>
        public string Heavy { get; set; }

        /// <summary>
        /// 是否锁定
        /// </summary>
        public YesOrNot IsLock { get; set; } = YesOrNot.N;

        /// <summary>
        /// 是否空托盘
        /// </summary>
        public YesOrNot IsEmptyContainer { get; set; } = YesOrNot.N;

        /// <summary>
        /// 库位状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1
        /// </summary>
        public LocationStatusEnum? LocationStatus { get; set; }

        /// <summary>
        /// 入库时间
        /// </summary>
        public string CreatedTime { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        public int RowNo { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        public int ColumnNo { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        public int LayerNo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 库位对应的物料明细
        /// </summary>
        public List<WareContainerVsMaterial> LoactionMaterialDetails { get; set; }
    }
}
