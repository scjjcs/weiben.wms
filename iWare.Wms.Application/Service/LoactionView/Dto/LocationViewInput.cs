﻿using iWare.Wms.Core.Service;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 库位试图输入参数
    /// </summary>
    public class LocationViewInput
    {
        /// <summary>
        /// 所属厂区名称
        /// </summary>
        public string WareTwoName { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 所属排
        /// </summary>
        public int ? RowNo { get; set; }
    }

    /// <summary>
    /// 库位试图输入参数
    /// </summary>
    public class LocationRowsViewInput
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }
    }

    /// <summary>
    /// 查询库位明细
    /// </summary>
    public class LocationDetailInput : BaseId
    {
    }

    /// <summary>
    /// 置为空闲输入参
    /// </summary>
    public class LocationIdleInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
