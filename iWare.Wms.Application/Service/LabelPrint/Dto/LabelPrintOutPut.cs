﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 物料标签打印输出参数
    /// </summary>
    public class MaterialPrintOutput
    {
        /// <summary>
        /// ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 入库时间
        /// </summary>
        public DateTimeOffset? ManufactureDate { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string Xuhao { get; set; }

        /// <summary>
        /// 物料二维码
        /// </summary>
        public string QRCode { get; set; }
    }

    /// <summary>
    /// 物料标签打印输出参数
    /// </summary>
    public class MaterialPOutput
    {
        /// <summary>
        /// ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 入库时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 物料二维码
        /// </summary>
        public string QRCode { get; set; }
    }

    /// <summary>
    /// 库位标签打印输出参数
    /// </summary>
    public class LocationOutput
    {
        /// <summary>
        /// 所属库区ID
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 库位id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 库位类型
        /// </summary>
        public string WareThree { get; set; }

        /// <summary>
        /// 长
        /// </summary>
        public decimal Long { get; set; }

        /// <summary>
        /// 宽
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// 高
        /// </summary>
        public decimal High { get; set; }

        /// <summary>
        /// 巷道货架
        /// </summary>
        public int Aisle { get; set; }

        /// <summary>
        /// 是否虚拟（字典 1是 0否）   
        /// </summary>
        public YesOrNot IsVirtual { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }
    }

    /// <summary>
    /// 容器标签打印输出参数
    /// </summary>
    public class CountainerOutput
    {
        /// <summary>
        /// 所属库区ID
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 容器id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        public string Parameter2 { get; set; }

        /// <summary>
        /// 是否虚拟（字典 1是 0否）   
        /// </summary>
        public YesOrNot IsVirtual { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }
    }

    /// <summary>
    /// 物料标签打印输出参数
    /// </summary>
    public class MaterialOutput
    {
        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 物料二维码
        /// </summary>
        public string QRCode { get; set; }
    }

    /// <summary>
    /// 物料二维码序列化对象
    /// </summary>
    public class QRCodeOutput
    {
        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }
    }
}
