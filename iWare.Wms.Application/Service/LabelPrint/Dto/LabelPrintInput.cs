﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 物料标签打印输入参数
    /// </summary>
    public class MaterialPrintInput : PageInputBase
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }
    }

    /// <summary>
    /// 物料标签打印输入参数
    /// </summary>
    public class MaterialPInput : PageInputBase
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }
    }

    /// <summary>
    /// 库位标签打印输入参数
    /// </summary>
    public class LocationPrintInput : PageInputBase
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 是否虚拟（字典 1是 0否）   
        /// </summary>
        public YesOrNot IsVirtual { get; set; }
    }

    /// <summary>
    /// 容器标签打印输入参数
    /// </summary>
    public class ContainerPrintPrintInput : PageInputBase
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        public string Parameter2 { get; set; }

        /// <summary>
        /// 是否虚拟（字典 1是 0否）   
        /// </summary>
        public YesOrNot IsVirtual { get; set; }
    }

    /// <summary>
    /// 打印输入参数
    /// </summary>
    public class PrintInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public List<long> IdList { get; set; }

        /// <summary>
        /// 打印类型-物料_1、库位_2、容器_3
        /// </summary>
        public int Type { get; set; }
    }

    /// <summary>
    /// 整单打印物料标签输入参
    /// </summary>
    public class MaterialDeliveryInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }
    }

    /// <summary>
    /// 单据详情物料标签输入参
    /// </summary>
    public class MaterialInput
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
    }

    /// <summary>
    /// 新物料标签输入参
    /// </summary>
    public class NewMaterialInput
    {
        /// <summary>
        /// Id
        /// </summary>
        public List<long> Id { get; set; }
    }
}
