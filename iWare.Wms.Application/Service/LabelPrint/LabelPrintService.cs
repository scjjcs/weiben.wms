﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 标签打印服务
    /// </summary>
    [Route("api/LabelPrint")]
    [ApiDescriptionSettings("标签打印", Name = "LabelPrint", Order = 100)]

    public class LabelPrint : IDynamicApiController, ITransient
    {
        private readonly IRepository<WareArea, MasterDbContextLocator> _wareAreaRep; //库区仓储
        private readonly IRepository<WareContainer, MasterDbContextLocator> _wareContainerRep; //容器表
        private readonly IRepository<WareLocation, MasterDbContextLocator> _wareLocationRep; //库位表
        private readonly IRepository<WareMaterial, MasterDbContextLocator> _wareMaterialRep; //物料表
        private readonly IRepository<GoodsDelivery, MasterDbContextLocator> _goodsDelivery; //送货单表
        private readonly IRepository<GoodsDeliveryDetails, MasterDbContextLocator> _goodsDeliveryDetails; //送货单明细表
        private readonly IRepository<v_materialtag_project, MasterDbContextLocator> _vMaterialtagProjectRep; //物料与项目编号试图

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="wareAreaRep"></param>
        /// <param name="wareContainerRep"></param>
        /// <param name="wareLocationRep"></param>
        /// <param name="wareMaterialRep"></param>
        /// <param name="goodsDelivery"></param>
        /// <param name="goodsDeliveryDetails"></param>
        /// <param name="vMaterialtagProjectRep"></param>
        public LabelPrint(
            IRepository<WareArea, MasterDbContextLocator> wareAreaRep,
            IRepository<WareContainer, MasterDbContextLocator> wareContainerRep,
            IRepository<WareLocation, MasterDbContextLocator> wareLocationRep,
            IRepository<WareMaterial, MasterDbContextLocator> wareMaterialRep,
        IRepository<GoodsDelivery, MasterDbContextLocator> goodsDelivery,
            IRepository<GoodsDeliveryDetails, MasterDbContextLocator> goodsDeliveryDetails,
            IRepository<v_materialtag_project, MasterDbContextLocator> vMaterialtagProjectRep
        )
        {
            _wareAreaRep = wareAreaRep;
            _wareContainerRep = wareContainerRep;
            _wareLocationRep = wareLocationRep;
            _wareMaterialRep = wareMaterialRep;
            _goodsDelivery = goodsDelivery;
            _goodsDeliveryDetails = goodsDeliveryDetails;
            _vMaterialtagProjectRep = vMaterialtagProjectRep;
        }
        #endregion

        /// <summary>
        /// 物料标签打印查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("materialPrintSearch")]
        public async Task<PageResult<MaterialPOutput>> materialPrintSearch([FromQuery] MaterialPInput input)
        {
            var list = await _wareMaterialRep.DetachedEntities
                 .Where(!string.IsNullOrEmpty(input.Code), u => EF.Functions.Like(u.Code, $"%{input.Code.Trim()}%"))
                 .Where(!string.IsNullOrEmpty(input.Name), u => EF.Functions.Like(u.Name, $"%{input.Name.Trim()}%"))
                 .Where(!string.IsNullOrEmpty(input.SpecificationModel), u => EF.Functions.Like(u.SpecificationModel, $"%{input.SpecificationModel.Trim()}%"))
                 .ProjectToType<MaterialPOutput>()
                 .ToADPagedListAsync(input.PageNo, input.PageSize);

            foreach (var item in list.Rows)
            {
                item.BrandName = string.IsNullOrEmpty(item.BrandName) || item.BrandName == "无" ? "" : item.BrandName;
                item.QRCode = ("{\"Code\":\"" + item.Code + "\"}");
            }

            return list;
        }

        /// <summary>
        /// 库位标签打印查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("locationPrintSearch")]
        public async Task<PageResult<LocationOutput>> LocationPrintSearch([FromQuery] LocationPrintInput input)
        {
            var list = await _wareLocationRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(input.LocationCode), u => EF.Functions.Like(u.Code, $"%{input.LocationCode.Trim()}%"))
                .Where(input.AreaID != -10, u => u.AreaID == input.AreaID)
                .ProjectToType<LocationOutput>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);

            foreach (var item in list.Rows)
            {
                item.AreaName = await _wareAreaRep.DetachedEntities.Where(z => z.Id == item.AreaID).Select(z => z.AreaName).FirstOrDefaultAsync();
            }

            return list;
        }

        /// <summary>
        /// 容器标签打印查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("ContainerPrintSearch")]
        public async Task<PageResult<CountainerOutput>> ContainerPrintSearch([FromQuery] ContainerPrintPrintInput input)
        {
            var list = await _wareContainerRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(input.ContainerCode), u => EF.Functions.Like(u.Code, $"%{input.ContainerCode.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.Parameter1) && input.Parameter1 != "ALL", u => u.Parameter1 == input.Parameter1)
                .Where(!string.IsNullOrEmpty(input.Parameter2) && input.Parameter2 != "ALL", u => u.Parameter2 == input.Parameter2)
                .Where(input.AreaID != -10, u => u.AreaID == input.AreaID)
                .Where(input.IsVirtual != YesOrNot.All, u => u.IsVirtual == input.IsVirtual)
                .ProjectToType<CountainerOutput>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);

            foreach (var item in list.Rows)
            {
                item.AreaName = await _wareAreaRep.DetachedEntities.Where(z => z.Id == item.AreaID).Select(z => z.AreaName).FirstOrDefaultAsync();
            }

            return list;
        }

        /// <summary>
        /// 整单打印物料标签
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("MaDeliverSearch")]
        public async Task<List<MaterialOutput>> MaDeliverSearch([FromQuery] MaterialDeliveryInput input)
        {
            //整单打印物料标签输出参
            var listMaterialOutput = new List<MaterialOutput>();

            //查询送货单信息
            var Delivery = await _goodsDelivery.DetachedEntities.Where(z => z.DeliveryNo == input.DeliveryNo).FirstOrDefaultAsync();

            //查询送货单详情信息
            var DeliveryDetails = await _goodsDeliveryDetails.DetachedEntities.Where(z => z.GoodsDeliveryId == Delivery.Id).ToListAsync();
            //循环赋值
            foreach (var item in DeliveryDetails)
            {
                var MaterialOutput = new MaterialOutput
                {
                    Code = item.Code,
                    Name = item.Name,
                    SpecificationModel = item.SpecificationModel,
                    InspectionMethod = item.InspectionMethod,
                    QRCode = ("{\"Code\":\"" + item.Code + "\"}")
                };
                listMaterialOutput.Add(MaterialOutput);
            }
            return listMaterialOutput;
        }

        /// <summary>
        /// 单据物料详情打印物料标签
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("MaterialSearch")]
        public async Task<List<MaterialOutput>> MaterialSearch([FromQuery] MaterialInput input)
        {
            //整单打印物料标签输出参
            var listMaterialOutput = new List<MaterialOutput>();

            //查询送货单详情信息
            var DeliveryDetails = await _goodsDeliveryDetails.DetachedEntities.Where(z => z.Id == input.Id).ToListAsync();
            //循环赋值
            foreach (var item in DeliveryDetails)
            {
                var MaterialOutput = new MaterialOutput
                {
                    Code = item.Code,
                    Name = item.Name,
                    SpecificationModel = item.SpecificationModel,
                    InspectionMethod = item.InspectionMethod,
                    QRCode = ("{\"Code\":\"" + item.Code + "\"}")
                };
                listMaterialOutput.Add(MaterialOutput);
            }
            return listMaterialOutput;
        }

        /// <summary>
        /// 新物料标签打印
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("NewMaterialSearch")]
        public async Task<List<MaterialOutput>> NewMaterialSearch([FromQuery] NewMaterialInput input)
        {
            //整单打印物料标签输出参
            var listMaterialOutput = new List<MaterialOutput>();
            
            //循环赋值
            foreach (var item in input.Id)
            {
                //查询物料信息
                var materia = await _wareMaterialRep.DetachedEntities.Where(z => z.Id == item).FirstOrDefaultAsync();

                var MaterialOutput = new MaterialOutput
                {
                    Code = materia.Code,
                    Name = materia.Name,
                    SpecificationModel = materia.SpecificationModel,
                    InspectionMethod = materia.InspectionMethod,
                    QRCode = ("{\"Code\":\"" + materia.Code + "\"}")
                };
                listMaterialOutput.Add(MaterialOutput);
            }
            return listMaterialOutput;
        }
    }
}
