﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 采购人员信息输入参数
    /// </summary>
    public class PurchasePersonnelErpInput
    {
        /// <summary>
        /// 员工编号
        /// </summary>
        public string MV001 { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string MV002 { get; set; }
    }
}
