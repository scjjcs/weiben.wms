﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 单据类型信息输入参数
    /// </summary>
    public class OrderTypeErpInput
    {
        /// <summary>
        /// 单别
        /// </summary>
        public string MQ001 { get; set; }

        /// <summary>
        /// 单据名称
        /// </summary>
        public string MQ002 { get; set; }
    }
}
