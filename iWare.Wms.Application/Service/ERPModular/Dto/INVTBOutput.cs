﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 调拨单身 对应LES系统中的单据明细表
    /// </summary>
    public class INVTBOutput
    {
        /// <summary>
        /// 转出库
        /// </summary>
        public string TB012 { get; set; }

        /// <summary>
        /// 转入库
        /// </summary>
        public string TB013 { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string TB021 { get; set; }

        /// <summary>
        /// 转出库位
        /// </summary>
        public string TB029 { get; set; }

        /// <summary>
        /// 转入库位
        /// </summary>
        public string TB030 { get; set; }

        /// <summary>
        /// 单别
        /// </summary>
        public string TB001 { get; set; }

        /// <summary>
        /// 单号
        /// </summary>
        public string TB002 { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string TB003 { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string TB004 { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string TB005 { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string TB006 { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal TB007 { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string TB008 { get; set; }

        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal TB009 { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string TB017 { get; set; }

        /// <summary>
        /// 审核码
        /// </summary>
        public string TB018 { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        public string UDF02 { get; set; }
    }
}
