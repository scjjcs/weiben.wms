﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 调拨单头 对应LES系统中的单据主表
    /// </summary>
    public class INVTASearch
    {
        /// <summary>
        /// 单别(1201、1202、1203、1204（12开头)
        /// </summary>
        public string TA001 { get; set; }

        /// <summary>
        /// 单号
        /// </summary>
        public string TA002 { get; set; }
    }

    /// <summary>
    /// 返回结果类
    /// </summary>
    public class INVTAResult
    {
        /// <summary>
        /// 是否可以创建
        /// </summary>
        public bool IsCreate { get; set; }

        /// <summary>
        /// 提示
        /// </summary>
        public string Message { get; set; }
    }
}
