﻿using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 采购单头 对应LES系统中的采购主表
    /// </summary>
    public class PURTCSearch
    {
        /// <summary>
        /// 采购单别
        /// </summary>
        public string TC001 { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string TC002 { get; set; }
    }

    /// <summary>
    /// 返回结果类
    /// </summary>
    public class PURTCResult
    {
        /// <summary>
        /// 是否可以创建
        /// </summary>
        public bool IsCreate { get; set; }

        /// <summary>
        /// 提示
        /// </summary>
        public string Message { get; set; }
    }

    /// <summary>
    /// 判断采购单信息是否存在
    /// </summary>
    public class IsExsitPurchaseInput 
    {
        /// <summary>
        /// 采购单号
        /// </summary>
        [Required(ErrorMessage = "采购单号不能为空")]
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 采购类型
        /// </summary>
        [Required(ErrorMessage = "采购类型不能为空")]
        public string PurchaseType { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Required(ErrorMessage = "项目编号不能为空")]
        public string ProjectCode { get; set; }
    }
}
