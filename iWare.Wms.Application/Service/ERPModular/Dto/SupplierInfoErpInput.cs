﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 供应商基本信息输入参数
    /// </summary>
    public class SupplierInfoErpInput
    {
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string MA001 { get; set; }

        /// <summary>
        /// 简称
        /// </summary>
        public string MA002 { get; set; }

        /// <summary>
        /// 公司全称
        /// </summary>
        public string MA003 { get; set; }

        /// <summary>
        /// TEL(一)
        /// </summary>
        public string MA008 { get; set; }

        /// <summary>
        /// 负责人
        /// </summary>
        public string MA012 { get; set; }

        /// <summary>
        /// 联系人(一)
        /// </summary>
        public string MA013 { get; set; }

        /// <summary>
        /// 联系地址(一)
        /// </summary>
        public string MA014{ get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public string MA022 { get; set; }
    }
}
