﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 采购明细表
    /// </summary>
    public class PURTDOutput
    {
        /// <summary>
        /// 采购单别
        /// </summary>
        public string TD001 { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string TD002 { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string TD003 { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string TD004 { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string TD005 { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string TD006 { get; set; }

        /// <summary>
        /// 采购数量
        /// </summary>
        public decimal TD008 { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string TD009 { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string TD014 { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        public string UDF03 { get; set; }

        /// <summary>
        /// 是否结束-N:未结束、Y:自动结束、y:指定结束
        /// </summary>
        public string TD016 { get; set; }

        /// <summary>
        /// 已交数量
        /// </summary>
        public string TD015 { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string TD022 { get; set; }

        /// <summary>
        /// 审核码
        /// </summary>
        public string TD018 { get; set; }
    }
}
