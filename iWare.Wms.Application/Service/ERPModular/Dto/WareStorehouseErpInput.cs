﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 仓库信息输入参数
    /// </summary>
    public class WareStorehouseErpInput
    {
        /// <summary>
        /// 仓库编号
        /// </summary>
        public string MC001 { get; set; }

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string MC002 { get; set; }
    }
}
