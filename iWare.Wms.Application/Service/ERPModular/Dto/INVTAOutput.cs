﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 调拨单头 对应LES系统中的单据主表
    /// </summary>
    public class INVTAOutput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string TA033 { get; set; }

        /// <summary>
        /// 单别(1201、1202、1203、1204（12开头)
        /// </summary>
        public string TA001 { get; set; }

        /// <summary>
        /// 单号
        /// </summary>
        public string TA002 { get; set; }

        /// <summary>
        /// 交易日期（[FORMATE: YMD]）
        /// </summary>
        public string TA003 { get; set; }

        /// <summary>
        /// 部门编号
        /// </summary>
        public string TA004 { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string TA005 { get; set; }

        /// <summary>
        /// 审核码（Y/N/V）
        /// </summary>
        public string TA006 { get; set; }

        /// <summary>
        /// 工厂编号
        /// </summary>
        public string TA008 { get; set; }

        /// <summary>
        /// 单据性质码（11.一般单据、12.调拨单）
        /// </summary>
        public string TA009 { get; set; }

        /// <summary>
        /// 总数量
        /// </summary>
        public decimal TA011 { get; set; }

        /// <summary>
        /// 单据日期
        /// </summary>
        public string TA014 { get; set; }

        /// <summary>
        /// 审核者
        /// </summary>
        public string TA015 { get; set; }

        /// <summary>
        /// （工位号）用户自定义字段1
        /// </summary>
        public string UDF01 { get; set; }

        /// <summary>
        /// （主件品号）用户自定义字段1
        /// </summary>
        public string UDF02 { get; set; }

        /// <summary>
        /// （设备号）用户自定义字段1
        /// </summary>
        public string UDF08 { get; set; }
    }
}
