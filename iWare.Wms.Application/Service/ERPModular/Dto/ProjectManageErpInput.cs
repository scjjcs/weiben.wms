﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 项目管理信息输入参数
    /// </summary>
    public class ProjectManageErpInput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string NO001 { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string NO002 { get; set; }

        /// <summary>
        /// 项目说明
        /// </summary>
        public string NO003 { get; set; }
    }
}
