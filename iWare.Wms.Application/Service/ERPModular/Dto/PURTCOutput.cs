﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 采购单头 对应LES系统中的采购主表
    /// </summary>
    public class PURTCOutput
    {
        /// <summary>
        ///备注
        /// </summary>
        public string TC009 { get; set; }

        /// <summary>
        /// 结束 N:未结束、Y:结束、y:指定结束
        /// </summary>
        public string TCD01 { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string TC047 { get; set; }

        /// <summary>
        /// 采购单别
        /// </summary>
        public string TC001 { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string TC002 { get; set; }

        /// <summary>
        /// 采购日期[FORMATE:YMD]
        /// </summary>
        public string TC003 { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        public string TC004 { get; set; }

        /// <summary>
        /// 数量合计
        /// </summary>
        public decimal TC023 { get; set; }

        /// <summary>
        /// 采购人员
        /// </summary>
        public string TC011 { get; set; }

        /// <summary>
        /// 工厂
        /// </summary>
        public string TC010 { get; set; }

        /// <summary>
        /// 签核状态码
        /// </summary>
        public string TC030 { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string UDF01 { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string UDF07 { get; set; }
    }
}
