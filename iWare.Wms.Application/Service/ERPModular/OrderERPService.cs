﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Microsoft.AspNetCore.Mvc;

namespace iWare.Wms.Application
{
    /// <summary>
    /// ERP订单模块服务
    /// </summary>
    [Route("api/OrderERP")]
    [ApiDescriptionSettings("ERP模块", Name = "OrderERP", Order = 99)]
    public class OrderERPService : IDynamicApiController, ITransient
    {
        private readonly ISqlRepository<ERPDbContextLocator> _erpRep;
        private readonly IRepository<PurchaseOrder, MasterDbContextLocator> _purchaseOrderRep; //采购订单仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="erpRep"></param>
        /// <param name="purchaseOrderRep"></param>
        public OrderERPService(
             ISqlRepository<ERPDbContextLocator> erpRep,
             IRepository<PurchaseOrder, MasterDbContextLocator> purchaseOrderRep
         )
        {
            _erpRep = erpRep;
            _purchaseOrderRep = purchaseOrderRep;
        }
        #endregion

        /// <summary>
        /// 退货单头
        /// </summary>
        [HttpGet("returngoods")]
        public async Task ReturnGoods()
        {
            var purtcOutputList = await _erpRep.SqlQueryAsync<PURTCOutput>(@"
                                select
                                TI001,  --单别
                                TI002,  --单号
                                TI003,  --退货日期[FORMATE:YMD]
                                TI004,  --供应商编号
                                TI013,  --Y:已审核、N:未审核、V:作废
                                TI014,  --单据日期[FORMATE:YMD]
                                TI022,  --单身的总数量
                                TI032,  --0.待处理、S.传送中、1.签核中、2.退件、3.已核准、4.撤销审核中、5.作废中、6.取消作废中、N.不运行电子签核[DEF:N]
                                TI037, --来源单别
                                TI038   --来源单号
                                from PURTI");
        }

        /// <summary>
        /// 退货单明细
        /// </summary>
        [HttpGet("returngoodsdetail")]
        public async Task ReturnGoodsDetail()
        {
            var purtcOutputList = await _erpRep.SqlQueryAsync<PURTCOutput>(@"
                                select
                                    TJ001,  --单别
                                    TJ002,  --单号
                                    TJ003,  --序号
                                    TJ004,  --品号
                                    TJ005,  --品名
                                    TJ006,  --规格
                                    TJ007,  --单位
                                    TJ009,  --数量
                                    TJ011,  --退货仓库
                                    TJ019,  --备注
                                    TJ029   --项目编号
                                    from PURTJ");
        }
    }
}
