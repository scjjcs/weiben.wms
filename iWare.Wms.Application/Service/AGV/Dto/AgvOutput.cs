﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 创建指定名称的订单失败输出参数
    /// </summary>
    public class CreateTransportOrdersOutput
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        public string result { get; set; }

        /// <summary>
        /// 原因
        /// </summary>
        public string reason { get; set; }
    }

    /// <summary>
    /// 根据订单名称查询单个订单输出参数
    /// </summary>
    public class QueryTransportOrdersOutput
    {
        /// <summary>
        /// 命令名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 任务状态：
        /// RAW：初始化中
        /// ACTIVE：激活，可分配
        /// DISPATCHABLE：待分配
        /// BEING_PROCESSED：执行中FINISHED：任务完成
        /// FAILED：任务取消
        /// UNROUTABLE：路径生成错误WITHDRAWN：取消中
        /// </summary>
        public string state { get; set; }

        /// <summary>
        /// 分配车辆
        /// </summary>
        public object intendedVehicle { get; set; }

        /// <summary>
        /// 执行车辆
        /// </summary>
        public string processingVehicle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<DestinationsOutput> destinations { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<PropertiesOutput> properties { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DestinationsOutput
    {
        /// <summary>
        /// 位置
        /// </summary>
        public string locationName { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        public string operation { get; set; }

        /// <summary>
        /// 取货动作或放货动作状态PRISTINE:待执行
        /// TRAVELLING：执行中
        /// FINISHED：完成
        /// </summary>
        public string state { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PropertiesOutput
    {
        /// <summary>
        /// 
        /// </summary>
        public string code { get; set; }
    }

    /// <summary>
    /// 查询指定名称的车辆输出参数
    /// </summary>
    public class QueryTransportCarNameOutput
    {
        /// <summary>
        /// 车辆名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 电量
        /// </summary>
        public int energyLevel { get; set; }

        /// <summary>
        /// 当前位置
        /// </summary>
        public string currentPosition { get; set; }

        /// <summary>
        /// x坐标
        /// </summary>
        public string xcoordinate { get; set; }

        /// <summary>
        /// y坐标
        /// </summary>
        public string ycoordinate { get; set; }

        /// <summary>
        /// 状态
        /// IDLE：空闲，待机
        /// EXECUTING：运行中ERROR：报错
        /// UNKNOWN：离线（AGV处于半自动）
        /// UNAVALIABLE：离线（未连接调度软件）
        /// CHARGING：充电中
        /// </summary>
        public string state { get; set; }

        /// <summary>
        /// 当前执行的任务
        /// </summary>
        public string transportOrder { get; set; }
    }

    /// <summary>
    /// 查询车辆列表输出参数
    /// </summary>
    public class QueryTransportCarListOutput
    {
        /// <summary>
        /// 
        /// </summary>
        public List<TransportCarList> CarLists { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class TransportCarList
    {
        /// <summary>
        /// 车辆名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 电量
        /// </summary>
        public int energyLevel { get; set; }
       
        /// <summary>
        /// 当前位置
        /// </summary>
        public string currentPosition { get; set; }

        /// <summary>
        /// x坐标
        /// </summary>
        public string xcoordinate { get; set; }

        /// <summary>
        /// y坐标
        /// </summary>
        public string ycoordinate { get; set; }

        /// <summary>
        /// 状态
        /// IDLE：空闲，待机
        /// EXECUTING：运行中ERROR：报错
        /// UNKNOWN：离线（AGV处于半自动）
        /// UNAVALIABLE：离线（未连接调度软件）
        /// CHARGING：充电中
        /// </summary>
        public string state { get; set; }

        /// <summary>
        /// 当前执行的任务
        /// </summary>
        public string transportOrder { get; set; }
       
        /// <summary>
        /// 
        /// </summary>
        public int x { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int y { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string modelNumber { get; set; }
    }
}
