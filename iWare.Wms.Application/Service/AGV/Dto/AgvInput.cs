﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 创建指定名称的订单输入参数
    /// </summary>
    public class CreateTransportOrdersInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 指定执行的车辆
        /// </summary>
        public string intendedVehicle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<DestinationsInput> destinations { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<PropertiesInput> properties { get; set; }
    }



    /// <summary>
    /// 创建取放货订单输入参数
    /// </summary>
    public class CreateOrdersedInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 位置（起点）/ 位置（终点）
        /// </summary>
        public string locationName { get; set; }

        /// <summary>
        /// 取货动作：层数 / 放货动作：层数
        /// 平库有多层，直接改 "operation": "Unload cargo:02"
        /// </summary>
        public string operation { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DestinationsInput
    {
        /// <summary>
        /// 位置（起点）/ 位置（终点）
        /// </summary>
        public string locationName { get; set; }

        /// <summary>
        /// 取货动作：层数 / 放货动作：层数
        /// 平库有多层，直接改 "operation": "Unload cargo:02"
        /// </summary>
        public string operation { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PropertiesInput
    {
        /// <summary>
        /// 键：
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// 值：
        /// </summary>
        public string value { get; set; }
    }

    /// <summary>
    /// 根据订单名称查询单个订单输入参数
    /// </summary>
    public class QueryTransportOrdersInput
    {
        /// <summary>
        /// 订单名称
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 查询指定名称的车辆输入参数
    /// </summary>
    public class QueryTransportCarNameInput
    {
        /// <summary>
        /// 车辆名称
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// Agv任务回调输入参数
    /// </summary>
    public class AgvInput
    {
        /// <summary>
        /// 当前执行的任务
        /// </summary>
        public string TransportOrder { get; set; }

        /// <summary>
        /// 状态
        /// PRISTINE：待执行
        /// TRAVELLING：执行中
        /// FINISHED：完成
        /// </summary>
        public string AgvState { get; set; }
    }

    /// <summary>
    /// AGV报警输入参数
    /// </summary>
    public class AGVAAlarmInput
    {
        /// <summary>
        /// 几号小车
        /// </summary>
        public string CarNo { get; set; }

        /// <summary>
        /// 报警信息（正常、异常）
        /// </summary>
        public string AlarmMessage { get; set; }

        /// <summary>
        /// 报警时间
        /// </summary>
        public DateTime? AlarmTime { get; set; }

        /// <summary>
        /// 报警代码
        /// </summary>
        public string AlarmCode { get; set; }
    }

    /// <summary>
    /// Agv任务回调输入参数
    /// </summary>
    public class GetAgvInput
    {
        /// <summary>
        /// 当前执行的任务
        /// </summary>
        public string TransportOrder { get; set; }
        /// <summary>
        /// 重量
        /// large 3 ：大
        /// middle 2 ：中
        /// small 1 ：小
        /// </summary>
        public string AgvWeight { get; set; }
    }

    /// <summary>
    /// 快速入库输入参数
    /// </summary>
    public class FastWarehousingInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }
    }

    /// <summary>
    /// 从输送线上发AGV任务输入参数
    /// </summary>
    public class FastAddAgvTaskInput
    {
        /// <summary>
        /// 站点位置
        /// </summary>
        public string LocationName { get; set; }
    }
}
