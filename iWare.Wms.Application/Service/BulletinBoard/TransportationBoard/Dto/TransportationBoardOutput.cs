﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 出入库数据输出参数
    /// </summary>
    public class WarehousingIssueOutput
    {
        /// <summary>
        /// 出库数
        /// </summary>
        public object WarehousingNumber { get; set; }

        /// <summary>
        /// 入库数
        /// </summary>
        public object IssueNumber { get; set; }

        /// <summary>
        /// 盘点数
        /// </summary>
        public object InventoryNumber { get; set; }

        /// <summary>
        /// 当天时间
        /// </summary>
        public object dateStr { get; set; }
    }

    /// <summary>
    /// 单据明细数输出参数
    /// </summary>
    public class DocumentsNumberOutput
    {
        /// <summary>
        /// 入库单
        /// </summary>
        public object ReceiptDoc { get; set; }

        /// <summary>
        /// 出库单
        /// </summary>
        public object OutboundOrder { get; set; }

        /// <summary>
        /// 执行日期
        /// </summary>
        public object dateStr { get; set; }
    }

    /// <summary>
    /// 库存库位单据输出参数
    /// </summary>
    public class WarehouseOperationOutput
    {
        /// <summary>
        /// 库存数
        /// </summary>
        public StockNumber StockNumbertion { get; set; }

        /// <summary>
        /// 库位状态数
        /// </summary>
        public List<LocationNumber> LocationNumbertion { get; set; }

        /// <summary>
        /// 单据数量
        /// </summary>
        public BillNumber BillNumbertion { get; set; }
    }

    /// <summary>
    /// 订单状态输出参数
    /// </summary>
    public class OrderStatusOutput
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public object OrderNo { get; set; }

        /// <summary>
        /// 订单类型
        /// </summary>
        public object OrderType { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public object OrderStatus { get; set; }
    }

    /// <summary>
    /// 百分比模型输出参数
    /// </summary>
    public class PercentageOutput
    {
        /// <summary>
        /// 百分比出入库单据数
        /// </summary>
        public PercentageBill percentageBilltion { get; set; }

        /// <summary>
        /// 仓库状态百分比数
        /// </summary>
        public List<LocationNumber> LocationNumbertion { get; set; }

        /// <summary>
        /// 百分比不良明细数
        /// </summary>
        public List<QualityTesting> QualityTestingtion { get; set; }
    }

    /// <summary>
    /// 百分比出入库单据数输出参数
    /// </summary>
    public class PercentageBill
    {
        /// <summary>
        /// 出库单总数
        /// </summary>
        public object IssueSum { get; set; }

        /// <summary>
        /// 出库单完成数
        /// </summary>
        public object IssueImplement { get; set; }

        /// <summary>
        /// 出库单未完成数
        /// </summary>
        public object IssueUnexecuted { get; set; }

        /// <summary>
        /// 入库单总数
        /// </summary>
        public object WarehousingSum { get; set; }

        /// <summary>
        /// 入库单完成数
        /// </summary>
        public object WarehousingImplement { get; set; }

        /// <summary>
        /// 入库单未完成数
        /// </summary>
        public object WarehousingUnexecuted { get; set; }

        /// <summary>
        /// 盘点单总数
        /// </summary>
        public object InventorySum { get; set; }

        /// <summary>
        /// 盘点单完成数
        /// </summary>
        public object InventoryImplement { get; set; }

        /// <summary>
        /// 盘点单未完成数
        /// </summary>
        public object InventoryUnexecuted { get; set; }
    }

    /// <summary>
    /// 百分比不良明细输出参数
    /// </summary>
    public class QualityTesting
    {
        /// <summary>
        /// 质检状态数量
        /// </summary>
        public object QualityNumber { get; set; }

        /// <summary>
        /// 质检状态名称
        /// </summary>
        public object QualityName { get; set; }

        /// <summary>
        /// 质检状态颜色
        /// </summary>
        public object QualityColour { get; set; }

        ///// <summary>
        ///// 返修数
        ///// </summary>
        //public object FanXiu { get; set; }

        ///// <summary>
        ///// 报废
        ///// </summary>
        //public int BaoFei { get; set; }

        ///// <summary>
        ///// 退货数
        ///// </summary>
        //public int TuiHuo { get; set; }
    }

    /// <summary>
    /// 库存数输出参数
    /// </summary>
    public class StockNumber
    {
        /// <summary>
        /// 库存总数
        /// </summary>
        public object StockSum { get; set; }
    }

    /// <summary>
    /// 库位状态输出参数
    /// </summary>
    public class LocationNumber
    {
        /// <summary>
        /// 库位数量
        /// </summary>
        public object IibraryNumber { get; set; }

        /// <summary>
        /// 库位状态名称
        /// </summary>
        public object IibraryState { get; set; }

        /// <summary>
        /// 库位颜色
        /// </summary>
        public object IibraryColour { get; set; }
    }

    /// <summary>
    /// 单据数量输出参数
    /// </summary>
    public class BillNumber
    {
        /// <summary>
        /// 入库单数
        /// </summary>
        public object ReceiptDoc { get; set; }

        /// <summary>
        /// 出库单
        /// </summary>
        public object OutboundOrder { get; set; }

        /// <summary>
        /// 盘点单
        /// </summary>
        public object CountSheet { get; set; }
    }
}
