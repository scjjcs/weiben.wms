﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 质检看板服务
    /// </summary>
    [Route("api/QualityBoard")]
    [ApiDescriptionSettings("看板", Name = "QualityBoard", Order = 113)]

    public class QualityBoardService : IDynamicApiController, ITransient
    {
        private readonly IRepository<v_tobe_quality, MasterDbContextLocator> _tobeQualityRep; //待质检试图仓储
        private readonly IRepository<v_quality_result, MasterDbContextLocator> _qualityResultRep; //质检结果试图仓储
        private readonly IRepository<v_supplier_passrate, MasterDbContextLocator> _supplierPassrateRep; //时间段供应商合格率试图仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="tobeQualityRep"></param>
        /// <param name="qualityResultRep"></param>
        /// <param name="supplierPassrateRep"></param>
        public QualityBoardService(
             IRepository<v_tobe_quality, MasterDbContextLocator> tobeQualityRep,
             IRepository<v_quality_result, MasterDbContextLocator> qualityResultRep,
             IRepository<v_supplier_passrate, MasterDbContextLocator> supplierPassrateRep
         )
        {
            _tobeQualityRep = tobeQualityRep;
            _qualityResultRep = qualityResultRep;
            _supplierPassrateRep = supplierPassrateRep;
        }
        #endregion

        /// <summary>
        /// 待质检信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("ToBeQuality")]
        [AllowAnonymous]
        public async Task<List<ToBeQualityOutput>> ToBeQuality()
        {
            var list = await _tobeQualityRep.DetachedEntities.ToListAsync();
            return list.Adapt<List<ToBeQualityOutput>>();
        }

        /// <summary>
        /// 质检结果
        /// </summary>
        /// <returns></returns>
        [HttpGet("QualityResults")]
        [AllowAnonymous]
        public async Task<List<QualityResultOutput>> QualityResults()
        {
            var list = await _qualityResultRep.DetachedEntities.ToListAsync();
            return list.Adapt<List<QualityResultOutput>>();
        }

        /// <summary>
        /// 近一个月供应商合格率
        /// </summary>
        /// <returns></returns>
        [HttpGet("SupplierPassRate")]
        [AllowAnonymous]
        public async Task<List<SupplierPassRateOutput>> SupplierPassRate()
        {
            var list = await _supplierPassrateRep.DetachedEntities.ToListAsync();
            return list.Adapt<List<SupplierPassRateOutput>>();
        }
    }
}
