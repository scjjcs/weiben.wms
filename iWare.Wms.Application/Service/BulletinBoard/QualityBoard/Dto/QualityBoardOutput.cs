﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 待质检输出参数
    /// </summary>
    public class ToBeQualityOutput
    {
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? CollectQuantity { get; set; }

        /// <summary>
        /// 收货时间
        /// </summary>
        public string CreatedTime { get; set; }
    }

    /// <summary>
    /// 质检结果输出参数
    /// </summary>
    public class QualityResultOutput
    {
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 总数量
        /// </summary>
        public int? Ssum { get; set; }

        /// <summary>
        /// 合格数
        /// </summary>
        public int? Qualified { get; set; }

        /// <summary>
        /// 返修数
        /// </summary>
        public int? Rework { get; set; }

        /// <summary>
        /// 报废数
        /// </summary>
        public int? Scrap { get; set; }

        /// <summary>
        /// 退货数
        /// </summary>
        public int? Returnd { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        public string UpdatedTime { get; set; }
    }

    /// <summary>
    /// 近一个月供应商合格率输出参数
    /// </summary>
    public class SupplierPassRateOutput
    {
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 合格率
        /// </summary>
        public double? PassRate { get; set; }
    }
}
