﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 仓库物流物料看板服务
    /// </summary>
    [Route("api/WarehouseBoard")]
    [ApiDescriptionSettings("看板", Name = "WarehouseBoard", Order = 113)]

    public class WarehouseBoardService : IDynamicApiController, ITransient
    {
        private readonly IRepository<v_delivery_audit, MasterDbContextLocator> _deliveryAuditRep; //预约送货审核试图仓储
        private readonly IRepository<v_receipt, MasterDbContextLocator> _receiptRep; //收货试图仓储
        private readonly IRepository<v_warehousing, MasterDbContextLocator> _warehousingRep; //入库试图仓储
        private readonly IRepository<v_delivery, MasterDbContextLocator> _outRep; //出库试图仓储
        private readonly IRepository<v_today_task, MasterDbContextLocator> _todayTaskRep; //当日任务试图仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="deliveryAuditRep"></param>
        /// <param name="receiptRep"></param>
        /// <param name="warehousingRep"></param>
        /// <param name="outRep"></param>
        /// <param name="todayTaskRep"></param>
        public WarehouseBoardService(
             IRepository<v_delivery_audit, MasterDbContextLocator> deliveryAuditRep,
             IRepository<v_receipt, MasterDbContextLocator> receiptRep,
             IRepository<v_warehousing, MasterDbContextLocator> warehousingRep,
             IRepository<v_delivery, MasterDbContextLocator> outRep,
             IRepository<v_today_task, MasterDbContextLocator> todayTaskRep
         )
        {
            _deliveryAuditRep = deliveryAuditRep;
            _receiptRep = receiptRep;
            _warehousingRep = warehousingRep;
            _outRep = outRep;
            _todayTaskRep = todayTaskRep;
        }
        #endregion

        /// <summary>
        /// 预约送货审核信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("DeliveryAudit")]
        [AllowAnonymous]
        public async Task<List<DeliveryAuditOutput>> DeliveryAudit()
        {
            var list = await _deliveryAuditRep.DetachedEntities.ToListAsync();
            return list.Adapt<List<DeliveryAuditOutput>>();
        }

        /// <summary>
        /// 收货信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("Receipt")]
        [AllowAnonymous]
        public async Task<List<ReceiptOutput>> Receipt()
        {
            var list = await _receiptRep.DetachedEntities.ToListAsync();

            var ReceiptList = new List<ReceiptOutput>();
            foreach (var item in list)
            {
                var Receiptdt = new ReceiptOutput()
                {
                    ProjectCode = item.ProjectCode,
                    SpecificationModel = item.SpecificationModel,
                    DeliveryQuantity = item.DeliveryQuantity,
                    ReceiptQuantity = item.ReceiptQuantity > 0 ? "收货" : "未收货"
                };
                ReceiptList.Add(Receiptdt);
            }

            return ReceiptList;
        }

        /// <summary>
        /// 入库信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("Warehousing")]
        [AllowAnonymous]
        public async Task<List<WarehousingOutput>> Warehousing()
        {
            var list = await _warehousingRep.DetachedEntities.ToListAsync();

            var WarehousingList = new List<WarehousingOutput>();
            foreach (var item in list)
            {
                var Warehousingdt = new WarehousingOutput()
                {
                    CollectQuantity = item.CollectQuantity,
                    SpecificationModel = item.SpecificationModel,
                    ProjectCode = item.ProjectCode,
                    WarehousingQuantity = item.WarehousingQuantity > 0 ? "完成" : "未完成",
                };
                WarehousingList.Add(Warehousingdt);
            }

            return WarehousingList;
        }

        /// <summary>
        /// 出库信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("Delivery")]
        [AllowAnonymous]
        public async Task<List<DeliveryOutput>> Delivery()
        {
            var list = await _outRep.DetachedEntities.ToListAsync();

            var DeliveryList = new List<DeliveryOutput>();
            foreach (var item in list)
            {
                var Deliverydt = new DeliveryOutput()
                {
                    BindQuantity = item.BindQuantity,
                    ProjectCode = item.ProjectCode,
                    SpecificationModel = item.SpecificationModel,
                    TaskStatus = item.TaskStatus > 0 ? "完成" : "未完成",
                };
                DeliveryList.Add(Deliverydt);
            }

            return DeliveryList;
        }

        /// <summary>
        /// 当日任务详情
        /// </summary>
        /// <returns></returns>
        [HttpGet("TodayTask")]
        [AllowAnonymous]
        public async Task<TodayTaskOutput> TodayTask()
        {
            var list = await _todayTaskRep.DetachedEntities.FirstAsync();
            return list.Adapt<TodayTaskOutput>();
        }
    }
}
