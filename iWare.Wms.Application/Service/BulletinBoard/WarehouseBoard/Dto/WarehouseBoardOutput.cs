﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 预约送货审核输出参数
    /// </summary>
    public class DeliveryAuditOutput
    {
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SuppName { get; set; }

        /// <summary>
        /// 未审核
        /// </summary>
        public int UnapprovedCount { get; set; }

        /// <summary>
        /// 审核通过
        /// </summary>
        public int ExamineCount { get; set; }

        /// <summary>
        /// 驳回
        /// </summary>
        public int RejectCount { get; set; }
    }

    /// <summary>
    /// 收货详情输出参数
    /// </summary>
    public class ReceiptOutput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? DeliveryQuantity { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public object ReceiptQuantity { get; set; }
    }

    /// <summary>
    /// 入库详情输出参数
    /// </summary>
    public class WarehousingOutput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? CollectQuantity { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public object WarehousingQuantity { get; set; }
    }

    /// <summary>
    /// 出库详情输出参数
    /// </summary>
    public class DeliveryOutput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? BindQuantity { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public object TaskStatus { get; set; }
    }

    /// <summary>
    /// 当日任务输出参数
    /// </summary>
    public class TodayTaskOutput
    {
        /// <summary>
        /// 送货总数
        /// </summary>
        public decimal? DeliverySum { get; set; }

        /// <summary>
        /// 已完成
        /// </summary>
        public decimal? Completed { get; set; }

        /// <summary>
        /// 未完成
        /// </summary>
        public decimal? Unfinished { get; set; }

        /// <summary>
        /// 收货总数
        /// </summary>
        public decimal? ReceiptSum { get; set; }

        /// <summary>
        /// 已收货
        /// </summary>
        public decimal? GoodsReceived { get; set; }

        /// <summary>
        /// 未收货
        /// </summary>
        public decimal? UnreceivedGoods { get; set; }

        /// <summary>
        /// 入库总数
        /// </summary>
        public decimal? WarehousingSum { get; set; }

        /// <summary>
        /// 已入库
        /// </summary>
        public decimal? Warehousing { get; set; }

        /// <summary>
        /// 未入库
        /// </summary>
        public decimal? NoWarehousing { get; set; }

        /// <summary>
        /// 出库总数
        /// </summary>
        public decimal? IssueSum { get; set; }

        /// <summary>
        /// 已出库
        /// </summary>
        public decimal? Issue { get; set; }

        /// <summary>
        /// 未出库
        /// </summary>
        public decimal? NotIssued { get; set; }

        /// <summary>
        /// 送货完成率
        /// </summary>
        public double? DeliveryRate { get; set; }

        /// <summary>
        /// 收货完成率
        /// </summary>
        public double? ReceiptRate { get; set; }

        /// <summary>
        /// 入库率
        /// </summary>
        public double? WarehousingRate { get; set; }

        /// <summary>
        /// 出库率
        /// </summary>
        public double? IssueRate { get; set; }
    }
}
