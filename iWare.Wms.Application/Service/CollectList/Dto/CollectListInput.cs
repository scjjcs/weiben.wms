﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 收货列表输入参数
    /// </summary>
    public class CollectListInput : PageInputBase
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 是否质检
        /// </summary>
        public YesOrNot IsQuality { get; set; } = YesOrNot.All;

        /// <summary>
        /// 质检结果
        /// </summary>
        public QualityEnum QualityResult { get; set; } = QualityEnum.All;

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string CreatedUserName { get; set; }
    }

    /// <summary>
    /// 更新是否质检
    /// </summary>
    public class UpdateIsQualityInput : BaseId
    {
        /// <summary>
        /// 质检结果
        /// </summary>
        public QualityEnum QualityResult { get; set; }

        /// <summary>
        /// 质检结果数量
        /// </summary>
        public int QualityNumber { get; set; }
    }

    /// <summary>
    /// 更新是否质检
    /// </summary>
    public class DeleteModel : BaseId
    {

    }

    /// <summary>
    /// 收货列表页面送货质检明细输入参数
    /// </summary>
    public class CollectQualityDetailsInput : PageInputBase
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }
    }

    /// <summary>
    /// 单个质检输入参数
    /// </summary>
    public class CollectDeliveryNoInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        [Required(ErrorMessage = "送货单号不能为空")]
        public string DeliveryNo { get; set; }
    }

    /// <summary>
    /// 一键质检输入参数
    /// </summary>
    public class BatchCollectDeliveryNoInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        [Required(ErrorMessage = "送货单号不能为空")]
        public List<string> DeliveryNo { get; set; }
    }
}
