﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 收货列表输出参数
    /// </summary>
    public class CollectListOutput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 检验方式名称-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public string InspectionMethodName { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量 
        /// </summary>
        public decimal CollectQuantity { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        ///<summary>
        /// 入库数量
        /// </summary>
        public decimal WarehousingQuantity { get; set; }

        /// <summary>
        /// 是否质检
        /// </summary>
        public YesOrNot IsQuality { get; set; }

        /// <summary>
        /// 物料质检是否合格
        /// </summary>
        public YesOrNot IsQualified { get; set; }

        /// <summary>
        /// 质检结果
        /// </summary>
        public QualityEnum QualityResult { get; set; }

        /// <summary>
        /// 质检结果数量
        /// </summary>
        public int QualityNumber { get; set; }

        /// <summary>
        /// 是否收货完成-否_0、是_1
        /// </summary>
        public YesOrNot IsComplete { get; set; } = YesOrNot.N;

        /// <summary>
        /// 收货单信息
        /// </summary>
        public CollectDelivery CollectDelivery { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public string CreatedUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新者名称
        /// </summary>
        public string UpdatedUserName { get; set; }

        /// <summary>
        /// 质检时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 物料二维码
        /// </summary>
        public string MaterialTagCode { get; set; }
    }

    /// <summary>
    /// 导出收货列表输出参数
    /// </summary>
    public class ExportCollectListOutput
    {
        /// <summary>
        /// 收货主表Id
        /// </summary>
        public long CollectDeliveryId { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 收货时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 检验方式名称-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public string InspectionMethodName { get; set; }

        /// <summary>
        /// 收货数量 
        /// </summary>
        public decimal CollectQuantity { get; set; }

        /// <summary>
        /// 质检结果
        /// </summary>
        public QualityEnum QualityResult { get; set; }

        /// <summary>
        /// 质检结果名称
        /// </summary>
        public string QualityResultName { get; set; }

        /// <summary>
        /// 质检结果数量
        /// </summary>
        public int QualityNumber { get; set; }

        /// <summary>
        /// 质检时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 是否质检
        /// </summary>
        public YesOrNot IsQuality { get; set; }

        /// <summary>
        /// 是否质检名称
        /// </summary>
        public string IsQualityName { get; set; }

        /// <summary>
        /// 合格数
        /// </summary>
        public decimal HeGeQuality { get; set; }

        /// <summary>
        /// 不合格原因
        /// </summary>
        public string NotHeGeReason { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string CreatedUserName { get; set; }
    }
}
