﻿using iWare.Wms.Core;

namespace iWare.Wms.Application.Service.PDA
{
    /// <summary>
    /// 生成虚拟容器输出参数
    /// </summary>
    public class GenerateVirtuallyContainerOutput
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 容器编号   
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        public string Parameter2 { get; set; }
    }

    /// <summary>
    /// 生成虚拟库位输出参数
    /// </summary>
    public class GenerateVirtuallyLocationOutput
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 库位编号   
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 所属企业
        /// </summary>
        public string WareOne { get; set; }

        /// <summary>
        /// 所属厂区
        /// </summary>
        public string WareTwo { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        public string WareThree { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        public int RowNo { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        public int ColumnNo { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        public int LayerNo { get; set; }
    }

    /// <summary>
    /// 物料标签打印输出参数
    /// </summary>
    public class PDAMaterialPrintOutput
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 入库时间
        /// </summary>
        public DateTimeOffset? ManufactureDate { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public List<string> ProjectCode { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string Xuhao { get; set; }
    }
}
