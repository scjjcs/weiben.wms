﻿using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application.Service.PDA
{
    /// <summary>
    /// 查询系统容器信息
    /// </summary>
    public class GetSystemConatinerInput
    {
        /// <summary>
        /// 容器编号   
        /// </summary>
        [Required(ErrorMessage = "容器编号不能为空")]
        public string ConatinerCode { get; set; }
    }

    /// <summary>
    /// 生成虚拟容器输入参数
    /// </summary>
    public class GenerateVirtuallyContainerInput
    {
        /// <summary>
        /// 种类
        /// </summary>
        [Required(ErrorMessage = "种类不能为空")]
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        [Required(ErrorMessage = "材质不能为空")]
        public string Parameter2 { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        [Required(ErrorMessage = "所属库区不能为空")]
        public long AreaID { get; set; }
    }

    /// <summary>
    /// 确认生成虚拟容器输入参数
    /// </summary>
    public class ConfimGenerateContainerInput
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 容器编号   
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        public string Parameter2 { get; set; }
    }

    /// <summary>
    /// 查询系统库位信息
    /// </summary>
    public class GetSystemLocationInput
    {
        /// <summary>
        /// 库位编号   
        /// </summary>
        [Required(ErrorMessage = "库位编号不能为空")]
        public string LocationCode { get; set; }
    }

    /// <summary>
    /// 生成虚拟库位输入参数
    /// </summary>
    public class GenerateVirtuallyLocationInput
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        [Required(ErrorMessage = "所属库区不能为空")]
        public long AreaID { get; set; }
    }

    /// <summary>
    /// 确认生成虚拟库位输入参数
    /// </summary>
    public class ConfimGenerateLocationInput
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 库位编号   
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 所属企业
        /// </summary>
        public string WareOne { get; set; }

        /// <summary>
        /// 所属厂区
        /// </summary>
        public string WareTwo { get; set; }

        /// <summary>
        /// 库区类型
        /// </summary>
        public string WareThree { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        public int RowNo { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        public int ColumnNo { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        public int LayerNo { get; set; }
    }

    /// <summary>
    /// 物料打印输入参数
    /// </summary>
    public class PDAMaterialPrintInput 
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }
    }
}
