﻿namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 物料分拣输入参数
    /// </summary>
    public class MaterialSortingInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }
    }
}
