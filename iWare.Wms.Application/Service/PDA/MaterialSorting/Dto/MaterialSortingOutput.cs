﻿using iWare.Wms.Core;

namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 物料分拣输出参数
    /// </summary>
    public class MaterialSortingOutput
    {
        /// <summary>
        /// 是否强制完成
        /// </summary>
        public YesOrNot IsComplete { get; set; } = YesOrNot.N;

        /// <summary>
        /// 容器
        /// </summary>
        public WareContainerOutput WareContainer { get; set; }

        /// <summary>
        /// 物料明细
        /// </summary>
        public List<WareSortOrder> WareSortOrders { get; set; }
    }
}
