﻿using iWare.Wms.Core;

namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 送货信息
    /// </summary>
    public class GoodsDeliveryOutput
    {
        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 送货单别
        /// </summary>
        public string DeliveryType { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal DeliveryQuantityTotal { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 审核状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; } = AuditStatusEnum.yuyuezhong;

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SuppCode { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SuppName { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string SuppPhone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string SuppAddress { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 收货单位
        /// </summary>
        public string ReceName { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string ReceAddress { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string RecePhone { get; set; }

        /// <summary>
        /// 采购联系人
        /// </summary>
        public string ReceUser { get; set; }

        /// <summary>
        /// 是否加急件
        /// </summary>
        public YesOrNot IsJiaJiJian { get; set; } = YesOrNot.N;

        /// <summary>
        /// 送货单明细信息
        /// </summary>
        public List<GoodsDeliveryDetails> GoodsDeliveryDetails { get; set; }
    }
    /// <summary>
    /// 物料信息
    /// </summary>
    public class MaterialOutput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 序号   --对应ERP序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号   --对应ERP品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>   
        /// 品名    --对应ERP品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 送货数量
        /// </summary>
        public decimal DeliveryQuantity { get; set; }

        /// <summary>
        /// 已收货数量
        /// </summary>
        public decimal CollectQuantity { get; set; }

        /// <summary>
        /// 本次收货数
        /// </summary>
        public decimal ThisQuantity { get; set; }

        /// <summary>
        /// 质检结果
        /// </summary>
        public QualityEnum QualityResult { get; set; }

        /// <summary>
        /// 质检结果名称
        /// </summary>
        public string QualityResultName { get; set; }
    }

    /// <summary>
    /// 点击下一步输出参数
    /// </summary>
    public class ReturnCollectOutput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 送货总数
        /// </summary>
        public decimal DeliveryQuantity { get; set; }

        /// <summary>
        /// 已收货总数
        /// </summary>
        public decimal CollectQuantity { get; set; }

        /// <summary>
        /// 收货上传图片
        /// </summary>
        public List<string> Images { get; set; }

        /// <summary>
        /// 未收货上传图片
        /// </summary>
        public List<string> notImages { get; set; }

        /// <summary>
        /// 物料收货-收货明细
        /// </summary>
        public List<MaterialOutput> collectDetailss { get; set; }

        /// <summary>
        /// 物料收货-未收货明细
        /// </summary>
        public List<MaterialOutput> notCollectDetailss { get; set; }
    }

    /// <summary>
    /// 收货映射类
    /// </summary>
    public class CollectDeliveryDetailsDto
    {
        /// <summary>
        /// 收货单Id
        /// </summary>
        public long CollectDeliveryId { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量 
        /// </summary>
        public decimal CollectQuantity { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 是否入库
        /// </summary>
        public YesOrNot IsWarehousing { get; set; }

        /// <summary>
        /// 是否质检
        /// </summary>
        public YesOrNot IsQuality { get; set; }

        /// <summary>
        /// 物料质检是否合格
        /// </summary>
        public YesOrNot IsQualified { get; set; }

        /// <summary>
        /// 质检结果
        /// </summary>
        public QualityEnum QualityResult { get; set; }

        /// <summary>
        /// 本次收货数量
        /// </summary>
        public decimal ThisQuantity { get; set; }
    }
}
