﻿using iWare.Wms.Core;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 物料收货
    /// </summary>
    public class MaterialCollectInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }
        /// <summary>
        /// 扫描参数
        /// </summary>
        public MaterialDto ScannMaterial { get; set; }

        /// <summary>
        /// 收货参数
        /// </summary>
        public List<MaterialDto> CollectMaterials { get; set; }
    }

    /// <summary>
    /// 物料信息
    /// </summary>
    public class MaterialInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 序号   --对应ERP序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号   --对应ERP品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>   
        /// 品名    --对应ERP品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 送货数量
        /// </summary>
        public decimal DeliveryQuantity { get; set; }

        /// <summary>
        /// 收货数量
        /// </summary>
        public decimal CollectQuantity { get; set; }

        /// <summary>
        /// 本次收货数
        /// </summary>
        public decimal ThisQuantity { get; set; }

        /// <summary>
        /// 请求来源 组盘group 立体库入库ltkin 平库入库pkin 物料入库wlin 移库transfer 余料回库 ylin 无托入库wtin
        /// </summary>
        public string Source { get; set; }
    }

    /// <summary>
    /// 二维码物料信息
    /// </summary>
    public class OQCodeInput
    {
        /// <summary>
        /// 品号   --对应ERP品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>   
        /// 品名    --对应ERP品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 组盘信息
        /// </summary>
        public List<WareContainerVsMaterialOutput> wareContainerVsMaterials { get; set; }
    }

    /// <summary>
    /// 收货查询输入参数
    /// </summary>
    public class CollectSearchInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        [Required(ErrorMessage = "送货单号不能为空")]
        public string DeliveryNo { get; set; }
    }
}
