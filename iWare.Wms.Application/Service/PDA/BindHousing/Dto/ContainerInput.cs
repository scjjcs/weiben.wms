﻿using iWare.Wms.Core;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 验证容器输入参数
    /// </summary>
    public class ContainerInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        [Required(ErrorMessage = "容器编号不能为空")]
        public string ContainerCode { get; set; }

    }

    /// <summary>
    /// 验证容器变更输入参数
    /// </summary>
    public class ChangeContainerInput
    {
        /// <summary>
        /// 老的容器编号
        /// </summary>
        [Required(ErrorMessage = "容器编号不能为空")]
        public string OldContainerCode { get; set; }

        /// <summary>
        /// 新的容器编号
        /// </summary>
        [Required(ErrorMessage = "容器编号不能为空")]
        public string NewContainerCode { get; set; }

        /// <summary>
        /// 物料明细
        /// </summary>
        public List<WareContainerVsMaterialInput> WareContainerVsMaterials { get; set; }
    }

    /// <summary>
    /// 查询容器输入参数
    /// </summary>
    public class GetContainerInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        [Required(ErrorMessage = "容器编号不能为空")]
        public string ContainerCode { get; set; }
    }

    /// <summary>
    /// 验证入库容器输出参数
    /// </summary>
    public class ContainerHousingInput
    {
        /// <summary>
        /// 库区ID
        /// </summary>
        public long AreaId { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 任务方式;任务方式-手动_1、自动_2
        /// </summary>
        public TaskModel TaskModel { get; set; }

        /// <summary>
        /// 上料口
        /// </summary>
        public string Entrance { get; set; }

        /// <summary>
        /// 容器
        /// </summary>
        public WareContainerInput WareContainer { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public List<string> Images { get; set; }

        /// <summary>
        /// 物料明细
        /// </summary>
        public List<WareContainerVsMaterialInput> WareContainerVsMaterials { get; set; }

        /// <summary>
        /// 追加的物料信息
        /// </summary>
        public List<WareContainerVsMaterialInput> newWareContainerVsMaterials { get; set; }
    }

    /// <summary>
    /// 入库输入参数
    /// </summary>
    public class WarehousingInput
    {
        /// <summary>
        /// 库区ID
        /// </summary>
        public long AreaId { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 任务方式;任务方式-手动_1、自动_2
        /// </summary>
        public TaskModel TaskModel { get; set; }

        /// <summary>
        /// AGV上料库位
        /// </summary>
        public string AgvCode { get; set; }

        /// <summary>
        /// 容器
        /// </summary>
        public WareContainerInput WareContainer { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public List<string> Images { get; set; }

        /// <summary>
        /// 物料明细
        /// </summary>
        public List<WareContainerVsMaterialInput> WareContainerVsMaterials { get; set; }
    }

    /// <summary>
    /// 容器表输入参数
    /// </summary>
    public class WareContainerInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        public string Parameter2 { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-空闲_0、使用_1、禁用_-1
        /// </summary>
        public ContainerStatusEnum ContainerStatus { get; set; }

        /// <summary>
        /// 库区-区分立体库与平库容器
        /// </summary>
        public long AreaID { get; set; }
    }

    /// <summary>
    /// 验证库位输入参数
    /// </summary>
    public class LocationInput
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        [Required(ErrorMessage = "库位编号不能为空")]
        public string LocationCode { get; set; }

    }

    /// <summary>
    /// 物料明细
    /// </summary>
    public class WareContainerVsMaterialInput
    {
        /// <summary>
        /// 送货单号 2022-12-13新增 目的通过单号修改收货数据中的入库数据
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 组盘单据
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 项目号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 容器表Id
        /// </summary>
        public long ContainerId { get; set; }

        /// <summary>
        /// 容器编码
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 物料ID   
        /// </summary>
        public long MaterialId { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号    
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名    
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal BindQuantity { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus? ContainerVsMaterialStatus { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 供应商code
        /// </summary>
        public string SupplerCode { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTimeOffset? ExpirationTime { get; set; }
    }

    /// <summary>
    /// 空托入库
    /// </summary>
    public class EmptyContainerInInput
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        [Required(ErrorMessage = "库位编号不能为空")]
        public string LocationCode { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public List<string> Images { get; set; }

        /// <summary>
        /// 物料明细物料入库使用
        /// </summary>
        public List<WareContainerVsMaterialInput> newWareContainerVsMaterials { get; set; }
    }

    /// <summary>
    /// 根据容器编号查询站点信息
    /// </summary>
    public class WareSiteListByContainerInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        [Required(ErrorMessage = "容器编号不能为空")]
        public string ContainerCode { get; set; }
    }

    /// <summary>
    /// 呼叫AGV输入参
    /// </summary>
    public class CallsAgvInput
    {
        /// <summary>
        /// 起始点库位编号
        /// </summary>
        public string StartCode { get; set; }

        /// <summary>
        /// 目标点库位编号
        /// </summary>
        public string EndCode { get; set; }
    }
}
