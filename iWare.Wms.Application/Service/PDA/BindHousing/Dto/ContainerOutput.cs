﻿using iWare.Wms.Core;

namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 验证容器输出参数
    /// </summary>
    public class ContainerOutput
    {
        /// <summary>
        /// 容器
        /// </summary>
        public WareContainerOutput WareContainer{ get; set; }

        /// <summary>
        /// 物料明细
        /// </summary>
        public List<WareContainerVsMaterialOutput> WareContainerVsMaterials { get; set; }
    }

    /// <summary>
    /// 验证容器输出参数
    /// </summary>
    public class MaterialContainerOutput
    {
        /// <summary>
        /// 容器
        /// </summary>
        public WareContainerOutput WareContainer { get; set; }

        /// <summary>
        /// 物料明细
        /// </summary>
        public List<WareContainerVsMaterialOutput> WareContainerVsMaterials { get; set; }
    }

    /// <summary>
    /// 验证库位输出参数
    /// </summary>
    public class LocationOutput
    {
        /// <summary>
        /// 容器
        /// </summary>
        public WareLocationOutput WareLocation { get; set; }

        /// <summary>
        /// 库位与容器明细
        /// </summary>
        public List<WareLocationVsContainer> WareLocationVsContainers { get; set; }
    }

    /// <summary>
    /// 验证入库容器输出参数
    /// </summary>
    public class ContainerHousingOutput
    {
        /// <summary>
        /// 库位编码
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 上料口
        /// </summary>
        public string Entrance { get; set; }

        /// <summary>
        /// 容器
        /// </summary>
        public WareContainer WareContainer { get; set; }

        /// <summary>
        /// 物料明细
        /// </summary>
        public List<WareContainerVsMaterial> WareContainerVsMaterials { get; set; }
    }

    /// <summary>
    /// 容器输出类
    /// </summary>
    public class WareContainerOutput
    {
        /// <summary>
        /// 容器编号   
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        public string Parameter2 { get; set; }

        /// <summary>
        /// 库区ID 
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 是否虚拟（字典 1是 0否）   
        /// </summary>
        public YesOrNot IsVirtual { get; set; } = YesOrNot.N;

        /// <summary>
        /// 状态-空闲_0、使用_1、禁用_-1
        /// </summary>
        public ContainerStatusEnum? ContainerStatus { get; set; } = ContainerStatusEnum.kongxian;
    }

    /// <summary>
    /// 物料关系表
    /// </summary>
    public class WareContainerVsMaterialOutput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 组盘单据
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 项目号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 容器表Id
        /// </summary>
        public long ContainerId { get; set; }

        /// <summary>
        /// 容器编码
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 物料ID   
        /// </summary>
        public long MaterialId { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号    
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名    
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal BindQuantity { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus? ContainerVsMaterialStatus { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 供应商code
        /// </summary>
        public string SupplerCode { get; set; }

        /// <summary>
        /// 是否加急
        /// </summary>
        public YesOrNot IsJiaJi { get; set; }

        /// <summary>
        /// 质检结果
        /// </summary>
        public string QualityResultName { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTimeOffset ExpirationTime { get; set; }
    }

    /// <summary>
    /// AGV库位输出参
    /// </summary>
    public class AgvLocationOutput
    {
        /// <summary>
        /// 库位Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 原库位编号
        /// </summary>
        public string OriginalLocationCode { get; set; }
    }
}
