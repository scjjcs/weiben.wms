﻿using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 根据所属库区ID查询对应的库口输入参数
    /// </summary>
    public class EmptyBracketByEntranceInput
    {
        /// <summary>
        /// 所属库区ID
        /// </summary>
        [Required(ErrorMessage = "所属库区ID不能为空")]
        public long AreaID { get; set; }
    }

    /// <summary>
    /// 手动呼叫空托输入参数
    /// </summary>
    public class ManualEmptyBracketInput
    {
        /// <summary>
        /// 容器类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 容器数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string LocationCode { get; set; }
    }

    /// <summary>
    /// 自动呼叫空托输入参数
    /// </summary>
    public class AutomaticEmptyBracketInput
    {
        /// <summary>
        /// 容器类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 容器数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 库区ID
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 库口
        /// </summary>
        public string Entrance { get; set; }
    }
}
