﻿namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 临时库位入库输出参数
    /// </summary>
    public class TemporaryInOutput
    {
        /// <summary>
        /// 库区ID
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 物料明细物料入库使用
        /// </summary>
        public List<WareContainerVsMaterialInput> newWareContainerVsMaterials { get; set; }
    }

    /// <summary>
    /// 临时库位出库输出参数
    /// </summary>
    public class TemporaryOutOutput
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 当前库存数量
        /// </summary>
        public decimal CurrentQuantity { get; set; }
    }
}
