﻿using iWare.Wms.Core;

namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 临时库位输入参数
    /// </summary>
    public class TemporaryInInput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号   --对应ERP品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>   
        /// 品名    --对应ERP品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }
    }

    /// <summary>
    /// 临时库位入库输入参数
    /// </summary>
    public class TemporaryOutInput
    {
        /// <summary>
        /// 所属库区ID
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名    
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }
    }

    /// <summary>
    /// 根据条件查询库位列表
    /// </summary>
    public class AreaListInput
    {
        /// <summary>
        /// 类型
        /// </summary>
        public AreaTypeEnum? AreaType { get; set; }
    }

    /// <summary>
    /// 收货查询输入参数
    /// </summary>
    public class CollectcxInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 组盘信息
        /// </summary>
        public List<WareContainerVsMaterialOutput> wareContainerVsMaterials { get; set; }
    }
}
