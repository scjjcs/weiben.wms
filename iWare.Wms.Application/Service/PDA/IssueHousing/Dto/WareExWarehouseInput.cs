﻿using iWare.Wms.Core;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application.PDA
{
    /// <summary>
    /// 扫描单据号输入参数
    /// </summary>
    public class OrderNoInput
    {
        /// <summary>
        /// 单据号
        /// </summary>
        [Required(ErrorMessage = "单据号不能为空")]
        public string OrderNo { get; set; }

        /// <summary>
        /// 出库单别
        /// </summary>
        public string OrderCategory { get; set; }
    }

    /// <summary>
    /// 根据品号获取相应物料信息输入参数
    /// </summary>
    public class CodeInput
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }
    }

    /// <summary>
    /// 物料出库输入参数
    /// </summary>
    public class MaterialForContainerIssueInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        [Required(ErrorMessage = "容器编号不能为空")]
        public string ContainerCode { get; set; }
    }

    /// <summary>
    /// 物料出库输入参数
    /// </summary>
    public class MaterialForLocationIssueInput
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        [Required(ErrorMessage = "库位编号不能为空")]
        public string LocationCode { get; set; }
    }

    /// <summary>
    /// 出库操作输入参数
    /// </summary>
    public class MaterialOutInput
    {
        /// <summary>
        /// 库区ID
        /// </summary>
        public long AreaId { get; set; }

        /// <summary>
        /// 单据号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<WareOrderDetailInput> WareOrderDetailList { get; set; }
    }

    /// <summary>
    /// 出库操作输入参数
    /// </summary>
    public class OutboundInput
    {
        /// <summary>
        /// 单据号
        /// </summary>
        public string orderNo { get; set; }

        /// <summary>
        /// 出库单别
        /// </summary>
        public string typeWay { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        public string LocationCode { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 任务方式;任务方式-手动_1、自动_2
        /// </summary>
        public TaskModel taskModel { get; set; }

        /// <summary>
        /// 物料明细
        /// </summary>
        public List<WareContainerVsMaterialIssueOutput> WareContainerVsMaterials { get; set; }


    }

    /// <summary>
    /// 库存表输出参数
    /// </summary>
    public class WareOrderDetailInput
    {
        /// <summary>
        /// 单据明细ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string Describe { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 托盘数量
        /// </summary>
        public decimal StockQuantity { get; set; }

        /// <summary>
        /// 分拣数量
        /// </summary>
        public decimal SortQuantity { get; set; }
    }

    /// <summary>
    /// 盘点出库输入参数
    /// </summary>
    public class QueryPanDianOutListInput
    {
        /// <summary>
        /// 所属库区
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        public int RowNo { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        public int ColumnNo { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        public int LayerNo { get; set; }
    }

    /// <summary>
    /// 呼叫AGV输入参
    /// </summary>
    public class CallAgvInput
    {
        /// <summary>
        /// 起始站点编号
        /// </summary>
        public string StartCode { get; set; }

        /// <summary>
        /// 目标站点编号
        /// </summary>
        public string EndCode { get; set; }
    }
}
