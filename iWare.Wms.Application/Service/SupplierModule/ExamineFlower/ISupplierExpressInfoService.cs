﻿using iWare.Wms.Core;
using Microsoft.AspNetCore.Mvc;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 快递公司信息服务
    /// </summary>
    public interface ISupplierExpressInfoService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Add(AddSupplierExpressInfoInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Delete(DeleteSupplierExpressInfoInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<SupplierExpressInfoOutput> Get([FromQuery] QueryeSupplierExpressInfoInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<List<SupplierExpressInfoOutput>> List([FromQuery] SupplierExpressInfoInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<SupplierExpressInfoOutput>> Page([FromQuery] SupplierExpressInfoSearch input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Update(UpdateSupplierExpressInfoInput input);
    }
}