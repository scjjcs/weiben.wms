﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 快递公司信息服务
    /// </summary>
    [Route("api/SupplierExpressInfo")]
    //[ApiDescriptionSettings("采购收货模块", Name = "SupplierExpressInfo", Order = 100)]

    public class SupplierExpressInfoService : ISupplierExpressInfoService, IDynamicApiController, ITransient
    {
        private readonly IRepository<SupplierExpressInfo, MasterDbContextLocator> _supplierExpressInfoRep;

        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="supplierExpressInfoRep"></param>
        public SupplierExpressInfoService(
            IRepository<SupplierExpressInfo, MasterDbContextLocator> supplierExpressInfoRep
        )
        {
            _supplierExpressInfoRep = supplierExpressInfoRep;
        }

        /// <summary>
        /// 分页查询快递公司信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        public async Task<PageResult<SupplierExpressInfoOutput>> Page([FromQuery] SupplierExpressInfoSearch input)
        {
            var supplierExpressInfos = await _supplierExpressInfoRep.DetachedEntities
                                                                 .Where(!string.IsNullOrEmpty(input.Phone), u => u.Phone == input.Phone)
                                                                 .Where(!string.IsNullOrEmpty(input.Name), u => u.Name == input.Name)
                                                                 .Where(!string.IsNullOrEmpty(input.Code), u => u.Code == input.Code)
                                                                 .Where(input.GoodsDeliveryId != null, u => u.GoodsDeliveryId == input.GoodsDeliveryId)
                                                                 .Where(input.Status != null, u => u.Status == input.Status)
                                                                 .OrderBy(PageInputOrder.OrderBuilder<SupplierExpressInfoSearch>(input))
                                                                 .ProjectToType<SupplierExpressInfoOutput>()
                                                                 .ToADPagedListAsync(input.PageNo, input.PageSize);
            return supplierExpressInfos;
        }

        /// <summary>
        /// 增加快递公司信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task Add(AddSupplierExpressInfoInput input)
        {
            var supplierExpressInfo = input.Adapt<SupplierExpressInfo>();
            await _supplierExpressInfoRep.InsertAsync(supplierExpressInfo);
        }

        /// <summary>
        /// 删除快递公司信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public async Task Delete(DeleteSupplierExpressInfoInput input)
        {
            var supplierExpressInfo = await _supplierExpressInfoRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            await _supplierExpressInfoRep.DeleteAsync(supplierExpressInfo);
        }

        /// <summary>
        /// 更新快递公司信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        public async Task Update(UpdateSupplierExpressInfoInput input)
        {
            var isExist = await _supplierExpressInfoRep.AnyAsync(u => u.Id == input.Id, false);
            if (!isExist) throw Oops.Oh(ErrorCode.D3000);

            var supplierExpressInfo = input.Adapt<SupplierExpressInfo>();
            await _supplierExpressInfoRep.UpdateAsync(supplierExpressInfo, ignoreNullValues: true);
        }

        /// <summary>
        /// 获取快递公司信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("detail")]
        public async Task<SupplierExpressInfoOutput> Get([FromQuery] QueryeSupplierExpressInfoInput input)
        {
            return (await _supplierExpressInfoRep.DetachedEntities.FirstOrDefaultAsync(u => u.Id == input.Id)).Adapt<SupplierExpressInfoOutput>();
        }

        /// <summary>
        /// 获取快递公司信息列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<List<SupplierExpressInfoOutput>> List([FromQuery] SupplierExpressInfoInput input)
        {
            return await _supplierExpressInfoRep.DetachedEntities.ProjectToType<SupplierExpressInfoOutput>().ToListAsync();
        }
    }
}
