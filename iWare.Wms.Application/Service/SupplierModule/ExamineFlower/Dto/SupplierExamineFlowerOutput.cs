﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 审核记录表输出参数
    /// </summary>
    public class SupplierExamineFlowerOutput
    {
        /// <summary>
        /// 单据ID
        /// </summary>
        public long DeliveryID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }

    /// <summary>
    /// 消息提醒输出参数
    /// </summary>
    public class MessageReminderOutput
    {
        /// <summary>
        /// 管理员类型
        /// </summary>
        public AdminType AdminTypeName { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 消息提醒数据
        /// </summary>
        public List<MessageReminderList> List { get; set; }
    }

    /// <summary>
    /// 消息提醒数据
    /// </summary>
    public class MessageReminderList
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 送货单Id
        /// </summary>
        public long DeliveryID { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryOrderNo { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 审核状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }

    /// <summary>
    /// 送货单审核消息提醒输出参数
    /// </summary>
    public class DeliveryMessageRemindOutput
    {
        /// <summary>
        /// 总数
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 消息明细
        /// </summary>
        public List<DeliveryMessageDetai> detais { get; set; }
    }

    /// <summary>
    /// 送货单审核消息提醒输出参数
    /// </summary>
    public class DeliveryMessageDetai
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        public string Remarks { get; set; }
    }

    /// <summary>
    /// 收货质检通知提醒输出参数
    /// </summary>
    public class CollectNoticeRemindOutput
    {
        /// <summary>
        /// 数量
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public List<string> Messages { get; set; }
    }
}
