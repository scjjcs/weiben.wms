﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 快递公司表查询参数
    /// </summary>
    public class SupplierExpressInfoSearch : PageInputBase
    {
        /// <summary>
        /// 送货单Id
        /// </summary>
        public virtual long? GoodsDeliveryId { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// 快递公司名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 快递公司代码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public virtual CommonStatus? Status { get; set; }
    }

    /// <summary>
    /// 快递公司表输入参数
    /// </summary>
    public class SupplierExpressInfoInput
    {
        /// <summary>
        /// 送货单Id
        /// </summary>
        public virtual long GoodsDeliveryId { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// 快递公司名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 快递公司代码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 公司地址
        /// </summary>
        public virtual string Address { get; set; }

        /// <summary>
        /// 运输车辆
        /// </summary>
        public virtual string TransportVehicle { get; set; }

        /// <summary>
        /// 司机姓名
        /// </summary>
        public virtual string DriverName { get; set; }

        /// <summary>
        /// 司机电话
        /// </summary>
        public virtual string DriverPhone { get; set; }

        /// <summary>
        /// 驾驶员资格
        /// </summary>
        public virtual string DriverQualification { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Remarks { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public virtual CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddSupplierExpressInfoInput : SupplierExpressInfoInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteSupplierExpressInfoInput : BaseId
    {
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateSupplierExpressInfoInput : SupplierExpressInfoInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeSupplierExpressInfoInput : BaseId
    {
    }
}
