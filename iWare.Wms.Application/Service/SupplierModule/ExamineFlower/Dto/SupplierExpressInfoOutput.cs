﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 快递公司表输出参数
    /// </summary>
    public class SupplierExpressInfoOutput
    {
        /// <summary>
        /// 送货单Id
        /// </summary>
        public long GoodsDeliveryId { get; set; }
        
        /// <summary>
        /// 联系电话
        /// </summary>
        public string Phone { get; set; }
        
        /// <summary>
        /// 快递公司名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 快递公司代码
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// 公司地址
        /// </summary>
        public string Address { get; set; }
        
        /// <summary>
        /// 运输车辆
        /// </summary>
        public string TransportVehicle { get; set; }
        
        /// <summary>
        /// 司机姓名
        /// </summary>
        public string DriverName { get; set; }
        
        /// <summary>
        /// 司机电话
        /// </summary>
        public string DriverPhone { get; set; }
        
        /// <summary>
        /// 驾驶员资格
        /// </summary>
        public string DriverQualification { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }
        
        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
