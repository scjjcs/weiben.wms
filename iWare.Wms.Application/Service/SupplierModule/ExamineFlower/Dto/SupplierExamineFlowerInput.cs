﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 审核记录表查询参数
    /// </summary>
    public class SupplierExamineFlowerSearch : PageInputBase
    {
        /// <summary>
        /// 单据ID
        /// </summary>
        public long DeliveryID { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum? Status { get; set; }
    }

    /// <summary>
    /// 审核记录表输入参数
    /// </summary>
    public class SupplierExamineFlowerInput
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        public long DeliveryID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddSupplierExamineFlowerInput : SupplierExamineFlowerInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteSupplierExamineFlowerInput : BaseId
    {
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateSupplierExamineFlowerInput : SupplierExamineFlowerInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeSupplierExamineFlowerInput : BaseId
    {
    }

    /// <summary>
    /// 读取消息输入参数
    /// </summary>
    public class ReadMessageInout
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public List<long> IdList { get; set; }
    }

    /// <summary>
    /// 收货质检通知读取输入参数
    /// </summary>
    public class CollectNoticeReadInput
    {
        /// <summary>
        /// Id
        /// </summary>
        public List<long> IdList { get; set; }

        ///// <summary>
        ///// 送货单号
        ///// </summary>
        //public string DeliveryNo { get; set; }

        ///// <summary>
        ///// 供应商
        ///// </summary>
        //public string SupperCode { get; set; }
    }

    /// <summary>
    /// 送货单消息读取输入参数
    /// </summary>
    public class DeliveryMessageReadInput
    {
        /// <summary>
        /// 供应商审核记录表Id
        /// </summary>
        public long Id { get; set; }
    }
}
