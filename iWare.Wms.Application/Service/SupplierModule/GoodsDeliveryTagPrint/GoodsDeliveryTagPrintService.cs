﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using iWare.Wms.Core;
using iWare.Wms.Core.Entity.Receiving;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 送货标签打印记录服务
    /// </summary>
    [Route("api/GoodsDeliveryTagPrint")]
    [ApiDescriptionSettings("供应商模块", Name = "GoodsDeliveryTagPrint", Order = 100)]

    public class GoodsDeliveryTagPrintService : IDynamicApiController, ITransient
    {
        private readonly IRepository<GoodsDelivery, MasterDbContextLocator> _goodsDeliveryRep; //送货单仓储
        private readonly IRepository<GoodsDeliveryTagPrint, MasterDbContextLocator> _goodsDeliveryTagPrintRep; //送货单标签打印仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="goodsDeliveryRep"></param>
        /// <param name="goodsDeliveryTagPrintRep"></param>
        public GoodsDeliveryTagPrintService(
            IRepository<GoodsDelivery, MasterDbContextLocator> goodsDeliveryRep,
            IRepository<GoodsDeliveryTagPrint, MasterDbContextLocator> goodsDeliveryTagPrintRep
        )
        {
            _goodsDeliveryRep = goodsDeliveryRep;
            _goodsDeliveryTagPrintRep = goodsDeliveryTagPrintRep;
        }
        #endregion

        /// <summary>
        /// 分页查询送货标签打印记录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("Page")]
        public async Task<PageResult<GoodsDeliveryTagPrintOutput>> Page([FromQuery] GoodsDeliveryTagPrintSearch input)
        {
            var list = await _goodsDeliveryTagPrintRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(input.ProjectCode), u => EF.Functions.Like(u.ProjectCode, $"%{input.ProjectCode.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.Code), u => EF.Functions.Like(u.Code, $"%{input.Code.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.Name), u => EF.Functions.Like(u.Name, $"%{input.Name.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.DeliveryNo), u => EF.Functions.Like(u.DeliveryNo, $"%{input.DeliveryNo.Trim()}%"))
                .OrderBy(PageInputOrder.OrderBuilder(input))
                .ProjectToType<GoodsDeliveryTagPrintOutput>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);

            foreach (var item in list.Rows)
            {
                InspectionMethodEnum aa = item.InspectionMethod;
                item.QRCode = ("{\"Name\":\"" + item.Name + "\",\"Code\":\"" + item.Code + "\",\"SpecificationModel\":\"" + item.SpecificationModel + "\",\"InspectionMethod\":" + (int)item.InspectionMethod + ",}");
            }
            return list;
        }

        /// <summary>
        /// 生成物料标签二维码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("GetMaterialTagCode")]
        public string GetMaterialTagCode([FromQuery] GoodsDeliveryMaterialCodeInput input)
        {
            return input.ProjectCode + "," + input.Code + "," + input.DeliveryNo + "," + input.Quantity + "," + input.XuHao;
        }
    }
}
