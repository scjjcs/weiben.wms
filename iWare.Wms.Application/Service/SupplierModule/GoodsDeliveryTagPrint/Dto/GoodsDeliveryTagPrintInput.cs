﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 送货标签打印记录查询参数
    /// </summary>
    public class GoodsDeliveryTagPrintSearch : PageInputBase
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 送货标签打印记录输入参数
    /// </summary>
    public class GoodsDeliveryTagPrintInput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string Describe { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 是否组盘
        /// </summary>
        public YesOrNot IsGroupDisk { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddGoodsDeliveryTagPrintInput
    {
        /// <summary>
        /// 送货单明细Id
        /// </summary>
        public List<long> GoodsDeliveryId { get; set; }
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteGoodsDeliveryTagPrintInput : BaseId
    {
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateWareMaterialTagInput : GoodsDeliveryTagPrintInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeGoodsDeliveryTagPrintInput : BaseId
    {
    }

    /// <summary>
    /// 送货单物料二维码输入参数
    /// </summary>
    public class GoodsDeliveryMaterialCodeInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Required(ErrorMessage = "项目编号不能为空")]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        [Required(ErrorMessage = "品号不能为空")]
        public string Code { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }
    }
}
