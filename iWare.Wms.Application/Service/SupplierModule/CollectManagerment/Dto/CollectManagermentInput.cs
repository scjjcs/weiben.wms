﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 收货列表输入参数
    /// </summary>
    public class CollectManagermentInput : PageInputBase
    {
        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }
    }
}
