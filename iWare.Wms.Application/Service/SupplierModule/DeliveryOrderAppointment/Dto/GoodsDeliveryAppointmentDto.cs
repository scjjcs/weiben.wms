﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 送货单预约输出参数
    /// </summary>
    public class GoodsDeliveryAppointmentDto
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        public long DeliveryID { get; set; }

        /// <summary>
        /// 预约单号
        /// </summary>
        public string AppointmentNo { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 运输公司
        /// </summary>
        public string TransportCompany { get; set; }

        /// <summary>
        /// 物流单号
        /// </summary>
        public string ExpressInfoNo { get; set; }

        /// <summary>
        /// 运输车辆
        /// </summary>
        public string TransportVehicle { get; set; }

        /// <summary>
        /// 司机姓名
        /// </summary>
        public string DriverName { get; set; }

        /// <summary>
        /// 驾驶员资格
        /// </summary>
        public string DriverQualification { get; set; }

        /// <summary>
        /// 司机电话
        /// </summary>
        public string DriverPhone { get; set; }

        /// <summary>
        /// 发货地址
        /// </summary>
        public string DeliverGoodsPlaceShip { get; set; }

        /// <summary>
        /// 发货仓库
        /// </summary>
        public string DeliverGoodsWarehouseCode { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string CollectDeliveryPlaceShip { get; set; }

        /// <summary>
        /// 收货仓库
        /// </summary>
        public string CollectDeliveryWarehouseCode { get; set; }

        /// <summary>
        /// 预计到达日期
        /// </summary>
        public DateTimeOffset EstimatedDate { get; set; }

        /// <summary>
        /// 发货日期
        /// </summary>
        public DateTimeOffset DeliverDate { get; set; }

        /// <summary>
        /// 收货人
        /// </summary>
        public string CollectDeliveryUserName { get; set; }

        /// <summary>
        /// 收货人电话
        /// </summary>
        public string CollectDeliveryUserPhone { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
