﻿using iWare.Wms.Core;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 送货预约查询参数
    /// </summary>
    public class GoodsDeliveryAppointmentSearch : PageInputBase
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        public long DeliveryID { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货/采购类型
        /// </summary>
        public string DeliveryType { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        public string SuppName { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; } = AuditStatusEnum.all;

        /// <summary>
        /// 是否加急
        /// </summary>
        public YesOrNot IsJiaJiJian { get; set; } = YesOrNot.All;
    }

    /// <summary>
    /// 送货预约输入参数
    /// </summary>
    public class GoodsDeliveryAppointmentInput
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        public long DeliveryID { get; set; }

        /// <summary>
        /// 预约单号
        /// </summary>
        public string AppointmentNo { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 运输公司
        /// </summary>
        public string TransportCompany { get; set; }

        /// <summary>
        /// 物流单号
        /// </summary>
        public string ExpressInfoNo { get; set; }

        /// <summary>
        /// 运输车辆
        /// </summary>
        public string TransportVehicle { get; set; }

        /// <summary>
        /// 司机姓名
        /// </summary>
        public string DriverName { get; set; }

        /// <summary>
        /// 驾驶员资格
        /// </summary>
        public string DriverQualification { get; set; }

        /// <summary>
        /// 司机电话
        /// </summary>
        public string DriverPhone { get; set; }

        /// <summary>
        /// 发货地址
        /// </summary>
        public string DeliverGoodsPlaceShip { get; set; }

        /// <summary>
        /// 发货仓库
        /// </summary>
        public string DeliverGoodsWarehouseCode { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string CollectDeliveryPlaceShip { get; set; }

        /// <summary>
        /// 收货仓库
        /// </summary>
        public string CollectDeliveryWarehouseCode { get; set; }

        /// <summary>
        /// 预计到达日期
        /// </summary>
        public DateTimeOffset EstimatedDate { get; set; }

        /// <summary>
        /// 发货日期
        /// </summary>
        public DateTimeOffset DeliverDate { get; set; }

        /// <summary>
        /// 收货人
        /// </summary>
        public string CollectDeliveryUserName { get; set; }

        /// <summary>
        /// 收货人电话
        /// </summary>
        public string CollectDeliveryUserPhone { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public List<string> Images { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddGoodsDeliveryAppointmentInput : GoodsDeliveryAppointmentInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 送货单Id
        /// </summary>
        public long DeliveryId { get; set; }
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteGoodsDeliveryAppointmentInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateGoodsDeliveryAppointmentInput : GoodsDeliveryAppointmentInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeGoodsDeliveryAppointmentInput
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        [Required(ErrorMessage = "送货单ID不能为空")]
        public long DeliveryID { get; set; }
    }

    /// <summary>
    /// 提交输入参数
    /// </summary>
    public class SubmitGoodsDeliveryAppointmentInput
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        [Required(ErrorMessage = "送货单ID不能为空")]
        public long DeliveryID { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        [Required(ErrorMessage = "送货单号不能为空")]
        public string DeliveryNo { get; set; }
    }

    /// <summary>
    /// 驳回输入参数
    /// </summary>
    public class RejectGoodsDeliveryAppointmentInput
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        [Required(ErrorMessage = "送货单ID不能为空")]
        public long DeliveryID { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        [Required(ErrorMessage = "送货单号不能为空")]
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 驳回备注
        /// </summary>
        [Required(ErrorMessage = "驳回备注不能为空")]
        public string Remarks { get; set; }

        /// <summary>
        /// 送货预约状态
        /// </summary>
        public string Status { get; set; }
    }

    /// <summary>
    /// 根据采购单号获取所有的送货预约输入参数
    /// </summary>
    public class PurchaseNoGetGoodsDeliveryAppointment : PageInputBase
    {
        /// <summary>
        /// 采购单号不能为空
        /// </summary>
        [Required(ErrorMessage = "采购单号不能为空")]
        public string PurchaseNo { get; set; }
    }

    /// <summary>
    /// 供应商审核记录输入参数
    /// </summary>
    public class SupplierExamineFlowerByAppointmentInput
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        [Required(ErrorMessage = "送货单ID不能为空")]
        public long DeliveryID { get; set; }
    }
}
