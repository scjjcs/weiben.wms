﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 采购订单输出参数
    /// </summary>
    public class PurchaseOrderDto
    {
        /// <summary>
        /// 采购单别
        /// </summary>
        public string PurchaseType { get; set; }
        
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }
        
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }
        
        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }
        
        /// <summary>
        /// 采购日期
        /// </summary>
        public DateTimeOffset PurchaseDate { get; set; }
        
        /// <summary>
        /// 采购人ID
        /// </summary>
        public string PurchaserUserId { get; set; }
        
        /// <summary>
        /// 数量合计
        /// </summary>
        public decimal PurchaseNumber { get; set; }
        
        /// <summary>
        /// 收货地址
        /// </summary>
        public string Address { get; set; }
        
        /// <summary>
        /// 工厂
        /// </summary>
        public string Factory { get; set; }
        
        /// <summary>
        /// 参数一
        /// </summary>
        public string Parameter1 { get; set; }
        
        /// <summary>
        /// 参数二
        /// </summary>
        public string Parameter2 { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        
        /// <summary>
        /// 状态-撤销或者删除_-1、未进行_0、进行中_1、完成_2
        /// </summary>
        public PurchaseStatusEnum Status { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
