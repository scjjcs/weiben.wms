﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 采购订单详情查询参数
    /// </summary>
    public class PurchaseOrderDetailsSearch : PageInputBase
    {
        /// <summary>
        /// 采购订单主表ID
        /// </summary>
        public long PurchaseOrderId { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 采购单别
        /// </summary>
        public string PurchaseType { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseOrderNo { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 采购订单详情输入参数
    /// </summary>
    public class PurchaseOrderDetailsInput : PageInputBase
    {
        /// <summary>
        /// 采购订单主表ID
        /// </summary>
        public long PurchaseID { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string PurchaseName { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string PurchaseCode { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddPurchaseOrderDetailsInput : PurchaseOrderDetailsInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeletePurchaseOrderDetailsInput : BaseId
    {
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdatePurchaseOrderDetailsInput : PurchaseOrderDetailsInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryePurchaseOrderDetailsInput : BaseId
    {
    }
}
