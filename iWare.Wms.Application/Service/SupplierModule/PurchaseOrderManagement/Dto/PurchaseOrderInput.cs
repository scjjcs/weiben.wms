﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 采购订单查询参数
    /// </summary>
    public class PurchaseOrderSearch : PageInputBase
    {
        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 采购状态-撤销或者删除_-1、未进行_0、进行中_1、完成_2
        /// </summary>
        public PurchaseStatusEnum Status { get; set; } = PurchaseStatusEnum.All;

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 是否加急
        /// </summary>
        public YesOrNot IsJiaJiJian { get; set; } = YesOrNot.All;
    }

    /// <summary>
    /// 采购订单输入参数
    /// </summary>
    public class PurchaseOrderInput
    {
        /// <summary>
        /// 采购单别
        /// </summary>
        public string PurchaseType { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 采购日期
        /// </summary>
        public DateTimeOffset PurchaseDate { get; set; }

        /// <summary>
        /// 采购人ID
        /// </summary>
        public string PurchaserUserId { get; set; }

        /// <summary>
        /// 数量合计
        /// </summary>
        public decimal PurchaseNumber { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 工厂
        /// </summary>
        public string Factory { get; set; }

        /// <summary>
        /// 参数一
        /// </summary>
        public string Parameter1 { get; set; }

        /// <summary>
        /// 参数二
        /// </summary>
        public string Parameter2 { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 状态-撤销或者删除_-1、未进行_0、进行中_1、完成_2
        /// </summary>
        public PurchaseStatusEnum Status { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddPurchaseOrderInput : PurchaseOrderInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeletePurchaseOrderInput : BaseId
    {
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdatePurchaseOrderInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryePurchaseOrderInput : BaseId
    {
    }

    /// <summary>
    /// 获取采购单详情输入参数
    /// </summary>
    public class GetPurchaseOrderDetailsInput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string TC047 { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string TC002 { get; set; }
    }

    /// <summary>
    /// 获取订单号下送货单以及送货单详情输入参数
    /// </summary>
    public class PurchaseIdGetGoodsDeliveryInput : PageInputBase
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long PurchaseOrderId { get; set; }
    }
}
