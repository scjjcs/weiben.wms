﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 加急件输入参数
    /// </summary>
    public class PurchaseUrgentItemInput : PageInputBase
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 创建非标件采购单输入参数
    /// </summary>
    public class AddPurchaseForJiaJiJianInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 加急件物料明细
        /// </summary>
        public List<WareMaterialJiaJiJianInput> MaterialJiaJiJians { get; set; }
    }

    /// <summary>
    /// 加急件物料明细输入参数
    /// </summary>
    public class WareMaterialJiaJiJianInput
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 送货总数 
        /// </summary>
        public decimal deliveryQuantity { get; set; }
    }
}
