﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 供应商基本信息查询参数
    /// </summary>
    public class SupplierInfoSearch : PageInputBase
    {
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 简称
        /// </summary>
        public string AbbreviationName { get; set; }

        /// <summary>
        /// 公司全称
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 供应商基本信息输入参数
    /// </summary>
    public class SupplierInfoInput
    {
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 简称
        /// </summary>
        public string AbbreviationName { get; set; }

        /// <summary>
        /// 公司全称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// TEL(一)
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// 负责人
        /// </summary>
        public string LeadingCadre { get; set; }

        /// <summary>
        /// 联系人(一)
        /// </summary>
        public string Contacts { get; set; }

        /// <summary>
        /// 联系地址(一)
        /// </summary>
        public string ContactAddress { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddSupplierInfoInput : SupplierInfoInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteSupplierInfoInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "Id不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateSupplierInfoInput : SupplierInfoInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeSupplierInfoInput : BaseId
    {
    }
}
