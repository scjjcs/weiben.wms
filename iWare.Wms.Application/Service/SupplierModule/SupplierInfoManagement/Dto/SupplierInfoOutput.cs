﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 供应商基本信息输出参数
    /// </summary>
    public class SupplierInfoOutput
    {
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 简称
        /// </summary>
        public string AbbreviationName { get; set; }

        /// <summary>
        /// 公司全称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// TEL(一)
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// 负责人
        /// </summary>
        public string LeadingCadre { get; set; }

        /// <summary>
        /// 联系人(一)
        /// </summary>
        public string Contacts { get; set; }

        /// <summary>
        /// 联系地址(一)
        /// </summary>
        public string ContactAddress { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
