﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using iWare.Wms.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 供应商基本信息服务
    /// </summary>
    [Route("api/SupplierInfo")]
    [ApiDescriptionSettings("供应商模块", Name = "SupplierInfo", Order = 100)]

    public class SupplierInfoService : IDynamicApiController, ITransient
    {
        private readonly IRepository<SupplierInfo, MasterDbContextLocator> _supplierInfoRep; //供应商基本信息仓储

        #region 默认
        /// <summary>
        /// 默认
        /// </summary>
        /// <param name="supplierInfoRep"></param>
        public SupplierInfoService(
            IRepository<SupplierInfo, MasterDbContextLocator> supplierInfoRep
        )
        {
            _supplierInfoRep = supplierInfoRep;
        }
        #endregion

        /// <summary>
        /// 分页查询供应商基本信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("Page")]
        public async Task<PageResult<SupplierInfoOutput>> Page([FromQuery] SupplierInfoSearch input)
        {
            return await _supplierInfoRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(input.Code), u => EF.Functions.Like(u.Code, $"%{input.Code.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.AbbreviationName), u => EF.Functions.Like(u.AbbreviationName, $"%{input.AbbreviationName.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.Name), u => EF.Functions.Like(u.Name, $"%{input.Name.Trim()}%"))
                .OrderBy(PageInputOrder.OrderBuilder(input))
                .ProjectToType<SupplierInfoOutput>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);
        }

        /// <summary>
        /// 新增供应商基本信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("Add")]
        public async Task Add(AddSupplierInfoInput input)
        {
            var isExist = await _supplierInfoRep.DetachedEntities
                .AnyAsync(u => u.Code.Equals(input.Code));
            if (isExist) throw Oops.Oh("数据已存在");

            var supplierInfo = input.Adapt<SupplierInfo>();
            await _supplierInfoRep.InsertAsync(supplierInfo);
        }

        /// <summary>
        /// 删除供应商基本信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task Delete(DeleteSupplierInfoInput input)
        {
            int length = input.Id.Count;
            for (int i = 0; i < length; i++)
            {
                var isExist = await _supplierInfoRep
                    .AnyAsync(u => u.Id == input.Id[i], false);
                if (!isExist) if (isExist) throw Oops.Oh("数据不存在");

                var supplierInfo = await _supplierInfoRep
                    .FirstOrDefaultAsync(u => u.Id == input.Id[i]);
                await _supplierInfoRep.DeleteAsync(supplierInfo);
            }
        }

        /// <summary>
        /// 更新供应商基本信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("Edit")]
        public async Task Update(UpdateSupplierInfoInput input)
        {
            var isExist = await _supplierInfoRep
                .AnyAsync(u => u.Id == input.Id, false);
            if (!isExist) throw Oops.Oh("数据不存在");

            var supplierInfo = input.Adapt<SupplierInfo>();
            await _supplierInfoRep.UpdateAsync(supplierInfo, ignoreNullValues: true);
        }
    }
}
