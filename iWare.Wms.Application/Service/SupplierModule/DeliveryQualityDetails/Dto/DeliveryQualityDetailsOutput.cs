﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 送货质检明细输出参数
    /// </summary>
    public class DeliveryQualityDetailsOutput
    {
        /// <summary>
        /// 收货单明细Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 采购类型
        /// </summary>
        public string PurchaseType { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 收货单Id
        /// </summary>
        public long CollectDeliveryId { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量 
        /// </summary>
        public decimal CollectQuantity { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 是否入库
        /// </summary>
        public YesOrNot IsWarehousing { get; set; }

        /// <summary>
        /// 是否质检
        /// </summary>
        public YesOrNot IsQuality { get; set; }

        /// <summary>
        /// 是否合格
        /// </summary>
        public YesOrNot IsQualified { get; set; }

        /// <summary>
        /// 质检结果
        /// </summary>
        public QualityEnum QualityResult { get; set; }

        /// <summary>
        /// 质检结果数量
        /// </summary>
        public int QualityNumber { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public string CreatedUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新者名称
        /// </summary>
        public string UpdatedUserName { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 供应商是否已处理
        /// </summary>
        public YesOrNot IsSupperHandle { get; set; }

        /// <summary>
        /// 收货单信息
        /// </summary>
        public CollectDelivery CollectDelivery { get; set; }
    }
}
