﻿namespace iWare.Wms.Application
{
    /// <summary>
    /// 本周送货量输出参数
    /// </summary>
    public class DeliveryWeekAndMonthOutput
    {
        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 执行日期
        /// </summary>
        public object dateStr { get; set; }

    }

    /// <summary>
    /// 本年送货量输出参数
    /// </summary>
    public class DeliveryYearOutput
    {
        /// <summary>
        /// 几月份
        /// </summary>
        public string Month { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public List<decimal> Quantitys { get; set; }
    }

    /// <summary>
    /// 本月送货率输出参
    /// </summary>
    public class DeliveryMonthOutput
    {
        /// <summary>
        /// 状态名称
        /// </summary>
        public object Name { get; set; }

        /// <summary>
        /// 状态值
        /// </summary>
        public object Value { get; set; } = 0;
    }
}
