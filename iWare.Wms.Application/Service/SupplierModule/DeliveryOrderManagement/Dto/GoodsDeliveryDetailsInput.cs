﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 送货单明细查询参数
    /// </summary>
    public class GoodsDeliveryDetailsSearch : PageInputBase
    {
        /// <summary>
        /// 送货单主表Id
        /// </summary>
        public long? GoodsDeliveryId { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 送货单明细输入参数
    /// </summary>
    public class GoodsDeliveryDetailsInput
    {
        /// <summary>
        /// 送货单主表Id
        /// </summary>
        public long GoodsDeliveryId { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal DeliveryQuantity { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        public string Parameter { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddGoodsDeliveryDetailsInput : GoodsDeliveryDetailsInput
    {
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteGoodsDeliveryDetailsInput : BaseId
    {
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateGoodsDeliveryDetailsInput : GoodsDeliveryDetailsInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 根据送货单id修改详情输入参数
    /// </summary>
    public class QueryeGoodsDeliveryDetailsInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 送货单Id
        /// </summary>
        [Required(ErrorMessage = "送货单Id不能为空")]
        public long GoodsDeliveryId { get; set; }

        /// <summary>
        /// 送货单详情
        /// </summary>
        public virtual List<GoodsDeliveryDetailsInput> GoodsDeliveryDetailsInputList { get; set; }
    }
}
