﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 送货单明细输出参数
    /// </summary>
    public class GoodsDeliveryDetailsOutput
    {
        /// <summary>
        /// 送货单主表Id
        /// </summary>
        public long GoodsDeliveryId { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal DeliveryQuantity { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        public string Parameter { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 采购单送货数量
        /// </summary>
        public decimal PurchaseNumber { get; set; }

        /// <summary>
        /// 送货单已经送货数量
        /// </summary>
        public decimal OutdeliveryQuantity { get; set; }

        /// <summary>
        /// 是否勾选
        /// </summary>
        public bool IsCheck { get; set; }
 
        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 日期
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 未配送数
        /// </summary>
        public decimal NotPurchaseNumber { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 是否结束-N:未结束、Y:自动结束、y:指定结束
        /// </summary>
        public string IsEnd { get; set; }

        /// <summary>
        /// 送货单主表信息
        /// </summary>
        public GoodsDelivery GoodsDelivery { get; set; }
    }
}
