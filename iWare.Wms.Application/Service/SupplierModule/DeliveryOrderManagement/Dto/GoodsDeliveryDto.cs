﻿using iWare.Wms.Core;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 送货单输出参数
    /// </summary>
    public class GoodsDeliveryDto
    {
        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }
        
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 送货单别
        /// </summary>
        public string DeliveryType { get; set; }
        
        /// <summary>
        /// 数量
        /// </summary>
        public decimal DeliveryQuantityTotal { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        
        /// <summary>
        /// 审核状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SuppCode { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SuppName { get; set; }
        
        /// <summary>
        /// 联系电话
        /// </summary>
        public string SuppPhone { get; set; }
        
        /// <summary>
        /// 详细地址
        /// </summary>
        public string SuppAddress { get; set; }
        
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }
        
        /// <summary>
        /// 收货单位
        /// </summary>
        public string ReceName { get; set; }
        
        /// <summary>
        /// 收货地址
        /// </summary>
        public string ReceAddress { get; set; }
        
        /// <summary>
        /// 联系电话
        /// </summary>
        public string RecePhone { get; set; }
        
        /// <summary>
        /// 采购联系人
        /// </summary>
        public string ReceUser { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
