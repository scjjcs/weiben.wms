﻿using iWare.Wms.Core;
using iWare.Wms.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.Application
{
    /// <summary>
    /// 送货单查询参数
    /// </summary>
    public class GoodsDeliverySearch : PageInputBase
    {
        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 审核状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; } = AuditStatusEnum.all;

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 是否加急
        /// </summary>
        public YesOrNot IsJiaJiJian { get; set; } = YesOrNot.All;
    }

    /// <summary>
    /// 送货单输入参数
    /// </summary>
    public class GoodsDeliveryInput
    {
        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 送货单别
        /// </summary>
        public string DeliveryType { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal DeliveryQuantity { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 审核状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        public AuditStatusEnum Status { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SuppCode { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SuppName { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string SuppPhone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string SuppAddress { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 收货单位
        /// </summary>
        public string ReceName { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string ReceAddress { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string RecePhone { get; set; }

        /// <summary>
        /// 采购联系人
        /// </summary>
        public string ReceUser { get; set; }
    }

    /// <summary>
    /// 新增输入参数
    /// </summary>
    public class AddGoodsDeliveryInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public virtual string DeliveryNo { get; set; }

        /// <summary>
        /// 送货单类型
        /// </summary>
        public virtual string DeliveryType { get; set; }

        /// <summary>
        /// 采购单ID   
        /// </summary>
        public virtual long PurchaseID { get; set; }

        /// <summary>
        /// 送货单详情
        /// </summary>
        public virtual List<GoodsDeliveryDetailsInput> GoodsDeliveryDetailsInputList { get; set; }
    }

    /// <summary>
    /// 删除输入参数
    /// </summary>
    public class DeleteGoodsDeliveryInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }
    }

    /// <summary>
    /// 更新输入参数
    /// </summary>
    public class UpdateGoodsDeliveryInput : GoodsDeliveryInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 送货单详情
        /// </summary>
        public virtual List<GoodsDeliveryDetailsInput> GoodsDeliveryDetailsInputList { get; set; }
    }

    /// <summary>
    /// 查询输入参数
    /// </summary>
    public class QueryeGoodsDeliveryInput : BaseId
    {

    }

    /// <summary>
    /// 获取送货输入参数
    /// </summary>
    public class GetGoodsDeliveryNoInput
    {
        /// <summary>
        /// 采购单ID   
        /// </summary>
        public virtual long PurchaseId { get; set; }
    }

    /// <summary>
    /// 获取送货详情输入参数
    /// </summary>
    public class GetGoodsDeliveryDetailsInput : PageInputBase
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 送货单打印输入参数
    /// </summary>
    public class PrintGoodsDeliveryInput
    {
        /// <summary>
        /// 送货单ID
        /// </summary>
        public long DeliveryID { get; set; }
    }

    /// <summary>
    /// 测试数据输入参数
    /// </summary>
    public class TestAddGoodsDeliveryInput
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }
    }
}
