using Furion.DatabaseAccessor;
using iWare.Wms.WorkerService.DbContexts;
using iWare.Wms.WorkerService.ThirdDB;
using Microsoft.AspNetCore.Builder;

namespace iWare.Wms.WorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).UseWindowsService().Inject()
                .ConfigureServices((hostContext, services) =>
                {
                    //注册任务的服务
                    //services.AddHostedService<WorkerLog>();

                    //立体库任务服务
                    //services.AddHostedService<TaskLiWorker>();

                    //ERP任务服务
                    //services.AddHostedService<MaterialERP>();
                    //services.AddHostedService<MaterialWarehouseERP>();
                    //services.AddHostedService<OrderERP>();
                    ////services.AddHostedService<ProjectERP>();
                    //services.AddHostedService<PurchaseERP>();
                    //services.AddHostedService<PurchaseStatusERP>();
                    //services.AddHostedService<SupplierInfoERP>();

                    //services.AddHostedService<WorkerStereoscopic>();
                    // 注册数据库服务
                    services.AddDatabaseAccessor(options =>
                    {
                        options.AddDb<DefaultDbContext>();
                        options.AddDbPool<ERPDbContext, ERPDbContextLocator>(DbProvider.SqlServer);
                        options.AddDbPool<StereoscopicDbContext, StereoscopicDbContextLocation>(DbProvider.SqlServer);
                    });

                });
    }
}

