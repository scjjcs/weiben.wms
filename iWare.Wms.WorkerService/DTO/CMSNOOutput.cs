﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iWare.Wms.WorkerService.DTO
{
    /// <summary>
    /// 项目管理
    /// </summary>
    public class CMSNOOutput
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string NO001 { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string NO002 { get; set; }

        /// <summary>
        /// 项目说明
        /// </summary>
        public string NO003 { get; set; }
    }
}
