﻿using System.ComponentModel;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 库位与容器关系状态枚举
    /// <summary>
    /// 库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1
    /// </summary>
    public enum LocationStatusEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        all = -10,

        /// <summary>
        /// 待入
        /// </summary>
        [Description("待入")]
        dairu = 1,

        /// <summary>
        /// 存货
        /// </summary>
        [Description("存货")]
        cunhuo = 2,

        /// <summary>
        /// 待出
        /// </summary>
        [Description("待出")]
        daichu = 3,

        /// <summary>
        /// 空闲
        /// </summary>
        [Description("空闲")]
        kongxian = 0,

        /// <summary>
        /// 禁用
        /// </summary>
        [Description("禁用")]
        jinyong = -1,
    }
}
