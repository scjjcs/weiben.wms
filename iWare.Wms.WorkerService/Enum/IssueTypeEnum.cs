﻿using System.ComponentModel;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 单据类型枚举
    /// </summary>
    /// 单据类型枚举-调拨单_0、手动单_1
    public enum IssueTypeEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        ALL = -10,

        /// <summary>
        /// 调拨单
        /// </summary>
        [Description("调拨单")]
        DIAOBO = 0,

        /// <summary>
        /// 手动单
        /// </summary>
        [Description("手动单")]
        SHOUDONG = 1,
    }
}
