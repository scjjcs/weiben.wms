﻿using System.ComponentModel;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 容器状态枚举
    /// <summary>
    /// 容器状态-空闲_0、使用_1、禁用_-1
    public enum ContainerStatusEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        all = -10,

        /// <summary>
        /// 空闲
        /// </summary>
        [Description("空闲")]
        kongxian = 0,

        /// <summary>
        /// 组盘
        /// </summary>
        [Description("组盘")]
        zupan = 1,

        /// <summary>
        /// 库位
        /// </summary>
        [Description("库位")]
        kuwei = 2,

        /// <summary>
        /// 分拣
        /// </summary>
        [Description("分拣")]
        fenjian = 3,

        /// <summary>
        /// 禁用
        /// </summary>
        [Description("禁用")]
        jinyong = -1,
    }
}
