﻿using System.ComponentModel;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 审核状态枚举
    /// <summary>
    /// 审核状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
    public enum AuditStatusEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        all = -10,

        /// <summary>
        /// 预约中
        /// </summary>
        [Description("预约中")]
        yuyuezhong = 0,

        /// <summary>
        /// 预约成功
        /// </summary>
        [Description("预约成功")]
        yuyuechenggong = 1,

        /// <summary>
        /// 审核中
        /// </summary>
        [Description("审核中")]
        shenhezhong = 2,

        /// <summary>
        /// 驳回
        /// </summary>
        [Description("驳回")]
        bohui = 3,

        /// <summary>
        /// 审核通过
        /// </summary>
        [Description("审核通过")]
        shenhetongguo = 4,

        /// <summary>
        /// 送货中
        /// </summary>
        [Description("送货中")]
        songhuozhong = 5,

        /// <summary>
        /// 送货完成
        /// </summary>
        [Description("送货完成")]
        songhuowancheng = 6,

        /// <summary>
        /// 强制完成
        /// </summary>
        [Description("强制完成")]
        qiangzhiwancheng = 7,
    }
}
