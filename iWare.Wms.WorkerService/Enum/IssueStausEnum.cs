﻿using System.ComponentModel;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 单据状态枚举
    /// </summary>
    /// 单据状态枚举-未执行_0、执行中_1、已完成_2
    public enum IssueStausEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        ALL = -10,

        /// <summary>
        /// 未执行
        /// </summary>
        [Description("未执行")]
        NOTZHIXING = 0,

        /// <summary>
        /// 执行中
        /// </summary>
        [Description("执行中")]
        ZHIXINGZHONG = 1,

        /// <summary>
        /// 已完成
        /// </summary>
        [Description("已完成")]
        WANCHENG = 2,
    }
}
