﻿using System.ComponentModel;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 物料库存周期
    /// </summary>
    public enum StockCircleEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        ALL = -10,

        /// <summary>
        /// 绿
        /// </summary>
        [Description("绿")]
        GREEN = 1,

        /// <summary>
        /// 黄
        /// </summary>
        [Description("黄")]
        YELLOW = 2,

        /// <summary>
        /// 红
        /// </summary>
        [Description("红")]
        RED = 3,
    }
}
