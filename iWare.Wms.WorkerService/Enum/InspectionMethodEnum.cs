﻿using System.ComponentModel;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
    /// </summary>
    public enum InspectionMethodEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        all = -10,

        /// <summary>
        /// 免检
        /// </summary>
        [Description("免检")]
        mianjian = 0,

        /// <summary>
        /// 抽检(减量)
        /// </summary>
        [Description("抽检(减量)")]
        choujian_jianliang = 1,

        /// <summary>
        /// 抽检(正常)
        /// </summary>
        [Description("抽检(正常)")]
        choujian_zhengchang = 2,

        /// <summary>
        /// 抽检(加严)
        /// </summary>
        [Description("抽检(加严)")]
        choujian_jiayan = 3,

        /// <summary>
        /// 全检
        /// </summary>
        [Description("全检")]
        quanjian = 4,
    }
}
