﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 采购订单-来源ERP
    /// </summary>
    [Table("purchase_order")]
    [Comment("采购订单")]
    public class PurchaseOrder : DEntityBase, IEntityTypeBuilder<PurchaseOrder>
    {
        /// <summary>
        /// 采购单别
        /// </summary>
        [Comment("采购单别")]
        [MaxLength(50)]
        public string? PurchaseType { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string? ProjectCode { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        [Comment("供应商编号")]
        [MaxLength(50)]
        public string? SupplierInfoCode { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        [Comment("采购单号")]
        [MaxLength(50)]
        public string? PurchaseNo { get; set; }

        /// <summary>
        /// 采购日期
        /// </summary>
        [Comment("采购日期")]
        public DateTimeOffset PurchaseDate { get; set; }

        /// <summary>
        /// 采购人ID
        /// </summary>
        [Comment("采购人ID")]
        [MaxLength(50)]
        public string? PurchaserUserId { get; set; }

        /// <summary>
        /// 数量合计
        /// </summary>
        [Comment("数量合计")]
        public decimal PurchaseNumber { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        [Comment("收货地址")]
        [MaxLength(250)]
        public string? Address { get; set; }

        /// <summary>
        /// 工厂
        /// </summary>
        [Comment("工厂")]
        [MaxLength(250)]
        public string? Factory { get; set; }

        /// <summary>
        /// 参数一
        /// </summary>
        [Comment("参数一")]
        [MaxLength(200)]
        public string? Parameter1 { get; set; }

        /// <summary>
        /// 参数二
        /// </summary>
        [Comment("参数二")]
        [MaxLength(200)]
        public string? Parameter2 { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string? Remark { get; set; }

        /// <summary>
        /// 状态-撤销或者删除_-1、未进行_0、进行中_1、完成_2
        /// </summary>
        [Comment("状态-撤销或者删除_-1、未进行_0、进行中_1、完成_2")]
        public PurchaseStatusEnum Status { get; set; } = PurchaseStatusEnum.NotProgress;

        /// <summary>
        /// 是否加急件
        /// </summary>
        [Comment("是否加急件")]
        public YesOrNot IsJiaJiJian { get; set; } = YesOrNot.N;

        /// <summary>
        /// 一对多中间表（采购-送货）
        /// </summary>
        public ICollection<PurchaseOrderVsGoodsDelivery>? PurchaseOrderVsGoodsDeliverys { get; set; }

        /// <summary>
        /// 一对一配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<PurchaseOrder> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder.HasMany(x => x.PurchaseOrderVsGoodsDeliverys)
                .WithOne(x => x.PurchaseOrder)
                .HasForeignKey(x => x.PurchaseOrderId);
        }
    }
}