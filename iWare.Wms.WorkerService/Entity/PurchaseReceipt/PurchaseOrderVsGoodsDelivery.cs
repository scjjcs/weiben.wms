﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 采购单与送货单的关系
    /// </summary>
    [Table("purchaseorder_vs_goodsdelivery")]
    [Comment("采购单与送货单的关系")]
    public class PurchaseOrderVsGoodsDelivery : DEntityBase
    {
        /// <summary>
        /// 采购单表Id
        /// </summary>
        [Comment("采购单表Id")]
        public long PurchaseOrderId { get; set; }

        /// <summary>
        /// 送货单Id
        /// </summary>
        [Comment("送货单Id")]
        public long GoodsDeliveryId { get; set; }

        /// <summary>
        /// 一对一引用（采购单表）
        /// </summary>
        public PurchaseOrder? PurchaseOrder { get; set; }

        /// <summary>
        /// 一对一引用（送货单表）
        /// </summary>
        public GoodsDelivery? GoodsDelivery { get; set; }
    }
}
