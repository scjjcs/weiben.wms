﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 收货单
    /// </summary>
    [Table("collect_delivery")]
    [Comment("收货单")]
    public class CollectDelivery : DEntityBase, IEntityTypeBuilder<CollectDelivery>
    {
        // <summary>
        // 收货单号
        // </summary>
        [Comment("收货单号")]
        [MaxLength(50)]
        public string? CollectNo { get; set; }

        /// <summary>
        /// 收货单别
        /// </summary>
        [Comment("收货单别")]
        [MaxLength(50)]
        public string? CollectType { get; set; }

        // <summary>
        // 送货单号
        // </summary>
        [Comment("送货单号")]
        [MaxLength(50)]
        public string? DeliveryNo { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string? ProjectCode { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        [Comment("供应商编号")]
        [MaxLength(50)]
        public string? SupplierInfoCode { get; set; }

        /// <summary>
        /// 数量 
        /// </summary>
        [Comment("数量")]
        public decimal CollectQuantityTotal { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        [Comment("照片")]
        [MaxLength(500)]
        public string? Images { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string? Remark { get; set; }

        /// <summary>
        /// 是否收货完成-否_0、是_1
        /// </summary>
        [Comment("是否收货完成-否_0、是_1")]
        public YesOrNot IsComplete { get; set; } = YesOrNot.N;

        /// <summary>
        /// 收货单明细信息
        /// </summary>
        public ICollection<CollectDeliveryDetails>? CollectDeliveryDetailss { get; set; }

        /// <summary>
        /// 一对多中间表（送货单与收货单的关系）
        /// </summary>
        public ICollection<GoodsDeliveryVsCollectDelivery>? GoodsDeliveryVsCollectDeliverys { get; set; }

        /// <summary>
        /// 一对多配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<CollectDelivery> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder.HasMany(x => x.CollectDeliveryDetailss)
                .WithOne(x => x.CollectDelivery)
                .HasForeignKey(x => x.CollectDeliveryId);

            entityBuilder.HasMany(x => x.GoodsDeliveryVsCollectDeliverys)
                .WithOne(x => x.CollectDelivery)
                .HasForeignKey(x => x.CollectDeliveryId);
        }
    }
}
