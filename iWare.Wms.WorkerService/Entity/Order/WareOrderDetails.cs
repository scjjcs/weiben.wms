﻿
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 单据明细表
    /// </summary>
    [Comment("单据明细表")]
    [Table("ware_order_details")]
    public class WareOrderDetails:DEntityBase
    {
        /// <summary>
        /// 单据ID  
        /// </summary>
        [Comment("单据ID")]
        [MaxLength(50)]
        public long OrderId { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        [Comment("容器编号")]
        [MaxLength(50)]
        public string? ContainerCode { get; set; }

        /// <summary>
        /// 库位编号    
        /// </summary>
        [Comment("库位编号")]
        [MaxLength(50)]
        public string? LocationCode { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string? ProjectCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [Comment("品牌名称")]
        [MaxLength(200)]
        public string? BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Comment("序号")]
        [MaxLength(50)]
        public string? XuHao { get; set; }

        /// <summary>
        /// 品号  
        /// </summary>
        [Comment("品号")]
        [MaxLength(50)]
        public string? Code { get; set; }

        /// <summary>
        /// 品名  
        /// </summary>
        [Comment("品名")]
        [MaxLength(150)]
        public string? Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        [Comment("规格")]
        [MaxLength(200)]
        public string? SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        [Comment("单位")]
        [MaxLength(50)]
        public string? Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4    
        /// </summary>
        [Comment("检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4")]
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单据数    
        /// </summary>
        [Comment("单据数")]
        public decimal OrderQuantity { get; set; }

        /// <summary>
        /// 下发数    
        /// </summary>
        [Comment("下发数")]
        public decimal ActualQuantity { get; set; }

        /// <summary>
        /// 备注    
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string? Remarks { get; set; }

        /// <summary>
        /// 单别  ----ERP对应的单别
        /// </summary>
        [Comment("单别")]
        [MaxLength(50)]
        public string? SingleCategory { get; set; }

        /// <summary>
        /// 转入库  ----ERP对应的转入库    
        /// </summary>
        [Comment("转入库")]
        [MaxLength(50)]
        public string? TransferToReceipt { get; set; }

        /// <summary>
        /// 转出库  ----ERP对应的转出库    
        /// </summary>
        [Comment("转出库")]
        [MaxLength(50)]
        public string? TransferToIssue { get; set; }

        /// 转入库位  ----ERP对应的转入库位   
        /// </summary>
        [Comment("转入库位")]
        [MaxLength(50)]
        public string? TransferredILocation { get; set; }

        /// 转出库位  ----ERP对应的转出库位    
        /// </summary>
        [Comment("转出库位")]
        [MaxLength(50)]
        public string? TransferOutLocation { get; set; }

        #region 新增 20230406
        /// <summary>
        /// 参数
        /// </summary>
        [Comment("参数")]
        [MaxLength(255)]
        public string? OrderParameter { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(255)]
        public string? OrderRemark { get; set; }
        #endregion

        /// <summary>
        /// 单据主表
        /// </summary>
        public WareOrder? WareOrder { get; set; }
    }
}
