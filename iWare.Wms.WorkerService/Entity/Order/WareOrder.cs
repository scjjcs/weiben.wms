﻿using Furion.DatabaseAccessor;
using iWare.Wms.WorkerService;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 单据
    /// </summary>
    [Comment("单据表")]
    [Table("ware_order")]
    public class WareOrder : DEntityBase, IEntityTypeBuilder<WareOrder>
    {
        /// <summary>
        /// 出库单据  
        /// </summary>
        [Comment("出库单据")]
        [MaxLength(50)]
        public string? OrderNo { get; set; }

        /// <summary>
        /// 单别
        /// </summary>
        [Comment("单别")]
        [MaxLength(50)]
        public string? OrderCategory { get; set; }

        /// <summary>
        /// 出库单据类型
        /// </summary>
        [Comment("出库单据类型-调拨单_0、手动单_1")]
        public IssueTypeEnum OrderType { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string? ProjectCode { get; set; }

        /// <summary>
        /// 单据总数    
        /// </summary>
        [Comment("单据总数")]
        public decimal OrderQuantityTotal { get; set; }

        /// <summary>
        /// 备注    
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string? Remarks { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        [Comment("单据状态")]
        public IssueStausEnum OrderStatus { get; set; } = IssueStausEnum.NOTZHIXING;

        #region 新增 20230406
        /// <summary>
        /// 部门编号
        /// </summary>
        [Comment("部门编号")]
        [MaxLength(10)]
        public string? DepartmentNumber { get; set; }

        /// <summary>
        /// 单据备注
        /// </summary>
        [Comment("单据备注")]
        [MaxLength(500)]
        public string? OrderRemark { get; set; }

        /// <summary>
        /// 审核码
        /// </summary>
        [Comment("审核码")]
        [MaxLength(10)]
        public string? AuditCode { get; set; }

        /// <summary>
        /// 工厂编号
        /// </summary>
        [Comment("工厂编号")]
        [MaxLength(20)]
        public string? FactoryCode { get; set; }

        /// <summary>
        /// 单据日期
        /// </summary>
        [Comment("单据日期")]
        [MaxLength(50)]
        public string? OrderDate { get; set; }

        /// <summary>
        /// 工位号
        /// </summary>
        [Comment("工位号")]
        [MaxLength(255)]
        public string? StationNumber { get; set; }

        /// <summary>
        /// 主件品号
        /// </summary>
        [Comment("主件品号")]
        [MaxLength(255)]
        public string? MainPartNumber { get; set; }

        /// <summary>
        /// 设备号
        /// </summary>
        [Comment("设备号")]
        [MaxLength(255)]
        public string? EquipmentNumber { get; set; }
        #endregion

        public ICollection<WareOrderDetails>? WareOrderDetails { get; set; }

        /// <summary>
        /// 1对多配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<WareOrder> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            // 一对多配置
            entityBuilder.HasMany(x => x.WareOrderDetails)
                .WithOne(x => x.WareOrder)
                .HasForeignKey(x => x.OrderId);
        }
    }
}
