﻿namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 物料信息输入参数
    /// </summary>
    public class MaterialErpInput
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string? MB001 { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string? MB002 { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string? MB003 { get; set; }

        /// <summary>
        /// 库存单位
        /// </summary>
        public string? MB004 { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string? MB009 { get; set; }

        /// <summary>
        /// 检验方式 0:免检、1:抽检(减量)、2:抽检(正常)、3:抽检(加严)、4:全检
        /// </summary>
        public int MB043 { get; set; }

        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal MB064 { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string? UDF07 { get; set; }
    }
}
