﻿namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 品号仓库信息输入参数
    /// </summary>
    public class MaterialWarehouseErpInput
    {
        /// <summary>
        /// 品号
        /// </summary>
        public string? MC001 { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string? MC002 { get; set; }

        /// <summary>
        /// 安全存量
        /// </summary>
        public decimal MC004 { get; set; }

        /// <summary>
        /// 经济批量
        /// </summary>
        public decimal MC006 { get; set; }

        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal MC007 { get; set; }
    }
}
