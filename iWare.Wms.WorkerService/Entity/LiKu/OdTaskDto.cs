﻿namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 楼下立体库对应的任务表
    /// </summary>
    public class OdTaskDto
    {
        /// <summary>
        /// 任务ID
        /// </summary>
        public int TASKID { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string? TASKNAME { get; set; }

        /// <summary>
        /// 设备ID
        /// </summary>
        public int? DODEVICEID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? DODEVICENODEID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? DODEVICETYPE { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public int? TASKTYPE { get; set; }

        /// <summary>
        /// 任务等级
        /// </summary>
        public string? TASKLEVEL { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public int? TASKSTATUS { get; set; }

        /// <summary>
        /// 任务内容
        /// </summary>
        public string? TASKCONTENTSTRING { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? TASKTYPEDESCRIPTION { get; set; }

        /// <summary>
        /// 容器ID
        /// </summary>
        public int? CONTAINERID { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        public int? SOURCEPLACE { get; set; }

        /// <summary>
        /// 目的地
        /// </summary>
        public int? TOPLACE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? ORDERDETAILSID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? ORDERHEADID { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public int? SENDTIMES { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? RELEASESTATUS { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? HADFINISH { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? VOID { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UPDATETIME { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public string? UPDATEUSER { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? WAREHOUSENO { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ISCURRENTTASK { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? INPUTLOCATIONLEVEL { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ISLASTTASK { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ISEMPTYCONTAINER { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? AGVFROMPLACE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? AGVTOPLACE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? AGVCURRENT { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? DevactNo { get; set; }

        ///添加两个字段

        /// <summary>
        /// 是否移库 0 无移库 1移库
        /// </summary>
        public int? ISYIKU { get; set; }

        /// <summary>
        /// 移库任务ID
        /// </summary>
        public int? YKTASKID { get; set; }
    }
}
