﻿namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 楼下立体库对应的托盘表
    /// </summary>
    public class ImContainerDto
    {
        /// <summary>
        /// 托盘ID
        /// </summary>
        public int CONTAINERID { get; set; }

        /// <summary>
        /// 托盘号
        /// </summary>
        public string CONTAINERNAME { get; set; }

        /// <summary>
        /// 托盘类型
        /// </summary>
        public string CONTAINERTYPE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? LENGTH { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? WIDTH { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? HEIGHT { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? MAXWEIGHT { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? VOID { get; set; }

        /// <summary>
        /// 托盘状态
        /// </summary>
        public int? STATUS { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string WAREHOUSENO { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ONLINEMODEL { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? UPDATETIME { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UPDATEUSER { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? PARENTID { get; set; }
    }
}
