﻿namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 楼下立体库对应的库位表
    /// </summary>
    public class PlPlaceDto
    {
        /// <summary>
        /// 库位ID
        /// </summary>
        public int PLACEID { get; set; }

        /// <summary>
        /// 库位号
        /// </summary>
        public string? PLACENAME { get; set; }

        /// <summary>
        /// 库位类型
        /// </summary>
        public string? PLACETYPE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? PLACEAREA { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        public int ROWNO { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        public int COLUMNNO { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        public int LAYERNO { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? DEEPCELLNO { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? AISLE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? LINE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? AISLESIDE { get; set; }

        /// <summary>
        /// 是否锁定
        /// </summary>
        public int? ISLOCK { get; set; }

        /// <summary>
        /// 库位上是否有托盘
        /// </summary>
        public int? ISFULL { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? HASTASKDOING { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? WAREHOUSENO { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? POSITIONNO_FOR_SRM { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? X { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? Y { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? Z { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? LENGTH { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? WIDTH { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? HEIGHT { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? MAXWEIGHT { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? HEIGHTLEVEL { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? PRIORITY { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? AREA_LOGIC { get; set; }
    }
}
