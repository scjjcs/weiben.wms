﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 出入库任务
    /// </summary>
    [Comment("出入库任务")]
    [Table("ware_task")]
    public class WareTask : DEntityBase
    {
        /// <summary>
        /// 组盘单据号    
        /// </summary>
        [Comment("单据号")]
        [MaxLength(50)]
        public string? OrderNo { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        [Comment("容器编号")]
        [MaxLength(50)]
        public string? ContainerCode { get; set; }

        /// <summary>
        /// 任务号  
        /// </summary>
        [Comment("任务号")]
        [MaxLength(50)]
        public string? TaskNo { get; set; }

        /// <summary>
        /// 任务方式;任务方式-手动_1、自动_2
        /// </summary>
        [Comment("任务方式-手动_1、自动_2")]
        public TaskModel TaskModel { get; set; }

        /// <summary>
        /// 任务类型-入库任务_1、出库任务_2、移库任务_3
        /// </summary>
        [Comment("任务类型-入库任务_1、出库任务_2、移库任务_3")]
        public TaskType TaskType { get; set; }

        /// <summary>
        /// 任务名称    
        /// </summary>
        [Comment("任务名称")]
        [MaxLength(150)]
        public string? TaskName { get; set; }

        /// <summary>
        /// 任务排序    
        /// </summary>
        [Comment("任务排序")]
        public int Priority { get; set; }

        /// <summary>
        /// 出发地库位编号   
        /// </summary>
        [Comment("出发地库位编号")]
        [MaxLength(50)]
        public string? FromLocationCode { get; set; }

        /// <summary>
        /// 目的地库位编号   
        /// </summary>
        [Comment("目的地库位编号")]
        [MaxLength(50)]
        public string? ToLocationCode { get; set; }

        /// <summary>
        /// 状态-未执行_0、执行中_1、已完成_2
        /// </summary>
        [Comment("状态-未执行_0、执行中_1、已完成_2")]
        public TaskStatusEnum TaskStatus { get; set; } = TaskStatusEnum.NotProgress;
    }
}
