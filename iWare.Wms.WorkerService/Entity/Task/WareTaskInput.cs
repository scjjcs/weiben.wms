﻿using System.ComponentModel.DataAnnotations;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 强制完成任务输入参数
    /// </summary>
    public class TaskCallbackInput : BaseId
    {
    }

    public class YiKuCallbackInput
    {
        /// <summary>
        /// 目标库位
        /// </summary>
        public string ?newCode { get; set; }

        /// <summary>
        /// 原库位
        /// </summary>
        public string? oldCode { get; set; }
    }

}
