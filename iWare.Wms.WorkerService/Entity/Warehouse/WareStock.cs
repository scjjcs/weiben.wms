﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{ 
    /// <summary>
    /// 库存表
    /// </summary>
    [Table("ware_stock")]
    [Comment("库存表")]
    public class WareStock : DEntityBase
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        [Comment("容器编号")]
        [MaxLength(50)]
        public string? ContainerCode { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        [Comment("库位编号")]
        [MaxLength(50)]
        public string? LocationCode { get; set; }

        /// <summary>
        /// 项目编号   
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string? ProjectCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [Comment("品牌名称")]
        [MaxLength(200)]
        public string? BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Comment("序号")]
        [MaxLength(50)]
        public string? XuHao { get; set; }

        /// <summary>
        /// 品号    
        /// </summary>
        [Comment("品号")]
        [MaxLength(50)]
        public string? Code { get; set; }

        /// <summary>
        /// 品名    
        /// </summary>
        [Comment("品名")]
        [MaxLength(150)]
        public string? Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        [Comment("规格")]
        [MaxLength(200)]
        public string? SpecificationModel { get; set; }

        /// <summary>
        /// 商品描述    
        /// </summary>
        [Comment("商品描述")]
        [MaxLength(500)]
        public string? Describe { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        [Comment("检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4")]
        public InspectionMethodEnum InspectionMethod { get; set; } = InspectionMethodEnum.mianjian;

        /// <summary>
        /// 物料库存周期（字典 1绿 2黄 3红）
        /// </summary>
        [Comment("物料库存周期（字典 1绿 2黄 3红）")]
        public StockCircleEnum StockCircle { get; set; } = StockCircleEnum.GREEN;

        /// <summary>
        /// 单位    
        /// </summary>
        [Comment("单位")]
        [MaxLength(50)]
        public string? Unit { get; set; }

        /// <summary>
        /// 实际库存数量
        /// </summary>
        [Comment("实际库存数量")]
        public decimal StockQuantity { get; set; }

        /// <summary>
        /// 当前库存数量
        /// </summary>
        [Comment("当前库存数量")]
        public decimal CurrentQuantity { get; set; }

        /// <summary>
        /// 参数一
        /// </summary>
        [Comment("参数一")]
        [MaxLength(200)]
        public string? Parameter1 { get; set; }

        /// <summary>
        /// 参数二
        /// </summary>
        [Comment("参数二")]
        [MaxLength(200)]
        public string? Parameter2 { get; set; }

        /// <summary>
        /// 参数三
        /// </summary>
        [Comment("参数三")]
        [MaxLength(200)]
        public string? Parameter3 { get; set; }

        /// <summary>
        /// 参数四
        /// </summary>
        [Comment("参数四")]
        [MaxLength(200)]
        public string? Parameter4 { get; set; }

        /// <summary>
        /// 参数五
        /// </summary>
        [Comment("参数五")]
        [MaxLength(200)]
        public string? Parameter5 { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [Comment("状态（字典 0正常 1停用 2删除）")]
        public CommonStatus? Status { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string? Remark { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        [Comment("过期时间")]
        public DateTimeOffset ExpirationTime { get; set; }
    }
}
