using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 容器表
    /// </summary>
    [Table("ware_container")]
    [Comment("容器表")]
    public class WareContainer : DEntityBase, IEntityTypeBuilder<WareContainer>
    {
        /// <summary>
        /// 容器编号   
        /// </summary>
        [Comment("容器编号")]
        [MaxLength(20)]
        public string? Code { get; set; }

        /// <summary>
        /// 种类
        /// </summary>
        [Comment("种类")]
        [MaxLength(10)]
        public string? Parameter1 { get; set; }

        /// <summary>
        /// 材质
        /// </summary>
        [Comment("材质")]
        [MaxLength(10)]
        public string? Parameter2 { get; set; }

        /// <summary>
        /// 库区ID 
        /// </summary>
        [Comment("库区-区分立体库与平库容器")]
        [MaxLength(10)]
        public long AreaID { get; set; }

        /// <summary>
        /// 是否虚拟（字典 1是 0否）   
        /// </summary>
        [Comment("是否虚拟（字典 1是 0否）")]
        public YesOrNot IsVirtual { get; set; } = YesOrNot.N;

        /// <summary>
        /// 备注    
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string? Remarks { get; set; }

        /// <summary>
        /// 状态-空闲_0、使用_1、禁用_-1
        /// </summary>
        [Comment("状态-空闲_0、使用_1、禁用_-1")]
        public ContainerStatusEnum? ContainerStatus { get; set; } = ContainerStatusEnum.kongxian;

        /// <summary>
        /// 一对多中间表（容器-物料）
        /// </summary>
        public ICollection<WareContainerVsMaterial>? WareContainerVsMaterials { get; set; }

        /// <summary>
        /// 一对多中间表（库位-容器）
        /// </summary>
        public ICollection<WareLocationVsContainer>? WareLocationVsContainers { get; set; }

        /// <summary>
        /// 1对多配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<WareContainer> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            // 1对1配置
            entityBuilder.HasMany(x => x.WareContainerVsMaterials)
                .WithOne(x => x.WareContainer)
                .HasForeignKey(x => x.ContainerId);

            // 1对1配置
            entityBuilder.HasMany(x => x.WareLocationVsContainers)
                .WithOne(x => x.WareContainer)
                .HasForeignKey(x => x.ContainerId).IsRequired(false);
        }
    }
}