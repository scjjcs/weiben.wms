﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 库位表
    /// </summary>
    [Table("ware_location")]
    [Comment("库位表")]
    public class WareLocation : DEntityBase, IEntityTypeBuilder<WareLocation>
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        [Comment("库位编号")]
        [MaxLength(50)]
        public string? Code { get; set; }

        /// <summary>
        /// 所属企业
        /// </summary>
        [Comment("所属企业")]
        [MaxLength(10)]
        public string? WareOne { get; set; }

        /// <summary>
        /// 所属厂区
        /// </summary>
        [Comment("所属厂区")]
        [MaxLength(10)]
        public string? WareTwo { get; set; }

        /// <summary>
        /// 所属库区
        /// </summary>
        [Comment("所属库区")]
        public long AreaID { get; set; }

        /// <summary>
        /// 库区类型
        /// </summary>
        [Comment("库区类型")]
        [MaxLength(10)]
        public string? WareThree { get; set; }

        // 排
        /// </summary>
        [Comment("排")]
        [Required]
        public int RowNo { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        [Comment("列")]
        [Required]
        public int ColumnNo { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        [Comment("层")]
        [Required]
        public int LayerNo { get; set; }

        /// <summary>
        /// 深号
        /// </summary>
        [Comment("深号")]
        [Required]
        public int DeepcellNo { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        [Comment("巷道")]
        [Required]
        public int Aisle { get; set; }

        /// <summary>
        /// 长
        /// </summary>
        [Comment("长")]
        public decimal Long { get; set; }

        /// <summary>
        /// 宽
        /// </summary>
        [Comment("宽")]
        public decimal Width { get; set; }

        /// <summary>
        /// 高
        /// </summary>
        [Comment("高")]
        public decimal High { get; set; }

        /// <summary>
        /// 承重
        /// </summary>
        [Comment("承重")]
        public string? Heavy { get; set; }

        /// <summary>
        /// 属性
        /// </summary>
        [Comment("属性-小、中、大")]
        public string Attribute { get; set; }

        /// <summary>
        /// 是否锁定
        /// </summary>
        [Comment("是否锁定（字典 1是 0否）")]
        public YesOrNot IsLock { get; set; } = YesOrNot.N;

        /// <summary>
        /// 是否空托盘
        /// </summary>
        [Comment("是否空托盘（字典 1是 0否）")]
        public YesOrNot IsEmptyContainer { get; set; } = YesOrNot.N;

        /// <summary>
        /// 库位状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1
        /// </summary>
        [Comment("库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1")]
        public LocationStatusEnum LocationStatus { get; set; } = LocationStatusEnum.kongxian;

        #region 新增字段
        /// <summary>
        /// 是否虚拟（字典 1是 0否）   
        /// </summary>
        [Comment("是否虚拟（字典 1是 0否）")]
        public YesOrNot IsVirtual { get; set; } = YesOrNot.N;

        /// <summary>
        /// 原库位编号
        /// </summary>
        [Comment("原库位编号")]
        [MaxLength(50)]
        public string OriginalLocationCode { get; set; }
        #endregion

        /// <summary>
        /// 1对1中间表（库位-容器）
        /// </summary>
        public ICollection<WareLocationVsContainer>? WareLocationVsContainers { get; set; }

        public void Configure(EntityTypeBuilder<WareLocation> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            // 1对多配置
            entityBuilder.HasMany(x => x.WareLocationVsContainers)
                .WithOne(x => x.WareLocation)
                .HasForeignKey(x => x.LocationId).IsRequired(false);
        }
    }
}
