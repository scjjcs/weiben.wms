﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.WorkerService
{
    /// <summary>
    /// 库位与容器的关系
    /// </summary>
    [Table("ware_location_vs_container")]
    [Comment("库位与容器的关系")]
    public class WareLocationVsContainer : DEntityBase
    {
        /// <summary>
        /// 库位Id
        /// </summary>
        [Comment("库位Id")]
        public long LocationId { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [MaxLength(20)]
        public string? LocationCode { get; set; }

        /// <summary>
        /// 容器Id
        /// </summary>
        [Comment("容器Id")]
        public long ContainerId { get; set; }

        /// <summary>
        /// 容器编码
        /// </summary>
        [Comment("容器编码")]
        [MaxLength(20)]
        public string? ContainerCode { get; set; }

        /// <summary>
        /// 库位状态（字典 0正常 1停用 2删除）
        /// </summary>
        [Comment("状态（字典 0正常 1停用 2删除）")]
        public CommonStatus? LocationVsContainerStatus { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 一对一引用（库位表）
        /// </summary>
        public WareLocation? WareLocation { get; set; }

        /// <容器表>
        /// 一对一引用（容器表）
        /// </summary>
        public WareContainer? WareContainer { get; set; }
    }
}
