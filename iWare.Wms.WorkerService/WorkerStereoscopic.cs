﻿using Furion.DatabaseAccessor;
using iWare.Wms.WorkerService.ThirdDB;

namespace iWare.Wms.WorkerService
{
    public class WorkerStereoscopic : BackgroundService
    {
        private readonly ILogger<WorkerStereoscopic> _logger;
        // 服务工厂
        private readonly IServiceScopeFactory _scopeFactory;

        public WorkerStereoscopic(ILogger<WorkerStereoscopic> logger,
                      IServiceScopeFactory scopeFactory)
        {
            _logger = logger;
            _scopeFactory = scopeFactory;
        }

        // 启动
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }

        // 执行逻辑
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var services = scope.ServiceProvider;
                    var respository = Db.GetSqlRepository<StereoscopicDbContextLocation>();
                    var updated = await respository.SqlNonQueryAsync("update IM_CONTAINER set UPDATETIME = '"+ DateTime.Now +"' where CONTAINERID = 344");
                    Console.WriteLine(updated + "执行更改时间操作");
                }
                //执行的方法
                await Task.Delay(5000, stoppingToken);
            }
        }

        // 停止
        public override Task StopAsync(CancellationToken cancellationToken)
        {
            return base.StopAsync(cancellationToken);
        }
    }
}
