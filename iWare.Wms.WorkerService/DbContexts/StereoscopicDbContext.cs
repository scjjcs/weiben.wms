﻿using Furion.DatabaseAccessor;
using iWare.Wms.WorkerService.ThirdDB;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.WorkerService.DbContexts
{
    [AppDbContext("StereoscopicContext", DbProvider.SqlServer)]
    public class StereoscopicDbContext : AppDbContext<StereoscopicDbContext, StereoscopicDbContextLocation>
    {
        public StereoscopicDbContext(DbContextOptions<StereoscopicDbContext> options) : base(options)
        {

        }
    }
}
