﻿using Furion.DatabaseAccessor;
using iWare.Wms.WorkerService.ThirdDB;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.WorkerService.DbContexts
{
    [AppDbContext("ERPConnection", DbProvider.SqlServer)]
    public class ERPDbContext : AppDbContext<ERPDbContext, ERPDbContextLocator>
    {
        public ERPDbContext(DbContextOptions<ERPDbContext> options) : base(options)
        {

        }
    }
}
