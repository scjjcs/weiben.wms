﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1024 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IsShuSongXian",
                table: "ware_task",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "是否在输送线上");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsShuSongXian",
                table: "ware_task");
        }
    }
}
