﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1018 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "OriginalLocationCode",
                table: "ware_location",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "原库位编号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "楼下立体库对应的库位编号");

            migrationBuilder.AddColumn<int>(
                name: "IsJiaJiJian",
                table: "goods_delivery",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "是否加急件");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsJiaJiJian",
                table: "goods_delivery");

            migrationBuilder.AlterColumn<string>(
                name: "OriginalLocationCode",
                table: "ware_location",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "楼下立体库对应的库位编号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "原库位编号");
        }
    }
}
