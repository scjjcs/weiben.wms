﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v107 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsWarehousing",
                table: "collect_delivery_details");

            migrationBuilder.AddColumn<decimal>(
                name: "CollectQuantity",
                table: "goods_delivery_details",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m,
                comment: "是否收货");

            migrationBuilder.AlterColumn<int>(
                name: "QualityResult",
                table: "collect_delivery_details",
                type: "int",
                maxLength: 500,
                nullable: true,
                comment: "质检结果",
                oldClrType: typeof(int),
                oldType: "int",
                oldMaxLength: 500,
                oldComment: "质检结果");

            migrationBuilder.AddColumn<decimal>(
                name: "WarehousingQuantity",
                table: "collect_delivery_details",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m,
                comment: "入库数量");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CollectQuantity",
                table: "goods_delivery_details");

            migrationBuilder.DropColumn(
                name: "WarehousingQuantity",
                table: "collect_delivery_details");

            migrationBuilder.AlterColumn<int>(
                name: "QualityResult",
                table: "collect_delivery_details",
                type: "int",
                maxLength: 500,
                nullable: false,
                defaultValue: 0,
                comment: "质检结果",
                oldClrType: typeof(int),
                oldType: "int",
                oldMaxLength: 500,
                oldNullable: true,
                oldComment: "质检结果");

            migrationBuilder.AddColumn<int>(
                name: "IsWarehousing",
                table: "collect_delivery_details",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "是否入库");
        }
    }
}
