﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1023 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "ware_site",
                type: "int",
                nullable: true,
                comment: "状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1",
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true,
                oldComment: "状态（字典 0正常 1停用 2删除）");

            migrationBuilder.AddColumn<string>(
                name: "ContainerCode",
                table: "ware_site",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "容器编号");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContainerCode",
                table: "ware_site");

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "ware_site",
                type: "int",
                nullable: true,
                comment: "状态（字典 0正常 1停用 2删除）",
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true,
                oldComment: "状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1");
        }
    }
}
