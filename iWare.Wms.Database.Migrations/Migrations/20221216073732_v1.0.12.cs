﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1012 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SingleCategory",
                table: "ware_order_details",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "单别");

            migrationBuilder.AddColumn<string>(
                name: "TransferOutLocation",
                table: "ware_order_details",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "转出库位");

            migrationBuilder.AddColumn<string>(
                name: "TransferToIssue",
                table: "ware_order_details",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "转出库");

            migrationBuilder.AddColumn<string>(
                name: "TransferToReceipt",
                table: "ware_order_details",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "转入库");

            migrationBuilder.AddColumn<string>(
                name: "TransferredILocation",
                table: "ware_order_details",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "转入库位");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SingleCategory",
                table: "ware_order_details");

            migrationBuilder.DropColumn(
                name: "TransferOutLocation",
                table: "ware_order_details");

            migrationBuilder.DropColumn(
                name: "TransferToIssue",
                table: "ware_order_details");

            migrationBuilder.DropColumn(
                name: "TransferToReceipt",
                table: "ware_order_details");

            migrationBuilder.DropColumn(
                name: "TransferredILocation",
                table: "ware_order_details");
        }
    }
}
