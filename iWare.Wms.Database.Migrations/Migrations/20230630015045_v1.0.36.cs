﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1036 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ware_assembly_workshop",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    ProjectName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "项目名称"),
                    PlanNum = table.Column<int>(type: "int", nullable: false, comment: "计划数"),
                    ActuaityNum = table.Column<int>(type: "int", nullable: false, comment: "实际数"),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "开始时间"),
                    Percentage = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "百分比"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_assembly_workshop", x => x.Id);
                },
                comment: "组装车间表");

            migrationBuilder.CreateTable(
                name: "ware_device_fault",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    device_no = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "设备编号"),
                    device_name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "设备名称"),
                    start_time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    close_time = table.Column<DateTime>(type: "datetime2", nullable: true),
                    use_times = table.Column<int>(type: "int", nullable: true),
                    fault_code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "故障代码"),
                    fault_name = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true, comment: "故障描述"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_device_fault", x => x.Id);
                },
                comment: "设备故障");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ware_assembly_workshop");

            migrationBuilder.DropTable(
                name: "ware_device_fault");
        }
    }
}
