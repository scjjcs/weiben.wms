﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v100 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "collect_delivery",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    CollectNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "收货单号"),
                    CollectType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "收货单别"),
                    DeliveryNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "送货单号"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    SupplierInfoCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商编号"),
                    CollectQuantityTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "数量"),
                    Images = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "照片"),
                    Remark = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    IsComplete = table.Column<int>(type: "int", nullable: false, comment: "是否收货完成-否_0、是_1"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_collect_delivery", x => x.Id);
                },
                comment: "收货单");

            migrationBuilder.CreateTable(
                name: "goods_delivery",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    PurchaseNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "采购单号"),
                    DeliveryNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "送货单号"),
                    DeliveryType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "送货单别"),
                    DeliveryQuantityTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "数量"),
                    Remark = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "审核状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6"),
                    SuppCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商编号"),
                    SuppName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商名称"),
                    SuppPhone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "联系电话"),
                    SuppAddress = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true, comment: "详细地址"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    ReceName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "收货单位"),
                    ReceAddress = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "收货地址"),
                    RecePhone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "联系电话"),
                    ReceUser = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "采购联系人"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goods_delivery", x => x.Id);
                },
                comment: "送货单");

            migrationBuilder.CreateTable(
                name: "goods_delivery_tagprint",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    GoodsDeliveryId = table.Column<long>(type: "bigint", nullable: false, comment: "送货单主表Id"),
                    DeliveryNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "送货单号"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    SupplierInfoCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商编号"),
                    BrandName = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true, comment: "品牌名称"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "品号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "品名"),
                    XuHao = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "货物序号"),
                    SpecificationModel = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "规格型号"),
                    InspectionMethod = table.Column<int>(type: "int", nullable: false, comment: "检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4"),
                    Unit = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "单位"),
                    Quantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "数量"),
                    Parameter = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "参数"),
                    Remark = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    IsGroupDisk = table.Column<int>(type: "int", nullable: false, comment: "是否组盘"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goods_delivery_tagprint", x => x.Id);
                },
                comment: "送货标签打印记录表");

            migrationBuilder.CreateTable(
                name: "project_manage",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "项目名称"),
                    Describe = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "项目说明"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_project_manage", x => x.Id);
                },
                comment: "项目表");

            migrationBuilder.CreateTable(
                name: "purchase_order",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    PurchaseType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "采购单别"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    SupplierInfoCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商编号"),
                    PurchaseNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "采购单号"),
                    PurchaseDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false, comment: "采购日期"),
                    PurchaserUserId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "采购人ID"),
                    PurchaseNumber = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "数量合计"),
                    Address = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true, comment: "收货地址"),
                    Factory = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true, comment: "工厂"),
                    Parameter1 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数一"),
                    Parameter2 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数二"),
                    Remark = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态-撤销或者删除_-1、未进行_0、进行中_1、完成_2"),
                    IsJiaJiJian = table.Column<int>(type: "int", nullable: false, comment: "是否加急件"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order", x => x.Id);
                },
                comment: "采购订单");

            migrationBuilder.CreateTable(
                name: "supplier_examine_flower",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    DeliveryID = table.Column<long>(type: "bigint", nullable: false, comment: "送货单据ID"),
                    Remarks = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_supplier_examine_flower", x => x.Id);
                },
                comment: "供应商审核记录表");

            migrationBuilder.CreateTable(
                name: "supplier_info",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商编号"),
                    AbbreviationName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "简称"),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "公司全称"),
                    Tel = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "TEL(一)"),
                    LeadingCadre = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "负责人"),
                    Contacts = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "联系人(一)"),
                    ContactAddress = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "联系地址(一)"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_supplier_info", x => x.Id);
                },
                comment: "供应商信息表");

            migrationBuilder.CreateTable(
                name: "sys_app",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false, comment: "名称"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "编码"),
                    Active = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "是否默认激活"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_app", x => x.Id);
                },
                comment: "系统应用表");

            migrationBuilder.CreateTable(
                name: "sys_code_gen",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    AuthorName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "作者姓名"),
                    TablePrefix = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "是否移除表前缀"),
                    GenerateType = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "生成方式"),
                    DatabaseName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "数据库名"),
                    TableName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "数据库表名"),
                    NameSpace = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "命名空间"),
                    BusName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "业务名"),
                    MenuApplication = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "菜单应用分类"),
                    MenuPid = table.Column<long>(type: "bigint", nullable: false, comment: "菜单编码"),
                    LowCodeId = table.Column<long>(type: "bigint", nullable: true),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_code_gen", x => x.Id);
                },
                comment: "代码生成表");

            migrationBuilder.CreateTable(
                name: "sys_code_gen_config",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    CodeGenId = table.Column<long>(type: "bigint", nullable: false, comment: "代码生成主表ID"),
                    ColumnName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "数据库字段名"),
                    ColumnComment = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "字段描述"),
                    NetType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: ".NET数据类型"),
                    DtoNetType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: ".NET数据类型(接口传入)"),
                    EffectType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "作用类型"),
                    FkEntityName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "外键实体名称"),
                    FkColumnName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "外键显示字段"),
                    FkColumnNetType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "外键显示字段.NET类型"),
                    DictTypeCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "字典Code"),
                    WhetherRetract = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "列表是否缩进"),
                    WhetherRequired = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "是否必填"),
                    QueryWhether = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "是否是查询条件"),
                    QueryType = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, comment: "查询方式"),
                    WhetherTable = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "列表显示"),
                    WhetherOrderBy = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "列表是否排序"),
                    WhetherAddUpdate = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "增改"),
                    ColumnKey = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "主键"),
                    DataType = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: true, comment: "数据库中类型"),
                    WhetherCommon = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "是否通用字段"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_code_gen_config", x => x.Id);
                },
                comment: "代码生成字段配置表");

            migrationBuilder.CreateTable(
                name: "sys_code_modular",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    AuthorName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "作者姓名"),
                    DatabaseName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "数据库名"),
                    NameSpace = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "命名空间"),
                    BusName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "业务名"),
                    MenuApplication = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "菜单应用分类"),
                    MenuPid = table.Column<long>(type: "bigint", nullable: false, comment: "菜单编码"),
                    FormDesignJson = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "动态表单"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_code_modular", x => x.Id);
                },
                comment: "动态生成模块管理表");

            migrationBuilder.CreateTable(
                name: "sys_config",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "名称"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "编码"),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "属性值"),
                    SysFlag = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "是否是系统参数"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态"),
                    GroupCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "常量所属分类的编码"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_config", x => x.Id);
                },
                comment: "参数配置表");

            migrationBuilder.CreateTable(
                name: "sys_dict_type",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "名称"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "编码"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_dict_type", x => x.Id);
                },
                comment: "字典类型表");

            migrationBuilder.CreateTable(
                name: "sys_emp",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "用户Id"),
                    JobNum = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true, comment: "工号"),
                    OrgId = table.Column<long>(type: "bigint", nullable: false, comment: "机构Id"),
                    OrgName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "机构名称")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_emp", x => x.Id);
                },
                comment: "员工表");

            migrationBuilder.CreateTable(
                name: "sys_file",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    FileLocation = table.Column<int>(type: "int", nullable: false, comment: "文件存储位置"),
                    FileBucket = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "文件仓库"),
                    FileOriginName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "文件名称"),
                    FileSuffix = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "文件后缀"),
                    FileSizeKb = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, comment: "文件大小kb"),
                    FileSizeInfo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "文件大小信息"),
                    FileObjectName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "存储到bucket的名称"),
                    FilePath = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "存储路径"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_file", x => x.Id);
                },
                comment: "文件信息表");

            migrationBuilder.CreateTable(
                name: "sys_log_audit",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TableName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "表名"),
                    ColumnName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "列名"),
                    NewValue = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "新值"),
                    OldValue = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "旧值"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "操作时间"),
                    UserId = table.Column<long>(type: "bigint", nullable: false, comment: "操作人Id"),
                    UserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "操作人名称"),
                    Operate = table.Column<int>(type: "int", nullable: false, comment: "操作方式")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_log_audit", x => x.Id);
                },
                comment: "审计日志表");

            migrationBuilder.CreateTable(
                name: "sys_log_ex",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Account = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "操作人"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "名称"),
                    ClassName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "类名"),
                    MethodName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "方法名"),
                    ExceptionName = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "异常名称"),
                    ExceptionMsg = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "异常信息"),
                    ExceptionSource = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "异常源"),
                    StackTrace = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "堆栈信息"),
                    ParamsObj = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "参数对象"),
                    ExceptionTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "异常时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_log_ex", x => x.Id);
                },
                comment: "异常日志表");

            migrationBuilder.CreateTable(
                name: "sys_log_op",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "名称"),
                    Success = table.Column<int>(type: "int", nullable: false, comment: "是否执行成功"),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "具体消息"),
                    Ip = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "IP"),
                    Location = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: true, comment: "地址"),
                    Browser = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "浏览器"),
                    Os = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "操作系统"),
                    Url = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "请求地址"),
                    ClassName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "类名称"),
                    MethodName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "方法名称"),
                    ReqMethod = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, comment: "请求方式"),
                    Param = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "请求参数"),
                    Result = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "返回结果"),
                    ElapsedTime = table.Column<long>(type: "bigint", nullable: false, comment: "耗时"),
                    OpTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "操作时间"),
                    Account = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "操作人")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_log_op", x => x.Id);
                },
                comment: "操作日志表");

            migrationBuilder.CreateTable(
                name: "sys_log_vis",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "名称"),
                    Success = table.Column<int>(type: "int", nullable: false, comment: "是否执行成功"),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "具体消息"),
                    Ip = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "IP"),
                    Location = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: true, comment: "地址"),
                    Browser = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "浏览器"),
                    Os = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "操作系统"),
                    VisType = table.Column<int>(type: "int", nullable: false, comment: "访问类型"),
                    VisTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "访问时间"),
                    Account = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "访问人")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_log_vis", x => x.Id);
                },
                comment: "访问日志表");

            migrationBuilder.CreateTable(
                name: "sys_low_code_module",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    AuthorName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "作者姓名"),
                    GenerateType = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "生成方式"),
                    DatabaseName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "数据库名"),
                    NameSpace = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "命名空间"),
                    BusName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "业务名"),
                    MenuApplication = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "菜单应用分类"),
                    MenuPid = table.Column<long>(type: "bigint", nullable: false, comment: "菜单编码"),
                    FormDesignType = table.Column<int>(type: "int", nullable: false, comment: "动态表单类型"),
                    FormDesign = table.Column<string>(type: "text", nullable: true, comment: "动态表单"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_low_code_module", x => x.Id);
                },
                comment: "低代码模块管理");

            migrationBuilder.CreateTable(
                name: "sys_menu",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Pid = table.Column<long>(type: "bigint", nullable: false, comment: "父Id"),
                    Pids = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "父Ids"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false, comment: "名称"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "编码"),
                    Type = table.Column<int>(type: "int", nullable: false, comment: "菜单类型"),
                    Icon = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "图标"),
                    Router = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "路由地址"),
                    Component = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "组件地址"),
                    Permission = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "权限标识"),
                    Application = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "应用分类"),
                    OpenType = table.Column<int>(type: "int", nullable: false, comment: "打开方式"),
                    Visible = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true, comment: "是否可见"),
                    Link = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "内链地址"),
                    Redirect = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "重定向地址"),
                    Weight = table.Column<int>(type: "int", nullable: false, comment: "权重"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_menu", x => x.Id);
                },
                comment: "菜单表");

            migrationBuilder.CreateTable(
                name: "sys_notice",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Title = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false, comment: "标题"),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "内容"),
                    Type = table.Column<int>(type: "int", nullable: false, comment: "类型"),
                    PublicUserId = table.Column<long>(type: "bigint", nullable: false, comment: "发布人Id"),
                    PublicUserName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "发布人姓名"),
                    PublicOrgId = table.Column<long>(type: "bigint", nullable: false, comment: "发布机构Id"),
                    PublicOrgName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "发布机构名称"),
                    PublicTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "发布时间"),
                    CancelTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "撤回时间"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_notice", x => x.Id);
                },
                comment: "通知公告表");

            migrationBuilder.CreateTable(
                name: "sys_notice_user",
                columns: table => new
                {
                    NoticeId = table.Column<long>(type: "bigint", nullable: false, comment: "通知公告Id"),
                    UserId = table.Column<long>(type: "bigint", nullable: false, comment: "用户Id"),
                    ReadTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "阅读时间"),
                    ReadStatus = table.Column<int>(type: "int", nullable: false, comment: "状态")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_notice_user", x => new { x.NoticeId, x.UserId });
                },
                comment: "通知公告用户表");

            migrationBuilder.CreateTable(
                name: "sys_org",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Pid = table.Column<long>(type: "bigint", nullable: false, comment: "父Id"),
                    Pids = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "父Ids"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "名称"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "编码"),
                    Contacts = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "联系人"),
                    Tel = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "电话"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态"),
                    OrgType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_org", x => x.Id);
                },
                comment: "组织机构表");

            migrationBuilder.CreateTable(
                name: "sys_pos",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false, comment: "名称"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "编码"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_pos", x => x.Id);
                },
                comment: "职位表");

            migrationBuilder.CreateTable(
                name: "sys_role",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false, comment: "名称"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "编码"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    DataScopeType = table.Column<int>(type: "int", nullable: false, comment: "数据范围类型"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态"),
                    RoleType = table.Column<int>(type: "int", nullable: false, comment: "角色类型-集团角色_0、加盟商角色_1、门店角色_2"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_role", x => x.Id);
                },
                comment: "角色表");

            migrationBuilder.CreateTable(
                name: "sys_timer",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    JobName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false, comment: "任务名称"),
                    DoOnce = table.Column<bool>(type: "bit", nullable: false, comment: "只执行一次"),
                    StartNow = table.Column<bool>(type: "bit", nullable: false, comment: "立即执行"),
                    ExecuteType = table.Column<int>(type: "int", nullable: false, comment: "执行类型"),
                    Interval = table.Column<int>(type: "int", nullable: true, comment: "间隔时间"),
                    Cron = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "Cron表达式"),
                    TimerType = table.Column<int>(type: "int", nullable: false, comment: "定时器类型"),
                    RequestUrl = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "请求url"),
                    RequestParameters = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "请求参数"),
                    Headers = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "Headers"),
                    RequestType = table.Column<int>(type: "int", nullable: false, comment: "请求类型"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "备注"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_timer", x => x.Id);
                },
                comment: "定时任务表");

            migrationBuilder.CreateTable(
                name: "sys_user",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Account = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "账号"),
                    Password = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false, comment: "密码"),
                    NickName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "昵称"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "姓名"),
                    Avatar = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "头像"),
                    Birthday = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "生日"),
                    Sex = table.Column<int>(type: "int", nullable: false, comment: "性别-男_1、女_2"),
                    Email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "邮箱"),
                    Phone = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "手机"),
                    Tel = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "电话"),
                    LastLoginIp = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "最后登录IP"),
                    LastLoginTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "最后登录时间"),
                    AdminType = table.Column<int>(type: "int", nullable: false, comment: "管理员类型-超级管理员_1、管理员_2、普通账号_3"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态-正常_0、停用_1、删除_2"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_user", x => x.Id);
                },
                comment: "用户表");

            migrationBuilder.CreateTable(
                name: "SysRoleOutputs",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: false),
                    DataScopeType = table.Column<int>(type: "int", nullable: false),
                    Remark = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    RoleType = table.Column<int>(type: "int", nullable: false),
                    RoleSums = table.Column<int>(type: "int", nullable: true),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysRoleOutputs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ware_area",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    AreaCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "编码"),
                    AreaName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "名称"),
                    AreaType = table.Column<int>(type: "int", maxLength: 150, nullable: false, comment: "名称"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_area", x => x.Id);
                },
                comment: "所属库区");

            migrationBuilder.CreateTable(
                name: "ware_container",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Code = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "容器编号"),
                    Parameter1 = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, comment: "种类"),
                    Parameter2 = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, comment: "材质"),
                    AreaID = table.Column<long>(type: "bigint", maxLength: 10, nullable: false, comment: "库区-区分立体库与平库容器"),
                    IsVirtual = table.Column<int>(type: "int", nullable: false, comment: "是否虚拟（字典 1是 0否）"),
                    Remarks = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    ContainerStatus = table.Column<int>(type: "int", nullable: true, comment: "状态-空闲_0、使用_1、禁用_-1"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_container", x => x.Id);
                },
                comment: "容器表");

            migrationBuilder.CreateTable(
                name: "ware_dic_type",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "名称"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "编码"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: true, comment: "状态（字典 0正常 1停用 2删除）"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_dic_type", x => x.Id);
                },
                comment: "仓库字典类型表");

            migrationBuilder.CreateTable(
                name: "ware_equipment",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    EquipmentTypeName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "设备类型"),
                    EquipmentName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "设备名称"),
                    ExhibitionName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "设备展示名"),
                    EquipmentBrand = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "设备品牌"),
                    IP = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true, comment: "设备IP"),
                    Port = table.Column<int>(type: "int", nullable: false, comment: "设备端口"),
                    RegionName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "所属区域code"),
                    EquipmentIdentification = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "设备标识"),
                    Remarks = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: true, comment: "状态（字典 0正常 1停用 2删除）"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_equipment", x => x.Id);
                },
                comment: "设备表");

            migrationBuilder.CreateTable(
                name: "ware_location",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "库位编号"),
                    WareOne = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, comment: "所属企业"),
                    WareTwo = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, comment: "所属厂区"),
                    AreaID = table.Column<long>(type: "bigint", nullable: false, comment: "所属库区"),
                    WareThree = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, comment: "库区类型"),
                    RowNo = table.Column<int>(type: "int", nullable: false, comment: "排"),
                    ColumnNo = table.Column<int>(type: "int", nullable: false, comment: "列"),
                    LayerNo = table.Column<int>(type: "int", nullable: false, comment: "层"),
                    DeepcellNo = table.Column<int>(type: "int", nullable: false, comment: "深号"),
                    Aisle = table.Column<int>(type: "int", nullable: false, comment: "巷道"),
                    Long = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "长"),
                    Width = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "宽"),
                    High = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "高"),
                    Heavy = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "承重"),
                    Attribute = table.Column<int>(type: "int", nullable: false, comment: "属性-中、高、低"),
                    IsLock = table.Column<int>(type: "int", nullable: false, comment: "是否锁定（字典 1是 0否）"),
                    IsEmptyContainer = table.Column<int>(type: "int", nullable: false, comment: "是否空托盘（字典 1是 0否）"),
                    LocationStatus = table.Column<int>(type: "int", nullable: false, comment: "库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_location", x => x.Id);
                },
                comment: "库位表");

            migrationBuilder.CreateTable(
                name: "ware_material",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "品号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "品名"),
                    SpecificationModel = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "规格"),
                    Unit = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "单位"),
                    Describe = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "商品描述"),
                    InspectionMethod = table.Column<int>(type: "int", nullable: false, comment: "检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4"),
                    Quantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "数量"),
                    BrandName = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true, comment: "品牌名称"),
                    SupplerCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "供应商code"),
                    IsJiaJi = table.Column<int>(type: "int", maxLength: 20, nullable: false, comment: "是否加急"),
                    Parameter1 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数一"),
                    Parameter2 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数二"),
                    Parameter3 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数三"),
                    Parameter4 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数四"),
                    Parameter5 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数五"),
                    StandbyStatus = table.Column<int>(type: "int", nullable: false, comment: "备库状态-不备库_0；备库_1"),
                    Status = table.Column<int>(type: "int", nullable: true, comment: "状态（字典 0正常 1停用 2删除）"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_material", x => x.Id);
                },
                comment: "物料表");

            migrationBuilder.CreateTable(
                name: "ware_material_warehouse",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "品号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "仓库"),
                    SafetyQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "安全存量"),
                    EconomicQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "经济批量"),
                    InventoryQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "库存数量"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_material_warehouse", x => x.Id);
                },
                comment: "品号仓库");

            migrationBuilder.CreateTable(
                name: "ware_order",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    OrderNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "出库单据"),
                    OrderType = table.Column<int>(type: "int", nullable: false, comment: "出库单据类型-调拨单_1"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    OrderQuantityTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "单据总数"),
                    Remarks = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_order", x => x.Id);
                },
                comment: "单据表");

            migrationBuilder.CreateTable(
                name: "ware_site",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "站点编号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "站点名称"),
                    SiteTypeName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "站点类型"),
                    Describe = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "描述"),
                    Status = table.Column<int>(type: "int", nullable: true, comment: "状态（字典 0正常 1停用 2删除）"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_site", x => x.Id);
                },
                comment: "站点表");

            migrationBuilder.CreateTable(
                name: "ware_sortorder",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    SortOrderNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "分拣来源单号"),
                    ContainerOrderNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "分拣来源单号"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    ContainerCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "容器编号"),
                    BrandName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "品牌名称"),
                    XuHao = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "序号"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "品号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "品名"),
                    SpecificationModel = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "规格"),
                    Unit = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "单位"),
                    InspectionMethod = table.Column<int>(type: "int", nullable: false, comment: "检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4"),
                    SortQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "分拣数"),
                    ActualQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "实际分拣数"),
                    Images = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "照片"),
                    Remarks = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    SortStatus = table.Column<int>(type: "int", nullable: false, comment: "状态-未进行_0、未分拣_1、分拣中_2、分拣完成_3"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_sortorder", x => x.Id);
                },
                comment: "分拣表");

            migrationBuilder.CreateTable(
                name: "ware_stock",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    ContainerCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "容器编号"),
                    LocationCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "库位编号"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    BrandName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "品牌名称"),
                    XuHao = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "序号"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "品号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "品名"),
                    SpecificationModel = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "规格"),
                    Describe = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "商品描述"),
                    InspectionMethod = table.Column<int>(type: "int", nullable: false, comment: "检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4"),
                    Unit = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "单位"),
                    StockQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "实际库存数量"),
                    CurrentQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "当前库存数量"),
                    Parameter1 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数一"),
                    Parameter2 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数二"),
                    Parameter3 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数三"),
                    Parameter4 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数四"),
                    Parameter5 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "参数五"),
                    Status = table.Column<int>(type: "int", nullable: true, comment: "状态（字典 0正常 1停用 2删除）"),
                    Remark = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    ExpirationTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false, comment: "过期时间"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_stock", x => x.Id);
                },
                comment: "库存表");

            migrationBuilder.CreateTable(
                name: "ware_task",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    OrderNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "单据号"),
                    ContainerCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "容器编号"),
                    TaskNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "任务号"),
                    TaskModel = table.Column<int>(type: "int", nullable: false, comment: "任务方式-手动_1、自动_2"),
                    TaskType = table.Column<int>(type: "int", nullable: false, comment: "任务类型-入库任务_1、出库任务_2、移库任务_3"),
                    TaskName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "任务名称"),
                    Priority = table.Column<int>(type: "int", nullable: false, comment: "任务排序"),
                    FromLocationCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "出发地库位编号"),
                    ToLocationCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "目的地库位编号"),
                    TaskStatus = table.Column<int>(type: "int", nullable: false, comment: "状态-未执行_0、执行中_1、已完成_2"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_task", x => x.Id);
                },
                comment: "出入库任务");

            migrationBuilder.CreateTable(
                name: "collect_delivery_details",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    CollectDeliveryId = table.Column<long>(type: "bigint", nullable: false, comment: "收货单Id"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    SupplierInfoCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商编号"),
                    BrandName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "品牌名称"),
                    XuHao = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "序号"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "品号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "品名"),
                    SpecificationModel = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "规格型号"),
                    InspectionMethod = table.Column<int>(type: "int", nullable: false, comment: "检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4"),
                    Unit = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "单位"),
                    CollectQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "数量"),
                    Remark = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    IsWarehousing = table.Column<int>(type: "int", nullable: false, comment: "是否入库"),
                    IsQuality = table.Column<int>(type: "int", nullable: false, comment: "是否质检"),
                    IsQualified = table.Column<int>(type: "int", nullable: false, comment: "物料质检是否合格"),
                    QualityResult = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "质检结果"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_collect_delivery_details", x => x.Id);
                    table.ForeignKey(
                        name: "FK_collect_delivery_details_collect_delivery_CollectDeliveryId",
                        column: x => x.CollectDeliveryId,
                        principalTable: "collect_delivery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "收货单明细");

            migrationBuilder.CreateTable(
                name: "goods_delivery_appointment",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    AppointmentNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "预约单号"),
                    PurchaseNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "采购单号"),
                    DeliveryID = table.Column<long>(type: "bigint", nullable: false, comment: "送货单ID"),
                    DeliveryNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "送货单号"),
                    TransportCompany = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "运输公司"),
                    ExpressInfoNo = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "物流单号"),
                    TransportVehicle = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "运输车辆"),
                    DriverName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "司机姓名"),
                    DriverQualification = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "驾驶员资格"),
                    DriverPhone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "司机电话"),
                    DeliverGoodsPlaceShip = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "发货地址"),
                    DeliverGoodsWarehouseCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "发货仓库"),
                    CollectDeliveryPlaceShip = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "收货地址"),
                    CollectDeliveryWarehouseCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "收货仓库"),
                    EstimatedDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "预计到达日期"),
                    DeliverDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "发货日期"),
                    CollectDeliveryUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "收货人"),
                    CollectDeliveryUserPhone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "收货人电话"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    Remark = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6、强制收货_7"),
                    Images = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "图片"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goods_delivery_appointment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_goods_delivery_appointment_goods_delivery_DeliveryID",
                        column: x => x.DeliveryID,
                        principalTable: "goods_delivery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "送货单预约");

            migrationBuilder.CreateTable(
                name: "goods_delivery_details",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    GoodsDeliveryId = table.Column<long>(type: "bigint", nullable: false, comment: "送货单主表Id"),
                    SupplierInfoCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商编号"),
                    BrandName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "品牌名称"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    XuHao = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "序号"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "品号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "品名"),
                    SpecificationModel = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "规格型号"),
                    InspectionMethod = table.Column<int>(type: "int", nullable: false, comment: "检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4"),
                    Unit = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "单位"),
                    DeliveryQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "数量"),
                    Parameter = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "参数"),
                    Remark = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goods_delivery_details", x => x.Id);
                    table.ForeignKey(
                        name: "FK_goods_delivery_details_goods_delivery_GoodsDeliveryId",
                        column: x => x.GoodsDeliveryId,
                        principalTable: "goods_delivery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "送货单明细");

            migrationBuilder.CreateTable(
                name: "goodsdelivery_vs_collectdelivery",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    GoodsDeliveryId = table.Column<long>(type: "bigint", nullable: false, comment: "送货单Id"),
                    CollectDeliveryId = table.Column<long>(type: "bigint", nullable: false, comment: "收货单Id"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goodsdelivery_vs_collectdelivery", x => x.Id);
                    table.ForeignKey(
                        name: "FK_goodsdelivery_vs_collectdelivery_collect_delivery_CollectDeliveryId",
                        column: x => x.CollectDeliveryId,
                        principalTable: "collect_delivery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_goodsdelivery_vs_collectdelivery_goods_delivery_GoodsDeliveryId",
                        column: x => x.GoodsDeliveryId,
                        principalTable: "goods_delivery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "送货单与收货单的关系");

            migrationBuilder.CreateTable(
                name: "purchaseorder_vs_goodsdelivery",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    PurchaseOrderId = table.Column<long>(type: "bigint", nullable: false, comment: "采购单表Id"),
                    GoodsDeliveryId = table.Column<long>(type: "bigint", nullable: false, comment: "送货单Id"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchaseorder_vs_goodsdelivery", x => x.Id);
                    table.ForeignKey(
                        name: "FK_purchaseorder_vs_goodsdelivery_goods_delivery_GoodsDeliveryId",
                        column: x => x.GoodsDeliveryId,
                        principalTable: "goods_delivery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_purchaseorder_vs_goodsdelivery_purchase_order_PurchaseOrderId",
                        column: x => x.PurchaseOrderId,
                        principalTable: "purchase_order",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "采购单与送货单的关系");

            migrationBuilder.CreateTable(
                name: "sys_dict_data",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    TypeId = table.Column<long>(type: "bigint", nullable: false, comment: "字典类型Id"),
                    Value = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "值"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "编码"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: false, comment: "状态"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_dict_data", x => x.Id);
                    table.ForeignKey(
                        name: "FK_sys_dict_data_sys_dict_type_TypeId",
                        column: x => x.TypeId,
                        principalTable: "sys_dict_type",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "字典值表");

            migrationBuilder.CreateTable(
                name: "sys_low_code_module_database",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, comment: "Id主键"),
                    SysLowCodeId = table.Column<long>(type: "bigint", nullable: false),
                    Control_Key = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "组件Key"),
                    Control_Label = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "组件名称"),
                    Control_Model = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "组件字段"),
                    Control_Type = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "组件字段"),
                    TableName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "表名"),
                    ClassName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "类名"),
                    TableDesc = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "表描述"),
                    FieldName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "字段名称"),
                    DbTypeName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "数据类型"),
                    DtoTypeName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "传入数据类型"),
                    DbParam = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "数据类型补充参数"),
                    IsRequired = table.Column<bool>(type: "bit", nullable: true, comment: "是否必填"),
                    WhetherTable = table.Column<bool>(type: "bit", nullable: true, comment: "列表显示"),
                    WhetherOrderBy = table.Column<bool>(type: "bit", nullable: true, comment: "排序"),
                    WhetherAddUpdate = table.Column<bool>(type: "bit", nullable: true, comment: "增改"),
                    QueryWhether = table.Column<bool>(type: "bit", nullable: true, comment: "是否是查询"),
                    QueryType = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "查询方式"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_low_code_module_database", x => x.Id);
                    table.ForeignKey(
                        name: "FK_sys_low_code_module_database_sys_low_code_module_SysLowCodeId",
                        column: x => x.SysLowCodeId,
                        principalTable: "sys_low_code_module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "低代码模块管理");

            migrationBuilder.CreateTable(
                name: "sys_emp_ext_org_pos",
                columns: table => new
                {
                    SysEmpId = table.Column<long>(type: "bigint", nullable: false, comment: "员工Id"),
                    SysOrgId = table.Column<long>(type: "bigint", nullable: false, comment: "机构Id"),
                    SysPosId = table.Column<long>(type: "bigint", nullable: false, comment: "职位Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_emp_ext_org_pos", x => new { x.SysEmpId, x.SysOrgId, x.SysPosId });
                    table.ForeignKey(
                        name: "FK_sys_emp_ext_org_pos_sys_emp_SysEmpId",
                        column: x => x.SysEmpId,
                        principalTable: "sys_emp",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_sys_emp_ext_org_pos_sys_org_SysOrgId",
                        column: x => x.SysOrgId,
                        principalTable: "sys_org",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_sys_emp_ext_org_pos_sys_pos_SysPosId",
                        column: x => x.SysPosId,
                        principalTable: "sys_pos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "员工附属机构职位表");

            migrationBuilder.CreateTable(
                name: "sys_emp_pos",
                columns: table => new
                {
                    SysEmpId = table.Column<long>(type: "bigint", nullable: false, comment: "员工Id"),
                    SysPosId = table.Column<long>(type: "bigint", nullable: false, comment: "职位Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_emp_pos", x => new { x.SysEmpId, x.SysPosId });
                    table.ForeignKey(
                        name: "FK_sys_emp_pos_sys_emp_SysEmpId",
                        column: x => x.SysEmpId,
                        principalTable: "sys_emp",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_sys_emp_pos_sys_pos_SysPosId",
                        column: x => x.SysPosId,
                        principalTable: "sys_pos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "员工职位表");

            migrationBuilder.CreateTable(
                name: "sys_role_data_scope",
                columns: table => new
                {
                    SysRoleId = table.Column<long>(type: "bigint", nullable: false, comment: "角色Id"),
                    SysOrgId = table.Column<long>(type: "bigint", nullable: false, comment: "机构Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_role_data_scope", x => new { x.SysRoleId, x.SysOrgId });
                    table.ForeignKey(
                        name: "FK_sys_role_data_scope_sys_org_SysOrgId",
                        column: x => x.SysOrgId,
                        principalTable: "sys_org",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_sys_role_data_scope_sys_role_SysRoleId",
                        column: x => x.SysRoleId,
                        principalTable: "sys_role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "角色数据范围表");

            migrationBuilder.CreateTable(
                name: "sys_role_menu",
                columns: table => new
                {
                    SysRoleId = table.Column<long>(type: "bigint", nullable: false, comment: "角色Id"),
                    SysMenuId = table.Column<long>(type: "bigint", nullable: false, comment: "菜单Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_role_menu", x => new { x.SysRoleId, x.SysMenuId });
                    table.ForeignKey(
                        name: "FK_sys_role_menu_sys_menu_SysMenuId",
                        column: x => x.SysMenuId,
                        principalTable: "sys_menu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_sys_role_menu_sys_role_SysRoleId",
                        column: x => x.SysRoleId,
                        principalTable: "sys_role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "角色菜单表");

            migrationBuilder.CreateTable(
                name: "sys_user_data_scope",
                columns: table => new
                {
                    SysUserId = table.Column<long>(type: "bigint", nullable: false, comment: "用户Id"),
                    SysOrgId = table.Column<long>(type: "bigint", nullable: false, comment: "机构Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_user_data_scope", x => new { x.SysUserId, x.SysOrgId });
                    table.ForeignKey(
                        name: "FK_sys_user_data_scope_sys_org_SysOrgId",
                        column: x => x.SysOrgId,
                        principalTable: "sys_org",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_sys_user_data_scope_sys_user_SysUserId",
                        column: x => x.SysUserId,
                        principalTable: "sys_user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "用户数据范围表");

            migrationBuilder.CreateTable(
                name: "sys_user_role",
                columns: table => new
                {
                    SysUserId = table.Column<long>(type: "bigint", nullable: false, comment: "用户Id"),
                    SysRoleId = table.Column<long>(type: "bigint", nullable: false, comment: "角色Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_user_role", x => new { x.SysUserId, x.SysRoleId });
                    table.ForeignKey(
                        name: "FK_sys_user_role_sys_role_SysRoleId",
                        column: x => x.SysRoleId,
                        principalTable: "sys_role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_sys_user_role_sys_user_SysUserId",
                        column: x => x.SysUserId,
                        principalTable: "sys_user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "用户角色表");

            migrationBuilder.CreateTable(
                name: "ware_dict_data",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    TypeId = table.Column<long>(type: "bigint", nullable: false, comment: "字典类型Id"),
                    Value = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, comment: "值"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "编码"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    Status = table.Column<int>(type: "int", nullable: true, comment: "状态（字典 0正常 1停用 2删除）"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_dict_data", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ware_dict_data_ware_dic_type_TypeId",
                        column: x => x.TypeId,
                        principalTable: "ware_dic_type",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "仓库字典值表");

            migrationBuilder.CreateTable(
                name: "ware_location_vs_container",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    LocationId = table.Column<long>(type: "bigint", nullable: false, comment: "库位Id"),
                    LocationCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "库位编码"),
                    ContainerId = table.Column<long>(type: "bigint", nullable: false, comment: "容器Id"),
                    ContainerCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "供应商code"),
                    LocationVsContainerStatus = table.Column<int>(type: "int", nullable: true, comment: "状态（字典 0正常 1停用 2删除）"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_location_vs_container", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ware_location_vs_container_ware_container_ContainerId",
                        column: x => x.ContainerId,
                        principalTable: "ware_container",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ware_location_vs_container_ware_location_LocationId",
                        column: x => x.LocationId,
                        principalTable: "ware_location",
                        principalColumn: "Id");
                },
                comment: "库位与容器的关系");

            migrationBuilder.CreateTable(
                name: "ware_container_vs_material",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    OrderNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "组盘单据"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目号"),
                    ContainerId = table.Column<long>(type: "bigint", nullable: false, comment: "容器表Id"),
                    ContainerCode = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "容器表Id"),
                    MaterialId = table.Column<long>(type: "bigint", nullable: false, comment: "物料ID"),
                    BrandName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "品牌名称"),
                    XuHao = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "序号"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "品号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "品名"),
                    SpecificationModel = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "规格"),
                    Unit = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "单位"),
                    InspectionMethod = table.Column<int>(type: "int", nullable: false, comment: "检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4"),
                    BindQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "数量"),
                    ContainerVsMaterialStatus = table.Column<int>(type: "int", nullable: true, comment: "状态（字典 0正常 1停用 2删除）"),
                    SupplerCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "供应商code"),
                    IsJiaJi = table.Column<int>(type: "int", maxLength: 20, nullable: false, comment: "是否加急"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_container_vs_material", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ware_container_vs_material_ware_container_ContainerId",
                        column: x => x.ContainerId,
                        principalTable: "ware_container",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ware_container_vs_material_ware_material_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "ware_material",
                        principalColumn: "Id");
                },
                comment: "容器与物料的关系");

            migrationBuilder.CreateTable(
                name: "ware_order_details",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    OrderId = table.Column<long>(type: "bigint", maxLength: 50, nullable: false, comment: "单据ID"),
                    ProjectCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "项目编号"),
                    BrandName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "品牌名称"),
                    XuHao = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "序号"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "品号"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "品名"),
                    SpecificationModel = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true, comment: "规格"),
                    Unit = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "单位"),
                    OrderQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "单据数"),
                    ActualQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false, comment: "下发数"),
                    Remarks = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "备注"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_order_details", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ware_order_details_ware_order_OrderId",
                        column: x => x.OrderId,
                        principalTable: "ware_order",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "单据明细表");

            migrationBuilder.InsertData(
                table: "sys_app",
                columns: new[] { "Id", "Active", "Code", "CreatedTime", "CreatedUserId", "CreatedUserName", "IsDeleted", "Name", "Sort", "Status", "UpdatedTime", "UpdatedUserId", "UpdatedUserName" },
                values: new object[,]
                {
                    { 142307070898245L, "Y", "system", null, null, null, false, "开发管理", 100, 0, null, null, null },
                    { 142307070902341L, "N", "manage", null, null, null, false, "系统管理", 300, 0, null, null, null },
                    { 142307070922869L, "N", "busiapp", null, null, null, false, "业务应用", 400, 0, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "sys_config",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "CreatedUserName", "GroupCode", "IsDeleted", "Name", "Remark", "Status", "SysFlag", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Value" },
                values: new object[,]
                {
                    { 142307070902342L, "DILON_JWT_SECRET", null, null, null, "DEFAULT", false, "jwt密钥", "（重要）jwt密钥，默认为空，自行设置", 0, "Y", null, null, null, "weiben" },
                    { 142307070902343L, "DILON_DEFAULT_PASSWORD", null, null, null, "DEFAULT", false, "默认密码", "默认密码", 0, "Y", null, null, null, "123456" },
                    { 142307070902344L, "DILON_TOKEN_EXPIRE", null, null, null, "DEFAULT", false, "token过期时间", "token过期时间（单位：秒）", 0, "Y", null, null, null, "86400" },
                    { 142307070902345L, "DILON_SESSION_EXPIRE", null, null, null, "DEFAULT", false, "session会话过期时间", "session会话过期时间（单位：秒）", 0, "Y", null, null, null, "7200" },
                    { 142307070902361L, "DILON_FILE_UPLOAD_PATH_FOR_WINDOWS", null, null, null, "FILE_PATH", false, "Win本地上传文件路径", "Win本地上传文件路径", 0, "Y", null, null, null, "D:/tmp" },
                    { 142307070902363L, "DILON_UN_XSS_FILTER_URL", null, null, null, "DEFAULT", false, "放开XSS过滤的接口", "多个url可以用英文逗号隔开", 0, "Y", null, null, null, "/demo/xssfilter,/demo/unxss" },
                    { 142307070902364L, "DILON_ENABLE_SINGLE_LOGIN", null, null, null, "DEFAULT", false, "单用户登陆的开关", "true-打开，false-关闭，如果一个人登录两次，就会将上一次登陆挤下去", 0, "Y", null, null, null, "false" },
                    { 142307070902365L, "DILON_CAPTCHA_OPEN", null, null, null, "DEFAULT", false, "登录验证码的开关", "true-打开，false-关闭", 0, "Y", null, null, null, "true" }
                });

            migrationBuilder.InsertData(
                table: "sys_dict_type",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "CreatedUserName", "IsDeleted", "Name", "Remark", "Sort", "Status", "UpdatedTime", "UpdatedUserId", "UpdatedUserName" },
                values: new object[,]
                {
                    { 142307070906483L, "common_status", null, null, null, false, "通用状态", "通用状态", 100, 0, null, null, null },
                    { 142307070906484L, "sex", null, null, null, false, "性别", "性别字典", 100, 0, null, null, null },
                    { 142307070906485L, "consts_type", null, null, null, false, "常量的分类", "常量的分类，用于区别一组配置", 100, 0, null, null, null },
                    { 142307070906486L, "yes_or_no", null, null, null, false, "是否", "是否", 100, 0, null, null, null },
                    { 142307070906487L, "vis_type", null, null, null, false, "访问类型", "访问类型", 100, 0, null, null, null },
                    { 142307070906488L, "menu_type", null, null, null, false, "菜单类型", "菜单类型", 100, 0, null, null, null },
                    { 142307070906489L, "send_type", null, null, null, false, "发送类型", "发送类型", 100, 0, null, null, null },
                    { 142307070906490L, "open_type", null, null, null, false, "打开方式", "打开方式", 100, 0, null, null, null },
                    { 142307070906491L, "menu_weight", null, null, null, false, "菜单权重", "菜单权重", 100, 0, null, null, null },
                    { 142307070906492L, "data_scope_type", null, null, null, false, "数据范围类型", "数据范围类型", 100, 0, null, null, null },
                    { 142307070906494L, "op_type", null, null, null, false, "操作类型", "操作类型", 100, 0, null, null, null },
                    { 142307070906495L, "file_storage_location", null, null, null, false, "文件存储位置", "文件存储位置", 100, 0, null, null, null },
                    { 142307070910533L, "run_status", null, null, null, false, "运行状态", "定时任务运行状态", 100, 0, null, null, null },
                    { 142307070910534L, "notice_type", null, null, null, false, "通知公告类型", "通知公告类型", 100, 0, null, null, null },
                    { 142307070910535L, "notice_status", null, null, null, false, "通知公告状态", "通知公告状态", 100, 0, null, null, null },
                    { 142307070910536L, "yes_true_false", null, null, null, false, "是否boolean", "是否boolean", 100, 0, null, null, null },
                    { 142307070910537L, "code_gen_create_type", null, null, null, false, "代码生成方式", "代码生成方式", 100, 0, null, null, null },
                    { 142307070910538L, "request_type", null, null, null, false, "请求方式", "请求方式", 100, 0, null, null, null },
                    { 142307070922827L, "code_gen_effect_type", null, null, null, false, "代码生成作用类型", "代码生成作用类型", 100, 0, null, null, null },
                    { 142307070922828L, "code_gen_query_type", null, null, null, false, "代码生成查询类型", "代码生成查询类型", 100, 0, null, null, null },
                    { 142307070922829L, "code_gen_net_type", null, null, null, false, "代码生成.NET类型", "代码生成.NET类型", 100, 0, null, null, null },
                    { 142307070926941L, "role_type", null, null, null, false, "角色类型", "角色类型", 100, 0, null, null, null },
                    { 142307070926942L, "org_type", null, null, null, false, "机构类型", "机构类型", 100, 0, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "sys_emp",
                columns: new[] { "Id", "JobNum", "OrgId", "OrgName" },
                values: new object[,]
                {
                    { 142307070910551L, "D1001", 142307070910539L, "华夏集团" },
                    { 142307070910552L, "D1002", 142307070910539L, "华夏集团" },
                    { 142307070910553L, "D1003", 142307070910539L, "华夏集团" },
                    { 142307070910554L, "D1001", 142307070910547L, "租户1公司" },
                    { 142307070910555L, "D1002", 142307070910547L, "租户1公司" },
                    { 142307070910556L, "D1003", 142307070910547L, "租户1公司" },
                    { 142307070910557L, "D1001", 142307070910548L, "租户2公司" },
                    { 142307070910558L, "D1002", 142307070910548L, "租户2公司" }
                });

            migrationBuilder.InsertData(
                table: "sys_emp",
                columns: new[] { "Id", "JobNum", "OrgId", "OrgName" },
                values: new object[] { 142307070910559L, "D1003", 142307070910548L, "租户2公司" });

            migrationBuilder.InsertData(
                table: "sys_menu",
                columns: new[] { "Id", "Application", "Code", "Component", "CreatedTime", "CreatedUserId", "CreatedUserName", "Icon", "IsDeleted", "Link", "Name", "OpenType", "Permission", "Pid", "Pids", "Redirect", "Remark", "Router", "Sort", "Status", "Type", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Visible", "Weight" },
                values: new object[,]
                {
                    { 142307000914633L, "manage", "sys_mgr", "PageView", null, null, null, "team", false, null, "组织架构", 0, null, 0L, "[0],", null, null, "/sys", 100, 0, 0, null, null, null, "Y", 1 },
                    { 142307070910563L, "manage", "auth_manager", "PageView", null, null, null, "safety-certificate", false, null, "权限管理", 0, null, 0L, "[0],", null, null, "/auth", 100, 0, 0, null, null, null, "Y", 1 },
                    { 142307070910564L, "manage", "sys_user_mgr", "system/user/index", null, null, null, null, false, null, "用户管理", 1, null, 142307070910563L, "[0],[142307070910563],", null, null, "/mgr_user", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070910565L, "manage", "sys_user_mgr_page", null, null, null, null, null, false, null, "用户查询", 0, "sysUser:page", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910566L, "manage", "sys_user_mgr_edit", null, null, null, null, null, false, null, "用户编辑", 0, "sysUser:edit", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910567L, "manage", "sys_user_mgr_add", null, null, null, null, null, false, null, "用户增加", 0, "sysUser:add", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910568L, "manage", "sys_user_mgr_delete", null, null, null, null, null, false, null, "用户删除", 0, "sysUser:delete", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910569L, "manage", "sys_user_mgr_detail", null, null, null, null, null, false, null, "用户详情", 0, "sysUser:detail", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910570L, "manage", "sys_user_mgr_export", null, null, null, null, null, false, null, "用户导出", 0, "sysUser:export", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910571L, "manage", "sys_user_mgr_selector", null, null, null, null, null, false, null, "用户选择器", 0, "sysUser:selector", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910572L, "manage", "sys_user_mgr_grant_role", null, null, null, null, null, false, null, "用户授权角色", 0, "sysUser:grantRole", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910573L, "manage", "sys_user_mgr_own_role", null, null, null, null, null, false, null, "用户拥有角色", 0, "sysUser:ownRole", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910574L, "manage", "sys_user_mgr_grant_data", null, null, null, null, null, false, null, "用户授权数据", 0, "sysUser:grantData", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910575L, "manage", "sys_user_mgr_own_data", null, null, null, null, null, false, null, "用户拥有数据", 0, "sysUser:ownData", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910576L, "manage", "sys_user_mgr_update_info", null, null, null, null, null, false, null, "用户更新信息", 0, "sysUser:updateInfo", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910577L, "manage", "sys_user_mgr_update_pwd", null, null, null, null, null, false, null, "用户修改密码", 0, "sysUser:updatePwd", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910578L, "manage", "sys_user_mgr_change_status", null, null, null, null, null, false, null, "用户修改状态", 0, "sysUser:changeStatus", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910579L, "manage", "sys_user_mgr_update_avatar", null, null, null, null, null, false, null, "用户修改头像", 0, "sysUser:updateAvatar", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910580L, "manage", "sys_user_mgr_reset_pwd", null, null, null, null, null, false, null, "用户重置密码", 0, "sysUser:resetPwd", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910581L, "manage", "sys_org_mgr", "system/org/index", null, null, null, null, false, null, "机构管理", 1, null, 142307000914633L, "[0],[142307000914633],", null, null, "/org", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070910582L, "manage", "sys_org_mgr_page", null, null, null, null, null, false, null, "机构查询", 0, "sysOrg:page", 142307070910581L, "[0],[142307000914633],[142307070910581],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910583L, "manage", "sys_org_mgr_list", null, null, null, null, null, false, null, "机构列表", 0, "sysOrg:list", 142307070910581L, "[0],[142307000914633],[142307070910581],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910584L, "manage", "sys_org_mgr_add", null, null, null, null, null, false, null, "机构增加", 0, "sysOrg:add", 142307070910581L, "[0],[142307000914633],[142307070910581],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910585L, "manage", "sys_org_mgr_edit", null, null, null, null, null, false, null, "机构编辑", 0, "sysOrg:edit", 142307070910581L, "[0],[142307000914633],[142307070910581],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910586L, "manage", "sys_org_mgr_delete", null, null, null, null, null, false, null, "机构删除", 0, "sysOrg:delete", 142307070910581L, "[0],[142307000914633],[142307070910581],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910587L, "manage", "sys_org_mgr_detail", null, null, null, null, null, false, null, "机构详情", 0, "sysOrg:detail", 142307070910581L, "[0],[142307000914633],[142307070910581],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910588L, "manage", "sys_org_mgr_tree", null, null, null, null, null, false, null, "机构树", 0, "sysOrg:tree", 142307070910581L, "[0],[142307000914633],[142307070910581],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910589L, "manage", "sys_pos_mgr", "system/pos/index", null, null, null, null, false, null, "职位管理", 1, null, 142307000914633L, "[0],[142307000914633],", null, null, "/pos", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070910590L, "manage", "sys_pos_mgr_page", null, null, null, null, null, false, null, "职位查询", 0, "sysPos:page", 142307070910589L, "[0],[142307000914633],[142307070910589],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070910591L, "manage", "sys_pos_mgr_list", null, null, null, null, null, false, null, "职位列表", 0, "sysPos:list", 142307070910589L, "[0],[142307000914633],[142307070910589],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070911739L, "manage", "sys_log_mgr_ex_log", "system/log/exlog/index", null, null, null, null, false, null, "异常日志", 1, null, 142307070918732L, "[0],[142307070918732],", null, null, "/exlog", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070911740L, "manage", "sys_log_mgr_ex_log_page", null, null, null, null, null, false, null, "异常日志查询", 0, "sysExLog:page", 142307070911739L, "[0],[142307070918732],[142307070911739],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070911741L, "manage", "sys_log_mgr_ex_log_delete", null, null, null, null, null, false, null, "异常日志清空", 0, "sysExLog:delete", 142307070911739L, "[0],[142307070918732],[142307070911739],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914629L, "manage", "sys_pos_mgr_add", null, null, null, null, null, false, null, "职位增加", 0, "sysPos:add", 142307070910589L, "[0],[142307000914633],[142307070910589],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914630L, "manage", "sys_pos_mgr_edit", null, null, null, null, null, false, null, "职位编辑", 0, "sysPos:edit", 142307070910589L, "[0],[142307000914633],[142307070910589],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914631L, "manage", "sys_pos_mgr_delete", null, null, null, null, null, false, null, "职位删除", 0, "sysPos:delete", 142307070910589L, "[0],[142307000914633],[142307070910589],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914632L, "manage", "sys_pos_mgr_detail", null, null, null, null, null, false, null, "职位详情", 0, "sysPos:detail", 142307070910589L, "[0],[142307000914633],[142307070910589],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914633L, "system", "sys_platform", "PageView", null, null, null, "safety-certificate", false, null, "平台管理", 0, null, 0L, "[0],", null, null, "/platform", 100, 0, 0, null, null, null, "Y", 1 },
                    { 142307070914634L, "system", "sys_app_mgr", "system/app/index", null, null, null, null, false, null, "应用管理", 1, null, 142307070914633L, "[0],[142307070914633],", null, null, "/app", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070914635L, "system", "sys_app_mgr_page", null, null, null, null, null, false, null, "应用查询", 0, "sysApp:page", 142307070914634L, "[0],[142307070914633],[142307070914634],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914636L, "system", "sys_app_mgr_list", null, null, null, null, null, false, null, "应用列表", 0, "sysApp:list", 142307070914634L, "[0],[142307070914633],[142307070914634],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 }
                });

            migrationBuilder.InsertData(
                table: "sys_menu",
                columns: new[] { "Id", "Application", "Code", "Component", "CreatedTime", "CreatedUserId", "CreatedUserName", "Icon", "IsDeleted", "Link", "Name", "OpenType", "Permission", "Pid", "Pids", "Redirect", "Remark", "Router", "Sort", "Status", "Type", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Visible", "Weight" },
                values: new object[,]
                {
                    { 142307070914637L, "system", "sys_app_mgr_add", null, null, null, null, null, false, null, "应用增加", 0, "sysApp:add", 142307070914634L, "[0],[142307070914633],[142307070914634],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914638L, "system", "sys_app_mgr_edit", null, null, null, null, null, false, null, "应用编辑", 0, "sysApp:edit", 142307070914634L, "[0],[142307070914633],[142307070914634],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914639L, "system", "sys_app_mgr_delete", null, null, null, null, null, false, null, "应用删除", 0, "sysApp:delete", 142307070914634L, "[0],[142307070914633],[142307070914634],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914640L, "system", "sys_app_mgr_detail", null, null, null, null, null, false, null, "应用详情", 0, "sysApp:detail", 142307070914634L, "[0],[142307070914633],[142307070914634],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914641L, "system", "sys_app_mgr_set_as_default", null, null, null, null, null, false, null, "设为默认应用", 0, "sysApp:setAsDefault", 142307070914634L, "[0],[142307070914633],[142307070914634],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914642L, "system", "sys_menu_mgr", "system/menu/index", null, null, null, null, false, null, "菜单管理", 1, null, 142307070914633L, "[0],[142307070914633],", null, null, "/menu", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070914643L, "system", "sys_menu_mgr_list", null, null, null, null, null, false, null, "菜单列表", 0, "sysMenu:list", 142307070914642L, "[0],[142307070914633],[142307070914642],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914644L, "system", "sys_menu_mgr_add", null, null, null, null, null, false, null, "菜单增加", 0, "sysMenu:add", 142307070914642L, "[0],[142307070914633],[142307070914642],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914645L, "system", "sys_menu_mgr_edit", null, null, null, null, null, false, null, "菜单编辑", 0, "sysMenu:edit", 142307070914642L, "[0],[142307070914633],[142307070914642],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914646L, "system", "sys_menu_mgr_delete", null, null, null, null, null, false, null, "菜单删除", 0, "sysMenu:delete", 142307070914642L, "[0],[142307070914633],[142307070914642],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914647L, "system", "sys_menu_mgr_detail", null, null, null, null, null, false, null, "菜单详情", 0, "sysMenu:detail", 142307070914642L, "[0],[142307070914633],[142307070914642],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914648L, "manage", "sys_menu_mgr_grant_tree", null, null, null, null, null, false, null, "菜单授权树", 0, "sysMenu:treeForGrant", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914649L, "manage", "sys_menu_mgr_tree", null, null, null, null, null, false, null, "菜单树", 0, "sysMenu:tree", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914650L, "system", "sys_menu_mgr_change", null, null, null, null, null, false, null, "菜单切换", 0, "sysMenu:change", 142307070914642L, "[0],[142307070914633],[142307070914642],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914651L, "manage", "sys_role_mgr", "system/role/index", null, null, null, null, false, null, "角色管理", 1, null, 142307070910563L, "[0],[142307070910563],", null, null, "/role", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070914652L, "manage", "sys_role_mgr_page", null, null, null, null, null, false, null, "角色查询", 0, "sysRole:page", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914653L, "manage", "sys_role_mgr_add", null, null, null, null, null, false, null, "角色增加", 0, "sysRole:add", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914654L, "manage", "sys_role_mgr_edit", null, null, null, null, null, false, null, "角色编辑", 0, "sysRole:edit", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914655L, "manage", "sys_role_mgr_delete", null, null, null, null, null, false, null, "角色删除", 0, "sysRole:delete", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914656L, "manage", "sys_role_mgr_detail", null, null, null, null, null, false, null, "角色详情", 0, "sysRole:detail", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914657L, "manage", "sys_role_mgr_drop_down", null, null, null, null, null, false, null, "角色下拉", 0, "sysRole:dropDown", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914658L, "manage", "sys_role_mgr_grant_menu", null, null, null, null, null, false, null, "角色授权菜单", 0, "sysRole:grantMenu", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914659L, "manage", "sys_role_mgr_own_menu", null, null, null, null, null, false, null, "角色拥有菜单", 0, "sysRole:ownMenu", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914660L, "manage", "sys_role_mgr_grant_data", null, null, null, null, null, false, null, "角色授权数据", 0, "sysRole:grantData", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914661L, "manage", "sys_role_mgr_own_data", null, null, null, null, null, false, null, "角色拥有数据", 0, "sysRole:ownData", 142307070914651L, "[0],[142307070910563],[142307070914651],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914662L, "system", "system_tools", "PageView", null, null, null, "euro", false, null, "开发管理", 0, null, 0L, "[0],", null, null, "/tools", 100, 0, 0, null, null, null, "Y", 1 },
                    { 142307070914663L, "system", "system_tools_config", "system/config/index", null, null, null, null, false, null, "系统配置", 1, null, 142307070914662L, "[0],[142307070914662],", null, null, "/config", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070914664L, "system", "system_tools_config_page", null, null, null, null, null, false, null, "配置查询", 0, "sysConfig:page", 142307070914663L, "[0],[142307070914662],[142307070914663],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914665L, "system", "system_tools_config_list", null, null, null, null, null, false, null, "配置列表", 0, "sysConfig:list", 142307070914663L, "[0],[142307070914662],[142307070914663],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914666L, "system", "system_tools_config_add", null, null, null, null, null, false, null, "配置增加", 0, "sysConfig:add", 142307070914663L, "[0],[142307070914662],[142307070914663],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914667L, "system", "system_tools_config_edit", null, null, null, null, null, false, null, "配置编辑", 0, "sysConfig:edit", 142307070914663L, "[0],[142307070914662],[142307070914663],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914668L, "system", "system_tools_config_delete", null, null, null, null, null, false, null, "配置删除", 0, "sysConfig:delete", 142307070914663L, "[0],[142307070914662],[142307070914663],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914669L, "system", "system_tools_config_detail", null, null, null, null, null, false, null, "配置详情", 0, "sysConfig:detail", 142307070914663L, "[0],[142307070914662],[142307070914663],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914678L, "system", "sys_dict_mgr", "system/dict/index", null, null, null, null, false, null, "字典管理", 1, null, 142307070914662L, "[0],[142307070914662],", null, null, "/dict", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070914679L, "system", "sys_dict_mgr_dict_type_page", null, null, null, null, null, false, null, "字典类型查询", 0, "sysDictType:page", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914680L, "system", "sys_dict_mgr_dict_type_list", null, null, null, null, null, false, null, "字典类型列表", 0, "sysDictType:list", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914681L, "system", "sys_dict_mgr_dict_type_add", null, null, null, null, null, false, null, "字典类型增加", 0, "sysDictType:add", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914682L, "system", "sys_dict_mgr_dict_type_delete", null, null, null, null, null, false, null, "字典类型删除", 0, "sysDictType:delete", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914683L, "system", "sys_dict_mgr_dict_type_edit", null, null, null, null, null, false, null, "字典类型编辑", 0, "sysDictType:edit", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914684L, "system", "sys_dict_mgr_dict_type_detail", null, null, null, null, null, false, null, "字典类型详情", 0, "sysDictType:detail", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914685L, "system", "sys_dict_mgr_dict_type_drop_down", null, null, null, null, null, false, null, "字典类型下拉", 0, "sysDictType:dropDown", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070914686L, "system", "sys_dict_mgr_dict_type_change_status", null, null, null, null, null, false, null, "字典类型修改状态", 0, "sysDictType:changeStatus", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 }
                });

            migrationBuilder.InsertData(
                table: "sys_menu",
                columns: new[] { "Id", "Application", "Code", "Component", "CreatedTime", "CreatedUserId", "CreatedUserName", "Icon", "IsDeleted", "Link", "Name", "OpenType", "Permission", "Pid", "Pids", "Redirect", "Remark", "Router", "Sort", "Status", "Type", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Visible", "Weight" },
                values: new object[,]
                {
                    { 142307070914687L, "system", "sys_dict_mgr_dict_page", null, null, null, null, null, false, null, "字典值查询", 0, "sysDictData:page", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918725L, "system", "sys_dict_mgr_dict_list", null, null, null, null, null, false, null, "字典值列表", 0, "sysDictData:list", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918726L, "system", "sys_dict_mgr_dict_add", null, null, null, null, null, false, null, "字典值增加", 0, "sysDictData:add", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918727L, "system", "sys_dict_mgr_dict_delete", null, null, null, null, null, false, null, "字典值删除", 0, "sysDictData:delete", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918728L, "system", "sys_dict_mgr_dict_edit", null, null, null, null, null, false, null, "字典值编辑", 0, "sysDictData:edit", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918729L, "system", "sys_dict_mgr_dict_detail", null, null, null, null, null, false, null, "字典值详情", 0, "sysDictData:detail", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918730L, "system", "sys_dict_mgr_dict_change_status", null, null, null, null, null, false, null, "字典值修改状态", 0, "sysDictData:changeStatus", 142307070914678L, "[0],[142307070914662],[142307070914678],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918731L, "system", "sys_swagger_mgr", "Iframe", null, null, null, null, false, "http://localhost:6677/swagger/", "接口文档", 2, null, 142307070914662L, "[0],[142307070914662],", null, null, "/swagger", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918732L, "manage", "sys_log_mgr", "PageView", null, null, null, "read", false, null, "日志管理", 0, null, 0L, "[0],", null, null, "/log", 100, 0, 0, null, null, null, "Y", 1 },
                    { 142307070918733L, "manage", "sys_log_mgr_vis_log", "system/log/vislog/index", null, null, null, null, false, null, "访问日志", 1, null, 142307070918732L, "[0],[142307070918732],", null, null, "/vislog", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918734L, "manage", "sys_log_mgr_vis_log_page", null, null, null, null, null, false, null, "访问日志查询", 0, "sysVisLog:page", 142307070918733L, "[0],[142307070918732],[142307070918733],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918735L, "manage", "sys_log_mgr_vis_log_delete", null, null, null, null, null, false, null, "访问日志清空", 0, "sysVisLog:delete", 142307070918733L, "[0],[142307070918732],[142307070918733],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918736L, "manage", "sys_log_mgr_op_log", "system/log/oplog/index", null, null, null, null, false, null, "操作日志", 1, null, 142307070918732L, "[0],[142307070918732],", null, null, "/oplog", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918737L, "manage", "sys_log_mgr_op_log_page", null, null, null, null, null, false, null, "操作日志查询", 0, "sysOpLog:page", 142307070918736L, "[0],[142307070918732],[142307070918736],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918738L, "manage", "sys_log_mgr_op_log_delete", null, null, null, null, null, false, null, "操作日志清空", 0, "sysOpLog:delete", 142307070918736L, "[0],[142307070918732],[142307070918736],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918739L, "system", "sys_monitor_mgr", "PageView", null, null, null, "deployment-unit", false, null, "系统监控", 0, null, 0L, "[0],", null, null, "/monitor", 100, 0, 0, null, null, null, "Y", 1 },
                    { 142307070918740L, "system", "sys_monitor_mgr_machine_monitor", "system/machine/index", null, null, null, null, false, null, "服务监控", 1, null, 142307070918739L, "[0],[142307070918739],", null, null, "/machine", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918741L, "system", "sys_monitor_mgr_machine_monitor_query", null, null, null, null, null, false, null, "服务监控查询", 0, "sysMachine:query", 142307070918740L, "[0],[142307070918739],[142307070918740],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918742L, "system", "sys_monitor_mgr_online_user", "system/onlineUser/index", null, null, null, null, false, null, "在线用户", 1, null, 142307070918739L, "[0],[142307070918739],", null, null, "/onlineUser", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918743L, "system", "sys_monitor_mgr_online_user_page", null, null, null, null, null, false, null, "在线用户查询", 0, "sysOnlineUser:page", 142307070918742L, "[0],[142307070918739],[142307070918742],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918744L, "system", "sys_monitor_mgr_online_user_force_exist", null, null, null, null, null, false, null, "在线用户强退", 0, "sysOnlineUser:forceExist", 142307070918742L, "[0],[142307070918739],[142307070918742],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918745L, "system", "sys_monitor_mgr_druid", "Iframe", null, null, null, null, false, "http://localhost:82/druid/login.html", "数据监控", 2, null, 142307070918739L, "[0],[142307070918739],", null, null, "/druid", 100, 0, 1, null, null, null, "N", 1 },
                    { 142307070918746L, "manage", "sys_notice", "PageView", null, null, null, "sound", false, null, "通知公告", 0, null, 0L, "[0],", null, null, "/notice", 100, 0, 0, null, null, null, "Y", 1 },
                    { 142307070918747L, "manage", "sys_notice_mgr", "system/notice/index", null, null, null, null, false, null, "公告管理", 1, null, 142307070918746L, "[0],[142307070918746],", null, null, "/notice", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918748L, "manage", "sys_notice_mgr_page", null, null, null, null, null, false, null, "公告查询", 0, "sysNotice:page", 142307070918747L, "[0],[142307070918746],[142307070918747],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918749L, "manage", "sys_notice_mgr_add", null, null, null, null, null, false, null, "公告增加", 0, "sysNotice:add", 142307070918747L, "[0],[142307070918746],[142307070918747],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918750L, "manage", "sys_notice_mgr_edit", null, null, null, null, null, false, null, "公告编辑", 0, "sysNotice:edit", 142307070918747L, "[0],[142307070918746],[142307070918747],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918751L, "manage", "sys_notice_mgr_delete", null, null, null, null, null, false, null, "公告删除", 0, "sysNotice:delete", 142307070918747L, "[0],[142307070918746],[142307070918747],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918752L, "manage", "sys_notice_mgr_detail", null, null, null, null, null, false, null, "公告查看", 0, "sysNotice:detail", 142307070918747L, "[0],[142307070918746],[142307070918747],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918753L, "manage", "sys_notice_mgr_changeStatus", null, null, null, null, null, false, null, "公告修改状态", 0, "sysNotice:changeStatus", 142307070918747L, "[0],[142307070918746],[142307070918747],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918754L, "manage", "sys_notice_mgr_received", "system/noticeReceived/index", null, null, null, null, false, null, "已收公告", 1, null, 142307070918746L, "[0],[142307070918746],", null, null, "/noticeReceived", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918755L, "manage", "sys_notice_mgr_received_page", null, null, null, null, null, false, null, "已收公告查询", 0, "sysNotice:received", 142307070918754L, "[0],[142307070918746],[142307070918754],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918756L, "manage", "sys_file_mgr", "PageView", null, null, null, "file", false, null, "文件管理", 0, null, 0L, "[0],", null, null, "/file", 100, 0, 0, null, null, null, "Y", 1 },
                    { 142307070918757L, "manage", "sys_file_mgr_sys_file", "system/file/index", null, null, null, null, false, null, "系统文件", 1, null, 142307070918756L, "[0],[142307070918756],", null, null, "/file", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918758L, "manage", "sys_file_mgr_sys_file_page", null, null, null, null, null, false, null, "文件查询", 0, "sysFileInfo:page", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918759L, "manage", "sys_file_mgr_sys_file_list", null, null, null, null, null, false, null, "文件列表", 0, "sysFileInfo:list", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918760L, "manage", "sys_file_mgr_sys_file_delete", null, null, null, null, null, false, null, "文件删除", 0, "sysFileInfo:delete", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918761L, "manage", "sys_file_mgr_sys_file_detail", null, null, null, null, null, false, null, "文件详情", 0, "sysFileInfo:detail", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918762L, "manage", "sys_file_mgr_sys_file_upload", null, null, null, null, null, false, null, "文件上传", 0, "sysFileInfo:upload", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918763L, "manage", "sys_file_mgr_sys_file_download", null, null, null, null, null, false, null, "文件下载", 0, "sysFileInfo:download", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918764L, "manage", "sys_file_mgr_sys_file_preview", null, null, null, null, null, false, null, "图片预览", 0, "sysFileInfo:preview", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918765L, "system", "sys_timers", "PageView", null, null, null, "dashboard", false, null, "任务调度", 0, null, 0L, "[0],", null, null, "/timers", 100, 0, 0, null, null, null, "Y", 1 }
                });

            migrationBuilder.InsertData(
                table: "sys_menu",
                columns: new[] { "Id", "Application", "Code", "Component", "CreatedTime", "CreatedUserId", "CreatedUserName", "Icon", "IsDeleted", "Link", "Name", "OpenType", "Permission", "Pid", "Pids", "Redirect", "Remark", "Router", "Sort", "Status", "Type", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Visible", "Weight" },
                values: new object[,]
                {
                    { 142307070918766L, "system", "sys_timers_mgr", "system/timers/index", null, null, null, null, false, null, "任务管理", 1, null, 142307070918765L, "[0],[142307070918765],", null, null, "/timers", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918767L, "system", "sys_timers_mgr_page", null, null, null, null, null, false, null, "定时任务查询", 0, "sysTimers:page", 142307070918766L, "[0],[142307070918765],[142307070918766],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918768L, "system", "sys_timers_mgr_list", null, null, null, null, null, false, null, "定时任务列表", 0, "sysTimers:list", 142307070918766L, "[0],[142307070918765],[142307070918766],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918769L, "system", "sys_timers_mgr_detail", null, null, null, null, null, false, null, "定时任务详情", 0, "sysTimers:detail", 142307070918766L, "[0],[142307070918765],[142307070918766],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918770L, "system", "sys_timers_mgr_add", null, null, null, null, null, false, null, "定时任务增加", 0, "sysTimers:add", 142307070918766L, "[0],[142307070918765],[142307070918766],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918771L, "system", "sys_timers_mgr_delete", null, null, null, null, null, false, null, "定时任务删除", 0, "sysTimers:delete", 142307070918766L, "[0],[142307070918765],[142307070918766],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918772L, "system", "sys_timers_mgr_edit", null, null, null, null, null, false, null, "定时任务编辑", 0, "sysTimers:edit", 142307070918766L, "[0],[142307070918765],[142307070918766],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918773L, "system", "sys_timers_mgr_get_action_classes", null, null, null, null, null, false, null, "定时任务可执行列表", 0, "sysTimers:getActionClasses", 142307070918766L, "[0],[142307070918765],[142307070918766],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918774L, "system", "sys_timers_mgr_start", null, null, null, null, null, false, null, "定时任务启动", 0, "sysTimers:start", 142307070918766L, "[0],[142307070918765],[142307070918766],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918775L, "system", "sys_timers_mgr_stop", null, null, null, null, null, false, null, "定时任务关闭", 0, "sysTimers:stop", 142307070918766L, "[0],[142307070918765],[142307070918766],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070918776L, "system", "code_gen", "PageView", null, null, null, "thunderbolt", false, null, "代码生成", 0, null, 0L, "[0],", null, null, "/codeGenerate", 100, 0, 0, null, null, null, "Y", 1 },
                    { 142307070918777L, "manage", "sys_user_mgr_login", null, null, null, null, null, false, null, "用户登录信息", 0, "getLoginUser", 142307070910564L, "[0],[142307070910563],[142307070910564],", null, null, null, 100, 1, 2, null, null, null, "N", 1 },
                    { 142307070918778L, "system", "low_code", "gen/lowCode/index", null, null, null, null, false, null, "表单设计", 0, null, 142307070918776L, "[0],[142307070918776],", null, null, "/lowCode/index", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070918779L, "system", "code_gen_gen", "gen/codeGenerate/index", null, null, null, null, false, null, "代码生成", 0, null, 142307070918776L, "[0],[142307070918776],", null, null, "/codeGenerate/index", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070922870L, "system", "form_design", "system/formDesign/index", null, null, null, null, false, null, "表单设计", 0, null, 285599875018821L, "[0],[285599875018821],", null, null, "/formDesign/index", 100, 0, 1, null, null, null, "Y", 1 },
                    { 142307070922874L, "manage", "sys_file_mgr_sys_file_upload_avatar", null, null, null, null, null, false, null, "头像上传", 0, "sysFileInfo:uploadAvatar", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070922875L, "manage", "sys_file_mgr_sys_file_upload_document", null, null, null, null, null, false, null, "文档上传", 0, "sysFileInfo:uploadDocument", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 },
                    { 142307070922876L, "manage", "sys_file_mgr_sys_file_upload_shop", null, null, null, null, null, false, null, "商城上传", 0, "sysFileInfo:uploadShop", 142307070918757L, "[0],[142307070918756],[142307070918757],", null, null, null, 100, 0, 2, null, null, null, "Y", 1 }
                });

            migrationBuilder.InsertData(
                table: "sys_org",
                columns: new[] { "Id", "Code", "Contacts", "CreatedTime", "CreatedUserId", "CreatedUserName", "IsDeleted", "Name", "OrgType", "Pid", "Pids", "Remark", "Sort", "Status", "Tel", "UpdatedTime", "UpdatedUserId", "UpdatedUserName" },
                values: new object[,]
                {
                    { 142307070910539L, "wbgf", null, null, null, null, false, "伟本股份有限公司", null, 0L, "[0],", "伟本股份有限公司", 100, 0, null, null, null, null },
                    { 142307070910540L, "wbgf_sh", null, null, null, null, false, "伟本股份有限公司(上海)", null, 142307070910539L, "[0],[142307070910539],", "伟本股份有限公司-上海", 100, 0, null, null, null, null },
                    { 142307070910541L, "wbgf_cq", null, null, null, null, false, "伟本股份有限公司(重庆)", null, 142307070910539L, "[0],[142307070910539],", "伟本股份有限公司-重庆", 100, 0, null, null, null, null },
                    { 142307070910542L, "wbgf_sh_yfb", null, null, null, null, false, "研发部", null, 142307070910540L, "[0],[142307070910539],[142307070910540],", "", 100, 0, null, null, null, null },
                    { 142307070910543L, "wbgf_sh_qhb", null, null, null, null, false, "企划部", null, 142307070910540L, "[0],[142307070910539],[142307070910540],", "", 100, 0, null, null, null, null },
                    { 142307070910544L, "wbgf_cq_scb", null, null, null, null, false, "市场部", null, 142307070910541L, "[0],[142307070910539],[142307070910541],", "", 100, 0, null, null, null, null },
                    { 142307070910545L, "wbgf_cq_cwb", null, null, null, null, false, "财务部", null, 142307070910541L, "[0],[142307070910539],[142307070910541],", "", 100, 0, null, null, null, null },
                    { 142307070910546L, "wbgf_cq_scb", null, null, null, null, false, "市场部二部", null, 142307070910544L, "[0],[142307070910539],[142307070910541],[142307070910544],", "", 100, 0, null, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "sys_pos",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "CreatedUserName", "IsDeleted", "Name", "Remark", "Sort", "Status", "UpdatedTime", "UpdatedUserId", "UpdatedUserName" },
                values: new object[,]
                {
                    { 142307070910547L, "zjl", null, null, null, false, "总经理", "总经理", 100, 0, null, null, null },
                    { 142307070910548L, "fzjl", null, null, null, false, "副总经理", "副总经理", 101, 0, null, null, null },
                    { 142307070910549L, "bmjl", null, null, null, false, "部门经理", "部门经理", 102, 0, null, null, null },
                    { 142307070910550L, "gzry", null, null, null, false, "工作人员", "工作人员", 103, 0, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "sys_role",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "CreatedUserName", "DataScopeType", "IsDeleted", "Name", "Remark", "RoleType", "Sort", "Status", "UpdatedTime", "UpdatedUserId", "UpdatedUserName" },
                values: new object[,]
                {
                    { 142307070910554L, "sys_manager_role", null, null, null, 1, false, "系统管理员", "系统管理员", 0, 100, 0, null, null, null },
                    { 142307070910555L, "common_role", null, null, null, 5, false, "普通用户", "普通用户", 0, 101, 0, null, null, null },
                    { 142307070910556L, "sys_manager_role", null, null, null, 5, false, "系统管理员", "系统管理员", 0, 100, 0, null, null, null },
                    { 142307070910557L, "common_role", null, null, null, 5, false, "普通用户", "普通用户", 0, 101, 0, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "sys_timer",
                columns: new[] { "Id", "CreatedTime", "CreatedUserId", "CreatedUserName", "Cron", "DoOnce", "ExecuteType", "Headers", "Interval", "IsDeleted", "JobName", "Remark", "RequestParameters", "RequestType", "RequestUrl", "StartNow", "TimerType", "UpdatedTime", "UpdatedUserId", "UpdatedUserName" },
                values: new object[] { 142307070910556L, null, null, null, null, false, 1, null, 5, false, "百度api", "接口API", null, 2, "https://www.baidu.com", false, 0, null, null, null });

            migrationBuilder.InsertData(
                table: "sys_user",
                columns: new[] { "Id", "Account", "AdminType", "Avatar", "Birthday", "CreatedTime", "CreatedUserId", "CreatedUserName", "Email", "IsDeleted", "LastLoginIp", "LastLoginTime", "Name", "NickName", "Password", "Phone", "Sex", "Status", "Tel", "UpdatedTime", "UpdatedUserId", "UpdatedUserName" },
                values: new object[,]
                {
                    { 142307070910551L, "superAdmin", 1, null, new DateTimeOffset(new DateTime(1986, 7, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 0, 0, 0)), null, null, null, null, false, null, null, "超级管理员", "superAdmin", "e10adc3949ba59abbe56e057f20f883e", "18020030720", 1, 0, null, null, null, null },
                    { 142307070910552L, "admin", 2, null, new DateTimeOffset(new DateTime(1986, 7, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 0, 0, 0)), null, null, null, null, false, null, null, "系统管理员", "admin", "e10adc3949ba59abbe56e057f20f883e", "18020030720", 1, 0, null, null, null, null },
                    { 142307070910553L, "zuohuaijun", 3, null, new DateTimeOffset(new DateTime(1986, 7, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 0, 0, 0)), null, null, null, null, false, null, null, "普通用户", "zuohuaijun", "e10adc3949ba59abbe56e057f20f883e", "18020030720", 1, 0, null, null, null, null },
                    { 142307070910554L, "zuohuaijun@163.com", 2, null, new DateTimeOffset(new DateTime(1986, 7, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 0, 0, 0)), null, null, null, null, false, null, null, "系统管理员", "admin", "e10adc3949ba59abbe56e057f20f883e", "18020030720", 1, 0, null, null, null, null },
                    { 142307070910556L, "dilon@163.com", 3, null, new DateTimeOffset(new DateTime(1986, 7, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 0, 0, 0)), null, null, null, null, false, null, null, "普通用户", "dilon", "e10adc3949ba59abbe56e057f20f883e", "18020030720", 1, 0, null, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "sys_dict_data",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "CreatedUserName", "IsDeleted", "Remark", "Sort", "Status", "TypeId", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Value" },
                values: new object[,]
                {
                    { 142307070902375L, "1", null, null, null, false, "男性", 100, 0, 142307070906484L, null, null, null, "男" },
                    { 142307070902376L, "2", null, null, null, false, "女性", 100, 0, 142307070906484L, null, null, null, "女" },
                    { 142307070902377L, "3", null, null, null, false, "未知性别", 100, 0, 142307070906484L, null, null, null, "未知" },
                    { 142307070902384L, "0", null, null, null, false, "正常", 100, 0, 142307070906483L, null, null, null, "正常" },
                    { 142307070902385L, "1", null, null, null, false, "停用", 100, 0, 142307070906483L, null, null, null, "停用" },
                    { 142307070902386L, "2", null, null, null, false, "删除", 100, 0, 142307070906483L, null, null, null, "删除" },
                    { 142307070902387L, "N", null, null, null, false, "否", 100, 0, 142307070906486L, null, null, null, "否" },
                    { 142307070902388L, "Y", null, null, null, false, "是", 100, 0, 142307070906486L, null, null, null, "是" },
                    { 142307070902389L, "1", null, null, null, false, "登录", 100, 0, 142307070906487L, null, null, null, "登录" },
                    { 142307070902390L, "2", null, null, null, false, "登出", 100, 0, 142307070906487L, null, null, null, "登出" },
                    { 142307070902391L, "0", null, null, null, false, "目录", 100, 0, 142307070906488L, null, null, null, "目录" },
                    { 142307070902392L, "1", null, null, null, false, "菜单", 100, 0, 142307070906488L, null, null, null, "菜单" },
                    { 142307070902393L, "2", null, null, null, false, "按钮", 100, 0, 142307070906488L, null, null, null, "按钮" },
                    { 142307070902394L, "0", null, null, null, false, "未发送", 100, 0, 142307070906489L, null, null, null, "未发送" },
                    { 142307070902395L, "1", null, null, null, false, "发送成功", 100, 0, 142307070906489L, null, null, null, "发送成功" },
                    { 142307070902396L, "2", null, null, null, false, "发送失败", 100, 0, 142307070906489L, null, null, null, "发送失败" },
                    { 142307070902397L, "3", null, null, null, false, "失效", 100, 0, 142307070906489L, null, null, null, "失效" },
                    { 142307070902398L, "0", null, null, null, false, "无", 100, 0, 142307070906490L, null, null, null, "无" },
                    { 142307070902399L, "1", null, null, null, false, "组件", 100, 0, 142307070906490L, null, null, null, "组件" },
                    { 142307070906437L, "2", null, null, null, false, "内链", 100, 0, 142307070906490L, null, null, null, "内链" },
                    { 142307070906438L, "3", null, null, null, false, "外链", 100, 0, 142307070906490L, null, null, null, "外链" },
                    { 142307070906439L, "1", null, null, null, false, "系统权重", 100, 0, 142307070906491L, null, null, null, "系统权重" },
                    { 142307070906440L, "2", null, null, null, false, "业务权重", 100, 0, 142307070906491L, null, null, null, "业务权重" },
                    { 142307070906441L, "1", null, null, null, false, "全部数据", 100, 0, 142307070906492L, null, null, null, "全部数据" },
                    { 142307070906442L, "2", null, null, null, false, "本部门及以下数据", 100, 0, 142307070906492L, null, null, null, "本部门及以下数据" },
                    { 142307070906443L, "3", null, null, null, false, "本部门数据", 100, 0, 142307070906492L, null, null, null, "本部门数据" },
                    { 142307070906444L, "4", null, null, null, false, "仅本人数据", 100, 0, 142307070906492L, null, null, null, "仅本人数据" },
                    { 142307070906445L, "5", null, null, null, false, "自定义数据", 100, 0, 142307070906492L, null, null, null, "自定义数据" },
                    { 142307070906449L, "0", null, null, null, false, "其它", 100, 0, 142307070906494L, null, null, null, "其它" },
                    { 142307070906450L, "1", null, null, null, false, "增加", 100, 0, 142307070906494L, null, null, null, "增加" },
                    { 142307070906451L, "2", null, null, null, false, "删除", 100, 0, 142307070906494L, null, null, null, "删除" },
                    { 142307070906452L, "3", null, null, null, false, "编辑", 100, 0, 142307070906494L, null, null, null, "编辑" },
                    { 142307070906453L, "4", null, null, null, false, "更新", 100, 0, 142307070906494L, null, null, null, "更新" },
                    { 142307070906454L, "5", null, null, null, false, "查询", 100, 0, 142307070906494L, null, null, null, "查询" },
                    { 142307070906455L, "6", null, null, null, false, "详情", 100, 0, 142307070906494L, null, null, null, "详情" },
                    { 142307070906456L, "7", null, null, null, false, "树", 100, 0, 142307070906494L, null, null, null, "树" },
                    { 142307070906457L, "8", null, null, null, false, "导入", 100, 0, 142307070906494L, null, null, null, "导入" },
                    { 142307070906458L, "9", null, null, null, false, "导出", 100, 0, 142307070906494L, null, null, null, "导出" },
                    { 142307070906459L, "10", null, null, null, false, "授权", 100, 0, 142307070906494L, null, null, null, "授权" },
                    { 142307070906460L, "11", null, null, null, false, "强退", 100, 0, 142307070906494L, null, null, null, "强退" },
                    { 142307070906461L, "12", null, null, null, false, "清空", 100, 0, 142307070906494L, null, null, null, "清空" },
                    { 142307070906462L, "13", null, null, null, false, "修改状态", 100, 0, 142307070906494L, null, null, null, "修改状态" }
                });

            migrationBuilder.InsertData(
                table: "sys_dict_data",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "CreatedUserName", "IsDeleted", "Remark", "Sort", "Status", "TypeId", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Value" },
                values: new object[,]
                {
                    { 142307070906463L, "1", null, null, null, false, "阿里云", 100, 0, 142307070906495L, null, null, null, "阿里云" },
                    { 142307070906464L, "2", null, null, null, false, "腾讯云", 100, 0, 142307070906495L, null, null, null, "腾讯云" },
                    { 142307070906465L, "3", null, null, null, false, "minio", 100, 0, 142307070906495L, null, null, null, "minio" },
                    { 142307070906466L, "4", null, null, null, false, "本地", 100, 0, 142307070906495L, null, null, null, "本地" },
                    { 142307070906467L, "1", null, null, null, false, "运行", 100, 0, 142307070910533L, null, null, null, "运行" },
                    { 142307070906468L, "2", null, null, null, false, "停止", 100, 0, 142307070910533L, null, null, null, "停止" },
                    { 142307070906469L, "1", null, null, null, false, "通知", 100, 0, 142307070910534L, null, null, null, "通知" },
                    { 142307070906470L, "2", null, null, null, false, "公告", 100, 0, 142307070910534L, null, null, null, "公告" },
                    { 142307070906471L, "0", null, null, null, false, "草稿", 100, 0, 142307070910535L, null, null, null, "草稿" },
                    { 142307070906472L, "1", null, null, null, false, "发布", 100, 0, 142307070910535L, null, null, null, "发布" },
                    { 142307070906473L, "2", null, null, null, false, "撤回", 100, 0, 142307070910535L, null, null, null, "撤回" },
                    { 142307070906474L, "3", null, null, null, false, "删除", 100, 0, 142307070910535L, null, null, null, "删除" },
                    { 142307070906475L, "true", null, null, null, false, "是", 100, 0, 142307070910536L, null, null, null, "是" },
                    { 142307070906476L, "false", null, null, null, false, "否", 100, 0, 142307070910536L, null, null, null, "否" },
                    { 142307070906477L, "1", null, null, null, false, "下载压缩包", 100, 0, 142307070910537L, null, null, null, "下载压缩包" },
                    { 142307070906478L, "2", null, null, null, false, "生成到本项目", 100, 0, 142307070910537L, null, null, null, "生成到本项目" },
                    { 142307070906479L, "1", null, null, null, false, "GET", 100, 0, 142307070910538L, null, null, null, "GET" },
                    { 142307070906480L, "2", null, null, null, false, "POST", 100, 0, 142307070910538L, null, null, null, "POST" },
                    { 142307070906481L, "3", null, null, null, false, "PUT", 100, 0, 142307070910538L, null, null, null, "PUT" },
                    { 142307070906482L, "4", null, null, null, false, "DELETE", 100, 0, 142307070910538L, null, null, null, "DELETE" },
                    { 142307070922829L, "fk", null, null, null, false, "外键", 100, 0, 142307070922827L, null, null, null, "外键" },
                    { 142307070922830L, "input", null, null, null, false, "输入框", 100, 0, 142307070922827L, null, null, null, "输入框" },
                    { 142307070922831L, "datepicker", null, null, null, false, "时间选择", 100, 0, 142307070922827L, null, null, null, "时间选择" },
                    { 142307070922832L, "select", null, null, null, false, "下拉框", 100, 0, 142307070922827L, null, null, null, "下拉框" },
                    { 142307070922833L, "radio", null, null, null, false, "单选框", 100, 0, 142307070922827L, null, null, null, "单选框" },
                    { 142307070922834L, "switch", null, null, null, false, "开关", 100, 0, 142307070922827L, null, null, null, "开关" },
                    { 142307070922835L, "checkbox", null, null, null, false, "多选框", 100, 0, 142307070922827L, null, null, null, "多选框" },
                    { 142307070922836L, "inputnumber", null, null, null, false, "数字输入框", 100, 0, 142307070922827L, null, null, null, "数字输入框" },
                    { 142307070922837L, "textarea", null, null, null, false, "文本域", 100, 0, 142307070922827L, null, null, null, "文本域" },
                    { 142307070922838L, "==", null, null, null, false, "等于", 1, 0, 142307070922828L, null, null, null, "等于" },
                    { 142307070922839L, "like", null, null, null, false, "模糊", 2, 0, 142307070922828L, null, null, null, "模糊" },
                    { 142307070922840L, ">", null, null, null, false, "大于", 3, 0, 142307070922828L, null, null, null, "大于" },
                    { 142307070922841L, "<", null, null, null, false, "小于", 4, 0, 142307070922828L, null, null, null, "小于" },
                    { 142307070922842L, "!=", null, null, null, false, "不等于", 5, 0, 142307070922828L, null, null, null, "不等于" },
                    { 142307070922843L, ">=", null, null, null, false, "大于等于", 6, 0, 142307070922828L, null, null, null, "大于等于" },
                    { 142307070922844L, "<=", null, null, null, false, "小于等于", 7, 0, 142307070922828L, null, null, null, "小于等于" },
                    { 142307070922845L, "isNotNull", null, null, null, false, "不为空", 8, 0, 142307070922828L, null, null, null, "不为空" },
                    { 142307070922846L, "long", null, null, null, false, "long", 100, 0, 142307070922829L, null, null, null, "long" },
                    { 142307070922847L, "string", null, null, null, false, "string", 100, 0, 142307070922829L, null, null, null, "string" },
                    { 142307070922848L, "DateTime", null, null, null, false, "DateTime", 100, 0, 142307070922829L, null, null, null, "DateTime" },
                    { 142307070922850L, "bool", null, null, null, false, "bool", 100, 0, 142307070922829L, null, null, null, "bool" },
                    { 142307070922851L, "int", null, null, null, false, "int", 100, 0, 142307070922829L, null, null, null, "int" }
                });

            migrationBuilder.InsertData(
                table: "sys_dict_data",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "CreatedUserName", "IsDeleted", "Remark", "Sort", "Status", "TypeId", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Value" },
                values: new object[,]
                {
                    { 142307070922852L, "double", null, null, null, false, "double", 100, 0, 142307070922829L, null, null, null, "double" },
                    { 142307070922861L, "float", null, null, null, false, "float", 100, 0, 142307070922829L, null, null, null, "float" },
                    { 142307070922862L, "decimal", null, null, null, false, "decimal", 100, 0, 142307070922829L, null, null, null, "decimal" },
                    { 142307070922863L, "Guid", null, null, null, false, "Guid", 100, 0, 142307070922829L, null, null, null, "Guid" },
                    { 142307070922864L, "DateTimeOffset", null, null, null, false, "DateTimeOffset", 100, 0, 142307070922829L, null, null, null, "DateTimeOffset" },
                    { 142307070926943L, "0", null, null, null, false, "集团角色", 100, 0, 142307070926941L, null, null, null, "集团角色" },
                    { 142307070926944L, "1", null, null, null, false, "加盟商角色", 100, 0, 142307070926941L, null, null, null, "加盟商角色" },
                    { 142307070926945L, "2", null, null, null, false, "门店角色", 100, 0, 142307070926941L, null, null, null, "门店角色" },
                    { 142307070926946L, "1", null, null, null, false, "一级", 100, 0, 142307070926942L, null, null, null, "一级" },
                    { 142307070926947L, "2", null, null, null, false, "二级", 100, 0, 142307070926942L, null, null, null, "二级" },
                    { 142307070926948L, "3", null, null, null, false, "三级", 100, 0, 142307070926942L, null, null, null, "三级" },
                    { 142307070926949L, "4", null, null, null, false, "四级", 100, 0, 142307070926942L, null, null, null, "四级" }
                });

            migrationBuilder.InsertData(
                table: "sys_emp_ext_org_pos",
                columns: new[] { "SysEmpId", "SysOrgId", "SysPosId" },
                values: new object[,]
                {
                    { 142307070910551L, 142307070910539L, 142307070910547L },
                    { 142307070910551L, 142307070910540L, 142307070910548L },
                    { 142307070910551L, 142307070910541L, 142307070910549L },
                    { 142307070910551L, 142307070910542L, 142307070910550L },
                    { 142307070910553L, 142307070910542L, 142307070910547L }
                });

            migrationBuilder.InsertData(
                table: "sys_emp_pos",
                columns: new[] { "SysEmpId", "SysPosId" },
                values: new object[,]
                {
                    { 142307070910551L, 142307070910547L },
                    { 142307070910551L, 142307070910548L },
                    { 142307070910552L, 142307070910549L },
                    { 142307070910553L, 142307070910547L }
                });

            migrationBuilder.InsertData(
                table: "sys_user_data_scope",
                columns: new[] { "SysOrgId", "SysUserId" },
                values: new object[] { 142307070910540L, 142307070910554L });

            migrationBuilder.InsertData(
                table: "sys_user_role",
                columns: new[] { "SysRoleId", "SysUserId" },
                values: new object[,]
                {
                    { 142307070910554L, 142307070910552L },
                    { 142307070910556L, 142307070910554L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_collect_delivery_details_CollectDeliveryId",
                table: "collect_delivery_details",
                column: "CollectDeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_goods_delivery_appointment_DeliveryID",
                table: "goods_delivery_appointment",
                column: "DeliveryID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_goods_delivery_details_GoodsDeliveryId",
                table: "goods_delivery_details",
                column: "GoodsDeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_goodsdelivery_vs_collectdelivery_CollectDeliveryId",
                table: "goodsdelivery_vs_collectdelivery",
                column: "CollectDeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_goodsdelivery_vs_collectdelivery_GoodsDeliveryId",
                table: "goodsdelivery_vs_collectdelivery",
                column: "GoodsDeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_purchaseorder_vs_goodsdelivery_GoodsDeliveryId",
                table: "purchaseorder_vs_goodsdelivery",
                column: "GoodsDeliveryId");

            migrationBuilder.CreateIndex(
                name: "IX_purchaseorder_vs_goodsdelivery_PurchaseOrderId",
                table: "purchaseorder_vs_goodsdelivery",
                column: "PurchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_dict_data_TypeId",
                table: "sys_dict_data",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_emp_ext_org_pos_SysOrgId",
                table: "sys_emp_ext_org_pos",
                column: "SysOrgId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_emp_ext_org_pos_SysPosId",
                table: "sys_emp_ext_org_pos",
                column: "SysPosId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_emp_pos_SysPosId",
                table: "sys_emp_pos",
                column: "SysPosId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_low_code_module_database_SysLowCodeId",
                table: "sys_low_code_module_database",
                column: "SysLowCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_role_data_scope_SysOrgId",
                table: "sys_role_data_scope",
                column: "SysOrgId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_role_menu_SysMenuId",
                table: "sys_role_menu",
                column: "SysMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_data_scope_SysOrgId",
                table: "sys_user_data_scope",
                column: "SysOrgId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_role_SysRoleId",
                table: "sys_user_role",
                column: "SysRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ware_container_vs_material_ContainerId",
                table: "ware_container_vs_material",
                column: "ContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_ware_container_vs_material_MaterialId",
                table: "ware_container_vs_material",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_ware_dict_data_TypeId",
                table: "ware_dict_data",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ware_location_vs_container_ContainerId",
                table: "ware_location_vs_container",
                column: "ContainerId");

            migrationBuilder.CreateIndex(
                name: "IX_ware_location_vs_container_LocationId",
                table: "ware_location_vs_container",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_ware_order_details_OrderId",
                table: "ware_order_details",
                column: "OrderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "collect_delivery_details");

            migrationBuilder.DropTable(
                name: "goods_delivery_appointment");

            migrationBuilder.DropTable(
                name: "goods_delivery_details");

            migrationBuilder.DropTable(
                name: "goods_delivery_tagprint");

            migrationBuilder.DropTable(
                name: "goodsdelivery_vs_collectdelivery");

            migrationBuilder.DropTable(
                name: "project_manage");

            migrationBuilder.DropTable(
                name: "purchaseorder_vs_goodsdelivery");

            migrationBuilder.DropTable(
                name: "supplier_examine_flower");

            migrationBuilder.DropTable(
                name: "supplier_info");

            migrationBuilder.DropTable(
                name: "sys_app");

            migrationBuilder.DropTable(
                name: "sys_code_gen");

            migrationBuilder.DropTable(
                name: "sys_code_gen_config");

            migrationBuilder.DropTable(
                name: "sys_code_modular");

            migrationBuilder.DropTable(
                name: "sys_config");

            migrationBuilder.DropTable(
                name: "sys_dict_data");

            migrationBuilder.DropTable(
                name: "sys_emp_ext_org_pos");

            migrationBuilder.DropTable(
                name: "sys_emp_pos");

            migrationBuilder.DropTable(
                name: "sys_file");

            migrationBuilder.DropTable(
                name: "sys_log_audit");

            migrationBuilder.DropTable(
                name: "sys_log_ex");

            migrationBuilder.DropTable(
                name: "sys_log_op");

            migrationBuilder.DropTable(
                name: "sys_log_vis");

            migrationBuilder.DropTable(
                name: "sys_low_code_module_database");

            migrationBuilder.DropTable(
                name: "sys_notice");

            migrationBuilder.DropTable(
                name: "sys_notice_user");

            migrationBuilder.DropTable(
                name: "sys_role_data_scope");

            migrationBuilder.DropTable(
                name: "sys_role_menu");

            migrationBuilder.DropTable(
                name: "sys_timer");

            migrationBuilder.DropTable(
                name: "sys_user_data_scope");

            migrationBuilder.DropTable(
                name: "sys_user_role");

            migrationBuilder.DropTable(
                name: "SysRoleOutputs");

            migrationBuilder.DropTable(
                name: "ware_area");

            migrationBuilder.DropTable(
                name: "ware_container_vs_material");

            migrationBuilder.DropTable(
                name: "ware_dict_data");

            migrationBuilder.DropTable(
                name: "ware_equipment");

            migrationBuilder.DropTable(
                name: "ware_location_vs_container");

            migrationBuilder.DropTable(
                name: "ware_material_warehouse");

            migrationBuilder.DropTable(
                name: "ware_order_details");

            migrationBuilder.DropTable(
                name: "ware_site");

            migrationBuilder.DropTable(
                name: "ware_sortorder");

            migrationBuilder.DropTable(
                name: "ware_stock");

            migrationBuilder.DropTable(
                name: "ware_task");

            migrationBuilder.DropTable(
                name: "collect_delivery");

            migrationBuilder.DropTable(
                name: "goods_delivery");

            migrationBuilder.DropTable(
                name: "purchase_order");

            migrationBuilder.DropTable(
                name: "sys_dict_type");

            migrationBuilder.DropTable(
                name: "sys_emp");

            migrationBuilder.DropTable(
                name: "sys_pos");

            migrationBuilder.DropTable(
                name: "sys_low_code_module");

            migrationBuilder.DropTable(
                name: "sys_menu");

            migrationBuilder.DropTable(
                name: "sys_org");

            migrationBuilder.DropTable(
                name: "sys_role");

            migrationBuilder.DropTable(
                name: "sys_user");

            migrationBuilder.DropTable(
                name: "ware_material");

            migrationBuilder.DropTable(
                name: "ware_dic_type");

            migrationBuilder.DropTable(
                name: "ware_container");

            migrationBuilder.DropTable(
                name: "ware_location");

            migrationBuilder.DropTable(
                name: "ware_order");
        }
    }
}
