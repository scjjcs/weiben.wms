﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1027 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Images",
                table: "ware_task",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true,
                comment: "图片");

            migrationBuilder.AddColumn<string>(
                name: "Images",
                table: "ware_agv_task",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true,
                comment: "图片");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Images",
                table: "ware_task");

            migrationBuilder.DropColumn(
                name: "Images",
                table: "ware_agv_task");
        }
    }
}
