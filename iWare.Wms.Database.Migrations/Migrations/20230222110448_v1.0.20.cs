﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "OrderNo",
                table: "ware_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "组盘单据号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "单据号");

            migrationBuilder.AlterColumn<string>(
                name: "ContainerCode",
                table: "ware_location_vs_container",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                comment: "容器编码",
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true,
                oldComment: "供应商code");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "OrderNo",
                table: "ware_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "单据号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "组盘单据号");

            migrationBuilder.AlterColumn<string>(
                name: "ContainerCode",
                table: "ware_location_vs_container",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                comment: "供应商code",
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true,
                oldComment: "容器编码");
        }
    }
}
