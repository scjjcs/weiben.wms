﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1035 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Weight",
                table: "ware_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "重量",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "图片");

            migrationBuilder.AlterColumn<string>(
                name: "Attribute",
                table: "ware_location",
                type: "nvarchar(max)",
                nullable: true,
                comment: "属性-小、中、大",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "属性-小、中、大");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Weight",
                table: "ware_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "图片",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "重量");

            migrationBuilder.AlterColumn<int>(
                name: "Attribute",
                table: "ware_location",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "属性-小、中、大",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true,
                oldComment: "属性-小、中、大");
        }
    }
}
