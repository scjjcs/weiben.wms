﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1027 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OrderParameter",
                table: "ware_order_details",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                comment: "参数");

            migrationBuilder.AddColumn<string>(
                name: "OrderRemark",
                table: "ware_order_details",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                comment: "备注");

            migrationBuilder.AddColumn<string>(
                name: "AuditCode",
                table: "ware_order",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                comment: "审核码");

            migrationBuilder.AddColumn<string>(
                name: "DepartmentNumber",
                table: "ware_order",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                comment: "部门编号");

            migrationBuilder.AddColumn<string>(
                name: "EquipmentNumber",
                table: "ware_order",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                comment: "设备号");

            migrationBuilder.AddColumn<string>(
                name: "FactoryCode",
                table: "ware_order",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                comment: "工厂编号");

            migrationBuilder.AddColumn<string>(
                name: "MainPartNumber",
                table: "ware_order",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                comment: "主件品号");

            migrationBuilder.AddColumn<string>(
                name: "OrderDate",
                table: "ware_order",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "单据日期");

            migrationBuilder.AddColumn<string>(
                name: "OrderRemark",
                table: "ware_order",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true,
                comment: "单据备注");

            migrationBuilder.AddColumn<string>(
                name: "StationNumber",
                table: "ware_order",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                comment: "工位号");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderParameter",
                table: "ware_order_details");

            migrationBuilder.DropColumn(
                name: "OrderRemark",
                table: "ware_order_details");

            migrationBuilder.DropColumn(
                name: "AuditCode",
                table: "ware_order");

            migrationBuilder.DropColumn(
                name: "DepartmentNumber",
                table: "ware_order");

            migrationBuilder.DropColumn(
                name: "EquipmentNumber",
                table: "ware_order");

            migrationBuilder.DropColumn(
                name: "FactoryCode",
                table: "ware_order");

            migrationBuilder.DropColumn(
                name: "MainPartNumber",
                table: "ware_order");

            migrationBuilder.DropColumn(
                name: "OrderDate",
                table: "ware_order");

            migrationBuilder.DropColumn(
                name: "OrderRemark",
                table: "ware_order");

            migrationBuilder.DropColumn(
                name: "StationNumber",
                table: "ware_order");
        }
    }
}
