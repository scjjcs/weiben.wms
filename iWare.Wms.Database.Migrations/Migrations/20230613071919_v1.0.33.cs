﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1033 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ware_access_mouth");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ware_access_mouth",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    AccessMouthCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "编码"),
                    AccessMouthName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "名称"),
                    AccessMouthType = table.Column<int>(type: "int", nullable: false, comment: "类型"),
                    AreaID = table.Column<long>(type: "bigint", nullable: false, comment: "所属库区"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_access_mouth", x => x.Id);
                },
                comment: "出入口表");
        }
    }
}
