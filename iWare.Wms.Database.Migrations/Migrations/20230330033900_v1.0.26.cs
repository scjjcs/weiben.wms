﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1026 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "SiteTypeName",
                table: "ware_site",
                type: "int",
                maxLength: 50,
                nullable: false,
                defaultValue: 0,
                comment: "站点类型",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "站点类型");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SiteTypeName",
                table: "ware_site",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "站点类型",
                oldClrType: typeof(int),
                oldType: "int",
                oldMaxLength: 50,
                oldComment: "站点类型");
        }
    }
}
