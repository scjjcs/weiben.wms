﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1015 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IsAdminRead",
                table: "supplier_examine_flower",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "管理员是否已读");

            migrationBuilder.AddColumn<int>(
                name: "IsJiaJiJian",
                table: "supplier_examine_flower",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "是否加急件");

            migrationBuilder.AddColumn<int>(
                name: "IsSupperRead",
                table: "supplier_examine_flower",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "供应商是否已读");

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "goods_delivery_appointment",
                type: "int",
                nullable: false,
                comment: "状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6、强制收货_7");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAdminRead",
                table: "supplier_examine_flower");

            migrationBuilder.DropColumn(
                name: "IsJiaJiJian",
                table: "supplier_examine_flower");

            migrationBuilder.DropColumn(
                name: "IsSupperRead",
                table: "supplier_examine_flower");

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "goods_delivery_appointment",
                type: "int",
                nullable: false,
                comment: "状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6、强制收货_7",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6");
        }
    }
}
