﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v102 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedTime",
                table: "quality_feedback_details");

            migrationBuilder.DropColumn(
                name: "UpdatedTime",
                table: "quality_feedback_details");

            migrationBuilder.DropColumn(
                name: "QualityIsRead",
                table: "collect_delivery_details");

            migrationBuilder.AlterColumn<int>(
                name: "QualityResult",
                table: "collect_delivery_details",
                type: "int",
                maxLength: 500,
                nullable: false,
                defaultValue: 0,
                comment: "质检结果",
                oldClrType: typeof(string),
                oldType: "nvarchar(500)",
                oldMaxLength: 500,
                oldNullable: true,
                oldComment: "质检结果");

            migrationBuilder.AddColumn<int>(
                name: "QualityNumber",
                table: "collect_delivery_details",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "质检结果数量");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "QualityNumber",
                table: "collect_delivery_details");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CreatedTime",
                table: "quality_feedback_details",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                comment: "创建时间");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "UpdatedTime",
                table: "quality_feedback_details",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                comment: "更新时间");

            migrationBuilder.AlterColumn<string>(
                name: "QualityResult",
                table: "collect_delivery_details",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true,
                comment: "质检结果",
                oldClrType: typeof(int),
                oldType: "int",
                oldMaxLength: 500,
                oldComment: "质检结果");

            migrationBuilder.AddColumn<int>(
                name: "QualityIsRead",
                table: "collect_delivery_details",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "质检反馈是否已读");
        }
    }
}
