﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v108 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SortOrderNo",
                table: "ware_sortorder",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "分拣单号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "分拣来源单号");

            migrationBuilder.AlterColumn<string>(
                name: "ContainerOrderNo",
                table: "ware_sortorder",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "分拣组盘单号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "分拣来源单号");

            migrationBuilder.AddColumn<string>(
                name: "DeliveryNo",
                table: "ware_container_vs_material",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "送货单号");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryNo",
                table: "ware_container_vs_material");

            migrationBuilder.AlterColumn<string>(
                name: "SortOrderNo",
                table: "ware_sortorder",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "分拣来源单号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "分拣单号");

            migrationBuilder.AlterColumn<string>(
                name: "ContainerOrderNo",
                table: "ware_sortorder",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "分拣来源单号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "分拣组盘单号");
        }
    }
}
