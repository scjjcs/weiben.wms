﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1037 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "close_time",
                table: "ware_device_fault");

            migrationBuilder.DropColumn(
                name: "start_time",
                table: "ware_device_fault");

            migrationBuilder.DropColumn(
                name: "use_times",
                table: "ware_device_fault");

            migrationBuilder.RenameColumn(
                name: "fault_name",
                table: "ware_device_fault",
                newName: "FaultName");

            migrationBuilder.RenameColumn(
                name: "fault_code",
                table: "ware_device_fault",
                newName: "FaultCode");

            migrationBuilder.RenameColumn(
                name: "device_no",
                table: "ware_device_fault",
                newName: "DeviceNo");

            migrationBuilder.RenameColumn(
                name: "device_name",
                table: "ware_device_fault",
                newName: "DeviceName");

            migrationBuilder.AddColumn<DateTime>(
                name: "CloseTime",
                table: "ware_device_fault",
                type: "datetime2",
                nullable: true,
                comment: "关闭时间");

            migrationBuilder.AddColumn<DateTime>(
                name: "StartTime",
                table: "ware_device_fault",
                type: "datetime2",
                nullable: true,
                comment: "报警时间");

            migrationBuilder.AddColumn<int>(
                name: "UseTimes",
                table: "ware_device_fault",
                type: "int",
                nullable: true,
                comment: "持续时间");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CloseTime",
                table: "ware_device_fault");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "ware_device_fault");

            migrationBuilder.DropColumn(
                name: "UseTimes",
                table: "ware_device_fault");

            migrationBuilder.RenameColumn(
                name: "FaultName",
                table: "ware_device_fault",
                newName: "fault_name");

            migrationBuilder.RenameColumn(
                name: "FaultCode",
                table: "ware_device_fault",
                newName: "fault_code");

            migrationBuilder.RenameColumn(
                name: "DeviceNo",
                table: "ware_device_fault",
                newName: "device_no");

            migrationBuilder.RenameColumn(
                name: "DeviceName",
                table: "ware_device_fault",
                newName: "device_name");

            migrationBuilder.AddColumn<DateTime>(
                name: "close_time",
                table: "ware_device_fault",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "start_time",
                table: "ware_device_fault",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "use_times",
                table: "ware_device_fault",
                type: "int",
                nullable: true);
        }
    }
}
