﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1029 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AbnormalCause",
                table: "quality_feedback_details",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: true,
                comment: "异常原因");

            migrationBuilder.AddColumn<string>(
                name: "AbnormalCause",
                table: "collect_delivery_details",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: true,
                comment: "异常原因");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AbnormalCause",
                table: "quality_feedback_details");

            migrationBuilder.DropColumn(
                name: "AbnormalCause",
                table: "collect_delivery_details");
        }
    }
}
