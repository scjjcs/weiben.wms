﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "quality_feedback_details",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeliveryNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "送货单号"),
                    SupperCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商"),
                    IsAdminRead = table.Column<int>(type: "int", nullable: false, comment: "管理员是否已读"),
                    IsSupperRead = table.Column<int>(type: "int", nullable: false, comment: "供应商是否已读"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false, comment: "更新时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_quality_feedback_details", x => x.Id);
                },
                comment: "质检反馈明细");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "quality_feedback_details");
        }
    }
}
