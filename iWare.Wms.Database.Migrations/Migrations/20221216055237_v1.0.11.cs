﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1011 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "LocationStatus",
                table: "ware_location",
                type: "int",
                nullable: true,
                comment: "库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "LocationStatus",
                table: "ware_location",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1",
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true,
                oldComment: "库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1");
        }
    }
}
