﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v105 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContainerCode",
                table: "ware_order_details",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "容器编号");

            migrationBuilder.AddColumn<string>(
                name: "LocationCode",
                table: "ware_order_details",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "库位编号");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContainerCode",
                table: "ware_order_details");

            migrationBuilder.DropColumn(
                name: "LocationCode",
                table: "ware_order_details");
        }
    }
}
