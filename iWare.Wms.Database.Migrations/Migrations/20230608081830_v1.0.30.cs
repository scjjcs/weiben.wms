﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1030 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ware_site");

            migrationBuilder.AddColumn<string>(
                name: "Weight",
                table: "ware_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "图片");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Weight",
                table: "ware_task");

            migrationBuilder.CreateTable(
                name: "ware_site",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "站点编号"),
                    ContainerCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "容器编号"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    Describe = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true, comment: "描述"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true, comment: "站点名称"),
                    SiteTypeName = table.Column<int>(type: "int", nullable: false, comment: "站点类型"),
                    Status = table.Column<int>(type: "int", nullable: true, comment: "状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_site", x => x.Id);
                },
                comment: "站点表");
        }
    }
}
