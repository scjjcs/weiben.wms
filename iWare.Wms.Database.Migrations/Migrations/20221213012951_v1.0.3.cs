﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v103 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CreatedTime",
                table: "quality_feedback_details",
                type: "datetimeoffset",
                nullable: true,
                comment: "创建时间");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "UpdatedTime",
                table: "quality_feedback_details",
                type: "datetimeoffset",
                nullable: true,
                comment: "更新时间");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedTime",
                table: "quality_feedback_details");

            migrationBuilder.DropColumn(
                name: "UpdatedTime",
                table: "quality_feedback_details");
        }
    }
}
