﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1034 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Heavy",
                table: "ware_location",
                type: "nvarchar(max)",
                nullable: true,
                comment: "承重",
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldComment: "承重");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Heavy",
                table: "ware_location",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m,
                comment: "承重",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true,
                oldComment: "承重");
        }
    }
}
