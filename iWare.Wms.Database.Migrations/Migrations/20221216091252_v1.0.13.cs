﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1013 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "OrderType",
                table: "ware_order",
                type: "int",
                nullable: false,
                comment: "出库单据类型-调拨单_0、手动单_1",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "出库单据类型-调拨单_1");

            migrationBuilder.AddColumn<int>(
                name: "IsSupperHandle",
                table: "collect_delivery_details",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "供应商是否已处理");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSupperHandle",
                table: "collect_delivery_details");

            migrationBuilder.AlterColumn<int>(
                name: "OrderType",
                table: "ware_order",
                type: "int",
                nullable: false,
                comment: "出库单据类型-调拨单_1",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "出库单据类型-调拨单_0、手动单_1");
        }
    }
}
