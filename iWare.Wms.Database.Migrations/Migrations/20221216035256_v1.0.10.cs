﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1010 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "quality_feedback_details",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "品号");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "quality_feedback_details",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true,
                comment: "品名");

            migrationBuilder.AddColumn<string>(
                name: "ProjectCode",
                table: "quality_feedback_details",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "项目编号");

            migrationBuilder.AddColumn<int>(
                name: "QualityNumber",
                table: "quality_feedback_details",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "质检结果数量");

            migrationBuilder.AddColumn<int>(
                name: "QualityResult",
                table: "quality_feedback_details",
                type: "int",
                maxLength: 500,
                nullable: true,
                comment: "质检结果");

            migrationBuilder.AddColumn<string>(
                name: "SpecificationModel",
                table: "quality_feedback_details",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: true,
                comment: "规格型号");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "quality_feedback_details");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "quality_feedback_details");

            migrationBuilder.DropColumn(
                name: "ProjectCode",
                table: "quality_feedback_details");

            migrationBuilder.DropColumn(
                name: "QualityNumber",
                table: "quality_feedback_details");

            migrationBuilder.DropColumn(
                name: "QualityResult",
                table: "quality_feedback_details");

            migrationBuilder.DropColumn(
                name: "SpecificationModel",
                table: "quality_feedback_details");
        }
    }
}
