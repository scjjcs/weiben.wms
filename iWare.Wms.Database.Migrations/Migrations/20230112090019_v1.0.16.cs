﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1016 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StockCircle",
                table: "ware_stock",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "物料库存周期（字典 1绿 2黄 3红）");

            migrationBuilder.AlterColumn<int>(
                name: "LocationStatus",
                table: "ware_location",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1",
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true,
                oldComment: "库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1");

        //    migrationBuilder.InsertData(
        //        table: "sys_dict_data",
        //        columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "CreatedUserName", "IsDeleted", "Remark", "Sort", "Status", "TypeId", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Value" },
        //        values: new object[] { 142307070902382L, "FILE_PATH", null, null, null, false, "文件上传路径", 100, 0, 142307070906485L, null, null, null, "文件上传路径" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DeleteData(
            //    table: "sys_dict_data",
            //    keyColumn: "Id",
            //    keyValue: 142307070902382L);

            migrationBuilder.DropColumn(
                name: "StockCircle",
                table: "ware_stock");

            migrationBuilder.AlterColumn<int>(
                name: "LocationStatus",
                table: "ware_location",
                type: "int",
                nullable: true,
                comment: "库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "库位与容器关系状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1");
        }
    }
}
