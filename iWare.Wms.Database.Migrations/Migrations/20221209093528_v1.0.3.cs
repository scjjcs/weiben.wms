﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v103 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QualityFeedbackDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeliveryNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "送货单号"),
                    SupperCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "供应商"),
                    IsAdminRead = table.Column<int>(type: "int", nullable: false),
                    IsSupperRead = table.Column<int>(type: "int", nullable: false),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QualityFeedbackDetails", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QualityFeedbackDetails");
        }
    }
}
