﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v102 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "QualityIsRead",
                table: "collect_delivery_details",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "质检反馈是否已读");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "QualityIsRead",
                table: "collect_delivery_details");
        }
    }
}
