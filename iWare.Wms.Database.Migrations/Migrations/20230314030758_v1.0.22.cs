﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1022 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AgvName",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "CurrentPosition",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "EnergyLevel",
                table: "ware_agv_task");

            migrationBuilder.AlterColumn<string>(
                name: "TaskType",
                table: "ware_agv_task",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                comment: "任务类型-入库;出库",
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10,
                oldNullable: true,
                oldComment: "任务类型-1_代表入库;2_代表出库");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TaskType",
                table: "ware_agv_task",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                comment: "任务类型-1_代表入库;2_代表出库",
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10,
                oldNullable: true,
                oldComment: "任务类型-入库;出库");

            migrationBuilder.AddColumn<string>(
                name: "AgvName",
                table: "ware_agv_task",
                type: "nvarchar(max)",
                nullable: true,
                comment: "Agv车辆名称");

            migrationBuilder.AddColumn<string>(
                name: "CurrentPosition",
                table: "ware_agv_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "当前位置");

            migrationBuilder.AddColumn<int>(
                name: "EnergyLevel",
                table: "ware_agv_task",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "电量");
        }
    }
}
