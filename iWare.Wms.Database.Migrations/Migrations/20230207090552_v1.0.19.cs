﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1019 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Attribute",
                table: "ware_location",
                type: "int",
                nullable: false,
                comment: "属性-小、中、大",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "属性-中、高、低");

            migrationBuilder.AlterColumn<string>(
                name: "ContainerCode",
                table: "ware_container_vs_material",
                type: "nvarchar(max)",
                nullable: true,
                comment: "容器编码",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true,
                oldComment: "容器表Id");

            migrationBuilder.CreateTable(
                name: "ware_agv_task",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    AgvId = table.Column<int>(type: "int", nullable: true, comment: "AgvId"),
                    StartPlace = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "起始位置"),
                    EndPlace = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "结束位置"),
                    TaskType = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, comment: "任务类型-1_代表入库;2_代表出库"),
                    AgvStatus = table.Column<int>(type: "int", nullable: true, comment: "状态"),
                    TaskIf = table.Column<int>(type: "int", nullable: true, comment: "任务说明-1_代表有效任务;0_代表无效任务;2_代表出库完成等待任务"),
                    ContainerID = table.Column<int>(type: "int", nullable: true, comment: "托盘ID"),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: true, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ware_agv_task", x => x.Id);
                },
                comment: "Agv任务表");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ware_agv_task");

            migrationBuilder.AlterColumn<int>(
                name: "Attribute",
                table: "ware_location",
                type: "int",
                nullable: false,
                comment: "属性-中、高、低",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "属性-小、中、大");

            migrationBuilder.AlterColumn<string>(
                name: "ContainerCode",
                table: "ware_container_vs_material",
                type: "nvarchar(max)",
                nullable: true,
                comment: "容器表Id",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true,
                oldComment: "容器编码");
        }
    }
}
