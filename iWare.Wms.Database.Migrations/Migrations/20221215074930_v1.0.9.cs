﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v109 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SortOrderNo",
                table: "ware_sortorder");

            migrationBuilder.AddColumn<long>(
                name: "OrderDetailID",
                table: "ware_sortorder",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "OrderNo",
                table: "ware_sortorder",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "来源单号");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderDetailID",
                table: "ware_sortorder");

            migrationBuilder.DropColumn(
                name: "OrderNo",
                table: "ware_sortorder");

            migrationBuilder.AddColumn<string>(
                name: "SortOrderNo",
                table: "ware_sortorder",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "分拣单号");
        }
    }
}
