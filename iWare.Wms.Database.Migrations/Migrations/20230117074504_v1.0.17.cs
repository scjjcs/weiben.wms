﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1017 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "SortStatus",
                table: "ware_sortorder",
                type: "int",
                nullable: false,
                comment: "状态-未分拣_1、分拣完成_2",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "状态-未进行_0、未分拣_1、分拣中_2、分拣完成_3");

            migrationBuilder.AddColumn<int>(
                name: "IsVirtual",
                table: "ware_location",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "是否虚拟（字典 1是 0否）");

            migrationBuilder.AddColumn<string>(
                name: "OriginalLocationCode",
                table: "ware_location",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "楼下立体库对应的库位编号");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsVirtual",
                table: "ware_location");

            migrationBuilder.DropColumn(
                name: "OriginalLocationCode",
                table: "ware_location");

            migrationBuilder.AlterColumn<int>(
                name: "SortStatus",
                table: "ware_sortorder",
                type: "int",
                nullable: false,
                comment: "状态-未进行_0、未分拣_1、分拣中_2、分拣完成_3",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "状态-未分拣_1、分拣完成_2");
        }
    }
}
