﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace iWare.Wms.Database.Migrations.Migrations
{
    public partial class v1021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AgvId",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "AgvStatus",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "ContainerID",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "TaskIf",
                table: "ware_agv_task");

            migrationBuilder.AlterColumn<string>(
                name: "OrderNo",
                table: "ware_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "单据号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "组盘单据号");

            migrationBuilder.AlterColumn<string>(
                name: "StartPlace",
                table: "ware_agv_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "起始位置",
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true,
                oldComment: "起始位置");

            migrationBuilder.AlterColumn<string>(
                name: "EndPlace",
                table: "ware_agv_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "结束位置",
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true,
                oldComment: "结束位置");

            migrationBuilder.AddColumn<string>(
                name: "AgvName",
                table: "ware_agv_task",
                type: "nvarchar(max)",
                nullable: true,
                comment: "Agv车辆名称");

            migrationBuilder.AddColumn<string>(
                name: "AgvState",
                table: "ware_agv_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "状态");

            migrationBuilder.AddColumn<string>(
                name: "ContainerCode",
                table: "ware_agv_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "容器编号");

            migrationBuilder.AddColumn<string>(
                name: "CurrentPosition",
                table: "ware_agv_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "当前位置");

            migrationBuilder.AddColumn<int>(
                name: "EnergyLevel",
                table: "ware_agv_task",
                type: "int",
                nullable: false,
                defaultValue: 0,
                comment: "电量");

            migrationBuilder.AddColumn<string>(
                name: "TransportOrder",
                table: "ware_agv_task",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                comment: "当前执行的任务");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AgvName",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "AgvState",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "ContainerCode",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "CurrentPosition",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "EnergyLevel",
                table: "ware_agv_task");

            migrationBuilder.DropColumn(
                name: "TransportOrder",
                table: "ware_agv_task");

            migrationBuilder.AlterColumn<string>(
                name: "OrderNo",
                table: "ware_task",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "组盘单据号",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "单据号");

            migrationBuilder.AlterColumn<string>(
                name: "StartPlace",
                table: "ware_agv_task",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                comment: "起始位置",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "起始位置");

            migrationBuilder.AlterColumn<string>(
                name: "EndPlace",
                table: "ware_agv_task",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                comment: "结束位置",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true,
                oldComment: "结束位置");

            migrationBuilder.AddColumn<int>(
                name: "AgvId",
                table: "ware_agv_task",
                type: "int",
                nullable: true,
                comment: "AgvId");

            migrationBuilder.AddColumn<int>(
                name: "AgvStatus",
                table: "ware_agv_task",
                type: "int",
                nullable: true,
                comment: "状态");

            migrationBuilder.AddColumn<int>(
                name: "ContainerID",
                table: "ware_agv_task",
                type: "int",
                nullable: true,
                comment: "托盘ID");

            migrationBuilder.AddColumn<int>(
                name: "TaskIf",
                table: "ware_agv_task",
                type: "int",
                nullable: true,
                comment: "任务说明-1_代表有效任务;0_代表无效任务;2_代表出库完成等待任务");
        }
    }
}
