﻿namespace iWare.Wms.Core
{
    /// <summary>
    /// 任务名称
    /// </summary>
    public class TaskNameConst
    {
        /// <summary>
        /// PC手动组盘入库----
        /// </summary>
        public const string GroupDisk_ManualIn = "PC手动组盘入库";

        /// <summary>
        /// PC自动组盘入库----
        /// </summary>
        public const string GroupDisk_AutomaticIn = "PC自动组盘入库";

        /// <summary>
        /// PC自动单出库----
        /// </summary>
        public const string ShouDongDan_AutomaticOut = "PC自动单出库";

        /// <summary>
        /// PC手动单出库----
        /// </summary>
        public const string ShouDongDan_ManualOut = "PC手动单出库";

        /// <summary>
        /// PC自动调拨单出库----
        /// </summary>
        public const string DiaoBoDan_AutomaticOut = "PC自动调拨单出库";

        /// <summary>
        /// PC手动调拨单出库----
        /// </summary>
        public const string DiaoBoDan_ManualOut = "PC手动调拨单出库";

        /// <summary>
        /// PDA自动立体库入库----
        /// </summary>
        public const string Litiku_AutomaticIn = "PDA自动立体库入库";

        /// <summary>
        /// PDA自动空托立体库入库----
        /// </summary>
        public const string KongTuo_LitikuAutomaticIn = "PDA自动空托立体库入库";

        /// <summary>
        /// PDA自动空托平库入库----
        /// </summary>
        public const string KongTuo_PingkuAutomaticIn = "PDA自动空托平库入库";

        /// <summary>
        /// PDA手动立体库入库----
        /// </summary>
        public const string Litiku_ManualIn = "PDA手动立体库入库";

        /// <summary>
        /// PDA手动空托立体库入库----
        /// </summary>
        public const string KongTuo_LitikuManualIn = "PDA手动空托立体库入库";

        /// <summary>
        /// PDA自动平库入库----
        /// </summary>
        public const string Pingku_AutomaticIn = "PDA自动平库入库";

        /// <summary>
        /// PDA手动平库入库----
        /// </summary>
        public const string Pingku_ManualIn = "PDA手动平库入库";

        /// <summary>
        /// PDA手动空托平库入库----
        /// </summary>
        public const string KongTuo_PingkuManualIn = "PDA手动空托平库入库";

        /// <summary>
        /// PDA自动物料入库----
        /// </summary>
        public const string Material_AutomaticIn = "PDA自动物料入库";

        /// <summary>
        /// PDA手动物料入库----
        /// </summary>
        public const string Material_ManualIn = "PDA手动物料入库";

        /// <summary>
        /// PDA自动空托物料入库----
        /// </summary>
        public const string KongTuo_MaterialAutomaticIn = "PDA自动空托物料入库";

        /// <summary>
        /// PDA手动空托物料入库----
        /// </summary>
        public const string KongTuo_MaterialManualIn = "PDA手动空托物料入库";

        /// <summary>
        /// PDA自动空托余料回库----
        /// </summary>
        public const string KongTuo_MaterialReturnAutomaticIn = "PDA自动空托余料回库";

        /// <summary>
        /// PDA手动空托余料回库----
        /// </summary>
        public const string KongTuo_MaterialReturnManualIn = "PDA手动空托余料回库";

        /// <summary>
        /// PDA自动余料回库----
        /// </summary>
        public const string MaterialReturn_AutomaticIn = "PDA自动余料回库";

        /// <summary>
        /// PDA手动余料回库----
        /// </summary>
        public const string MaterialReturn_ManualIn = "PDA手动余料回库";

        /// <summary>
        /// PDA自动空托移库-----
        /// </summary>
        public const string KongTuo_AutomaticYiKu = "PDA自动空托移库";

        /// <summary>
        /// PDA手动空托移库-----
        /// </summary>
        public const string KongTuo_ManualYiKu = "PDA手动空托移库";

        /// <summary>
        /// PDA自动物料移库-----
        /// </summary>
        public const string Material_AutomaticYiKu = "PDA自动物料移库";

        /// <summary>
        /// PDA手动移库-----
        /// </summary>
        public const string Material_ManualYiKu = "PDA手动移库";

        /// <summary>
        /// PDA自动立体库出库-----
        /// </summary>
        public const string Litiku_AutomaticOut = "PDA自动立体库出库";

        /// <summary>
        /// PDA自动平库出库-----
        /// </summary>
        public const string Pingku_AutomaticOut = "PDA自动平库出库";

        /// <summary>
        /// PDA自动物料出库-----
        /// </summary>
        public const string Material_AutomaticOut = "PDA自动物料出库";

        /// <summary>
        /// PDA手动物料出库-----
        /// </summary>
        public const string Material_ManualOut = "PDA手动物料出库";

        /// <summary>
        /// PDA自动呼叫空托-----
        /// </summary>
        public const string CallKongTuo_AutomaticOut = "PDA自动呼叫空托";

        /// <summary>
        /// PDA手动呼叫空托-----
        /// </summary>
        public const string CallKongTuo_ManualOut = "PDA手动呼叫空托";

        /// <summary>
        /// PDA手动无托入库----
        /// </summary>
        public const string NotContainer_ManualIn = "PDA手动无托入库";

        /// <summary>
        /// PDA临时库位入库----
        /// </summary>
        public const string TemporaryLocation_ManualIn = "PDA临时库位入库";

        /// <summary>
        /// PDA临时库位出库----
        /// </summary>
        public const string TemporaryLocation_ManualOut = "PDA临时库位出库";

        /// <summary>
        /// PDA盘点出库----
        /// </summary>
        public const string CheckLocation_ManualOut = "PDA临时库位出库";
    }
}
