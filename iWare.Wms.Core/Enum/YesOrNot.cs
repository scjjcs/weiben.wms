﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 是否
    /// </summary>
    public enum YesOrNot
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        All = -10,

        /// <summary>
        /// 是
        /// </summary>
        [Description("是")]
        Y = 1,

        /// <summary>
        /// 否
        /// </summary>
        [Description("否")]
        N = 0
    }
}