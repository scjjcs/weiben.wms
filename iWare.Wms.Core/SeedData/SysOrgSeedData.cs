﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 系统机构表种子数据
    /// </summary>
    public class SysOrgSeedData : IEntitySeedData<SysOrg>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysOrg> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysOrg{Id=142307070910539, Pid=0, Pids="[0],", Name="伟本股份有限公司", Code="wbgf", Sort=100, Remark="伟本股份有限公司", Status=0 },
                new SysOrg{Id=142307070910540, Pid=142307070910539, Pids="[0],[142307070910539],", Name="伟本股份有限公司(上海)", Code="wbgf_sh", Sort=100, Remark="伟本股份有限公司-上海", Status=0 },
                new SysOrg{Id=142307070910541, Pid=142307070910539, Pids="[0],[142307070910539],", Name="伟本股份有限公司(重庆)", Code="wbgf_cq", Sort=100, Remark="伟本股份有限公司-重庆", Status=0 },
                new SysOrg{Id=142307070910542, Pid=142307070910540, Pids="[0],[142307070910539],[142307070910540],", Name="研发部", Code="wbgf_sh_yfb", Sort=100, Remark="", Status=0 },
                new SysOrg{Id=142307070910543, Pid=142307070910540, Pids="[0],[142307070910539],[142307070910540],", Name="企划部", Code="wbgf_sh_qhb", Sort=100, Remark="", Status=0 },
                new SysOrg{Id=142307070910544, Pid=142307070910541, Pids="[0],[142307070910539],[142307070910541],", Name="市场部", Code="wbgf_cq_scb", Sort=100, Remark="", Status=0 },
                new SysOrg{Id=142307070910545, Pid=142307070910541, Pids="[0],[142307070910539],[142307070910541],", Name="财务部", Code="wbgf_cq_cwb", Sort=100, Remark="", Status=0 },
                new SysOrg{Id=142307070910546, Pid=142307070910544, Pids="[0],[142307070910539],[142307070910541],[142307070910544],", Name="市场部二部", Code="wbgf_cq_scb", Sort=100, Remark="", Status=0 },
            };
        }
    }
}