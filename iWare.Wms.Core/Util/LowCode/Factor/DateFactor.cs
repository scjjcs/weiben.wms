﻿using Furion.Extras.iWare.Wms.Util.LowCode.Enum;
using Furion.Extras.iWare.Wms.Util.LowCode.Factor.Interface;
using System.Reflection;

namespace Furion.Extras.iWare.Wms.Util.LowCode.Factor
{
    public class DateFactor : IFactor
    {
        public PropertyInfo Field { get; set; }
        public string Describe { get; set; }
        public string FieldName { get; set; }

        public FieldType FieldType
        { get { return FieldType.Date; } }

        public string DbType
        {
            get
            {
                if (IsDateTime) return "DATETIME";
                return "DATE";
            }
        }

        public bool IsDateTime { get; set; } = true;
    }
}