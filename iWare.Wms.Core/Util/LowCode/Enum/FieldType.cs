﻿namespace Furion.Extras.iWare.Wms.Util.LowCode.Enum
{
    public enum FieldType
    {
        String,
        Int,
        Decimal,
        Date
    }
}