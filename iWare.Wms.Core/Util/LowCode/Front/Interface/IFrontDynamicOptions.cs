﻿namespace Furion.Extras.iWare.Wms.Util.LowCode.Front.Interface
{
    public interface IFrontDynamicOptions
    {
        /// <summary>
        /// 动态数据
        /// </summary>
        string DynamicKey { get; set; }

        /// <summary>
        /// 是否是动态数据
        /// </summary>
        bool Dynamic { get; set; }
    }
}