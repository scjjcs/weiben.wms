﻿namespace Furion.Extras.iWare.Wms.Util.LowCode.Front.Model
{
    public class NotFoundFrontException : Exception
    {
        public NotFoundFrontException(string message) : base(message)
        {
        }
    }
}