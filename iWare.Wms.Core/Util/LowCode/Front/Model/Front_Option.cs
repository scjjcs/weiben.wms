﻿namespace Furion.Extras.iWare.Wms.Util.LowCode.Front.Model
{
    public class Front_Option
    {
        public string Value { get; set; }
        public string Label { get; set; }
    }
}