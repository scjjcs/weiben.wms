﻿namespace Furion.Extras.iWare.Wms.Util.LowCode.Front.Model
{
    public class Front_Tree_Option : Front_Option
    {
        public List<Front_Tree_Option> Children { get; set; }
    }
}