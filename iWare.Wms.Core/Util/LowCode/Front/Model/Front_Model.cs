﻿using Furion.Extras.iWare.Wms.Util.LowCode.Front.Interface;

namespace Furion.Extras.iWare.Wms.Util.LowCode.Front.Model
{
    public class Front_Model
    {
        public List<IFront> List { get; set; }

        public Front_Config Config { get; set; }
    }
}