﻿namespace iWare.Wms.Core.Util.LowCode.Front.Model
{
    public class Front_Dynamic
    {
        public string Head { get; set; }

        public string Dynamic { get; set; }

        public string DynamicKey { get; set; }
    }
}