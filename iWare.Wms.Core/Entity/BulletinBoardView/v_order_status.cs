﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 订单状态
    /// </summary>
    public class v_order_status : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_order_status() : base("v_order_status") { }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 订单类型
        /// </summary>
        public int OrderType { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int OrderStatus { get; set; }

    }
}
