﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 质检结果视图
    /// </summary>
    public class v_quality_result : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_quality_result() : base("v_quality_result") { }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 总数量
        /// </summary>
        public int? Ssum { get; set; }

        /// <summary>
        /// 合格数
        /// </summary>
        public int? Qualified { get; set; }

        /// <summary>
        /// 返修数
        /// </summary>
        public int? Rework { get; set; }

        /// <summary>
        /// 报废数
        /// </summary>
        public int? Scrap { get; set; }

        /// <summary>
        /// 退货数
        /// </summary>
        public int? Returnd { get; set; }

        /// <summary>
        /// 完成时间
        /// </summary>
        public string UpdatedTime { get; set; }
    }
}
