﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 出库视图
    /// </summary>
    public class v_delivery : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_delivery() : base("v_delivery") { }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? BindQuantity { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int? TaskStatus { get; set; }
    }
}
