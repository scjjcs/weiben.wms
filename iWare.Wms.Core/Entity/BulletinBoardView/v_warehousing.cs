﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 入库视图
    /// </summary>
    public class v_warehousing : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_warehousing() : base("v_warehousing") { }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? CollectQuantity { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int? WarehousingQuantity { get; set; }

    }
}
