﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 收货视图
    /// </summary>
    public class v_receipt : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_receipt() : base("v_receipt") { }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? DeliveryQuantity { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int? ReceiptQuantity { get; set; }
    }
}
