﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 出入库数据视图
    /// </summary>
    public class v_warehousing_issue : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_warehousing_issue() : base("v_warehousing_issue") { }

        /// <summary>
        /// 出库数
        /// </summary>
        public decimal? WarehousingNumber { get; set; }

        // <summary>
        /// 入库数
        /// </summary>
        public decimal? IssueNumber { get; set; }

        // <summary>
        /// 盘点数
        /// </summary>
        public decimal? InventoryNumber { get; set; }

        // <summary>
        /// 当天时间
        /// </summary>
        public string CreatedTime { get; set; }

    }
}
