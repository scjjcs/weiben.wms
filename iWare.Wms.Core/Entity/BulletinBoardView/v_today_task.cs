﻿using Furion.DatabaseAccessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 当日任务视图
    /// </summary>
    public class v_today_task : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_today_task() : base("v_today_task") { }

        /// <summary>
        /// 送货总数
        /// </summary>
        public decimal? DeliverySum { get; set; }

        /// <summary>
        /// 已完成
        /// </summary>
        public decimal? Completed { get; set; }

        /// <summary>
        /// 未完成
        /// </summary>
        public decimal? Unfinished { get; set; }

        /// <summary>
        /// 收货总数
        /// </summary>
        public decimal? ReceiptSum { get; set; }

        /// <summary>
        /// 已收货
        /// </summary>
        public decimal? GoodsReceived { get; set; }

        /// <summary>
        /// 未收货
        /// </summary>
        public decimal? UnreceivedGoods { get; set; }

        /// <summary>
        /// 入库总数
        /// </summary>
        public decimal? WarehousingSum { get; set; }

        /// <summary>
        /// 已入库
        /// </summary>
        public decimal? Warehousing { get; set; }

        /// <summary>
        /// 未入库
        /// </summary>
        public decimal? NoWarehousing { get; set; }

        /// <summary>
        /// 出库总数
        /// </summary>
        public decimal? IssueSum { get; set; }
        
        /// <summary>
        /// 已出库
        /// </summary>
        public decimal? Issue { get; set; }

        /// <summary>
        /// 未出库
        /// </summary>
        public decimal? NotIssued { get; set; }

        /// <summary>
        /// 送货完成率
        /// </summary>
        public double? DeliveryRate { get; set; }

        /// <summary>
        /// 收货完成率
        /// </summary>
        public double? ReceiptRate { get; set; }

        /// <summary>
        /// 入库率
        /// </summary>
        public double? WarehousingRate { get; set; }

        /// <summary>
        /// 出库率
        /// </summary>
        public double? IssueRate { get; set; }
    }
}
