﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 近一个月供应商合格率视图
    /// </summary>
    public class v_supplier_passrate : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_supplier_passrate() : base("v_supplier_passrate") { }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 合格率
        /// </summary>
        public double? PassRate { get; set; }

    }
}
