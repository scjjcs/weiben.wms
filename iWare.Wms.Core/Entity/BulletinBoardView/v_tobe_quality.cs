﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 待质检视图
    /// </summary>
    public class v_tobe_quality : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_tobe_quality() : base("v_tobe_quality") { }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? CollectQuantity { get; set; }

        /// <summary>
        /// 收货时间
        /// </summary>
        public string CreatedTime { get; set; }
    }
}
