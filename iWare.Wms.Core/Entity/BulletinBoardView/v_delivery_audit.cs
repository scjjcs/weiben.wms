﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 预约送货审核视图
    /// </summary>
    public class v_delivery_audit : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_delivery_audit() : base("v_delivery_audit") { }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SuppName { get; set; }

        /// <summary>
        /// 未审核
        /// </summary>
        public int UnapprovedCount { get; set; }

        /// <summary>
        /// 审核通过
        /// </summary>
        public int ExamineCount { get; set; }

        /// <summary>
        /// 驳回
        /// </summary>
        public int RejectCount { get; set; }
    }
}
