﻿namespace iWare.Wms.Core
{
    /// <summary>
    /// 采购明细表
    /// </summary>
    public class PurtdDetailsOutput
    {
        /// <summary>
        /// 采购单别
        /// </summary>
        public string PurchaseType { get; set; } //ERP对应的字段TD001

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; } //ERP对应的字段TD002

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; } //ERP对应的字段TD022

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; } //ERP对应的字段TD004

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; } //ERP对应的字段TD005

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; } //ERP对应的字段TD003

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; } //ERP对应的字段TD006

        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; } //ERP对应的字段TD009

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 是否结束-N:未结束、Y:自动结束、y:指定结束
        /// </summary>
        public string IsEnd { get; set; } //ERP对应的字段TD016

        /// <summary>
        /// 采购单送货数量
        /// </summary>
        public decimal PurchaseNumber { get; set; } //ERP对应的字段TD008

        /// <summary>
        /// 送货单送货数量
        /// </summary>
        public decimal DeliveryQuantity { get; set; }

        /// <summary>
        /// 送货单已经送货数量
        /// </summary>
        public decimal OutdeliveryQuantity { get; set; }

        /// <summary>
        /// 未配送数
        /// </summary>
        public decimal NotPurchaseNumber { get; set; }

        /// <summary>
        /// 是否勾选
        /// </summary>
        public bool IsCheck { get; set; } = false;
    }
}
