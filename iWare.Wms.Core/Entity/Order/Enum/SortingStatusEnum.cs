﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 分拣状态枚举
    /// </summary>
    /// 分拣状态-未分拣_1、分拣完成_2
    public enum SortStatusEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        All = -10,

        /// <summary>
        /// 未进行
        /// </summary>
        [Description("未进行")]
        NotProgram = 0,

        /// <summary>
        /// 分拣中
        /// </summary>
        [Description("分拣中")]
        Sorting = 1,

        /// <summary>
        /// 分拣完成
        /// </summary>
        [Description("分拣完成")]
        Completed = 2,
    }
}
