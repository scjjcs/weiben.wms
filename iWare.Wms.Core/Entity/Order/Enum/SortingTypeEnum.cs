﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 分拣单据类型枚举
    /// </summary>
    /// 分拣单据类型-调拨单_1
    public enum SortTypeEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        ALL = -10,

        /// <summary>
        /// 调拨单
        /// </summary>
        [Description("调拨单")]
        DIAOBO = 0,
    }
}
