﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 分拣表
    /// </summary>
    [Comment("分拣表")]
    [Table("ware_sortorder")]
    public class WareSortOrder : DEntityBase
    {
        /// <summary>
        /// 来源单号    
        /// </summary>
        [Comment("来源单号")]
        [MaxLength(50)]
        public string OrderNo { get; set; }

        /// <summary>
        /// 单据明细Id
        /// </summary>
        public long OrderDetailID { get; set; }

        /// <summary>
        /// 分拣组盘单号    
        /// </summary>
        [Comment("分拣组盘单号")]
        [MaxLength(50)]
        public string ContainerOrderNo { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [MaxLength(20)]
        public string LocationCode { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        [Comment("容器编号")]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [Comment("品牌名称")]
        [MaxLength(200)]
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Comment("序号")]
        [MaxLength(50)]
        public string XuHao { get; set; }

        /// <summary>
        /// 品号  
        /// </summary>
        [Comment("品号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 品名  
        /// </summary>
        [Comment("品名")]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        [Comment("规格")]
        [MaxLength(200)]
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        [Comment("单位")]
        [MaxLength(50)]
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        [Comment("检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4")]
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 分拣数    
        /// </summary>
        [Comment("分拣数")]
        public decimal SortQuantity { get; set; }

        /// <summary>
        /// 实际分拣数    
        /// </summary>
        [Comment("实际分拣数")]
        public decimal ActualQuantity { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        [Comment("照片")]
        [MaxLength(500)]
        public string Images { get; set; }

        /// <summary>
        /// 备注    
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-未分拣_1、分拣完成_2
        /// </summary>
        [Comment("状态-未分拣_1、分拣完成_2")]
        public SortStatusEnum SortStatus { get; set; }
    }
}
