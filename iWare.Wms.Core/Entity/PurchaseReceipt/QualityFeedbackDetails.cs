﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 质检反馈明细
    /// </summary>
    [Table("quality_feedback_details")]
    [Comment("质检反馈明细")]
    public class QualityFeedbackDetails : EntityBase
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        [Comment("送货单号")]
        [MaxLength(50)]
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        [Comment("供应商")]
        [MaxLength(50)]
        public string SupperCode { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        [Comment("品号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        [Comment("品名")]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        [Comment("规格型号")]
        [MaxLength(200)]
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 质检结果
        /// </summary>
        [Comment("质检结果")]
        [MaxLength(500)]
        public QualityEnum? QualityResult { get; set; }

        /// <summary>
        /// 质检结果数量
        /// </summary>
        [Comment("质检结果数量")]
        public int QualityNumber { get; set; }

        /// <summary>
        /// 异常原因   
        /// </summary>
        [Comment("异常原因")]
        [MaxLength(200)]
        public string AbnormalCause { get; set; }

        /// <summary>
        /// 管理员是否已读
        /// </summary>
        [Comment("管理员是否已读")]
        public YesOrNot IsAdminRead { get; set; } = YesOrNot.N;

        /// <summary>
        /// 供应商是否已读
        /// </summary>
        [Comment("供应商是否已读")]
        public YesOrNot IsSupperRead { get; set; } = YesOrNot.N;

        /// <summary>
        /// 创建时间
        /// </summary>
        [Comment("创建时间")]
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Comment("更新时间")]
        public DateTimeOffset? UpdatedTime { get; set; }
    }
}
