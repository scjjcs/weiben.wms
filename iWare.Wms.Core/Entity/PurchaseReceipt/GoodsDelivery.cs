﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 送货单
    /// </summary>
    [Table("goods_delivery")]
    [Comment("送货单")]
    public partial class GoodsDelivery : DEntityBase, IEntityTypeBuilder<GoodsDelivery>
    {
        /// <summary>
        /// 采购单号
        /// </summary>
        [Comment("采购单号")]
        [MaxLength(50)]
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        [Comment("送货单号")]
        [MaxLength(50)]
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 送货单别
        /// </summary>
        [Comment("送货单别")]
        [MaxLength(50)]
        public string DeliveryType { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Comment("数量")]
        public decimal DeliveryQuantityTotal { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string Remark { get; set; }

        /// <summary>
        /// 审核状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        [Comment("审核状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6")]
        public AuditStatusEnum Status { get; set; } = AuditStatusEnum.yuyuezhong;

        /// <summary>
        /// 供应商编号
        /// </summary>
        [Comment("供应商编号")]
        [MaxLength(50)]
        public string SuppCode { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        [Comment("供应商名称")]
        [MaxLength(50)]
        public string SuppName { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        [Comment("联系电话")]
        [MaxLength(50)]
        public string SuppPhone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        [Comment("详细地址")]
        [MaxLength(250)]
        public string SuppAddress { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 收货单位
        /// </summary>
        [Comment("收货单位")]
        [MaxLength(50)]
        public string ReceName{ get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        [Comment("收货地址")]
        [MaxLength(150)]
        public string ReceAddress { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        [Comment("联系电话")]
        [MaxLength(50)]
        public string RecePhone { get; set; }

        /// <summary>
        /// 采购联系人
        /// </summary>
        [Comment("采购联系人")]
        [MaxLength(50)]
        public string ReceUser { get; set; }

        /// <summary>
        /// 是否加急件
        /// </summary>
        [Comment("是否加急件")]
        public YesOrNot IsJiaJiJian { get; set; } = YesOrNot.N;

        /// <summary>
        /// 送货单明细信息
        /// </summary>
        public List<GoodsDeliveryDetails> GoodsDeliveryDetails { get; set; }

        /// <summary>
        /// 一对多中间表（送货-收货）
        /// </summary>
        public ICollection<GoodsDeliveryVsCollectDelivery> GoodsDeliveryVsCollectDeliverys { get; set; }

        /// <summary>
        ///送货预约信息
        /// </summary>
        public GoodsDeliveryAppointment GoodsDeliveryAppointments { get; set; }

        /// <summary>
        /// 一对多中间表（采购-送货）
        /// </summary>
        public ICollection<PurchaseOrderVsGoodsDelivery> PurchaseOrderVsGoodsDeliverys { get; set; }

        /// <summary>
        /// 配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<GoodsDelivery> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            //一对多配置关系  送货表对明细表
            entityBuilder.HasMany(x => x.GoodsDeliveryDetails)
                .WithOne(x => x.GoodsDelivery)
                .HasForeignKey(x => x.GoodsDeliveryId);

            //一对一送货预约表
            entityBuilder.HasOne(x => x.GoodsDeliveryAppointments)
                .WithOne(x => x.GoodsDeliverys)
                .HasForeignKey<GoodsDeliveryAppointment>(x => x.DeliveryID);

            //一对多中间表（采购-送货）
            entityBuilder.HasMany(x => x.PurchaseOrderVsGoodsDeliverys)
                .WithOne(x => x.GoodsDelivery)
                .HasForeignKey(x => x.GoodsDeliveryId);

            //一对多中间表（送货-收货）
            entityBuilder.HasMany(x => x.GoodsDeliveryVsCollectDeliverys)
                .WithOne(x => x.GoodsDelivery)
                .HasForeignKey(x => x.GoodsDeliveryId);
        }
    }
}