﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 送货单与收货单的关系
    /// </summary>
    [Table("goodsdelivery_vs_collectdelivery")]
    [Comment("送货单与收货单的关系")]
    public class GoodsDeliveryVsCollectDelivery : DEntityBase
    {
        /// <summary>
        /// 送货单Id
        /// </summary>
        [Comment("送货单Id")]
        public long GoodsDeliveryId { get; set; }

        /// <summary>
        /// 收货单Id
        /// </summary>
        [Comment("收货单Id")]
        public long CollectDeliveryId { get; set; }

        /// <summary>
        /// 一对一引用（送货单表）
        /// </summary>
        public GoodsDelivery GoodsDelivery { get; set; }

        /// <summary>
        /// 一对一引用（收货单表）
        /// </summary>
        public CollectDelivery CollectDelivery { get; set; }
    }
}
