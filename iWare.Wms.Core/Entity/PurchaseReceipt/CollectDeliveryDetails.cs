﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 收货单明细
    /// </summary>
    [Table("collect_delivery_details")]
    [Comment("收货单明细")]
    public class CollectDeliveryDetails : DEntityBase
    {
        /// <summary>
        /// 收货单Id
        /// </summary>
        [Comment("收货单Id")]
        public long CollectDeliveryId { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        [Comment("供应商编号")]
        [MaxLength(50)]
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [Comment("品牌名称")]
        [MaxLength(200)]
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Comment("序号")]
        [MaxLength(50)]
        public string XuHao { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        [Comment("品号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        [Comment("品名")]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        [Comment("规格型号")]
        [MaxLength(200)]
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        [Comment("检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4")]
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        [Comment("单位")]
        [MaxLength(50)]
        public string Unit { get; set; }

        /// <summary>
        /// 数量 
        /// </summary>
        [Comment("数量")]
        public decimal CollectQuantity { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string Remark { get; set; }

        ///<summary>
        /// 入库数量
        /// </summary>
        [Comment("入库数量")]
        public decimal WarehousingQuantity { get; set; }

        /// <summary>
        /// 是否质检
        /// </summary>
        [Comment("是否质检")]
        public YesOrNot IsQuality { get; set; }

        /// <summary>
        /// 物料质检是否合格
        /// </summary>
        [Comment("物料质检是否合格")]
        public YesOrNot IsQualified { get; set; }

        /// <summary>
        /// 质检结果
        /// </summary>
        [Comment("质检结果")]
        [MaxLength(500)]
        public QualityEnum? QualityResult { get; set; }

        /// <summary>
        /// 质检结果数量
        /// </summary>
        [Comment("质检结果数量")]
        public int QualityNumber { get; set; }

        /// <summary>
        /// 异常原因   
        /// </summary>
        [Comment("异常原因")]
        [MaxLength(200)]
        public string AbnormalCause { get; set; }

        /// <summary>
        /// 供应商是否已处理
        /// </summary>
        [Comment("供应商是否已处理")]
        public YesOrNot IsSupperHandle { get; set; } = YesOrNot.N;

        /// <summary>
        /// 收货单信息
        /// </summary>
        public CollectDelivery CollectDelivery { get; set; }
    }
}
