﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 送货单预约
    /// </summary>
    [Table("goods_delivery_appointment")]
    [Comment("送货单预约")]
    public class GoodsDeliveryAppointment : DEntityBase
    {
        /// <summary>
        /// 预约单号
        /// </summary>
        [Comment("预约单号")]
        [MaxLength(50)]
        public string AppointmentNo { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        [Comment("采购单号")]
        [MaxLength(50)]
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 送货单ID
        /// </summary>
        [Comment("送货单ID")]
        public long DeliveryID { get; set; }

        /// <summary>
        /// 送货单号
        /// </summary>
        [Comment("送货单号")]
        [MaxLength(50)]
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 运输公司
        /// </summary>
        [Comment("运输公司")]
        [MaxLength(150)]
        public string TransportCompany { get; set; }

        /// <summary>
        /// 物流单号
        /// </summary>
        [Comment("物流单号")]
        [MaxLength(100)]
        public string ExpressInfoNo { get; set; }

        /// <summary>
        /// 运输车辆
        /// </summary>
        [Comment("运输车辆")]
        [MaxLength(200)]
        public string TransportVehicle { get; set; }

        /// <summary>
        /// 司机姓名
        /// </summary>
        [Comment("司机姓名")]
        [MaxLength(50)]
        public string DriverName { get; set; }

        /// <summary>
        /// 驾驶员资格
        /// </summary>
        [Comment("驾驶员资格")]
        [MaxLength(150)]
        public string DriverQualification { get; set; }

        /// <summary>
        /// 司机电话
        /// </summary>
        [Comment("司机电话")]
        [MaxLength(50)]
        public string DriverPhone { get; set; }

        /// <summary>
        /// 发货地址
        /// </summary>
        [Comment("发货地址")]
        [MaxLength(200)]
        public string DeliverGoodsPlaceShip { get; set; }

        /// <summary>
        /// 发货仓库
        /// </summary>
        [Comment("发货仓库")]
        [MaxLength(100)]
        public string DeliverGoodsWarehouseCode { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        [Comment("收货地址")]
        [MaxLength(200)]
        public string CollectDeliveryPlaceShip { get; set; }

        /// <summary>
        /// 收货仓库
        /// </summary>
        [Comment("收货仓库")]
        [MaxLength(100)]
        public string CollectDeliveryWarehouseCode { get; set; }

        /// <summary>
        /// 预计到达日期
        /// </summary>
        [Comment("预计到达日期")]
        public DateTimeOffset? EstimatedDate { get; set; }

        /// <summary>
        /// 发货日期
        /// </summary>
        [Comment("发货日期")]
        public DateTimeOffset? DeliverDate { get; set; }

        /// <summary>
        /// 收货人
        /// </summary>
        [Comment("收货人")]
        [MaxLength(50)]
        public string CollectDeliveryUserName { get; set; }

        /// <summary>
        /// 收货人电话
        /// </summary>
        [Comment("收货人电话")]
        [MaxLength(50)]
        public string CollectDeliveryUserPhone { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string Remark { get; set; }

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        [Comment("状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6")]
        public AuditStatusEnum Status { get; set; } = AuditStatusEnum.yuyuezhong;

        /// <summary>
        /// 图片
        /// </summary>
        [Comment("图片")]
        [MaxLength(500)]
        public string Images { get; set; }
        
        /// <summary>
        /// 送货单信息
        /// </summary>
        public GoodsDelivery GoodsDeliverys { get; set; }
    }
}
