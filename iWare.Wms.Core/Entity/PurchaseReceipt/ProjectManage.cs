﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 项目表
    /// </summary>
    [Table("project_manage")]
    [Comment("项目表")]
    public class ProjectManage : DEntityBase
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        [Comment("项目名称")]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// 项目说明
        /// </summary>
        [Comment("项目说明")]
        [MaxLength(500)]
        public string Describe { get; set; }
    }
}
