﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 送货单明细
    /// </summary>
    [Table("goods_delivery_details")]
    [Comment("送货单明细")]
    public class GoodsDeliveryDetails : DEntityBase
    {
        /// <summary>
        /// 送货单主表Id
        /// </summary>
        [Comment("送货单主表Id")]
        public long GoodsDeliveryId { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        [Comment("供应商编号")]
        [MaxLength(50)]
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [Comment("品牌名称")]
        [MaxLength(200)]
        public string BrandName { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(50)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 序号   --对应ERP序号
        /// </summary>
        [Comment("序号")]
        [MaxLength(50)]
        public string XuHao { get; set; }

        /// <summary>
        /// 品号   --对应ERP品号
        /// </summary>
        [Comment("品号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>   
        /// 品名    --对应ERP品名
        /// </summary>
        [Comment("品名")]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// 规格型号    
        /// </summary>
        [Comment("规格型号")]
        [MaxLength(200)]
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        [Comment("检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4")]
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        [Comment("单位")]
        [MaxLength(50)]
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Comment("数量")]
        public decimal DeliveryQuantity { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        [Comment("参数")]
        [MaxLength(500)]
        public string Parameter { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string Remark { get; set; }

        /// <summary>
        /// 收货数量
        /// </summary>
        [Comment("是否收货")]
        public decimal CollectQuantity { get; set; }

        /// <summary>
        /// 送货单信息
        /// </summary>
        public GoodsDelivery GoodsDelivery { get; set; }
    }
}
