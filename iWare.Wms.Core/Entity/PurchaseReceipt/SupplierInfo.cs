﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 供应商信息表
    /// </summary>
    [Table("supplier_info")]
    [Comment("供应商信息表")]
    public class SupplierInfo : DEntityBase
    {
        /// <summary>
        /// 供应商编号
        /// </summary>
        [Comment("供应商编号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 简称
        /// </summary>
        [Comment("简称")]
        [MaxLength(100)]
        public string AbbreviationName { get; set; }

        /// <summary>
        /// 公司全称
        /// </summary>
        [Comment("公司全称")]
        [MaxLength(200)]
        public string Name { get; set; }

        /// <summary>
        /// TEL(一)
        /// </summary>
        [Comment("TEL(一)")]
        [MaxLength(100)]
        public string Tel { get; set; }

        /// <summary>
        /// 负责人
        /// </summary>
        [Comment("负责人")]
        [MaxLength(100)]
        public string LeadingCadre { get; set; }

        /// <summary>
        /// 联系人(一)
        /// </summary>
        [Comment("联系人(一)")]
        [MaxLength(100)]
        public string Contacts { get; set; }

        /// <summary>
        /// 联系地址(一)
        /// </summary>
        [Comment("联系地址(一)")]
        [MaxLength(200)]
        public string ContactAddress { get; set; }
    }
}
