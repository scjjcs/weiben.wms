﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 质检结果
    /// </summary>
    public enum QualityEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        All = -10,

        /// <summary>
        /// 合格
        /// </summary>
        [Description("合格")]
        HeGe = 1,

        /// <summary>
        /// 返修
        /// </summary>
        [Description("返修")]
        FanXiu = 2,

        /// <summary>
        /// 报废
        /// </summary>
        [Description("报废")]
        BaoFei = 3,

        /// <summary>
        /// 技术确认
        /// </summary>
        [Description("技术确认")]
        JiShu = 4,

        /// <summary>
        /// 让步接收
        /// </summary>
        [Description("让步接收")]
        RangBu = 5,

        /// <summary>
        /// 退货
        /// </summary>
        [Description("退货")]
        TuiHuo = 6,

        /// <summary>
        /// 免检
        /// </summary>
        [Description("免检")]
        MianJian = 7,
    }
}
