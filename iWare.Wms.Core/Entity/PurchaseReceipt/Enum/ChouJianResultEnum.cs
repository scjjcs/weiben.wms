﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 抽检结果枚举
    /// <summary>
    public enum ChouJianResultEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        all = -10,

        /// <summary>
        /// 退货
        /// </summary>
        [Description("退货")]
        tuihuo = 1,
    }
}
