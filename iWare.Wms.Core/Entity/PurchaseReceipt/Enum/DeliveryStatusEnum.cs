﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 收货状态枚举
    /// <summary>
    /// 收货状态-未收货_0、收货中_1、收货完成_2
    /// </summary>
    public enum DeliveryStatusEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        All = -10,

        /// <summary>
        /// 未收货
        /// </summary>
        [Description("未收货")]
        NotProgress = 0,

        /// <summary>
        /// 收货中
        /// </summary>
        [Description("收货中")]
        Progress = 1,

        /// <summary>
        /// 收货完成
        /// </summary>
        [Description("收货完成")]
        Complete = 2,
    }
}
