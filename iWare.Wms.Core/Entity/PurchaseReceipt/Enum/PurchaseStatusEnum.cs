﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 采购单据状态枚举
    /// <summary>
    /// 采购单据状态-撤销或者删除_-1、未进行_0、进行中_1、完成_2 
    /// </summary>
    public enum PurchaseStatusEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        All = -10,

        /// <summary>
        /// 撤销或者删除
        /// </summary>
        [Description("撤销或者删除")]
        Delete = -1,

        /// <summary>
        /// 未进行
        /// </summary>
        [Description("未进行")]
        NotProgress = 0,

        /// <summary>
        /// 进行中
        /// </summary>
        [Description("进行中")]
        Progress = 1,

        /// <summary>
        /// 完成
        /// </summary>
        [Description("完成")]
        Complete = 2,
    }
}
