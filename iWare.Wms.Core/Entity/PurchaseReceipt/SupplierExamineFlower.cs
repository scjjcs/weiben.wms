﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 供应商审核记录表    
    /// </summary>
    [Comment("供应商审核记录表")]
    [Table("supplier_examine_flower")]
    public class SupplierExamineFlower : DEntityBase
    {
        /// <summary>
        /// 送货单据ID    
        /// </summary>
        [Comment("送货单据ID")]
        public long DeliveryID { get; set; }

        /// <summary>
        /// 备注    
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string Remarks { get; set; }

        /// <summary>
        /// 是否加急件
        /// </summary>
        [Comment("是否加急件")]
        public YesOrNot IsJiaJiJian { get; set; } = YesOrNot.N;

        /// <summary>
        /// 管理员是否已读
        /// </summary>
        [Comment("管理员是否已读")]
        public YesOrNot IsAdminRead { get; set; } = YesOrNot.N;

        /// <summary>
        /// 供应商是否已读
        /// </summary>
        [Comment("供应商是否已读")]
        public YesOrNot IsSupperRead { get; set; } = YesOrNot.N;

        /// <summary>
        /// 状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6
        /// </summary>
        [Comment("状态-预约中_0、预约成功_1、审核中_2、驳回_3、审核通过_4、送货中_5、送货完成_6")]
        public AuditStatusEnum Status { get; set; } = AuditStatusEnum.yuyuezhong;
    }
}
