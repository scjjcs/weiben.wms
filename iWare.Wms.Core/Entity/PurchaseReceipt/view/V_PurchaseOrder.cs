﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 采购订单视图
    /// </summary>
    public class V_PurchaseOrder : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public V_PurchaseOrder() : base("v_purchase_order") { }

        /// <summary>
        /// 采购单号ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string PurchaseNo { get; set; }

        /// <summary>
        /// 采购单类型
        /// </summary>
        public string PurchaseType { get; set; }

        /// <summary>
        /// 采购单类型名称
        /// </summary>
        public string PurchaseTypeName { get; set; }

        /// <summary>
        /// 采购日期
        /// </summary>
        public DateTimeOffset PurchaseDate { get; set; }

        /// <summary>
        /// 采购人ID
        /// </summary>
        public string PurchaserUserId { get; set; }

        /// <summary>
        /// 采购人名称
        /// </summary>
        public string PurchaserUserName { get; set; }

        /// <summary>
        /// 供应商编码
        /// </summary>
        public string SupplierInfoCode { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierInfoName { get; set; }

        /// <summary>
        /// 供应商联系地址
        /// </summary>
        public string SuppAddress { get; set; }

        /// <summary>
        /// 供应商电话
        /// </summary>
        public string SupplierTel { get; set; }

        /// <summary>
        /// 项目号编码
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 数量合计
        /// </summary>
        public decimal PurchaseNumber { get; set; }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 工厂
        /// </summary>
        public string Factory { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 送货单总数量
        /// </summary>
        public decimal? DeliveryQuantityTotal { get; set; }

        /// <summary>
        /// 剩余送货数量
        /// </summary>
        public decimal? OutQuantityTotal { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 采购单据状态-撤销或者删除_-1、未进行_0、进行中_1、完成_2
        /// </summary>
        public PurchaseStatusEnum Status { get; set; }

        /// <summary>
        /// 是否加急件
        /// </summary>
        public YesOrNot IsJiaJiJian { get; set; }
    }
}