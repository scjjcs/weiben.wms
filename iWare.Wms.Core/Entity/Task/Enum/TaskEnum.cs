﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 任务方式
    /// </summary>
    /// 任务方式-手动_1、自动_2
    /// </summary>
    public enum TaskModel
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        ALL = -10,

        /// <summary>
        /// 手动
        /// </summary>
        [Description("手动")]
        SHOUDONG = 1,

        /// <summary>
        /// 自动
        /// </summary>
        [Description("自动")]
        ZIDONG = 2
    }

    /// <summary>
    /// 任务状态枚举
    /// <summary>
    /// 任务状态-未执行_0、执行中_1、已完成_2
    /// </summary>
    public enum TaskStatusEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        All = -10,

        /// <summary>
        /// 未进行
        /// </summary>
        [Description("未进行")]
        NotProgress = 0,

        /// <summary>
        /// 执行中
        /// </summary>
        [Description("执行中")]
        Progress = 1,

        /// <summary>
        /// 已完成
        /// </summary>
        [Description("已完成")]
        Complete = 2,
    }

    /// <summary>
    /// 任务类型枚举
    /// <summary>
    /// 任务类型-入库任务_1、出库任务_2、移库任务_3
    /// </summary>
    public enum TaskType
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        All = -10,

        /// <summary>
        /// 入库
        /// </summary>
        [Description("入库")]
        In = 1,

        /// <summary>
        /// 出库
        /// </summary>
        [Description("出库")]
        Out = 2,

        /// <summary>
        /// 移库
        /// </summary>
        [Description("移库")]
        Move = 3,
    }
}
