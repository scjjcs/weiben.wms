﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// Agv任务表
    /// </summary>
    [Comment("Agv任务表")]
    [Table("ware_agv_task")]
    public class WareAgvTask : DEntityBase
    {
        /// <summary>
        /// 任务类型-入库;出库
        /// </summary>
        [Comment("任务类型-入库;出库")]
        [MaxLength(10)]
        public string TaskType { get; set; }

        /// <summary>
        /// 起始位置
        /// </summary>
        [Comment("起始位置")]
        [MaxLength(50)]
        public string StartPlace { get; set; }

        /// <summary>
        /// 结束位置
        /// </summary>
        [Comment("结束位置")]
        [MaxLength(50)]
        public string EndPlace { get; set; }

        /// <summary>
        /// 状态
        /// PRISTINE：待执行
        /// TRAVELLING：执行中
        /// FINISHED：完成
        /// </summary>
        [Comment("状态")]
        [MaxLength(50)]
        public string AgvState { get; set; }

        /// <summary>
        /// 当前执行的任务
        /// </summary>
        [Comment("当前执行的任务")]
        [MaxLength(100)]
        public string TransportOrder { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        [Comment("容器编号")]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        [Comment("图片")]
        [MaxLength(500)]
        public string Images { get; set; }
    }
}
