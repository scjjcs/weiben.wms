﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 站点表
    /// </summary>
    [Table("ware_site")]
    [Comment("站点表")]
    public class WareSite : DEntityBase
    {
        /// <summary>
        /// 站点编号
        /// </summary>
        [Comment("站点编号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 站点名称    
        /// </summary>
        [Comment("站点名称")]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// 站点类型
        /// </summary>
        [Comment("站点类型")]
        public SiteTypeEnum SiteTypeName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Comment("描述")]
        [MaxLength(500)]
        public string Describe { get; set; }

        ///// <summary>
        ///// 是否锁定
        ///// </summary>
        //[Comment("是否锁定（字典 1是 0否）")]
        //public YesOrNot IsLock { get; set; } = YesOrNot.N;

        /// <summary>
        /// 状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1
        /// </summary>
        [Comment("状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1")]
        public LocationStatusEnum? Status { get; set; } = LocationStatusEnum.kongxian;

        /// <summary>
        /// 容器编号
        /// </summary>
        [Comment("容器编号")]
        [MaxLength(50)]
        public string ContainerCode { get; set; }
    }
}
