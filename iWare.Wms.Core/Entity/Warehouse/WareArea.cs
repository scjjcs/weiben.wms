﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 所属库区表
    /// </summary>
    [Table("ware_area")]
    [Comment("所属库区")]
    public class WareArea : DEntityBase
    {
        /// <summary>
        /// 编码
        /// </summary>
        [Comment("编码")]
        [MaxLength(50)]
        public string AreaCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [MaxLength(150)]
        public string AreaName { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Comment("类型")]
        public AreaTypeEnum AreaType { get; set; }
    }
}
