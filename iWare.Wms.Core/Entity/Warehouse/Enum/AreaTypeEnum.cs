﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 库区类型枚举
    /// <summary>
    /// 库区类型-立体库_1、普通库_2、公共库_3、装配库_4、虚拟库_5
    /// </summary>
    public enum AreaTypeEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        ALL = -10,

        /// <summary>
        /// 自动库
        /// </summary>
        [Description("自动库")]
        ZD = 1,

        /// <summary>
        /// 非自动库
        /// </summary>
        [Description("非自动库")]
        NZD = 2,

        /// <summary>
        /// 虚拟库
        /// </summary>
        [Description("虚拟库")]
        XN = 3,

        /// <summary>
        /// Agv库区
        /// </summary>
        [Description("Agv库区")]
        AGV = 4,

        /// <summary>
        /// aa库区
        /// </summary>
        [Description("aa库区")]
        aa = 5,

    }
}
