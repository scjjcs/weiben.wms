﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 站点类型,上料口 0,分拣口 1，出入库口 2
    /// </summary>
    public enum SiteTypeEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        ALL = -10,

        /// <summary>
        /// 上料口
        /// </summary>
        [Description("上料口")]
        WULIAOKOU = 0,

        /// <summary>
        /// 分拣口
        /// </summary>
        [Description("分拣口")]
        FENJIANKOU = 1,

        /// <summary>
        /// 出入库口
        /// </summary>
        [Description("出入库口")]
        CHURUKUKOU = 2,
    }
}
