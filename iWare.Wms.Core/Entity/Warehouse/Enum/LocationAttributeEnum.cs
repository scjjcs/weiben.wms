﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 库位属性
    /// </summary>
    public enum LocationAttributeEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        all = -10,

        /// <summary>
        /// 小
        /// </summary>
        [Description("小")]
        小 = 5,

        /// <summary>
        /// 中
        /// </summary>
        [Description("中")]
        中 = 6,

        /// <summary>
        /// 大
        /// </summary>
        [Description("大")]
        大 = 7,
    }
}
