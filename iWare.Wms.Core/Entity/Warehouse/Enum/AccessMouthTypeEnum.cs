﻿using System.ComponentModel;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 库口类型枚举
    /// <summary>
    /// 库口类型-入库_1、出库_2
    /// </summary>
    public enum AccessMouthTypeEnum
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        ALL = -10,

        /// <summary>
        /// 入库
        /// </summary>
        [Description("入库")]
        In = 1,

        /// <summary>
        /// 出库
        /// </summary>
        [Description("出库")]
        Out = 2,
    }
}
