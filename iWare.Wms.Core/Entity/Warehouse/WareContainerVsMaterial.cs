﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 容器与物料的关系
    /// </summary>
    [Table("ware_container_vs_material")]
    [Comment("容器与物料的关系")]
    public class WareContainerVsMaterial : DEntityBase
    {
        /// <summary>
        /// 送货单号
        /// </summary>
        [Comment("送货单号")]
        [MaxLength(50)]
        public string DeliveryNo { get; set; }

        /// <summary>
        /// 组盘单据
        /// </summary>
        [Comment("组盘单据")]
        [MaxLength(50)]
        public string OrderNo { get; set; }

        /// <summary>
        /// 项目号
        /// </summary>
        [Comment("项目号")]
        [MaxLength(50)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 容器表Id
        /// </summary>
        [Comment("容器表Id")]
        public long ContainerId { get; set; }

        /// <summary>
        /// 容器编码
        /// </summary>
        [Comment("容器编码")]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 物料ID   
        /// </summary>
        [Comment("物料ID")]
        public long MaterialId { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [Comment("品牌名称")]
        [MaxLength(200)]
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [Comment("序号")]
        [MaxLength(20)]
        public string XuHao { get; set; }

        /// <summary>
        /// 品号    
        /// </summary>
        [Comment("品号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 品名    
        /// </summary>
        [Comment("品名")]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        [Comment("规格")]
        [MaxLength(200)]
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        [Comment("单位")]
        [MaxLength(50)]
        public string Unit { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        [Comment("检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4")]
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Comment("数量")]
        public decimal BindQuantity { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [Comment("状态（字典 0正常 1停用 2删除）")]
        public CommonStatus? ContainerVsMaterialStatus { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 供应商code
        /// </summary>
        [Comment("供应商code")]
        [MaxLength(20)]
        public string SupplerCode { get; set; }

        /// <summary>
        /// 是否加急
        /// </summary>
        [Comment("是否加急")]
        [MaxLength(20)]
        public YesOrNot IsJiaJi { get; set; } = YesOrNot.N;

        /// <summary>
        /// 过期时间
        /// </summary>
        [Comment("过期时间")]
        public DateTimeOffset? ExpirationTime { get; set; }

        /// <summary>
        /// 一对一引用（容器表）
        /// </summary>
        public WareContainer WareContainer { get; set; }

        /// <summary>
        /// 一对一引用（物料表）
        /// </summary>
        public WareMaterial WareMaterial { get; set; }
    }
}
