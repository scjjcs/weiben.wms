﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 设备故障表
    /// </summary>
    [Table("ware_device_fault")]
    [Comment("设备故障")]
    public class WareDeviceFault : DEntityBase
    {
        /// <summary>
        /// 设备编号
        /// </summary>
        [Comment("设备编号")]
        [MaxLength(50)]
        public string DeviceNo { get; set; }

        /// <summary>
        /// 设备名称
        /// </summary>
        [Comment("设备名称")]
        [MaxLength(150)]
        public string DeviceName { get; set; }

        /// <summary>
        /// 报警时间
        /// </summary>
        [Comment("报警时间")]
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 关闭时间
        /// </summary>
        [Comment("关闭时间")]
        public DateTime? CloseTime { get; set; }

        /// <summary>
        /// 持续时间
        /// </summary>
        [Comment("持续时间")]
        public int? UseTimes { get; set; }

        /// <summary>
        /// 故障代码
        /// </summary>
        [Comment("故障代码")]
        [MaxLength(50)]
        public string FaultCode { get; set; }

        /// <summary>
        /// 故障描述
        /// </summary>
        [Comment("故障描述")]
        [MaxLength(2500)]
        public string FaultName { get; set; }
    }
}
