﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 组装车间表
    /// </summary>
    [Table("ware_assembly_workshop")]
    [Comment("组装车间表")]
    public class WareAssemblyWorkshop : DEntityBase
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        [Comment("项目名称")]
        [MaxLength(200)]
        public string ProjectName { get; set; }

        /// <summary>
        /// 计划数
        /// </summary>
        [Comment("计划数")]
        public int PlanNum { get; set; }

        /// <summary>
        /// 实际数
        /// </summary>
        [Comment("实际数")]
        public int ActuaityNum { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        [Comment("开始时间")]
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 百分比
        /// </summary>
        [Comment("百分比")]
        [MaxLength(50)]
        public string Percentage { get; set; }
    }
}
