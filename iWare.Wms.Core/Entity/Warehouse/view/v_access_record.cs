﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 出入库明细视图
    /// </summary>
    public class v_accessrecord : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_accessrecord() : base("v_accessrecord") { }

        /// <summary>
        /// 组盘单据
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 任务号  
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务类型-入库_1、出库_2、移库_3
        /// </summary>
        public TaskType? TaskType { get; set; }

        /// <summary>
        /// 状态-未执行_0、1-已暂停、执行中_2、已完成_3
        /// </summary>
        public TaskStatusEnum? TaskStatus { get; set; } = TaskStatusEnum.NotProgress;

        /// <summary>
        /// 任务名称   
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 出发地库位编号   
        /// </summary>
        public string FromLocationCode { get; set; }

        /// <summary>
        /// 目的地库位编号   
        /// </summary>
        public string ToLocationCode { get; set; }

        /// <summary>
        /// 任务级别    
        /// </summary>
        public int? Priority { get; set; }

        /// <summary>
        /// 任务方式-手动_1、自动_2
        /// </summary>
        public TaskModel? TaskModel { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 容器表Id
        /// </summary>
        public long? ContainerId { get; set; }

        /// <summary>
        /// 物料ID   
        /// </summary>
        public long? MaterialId { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string XuHao { get; set; }

        /// <summary>
        /// 品号    
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名    
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public InspectionMethodEnum? InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? BindQuantity { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus? ContainerVsMaterialStatus { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 供应商code
        /// </summary>
        public string SupplerCode { get; set; }

        /// <summary>
        /// 是否加急
        /// </summary>
        public YesOrNot ?IsJiaJi { get; set; }

        /// <summary>
        /// 容器编码
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 项目号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 任务表对应的容器编号
        /// </summary>
        public string TaskContainerCode { get; set; }

        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string TaskCreatedUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? TaskCreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? TaskUpdatedTime { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        public string Weight { get; set; }
    }
}
