﻿using Furion.DatabaseAccessor;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 库位视图
    /// </summary>
    public class v_ware_location : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_ware_location() : base("v_ware_location") { }

        /// <summary>
        /// ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 所属企业
        /// </summary>
        public string WareOne { get; set; }

        /// <summary>
        /// 所属企业名称
        /// </summary>
        public string WareOneName { get; set; }

        /// <summary>
        /// 所属厂区
        /// </summary>
        public string WareTwo { get; set; }

        /// <summary>
        /// 所属厂区名称
        /// </summary>
        public string WareTwoName { get; set; }

        /// <summary>
        /// 所属库区类型
        /// </summary>
        public string WareThree { get; set; }

        /// <summary>
        /// 所属库区类型名称
        /// </summary>
        public string WareThreeName { get; set; }

        /// <summary>
        /// 库区ID
        /// </summary>
        public long AreaID { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        public int RowNo { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        public int ColumnNo { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        public int LayerNo { get; set; }

        /// <summary>
        /// 深号
        /// </summary>
        public int DeepcellNo { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }

        /// <summary>
        /// 长
        /// </summary>
        public decimal Long { get; set; }

        /// <summary>
        /// 宽
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// 高
        /// </summary>
        public decimal High { get; set; }

        /// <summary>
        /// 承重
        /// </summary>
        public string Heavy { get; set; }

        /// <summary>
        /// 属性
        /// </summary>
        public string Attribute { get; set; }

        /// <summary>
        /// 是否锁定
        /// </summary>
        public YesOrNot IsLock { get; set; }

        /// <summary>
        /// 是否空托盘
        /// </summary>
        public YesOrNot IsEmptyContainer { get; set; }

        /// <summary>
        /// 库位状态-待入_1、存货_2、待出_3、空闲_0、禁用_-1
        /// </summary>
        public LocationStatusEnum? LocationStatus { get; set; }

        /// <summary>
        /// 是否虚拟
        /// </summary>
        public YesOrNot? IsVirtual { get; set; }

        /// <summary>
        /// 原库位编号
        /// </summary>
        public string OriginalLocationCode { get; set; }
    }
}
