﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.Core.Entity.Warehouse.view
{
    /// <summary>
    /// 物料与品号仓库视图
    /// </summary>
    public class v_material_warehouse : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public v_material_warehouse() : base("v_material_warehouse") { }

        /// <summary>
        /// ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 品号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string Describe { get; set; }

        /// <summary>
        /// 检验方式
        /// </summary>
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// 生产日期
        /// </summary>
        public DateTimeOffset? ManufactureDate { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public string Xuhao { get; set; }

        /// <summary>
        /// 备库状态-不备库_0；备库_1
        /// </summary>
        public YesOrNot StandbyStatus { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        public CommonStatus Status { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Comment("创建时间")]
        public virtual DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 物料二维码
        /// </summary>
        public string QRCode { get; set; }

        /// <summary>
        /// 安全存量
        /// </summary>
        public decimal SafetyQuantity { get; set; }

        /// <summary>
        /// 经济批量
        /// </summary>
        public decimal EconomicQuantity { get; set; }

        /// <summary>
        /// 库存数量
        /// </summary>
        public decimal InventoryQuantity { get; set; }
    }
}
