﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 品号仓库
    /// </summary>
    [Table("ware_material_warehouse")]
    [Comment("品号仓库")]
    public class WareMaterialWarehouse : DEntityBase
    {
        /// <summary>
        /// 品号
        /// </summary>
        [Comment("品号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 仓库  
        /// </summary>
        [Comment("仓库")]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// 安全存量
        /// </summary>
        [Comment("安全存量")]
        public decimal SafetyQuantity { get; set; }

        /// <summary>
        /// 经济批量
        /// </summary>
        [Comment("经济批量")]
        public decimal EconomicQuantity { get; set; }

        /// <summary>
        /// 库存数量
        /// </summary>
        [Comment("库存数量")]
        public decimal InventoryQuantity { get; set; }
    }
}
