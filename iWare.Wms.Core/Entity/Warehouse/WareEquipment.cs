﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 设备表
    /// </summary>
    [Table("ware_equipment")]
    [Comment("设备表")]
    public class WareEquipment : DEntityBase
    {
        /// <summary>
        /// 设备类型
        /// </summary>
        [Comment("设备类型")]
        [MaxLength(50)]
        public string EquipmentTypeName { get; set; }

        /// <summary>
        /// 设备名称
        /// </summary>
        [Comment("设备名称")]
        [MaxLength(150)]
        public string EquipmentName { get; set; }

        /// <summary>
        /// 设备展示名
        /// </summary>
        [Comment("设备展示名")]
        [MaxLength(200)]
        public string ExhibitionName { get; set; }

        /// <summary>
        /// 设备品牌
        /// </summary>
        [Comment("设备品牌")]
        [MaxLength(150)]
        public string EquipmentBrand { get; set; }

        /// <summary>
        /// 设备IP
        /// </summary>
        [Comment("设备IP")]
        [MaxLength(30)]
        public string IP { get; set; }

        /// <summary>
        /// 设备端口
        /// </summary>
        [Comment("设备端口")]
        public int Port { get; set; }

        /// <summary>
        /// 所属区域
        /// </summary>
        [Comment("所属区域code")]
        [MaxLength(100)]
        public string RegionName { get; set; }

        /// <summary>
        /// 设备标识
        /// </summary>
        [Comment("设备标识")]
        [MaxLength(200)]
        public string EquipmentIdentification { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string Remarks { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [Comment("状态（字典 0正常 1停用 2删除）")]
        public CommonStatus? Status { get; set; } = CommonStatus.ENABLE;
    }
}
