﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace iWare.Wms.Core
{
    /// <summary>
    /// 物料表
    /// </summary>
    [Table("ware_material")]
    [Comment("物料表")]
    public class WareMaterial : DEntityBase, IEntityTypeBuilder<WareMaterial>
    {
        /// <summary>
        /// 品号    
        /// </summary>
        [Comment("品号")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 品名    
        /// </summary>
        [Comment("品名")]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// 规格    
        /// </summary>
        [Comment("规格")]
        [MaxLength(200)]
        public string SpecificationModel { get; set; }

        /// <summary>
        /// 单位    
        /// </summary>
        [Comment("单位")]
        [MaxLength(50)]
        public string Unit { get; set; }

        /// <summary>
        /// 商品描述    
        /// </summary>
        [Comment("商品描述")]
        [MaxLength(500)]
        public string Describe { get; set; }

        /// <summary>
        /// 检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4
        /// </summary>
        [Comment("检验方式-免检_0、抽检(减量)_1、抽检(正常)_2、抽检(加严)_3、全检_4")]
        public InspectionMethodEnum InspectionMethod { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Comment("数量")]
        public decimal Quantity { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        [Comment("品牌名称")]
        [MaxLength(300)]
        public string BrandName { get; set; }

        /// <summary>
        /// 供应商code
        /// </summary>
        [Comment("供应商code")]
        [MaxLength(20)]
        public string SupplerCode { get; set; }

        /// <summary>
        /// 是否加急
        /// </summary>
        [Comment("是否加急")]
        [MaxLength(20)]
        public YesOrNot IsJiaJi { get; set; } = YesOrNot.N;

        /// <summary>
        /// 参数一
        /// </summary>
        [Comment("参数一")]
        [MaxLength(200)]
        public string Parameter1 { get; set; }

        /// <summary>
        /// 参数二
        /// </summary>
        [Comment("参数二")]
        [MaxLength(200)]
        public string Parameter2 { get; set; }

        /// <summary>
        /// 参数三
        /// </summary>
        [Comment("参数三")]
        [MaxLength(200)]
        public string Parameter3 { get; set; }

        /// <summary>
        /// 参数四
        /// </summary>
        [Comment("参数四")]
        [MaxLength(200)]
        public string Parameter4 { get; set; }

        /// <summary>
        /// 参数五
        /// </summary>
        [Comment("参数五")]
        [MaxLength(200)]
        public string Parameter5 { get; set; }

        /// <summary>
        /// 备库状态-不备库_0；备库_1
        /// </summary>
        [Comment("备库状态-不备库_0；备库_1")]
        public YesOrNot StandbyStatus { get; set; } = YesOrNot.N;

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [Comment("状态（字典 0正常 1停用 2删除）")]
        public CommonStatus? Status { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 一对多中间表（容器-物料）
        /// </summary>
        public ICollection<WareContainerVsMaterial> WareContainerVsMaterials { get; set; }

        public void Configure(EntityTypeBuilder<WareMaterial> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {

            // 1对多配置
            entityBuilder.HasMany(x => x.WareContainerVsMaterials)
                .WithOne(x => x.WareMaterial)
                .HasForeignKey(x => x.MaterialId).IsRequired(false);
        }
    }
}
