﻿using Furion;
using Furion.DatabaseAccessor;
using iWare.Wms.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace iWare.Wms.EntityFramework.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseAccessor(options =>
            {
                options.CustomizeMultiTenants(); // 自定义租户

                options.AddDbPool<ERPDbContext, ERPDbContextLocator>(DbProvider.SqlServer);

                options.AddDbPool<StereoscopicDbContext, StereoscopicDbContextLocation>(DbProvider.SqlServer);

                options.AddDbPool<DefaultDbContext>(providerName: default, optionBuilder: (services, opt) =>
                {
                    opt.UseBatchEF_MSSQL();
                });
                //options.AddDbPool<MultiTenantDbContext, MultiTenantDbContextLocator>();
            }, "iWare.Wms.Database.Migrations");
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //// 自动迁移数据库（update-database命令）
            //if (env.IsDevelopment())
            //{
            //    Scoped.Create((_, scope) =>
            //    {
            //        var context = scope.ServiceProvider.GetRequiredService<DefaultDbContext>();
            //        context.Database.Migrate();
            //    });
            //}
        }
    }
}