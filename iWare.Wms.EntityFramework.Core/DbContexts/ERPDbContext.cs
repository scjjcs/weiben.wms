﻿using Furion.DatabaseAccessor;
using iWare.Wms.Core;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.EntityFramework.Core
{
    [AppDbContext("ERPConnection", DbProvider.SqlServer)]
    public class ERPDbContext : AppDbContext<ERPDbContext, ERPDbContextLocator>
    {
        public ERPDbContext(DbContextOptions<ERPDbContext> options) : base(options)
        {

        }
    }
}
