﻿using Furion.DatabaseAccessor;
using iWare.Wms.Core;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.EntityFramework.Core
{
    [AppDbContext("StereoscopicContext", DbProvider.SqlServer)]
    public class StereoscopicDbContext : AppDbContext<StereoscopicDbContext, StereoscopicDbContextLocation>
    {
        public StereoscopicDbContext(DbContextOptions<StereoscopicDbContext> options) : base(options)
        {

        }
    }
}
